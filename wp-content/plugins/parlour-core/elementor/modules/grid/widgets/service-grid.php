<?php
namespace ElementorExtend\Modules\Grid\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class Service Grid
 */
class Service_Grid extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'service-grid';
	}

	public function get_title() {
		return esc_html__( 'Service Grid', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-gallery-grid';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->register_query_section_controls();
	}

	private function register_query_section_controls() {
		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'parlour-core' ),
			]
		);

		$this->add_responsive_control(
			'columns',
			[
				'label' => esc_html__( 'Columns', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'item_gap',
			[
				'label' => esc_html__( 'Item Gap', 'elementor-pro' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 10,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-service-container'  => 'margin: 0 -{{SIZE}}px',
					'(desktop){{WRAPPER}} .bdt-grid-item' => 'width: calc( 100% / {{columns.SIZE}} ); border: {{SIZE}}px solid transparent',
					'(tablet){{WRAPPER}} .bdt-grid-item'  => 'width: calc( 100% / {{columns_tablet.SIZE}} ); border: {{SIZE}}px solid transparent',
					'(mobile){{WRAPPER}} .bdt-grid-item'  => 'width: calc( 100% / {{columns_mobile.SIZE}} ); border: {{SIZE}}px solid transparent',
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'posts_per_page',
			[
				'label' => esc_html__( 'Limit', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 6,
			]
		);

		$this->add_control(
			'show_filter_bar',
			[
				'label'     => esc_html__( 'Show Filter Bar', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => esc_html__( 'Off', 'parlour-core' ),
				'label_on'  => esc_html__( 'On', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'show_title',
			[
				'label' => esc_html__( 'Show Title', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_off' => esc_html__( 'Off', 'parlour-core' ),
				'label_on' => esc_html__( 'On', 'parlour-core' ),
			]
		);

		$this->add_control(
			'show_meta',
			[
				'label' => esc_html__( 'Show Meta', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_off' => esc_html__( 'Off', 'parlour-core' ),
				'label_on' => esc_html__( 'On', 'parlour-core' ),
			]
		);

		$this->add_control(
			'show_order_link',
			[
				'label'        => esc_html__( 'Order Link Button', 'parlour-core' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Show', 'parlour-core' ),
				'label_off'    => esc_html__( 'Hide', 'parlour-core' ),
				'return_value' => 'yes',
				'default'      => 'yes',
			]
		);

		$this->add_control(
			'show_read_more',
			[
				'label'        => esc_html__( 'Read More Button', 'parlour-core' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Show', 'parlour-core' ),
				'label_off'    => esc_html__( 'Hide', 'parlour-core' ),
				'return_value' => 'yes',
				'default'      => 'yes',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_query',
			[
				'label' => esc_html__( 'Query', 'parlour-core' ),
			]
		);

		$this->add_control(
			'source',
			[
				'label' => _x( 'Source', 'Posts Query Control', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => esc_html__( 'Show All', 'parlour-core' ),
					'by_name' => esc_html__( 'Manual Selection', 'parlour-core' ),
				],
				'label_block' => true,
			]
		);

		$categories = get_terms( 'service-categories' );

		$options = [];
		foreach ( $categories as $category ) :
			$options[ $category->slug ] = $category->name;
		endforeach;

		$this->add_control(
			'categories',
			[
				'label' => esc_html__( 'Categories', 'parlour-core' ),
				'type' => Controls_Manager::SELECT2,
				'options' => $options,
				'default' => [],
				'label_block' => true,
				'multiple' => true,
				'condition' => [
					'source' => 'by_name',
				],
			]
		);

		$this->add_control(
			'hide_empty',
			[
				'label' => esc_html__( 'Hide Empty', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => 'Hide',
				'label_off' => 'Show',
				'return_value' => 'yes',
			]
		);

		$this->add_control(
			'advanced',
			[
				'label' => esc_html__( 'Advanced', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => esc_html__( 'Order By', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'post_date',
				'options' => [
					'post_date'  => esc_html__( 'Date', 'parlour-core' ),
					'post_title' => esc_html__( 'Title', 'parlour-core' ),
					'menu_order' => esc_html__( 'Menu Order', 'parlour-core' ),
					'rand'       => esc_html__( 'Random', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label' => esc_html__( 'Order', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'desc',
				'options' => [
					'asc' => esc_html__( 'ASC', 'parlour-core' ),
					'desc' => esc_html__( 'DESC', 'parlour-core' ),
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_items',
			[
				'label' => esc_html__( 'Items', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'item_background',
			[
				'label'     => esc_html__( 'Background', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-grid .bdt-service-content-wrapper' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .bdt-service-grid .bdt-service-img-wrapper:before' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .bdt-service-grid .bdt-service-img-wrapper:after' => 'background-color: {{VALUE}};',

					'{{WRAPPER}} .bdt-service-grid .uk-overlay-gradient' => 'background: -moz-linear-gradient(top, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%); background: -webkit-linear-gradient(top, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%); background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%);',
				],
			]
		);

		$this->add_responsive_control(
			'border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .bdt-service-grid .bdt-service-content-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
					'{{WRAPPER}} .bdt-service-grid .bdt-service-img .bdt-service-img-design' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} 0 0;',


				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'        => 'border',
				'label'       => esc_html__( 'Border Color', 'parlour-core' ),
				'placeholder' => '1px',
				'default'     => '1px',
				'selector'    => '{{WRAPPER}} .bdt-service-grid .bdt-service-content-wrapper',
				'condition'   => [
					'border_radius' => true,
				],
			]
		);
		
		$this->add_control(
			'item_hover_border_color',
			[
				'label' => esc_html__( 'Hover Border Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'border_border!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-service-grid .bdt-service-content-wrapper:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'item_box_shadow',
				'selector' => '{{WRAPPER}} .bdt-service-grid .bdt-service-content-wrapper',
			]
		);

		$this->add_responsive_control(
			'desc_padding',
			[
				'label' => esc_html__( 'Description Padding', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-service-grid .bdt-service-desc' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'alignment',
			[
				'label'       => esc_html__( 'Alignment', 'parlour-core' ),
				'type'        => Controls_Manager::CHOOSE,
				'separator' => 'before',
				'label_block' => false,
				'options'     => [
					'left'        => [
						'title'       => esc_html__( 'Left', 'parlour-core' ),
						'icon'        => 'fa fa-align-left',
					],
				'center'      => [
					'title'       => esc_html__( 'Center', 'parlour-core' ),
					'icon'        => 'fa fa-align-center',
					],
				'right'       => [
					'title'       => esc_html__( 'Right', 'parlour-core' ),
					'icon'        => 'fa fa-align-right',
					],
				],
				'default'     => 'center',
				'separator' => 'before',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_title',
			[
				'label'     => esc_html__( 'Title', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_title' => 'yes',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => esc_html__( 'Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-grid .bdt-service-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-service-grid .bdt-service-title',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_meta',
			[
				'label'     => esc_html__( 'Meta', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_meta' => 'yes',
				],
			]
		);

		$this->add_control(
			'meta_color',
			[
				'label' => esc_html__( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-grid .bdt-service-meta li span' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'meta_separator_color',
			[
				'label' => esc_html__( 'Separator Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-grid  .bdt-service-meta li:not(:last-child)::after' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'meta_typography',
				'label' => esc_html__( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-service-grid .bdt-service-meta li span',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_icon',
			[
				'label' => esc_html__( 'Icon', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_order_link, show_read_more' => 'yes',
				],
			]
		);

		$this->add_control(
			'icon_background',
			[
				'label' => esc_html__( 'Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-grid .bdt-service-icon' => 'background: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-grid .bdt-service-icon svg' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_filter_bar',
			[
				'label' => esc_html__( 'Filter Bar', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_filter_bar' => 'yes',
				],
			]
		);

		$this->add_control(
			'color_filter',
			[
				'label' => esc_html__( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-filter-item' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'color_filter_active',
			[
				'label' => esc_html__( 'Active Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-filter-item.bdt-service-filter-item-active' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'filter_separator_color',
			[
				'label' => esc_html__( 'Separator Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-grid .uk-subnav-divider > :nth-child(n+2):not(.uk-first-column)::before' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'filter_item_spacing',
			[
				'label' => esc_html__( 'Space Between', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 40,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-service-filter.uk-subnav-divider > :nth-child(n+2):not(.uk-first-column)::before' => 'margin-right: calc({{SIZE}}{{UNIT}}/2)',
					'{{WRAPPER}} .bdt-service-filter.uk-subnav' => 'margin-left: calc({{SIZE}}{{UNIT}}/2)',
					'{{WRAPPER}} .bdt-service-filter.uk-subnav > *' => 'padding-left: calc({{SIZE}}{{UNIT}}/2)',
				],
			]
		);

		$this->add_responsive_control(
			'filter_spacing',
			[
				'label' => esc_html__( 'Bottom Spacing', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 40,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-service-filter-wrapper .bdt-service-filter' => 'margin-bottom: {{SIZE}}{{UNIT}} !important',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_filter',
				'label' => esc_html__( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .bdt-service-filter-item',
			]
		);

		$this->end_controls_section();
	}

	public function render() {
		$settings  = $this->get_settings();

		global $post;
		$id         = uniqid('bdtsg_');
		
		$large      = ($settings['columns']) ? 'uk-width-1-'.$settings['columns'].'@l' : '';
		$medium     = ($settings['columns_tablet']) ? 'uk-width-1-'.$settings['columns_tablet'].'@m' : '';
		$small      = ($settings['columns_mobile']) ? 'uk-width-1-'.$settings['columns_mobile'].'@s' : '';
		$responsive = [$small, $medium, $large];

		$args = array(
			'post_type'      => 'services',
			'posts_per_page' => ($settings['posts_per_page']),
			'orderby'        => $settings['orderby'],
			'order'          => $settings['order'],
			'post_status'    => 'publish',
		);
			
		if('by_name' === $settings['source']) :
			$args['tax_query'][] = array(
				'taxonomy' => 'service-categories',
				'field'    => 'slug',
				'terms'    => $settings['categories'],
			);
		endif;

		$wp_query = new \WP_Query($args);

		if( $wp_query->have_posts() ) : ?>
			<div class="bdt-service-grid">

				<?php if( 'yes' == $settings['show_filter_bar'] ) : 

					// Get Filters from Shortcode Options
					if ( 'by_name' === $settings['source'] ) :
						$service_categories = $settings['categories'];
				 	else :
						$service_categories = get_terms( 'service-categories' );
						$arraytostring = '';

						foreach( $service_categories as $service_categories ) :
							$arraytostring .= $service_categories->slug . ',';
						endforeach;

						$arraytostring = rtrim( $arraytostring,',' ); // Remove last commata;
						$service_categories = explode( ',', $arraytostring ); // Create array
					endif;
	?>
					<div class="bdt-service-filter-wrapper">
						<ul id="<?php echo $id; ?>-filter" class="bdt-service-filter uk-flex-<?php echo esc_attr($settings['alignment']);?> uk-subnav uk-subnav-divider uk-margin-large-bottom" >
							<?php if($service_categories): ?>
									<li>
										<a data-filter="*" class="bdt-service-filter-item-active bdt-service-filter-item" href="javascript:;">
											<?php esc_html_e('All Service', 'parlour-core'); ?>
										</a>
									</li>	
								<?php foreach($service_categories as $service_categories => $value) : ?>
									<?php $filter_name = get_term_by('slug',$value,'service-categories'); ?>
									<li>
										<a class="bdt-service-filter-item" data-filter=".sci-<?php echo esc_attr(trim($value)); ?>" href="javascript:;">
											<?php echo esc_html($filter_name->name); ?>
										</a>
									</li>
								<?php endforeach; ?>
							<?php endif; ?>
						</ul>
					</div>
				<?php endif //end if filter ?>
				
				<div id="<?php echo $id; ?>" class="bdt-grid bdt-service-container">
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
						<?php $terms = get_the_terms( get_the_ID(), 'service-categories' ); ?>
						<div class="bdt-service-item-wrapper bdt-grid-item
							 <?php if( $settings['show_filter_bar'] ) : 
				    			foreach ($terms as $term) :
				    				echo ' sci-'.esc_attr($term->slug);
				    			endforeach;
				    		endif; ?>">
							<div class="bdt-service-item bdt-service-align-<?php echo $settings['alignment']; ?>">
								
								<div class="bdt-service-content-wrapper uk-background-default uk-box-shadow-small">
							    	<?php if (has_post_thumbnail()) : ?>
								       <?php $order_link   = get_post_meta( get_the_ID(), 'bdthemes_service_order_link', true ); ?>

								        <div class="bdt-service-img-wrapper uk-position-relative">
								            <div class="bdt-service-img-design">
								                <div class="bdt-service-img">
										            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
										                <?php echo  the_post_thumbnail('medium', array('class' => ''));  ?>
										            </a>
											    </div>
								                <div class="bdt-service-overlay uk-position-cover uk-overlay-gradient uk-position-z-index"></div>
								            </div>  
								        </div>
								    <?php endif; ?>

								    <?php if(( 'yes' == $settings['show_title'] ) || ( 'yes' == $settings['show_meta'] )) : ?>
								        <div class="bdt-service-desc uk-padding uk-position-relative">

								            <?php if( 'yes' == $settings['show_title'] ) : ?>
								                <h3 class="bdt-service-title uk-text-<?php echo $settings['alignment']; ?> uk-margin-remove-top uk-margin-small-bottom">
												    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="uk-link-reset"><?php the_title(); ?></a>
												</h3>
								            <?php endif; ?>

								            <?php if( 'yes' == $settings['show_meta']) : ?>
								                <?php $duration = get_post_meta( get_the_ID(), 'bdthemes_service_duration', true ); ?>
								                <?php $charge   = get_post_meta( get_the_ID(), 'bdthemes_service_price', true ); ?>
								                
								                <ul class="bdt-service-meta uk-subnav uk-flex-<?php echo $settings['alignment']; ?> uk-margin-small-top uk-margin-remove-bottom" uk-margin>
								                    <li><span><?php echo esc_html('Duration: ', 'parlour').esc_html($duration); ?></span></li>
								                    <li><span><?php echo esc_html('Charge: ', 'parlour').esc_html($charge); ?></span></li>
								                </ul>
								            <?php endif; ?>
								        </div>
								    <?php endif; ?>
								</div>
						        <?php if( 'yes' == $settings['show_order_link'] ) : ?>
						            <a href="<?php echo esc_url($order_link); ?>" class="bdt-service-icon bdt-service-cart-link uk-icon-button uk-margin-small-right" title="<?php echo esc_html('Order Service', 'parlour'); ?>"><span uk-icon="icon: cart"></span></a>
						        <?php endif; ?>

						        <?php if( 'yes' == $settings['show_read_more'] ) : ?>
						        	<a href="<?php echo esc_url(get_permalink()); ?>" class="bdt-service-icon bdt-service-read-more uk-icon-button" title="<?php echo esc_attr(get_the_title()); ?>"><span uk-icon="icon: arrow-right"></span></a>
								<?php endif; ?>
							</div>
						</div>
					<?php endwhile; ?>					
					<?php wp_reset_postdata(); ?> 
				</div>
			</div>
			
			<?php if ( 'yes' == $settings['show_filter_bar'] ) : ?>
				<script>
					jQuery(document).ready(function($) {
					    'use strict';
					    var $grid = $('#<?php echo $id; ?>').isotope({
						  itemSelector: '.bdt-service-item-wrapper',
						  percentPosition: true,
						  layoutMode: 'masonry',
						});

						$('#<?php echo $id; ?>-filter').on( 'click', 'a', function() {
						  var filterValue = $(this).attr('data-filter');
						  $grid.isotope({ filter: filterValue });
						});

						$('#<?php echo $id; ?>-filter > li > .bdt-service-filter-item').click(function(){
						    $('#<?php echo $id; ?>-filter > li > .bdt-service-filter-item').removeClass('bdt-service-filter-item-active');
						    $(this).addClass('bdt-service-filter-item-active');
						});
					});
				</script>
			<?php endif; ?>
		<?php endif;
	}
}

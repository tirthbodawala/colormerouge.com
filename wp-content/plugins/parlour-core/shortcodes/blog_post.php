<?php
// Bdthemes blog masonry

if (!function_exists('bdthemes_blog_post')) {
	function bdthemes_blog_post($atts){
		extract(shortcode_atts(array( 
			'limit'          => '8',
			'orderby'        => 'date',
			'order'          => 'DESC',
			'categories'     => 'all',
			'filter'         => 'yes',
			'align'          => 'center',
			'padding'        => 'medium',
			'custom_padding' => '',
			'shadow'         => 'small',
			'hover_shadow'   => 'none',
			'radius'         => 'no',
			'large'          => 3,
			'medium'         => 2,
			'small'          => 1,
			'column_gap'     => 'medium',
			'thumb'          => 'yes',
			'tags'           => 'yes',
			'title'          => 'yes',
			'meta'           => 'yes',
			'excerpt'        => 'yes',
			'excerpt_limit'  => 15,
			'read_more'      => 'yes',
			'class'          => '',
			'css_class'      => '',
		), $atts));
		
		global $post;

		$id         = uniqid('bdtbp_');

		$link_icon = '';

		$classes        = ['uk-background-default', 'uk-transition-toggle'];
		$css_class      = ['bdtp-desc-wrap'];
		$classes[]      = ($radius =='yes') ? 'bdt-post-radius' : '';
		$custom_padding = ($custom_padding) ? 'padding:' . $custom_padding. ';' : '';
		
		$large          = ($large) ? 'uk-width-1-'.$large.'@l' : '';
		$medium         = ($medium) ? 'uk-width-1-'.$medium.'@m' : '';
		$small          = ($small) ? 'uk-width-1-'.$small.'' : '';
		$responsive     = [$small, $medium, $large];

		if ($shadow !=='none') {
            $classes[] = 'uk-box-shadow-'.$shadow;
        }	
        if ($hover_shadow !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$hover_shadow;
        }
        if (($padding !=='custom') and ($padding !=='none')) {
            $css_class[] = ($padding =='medium') ? 'uk-padding' : 'uk-padding-'.$padding;
        }


		$args = array(
			'post_type'      => 'post',
			'posts_per_page' => intval($limit),
			'orderby'        => $orderby,
			'order'          => $order,
			'post_status'    => 'publish',
		);
			
		if($categories != 'all' && $categories != '') {
			// string to array
			$str = $categories;
			$arr = explode(',', $str);
			  
			$args['tax_query'][] = array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		$wp_query = new WP_Query($args);
		ob_start(); // start buffer

		if( $wp_query->have_posts() ) { 


			if($filter == 'yes'){ 

				// Get Filters from Shortcode Options
				if($categories != '' && $categories != 'all') {
					$post_categories = explode(',', $categories);
				} else {
					$post_categories = get_terms('category');
					$arraytostring = '';
					foreach($post_categories as $post_categories){
						$arraytostring .= $post_categories->slug . ',';
					}
					$arraytostring = rtrim($arraytostring,','); // Remove last commata;
					$post_categories = explode(',', $arraytostring); // Create array
				} 
			?>

			<div class="bdt-filter-wrapper">
				<ul id="<?php echo $id; ?>-filter" class="bdt-post-filter uk-flex-<?php echo esc_attr($align);?> uk-subnav uk-subnav-divider uk-margin-large-bottom" >
					<?php if($post_categories): ?>
							<li>
								<a data-filter="*" class="bdt-filter-item-active bdt-filter-item" href="javascript:;">
									<?php esc_html_e('All Services', 'parlour-core'); ?>
								</a>
							</li>	
						<?php foreach($post_categories as $post_categories => $value) { ?>
							<?php $filter_name = get_term_by('slug',$value,'category'); ?>
							<li>
								<a class="bdt-filter-item" data-filter=".sci-<?php echo esc_attr(trim($value)); ?>" href="javascript:;">
									<?php echo esc_html($filter_name->name); ?>
								</a>
							</li>
						<?php } ?>
					<?php endif; ?>
				</ul>
			</div>

			<?php } //end if filter ?>
			


			<div class="bdt-post-container uk-grid-<?php echo $column_gap; ?>" uk-grid>
				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					<?php $terms = get_the_terms( get_the_ID(), 'category' ); ?>
					<div class="bdt-post-item <?php echo implode(" ", $responsive);  ?> <?php if($terms and $filter == 'yes') : foreach ($terms as $term) { echo 'sci-'.esc_attr($term->slug).' '; } endif; ?>">
				  		<div class="<?php echo bdt_acssc($classes); ?>">
				  
					  		<?php if( $thumb == 'yes') : 
						  		$blog_thumbnail= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );

						  		if($blog_thumbnail[0] != '') : ?>
						  			<div class="tm-blog-thumbnail">
							  			<a href="<?php echo esc_url(get_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>" class="bdt-pc-thumb"><img src="<?php echo esc_url($blog_thumbnail[0]); ?>" alt="<?php echo esc_attr(get_the_title()); ?>" /></a>
							  			<img class="tm-blog-entry-overlay" src="<?php echo get_template_directory_uri(); ?>/images/blog-entry-overlay.svg" alt="">
							  		</div>
						  	<?php endif; endif; ?>
				  		
				  		<div class="<?php esc_attr_e(implode(' ', $css_class)); ?>" style="<?php esc_attr_e($custom_padding); ?>">

				  			<?php if ( $tags == 'yes' ) :
					  			$tags_list = get_the_tag_list( '<span class="bdt-pc-tag">', '</span> <span class="bdt-pc-tag">', '</span>');
				                if ( $tags_list ) : ?>
				                    <p class="bdt-pc-tags uk-position-medium uk-position-top-right" ><?php wp_kses_post($tags_list); ?></p>
				            <?php endif; endif; ?>

			            	<?php if ( $title == 'yes' ) : ?>
								<h4 class="uk-margin-small-bottom uk-margin-small-top"><a href="<?php echo esc_url(get_permalink());?>" title="<?php echo esc_attr(get_the_title()); ?>" class="uk-link-reset"><?php echo esc_html(get_the_title()); ?></a></h4>
							<?php endif; ?>

							<?php if ( $meta == 'yes' ) : ?>
								<ul class="uk-subnav uk-margin-small-top uk-subnav-divider" uk-margin>
									<li><span><?php echo esc_html__('Posted:', 'parlour-core').' '.esc_attr(get_the_date()); ?></span></li>
									<li><span><?php echo esc_html__('By:', 'parlour-core');?> <a href="<?php echo get_author_posts_url(get_the_author_meta('ID'));?>" title="<?php get_the_author(); ?>"><?php echo get_the_author(); ?></a></span></li>
								</ul>
							<?php endif; ?>

							<?php if ( $excerpt == 'yes' ) : ?>
								<div><?php echo wp_kses_post(parlour_core_custom_excerpt($excerpt_limit)); ?></div>
							<?php endif; ?>

							<?php if ( $read_more == 'yes') : ?>
								<a href="<?php echo esc_url(get_permalink()); ?>" class="uk-button uk-padding-remove-horizontal uk-margin-top uk-margin-small-right uk-button-text"><?php echo esc_html('Read More', 'parlour-core'); ?><span uk-icon="icon: arrow-right"></span></a>
							<?php endif; ?>
						  		
					  		</div>
						</div>
					</div>
				<?php endwhile; ?>					
			</div>
			
			<?php if ($filter == 'yes') : ?>
				<script>
					jQuery(document).ready(function($) {
					    'use strict';
					    var $grid = jQuery('.bdt-post-container').isotope({
						  // options
						  itemSelector: '.bdt-post-item',
						  percentPosition: true,
						  layoutMode: 'masonry',
						});

						jQuery('.bdt-post-filter').on( 'click', 'a', function() {
						  var filterValue = $(this).attr('data-filter');
						  $grid.isotope({ filter: filterValue });
						});

						$('#<?php echo $id; ?>-filter > li > .bdt-filter-item').click(function(){
						    $('#<?php echo $id; ?>-filter > li > .bdt-filter-item').removeClass('bdt-filter-item-active');
						    $(this).addClass('bdt-filter-item-active');
						});

					});
				</script>
			<?php endif; ?>
			
			<?php wp_reset_query(); 
		};

		$post_output = ob_get_contents(); // get buffered content

		ob_end_clean(); // clean buffer

		wp_reset_postdata();

		return $post_output;
	}
	add_shortcode('bdt_blog', 'bdthemes_blog_post');
}
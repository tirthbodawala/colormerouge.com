<?php
namespace ElementorExtend\Modules\Box;

use ElementorExtend\Base\Module_Base;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); }

class Module extends Module_Base {

	public function get_name() {
		return 'box';
	}

	public function get_widgets() {

		$widgets = [
			'Call_Out',
			'Featured_Icon_Box',
			'Image_Compare',
			'Modal',
			'Progress_Pie',
			'Trailer_Box',
			'Video_Lightbox',
		];

		return $widgets;
	}
}

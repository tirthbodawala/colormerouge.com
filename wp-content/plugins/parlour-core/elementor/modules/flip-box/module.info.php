<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

return [
	'title' => __( 'Flip Box', 'parlour-core' ),
	'required' => true,
	'default_activation' => true,
];

<?php
/**
 * Load the Customizer with some custom extended addons
 *
 * @package Parlour
 * @link http://codex.wordpress.org/Theme_Customization_API
 */

load_template( get_template_directory() . '/customizer/class-customizer-control.php' );
load_template( get_template_directory() . '/customizer/fonts-helper.php' );

/**
 * This funtion is only called when the user is actually on the customizer page
 * @param  WP_Customize_Manager $wp_customize
 */
if ( ! function_exists( 'parlour_customizer' ) ) {
	function parlour_customizer( $wp_customize ) {
		
		// add required files
		load_template( get_template_directory() . '/customizer/class-customizer-base.php' );
		load_template( get_template_directory() . '/customizer/class-customizer-dynamic-css.php' );

		new parlour_Customizer_Base( $wp_customize );
	}
	add_action( 'customize_register', 'parlour_customizer' );
}


/**
 * Takes care for the frontend output from the customizer and nothing else
 */
if ( ! function_exists( 'parlour_customizer_frontend' ) && ! class_exists( 'Parlour_Customize_Frontent' ) ) {
	function parlour_customizer_frontend() {
		load_template( get_template_directory() . '/customizer/class-customizer-frontend.php' );
		new parlour_Customize_Frontent();
	}
	add_action( 'init', 'parlour_customizer_frontend' );
}
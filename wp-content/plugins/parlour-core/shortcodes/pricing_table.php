<?php
// BdThemes Pricing Table Shortcode

if (!function_exists('bdthemes_pricing_table')) {
    
    function bdthemes_pricing_table( $atts, $content = null) {
        extract(shortcode_atts(array(
            'id'    => uniqid('bdtpl_'),
            'style' => 'divider',
            'space' => 'no',
            'class' => ''
        ), $atts)); 

        $classes   = ['bdt-pricing-table', 'uk-child-width-1-1@m', $class];
        $output    = [];

        $style = ($style !='none') ? ' uk-list-'.$style : '';
        $space = ($space != 'no') ? ' uk-list-large' : '';
        
        $output[]      = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" uk-grid>';
        $output[]      = '<ul class="uk-list'.$style.$space.'">';
        $output[]      = do_shortcode( $content ); 
        $output[]      = '</ul>';
        $output[]      = '</div>';

        return implode("", $output);
    }
    // end of Pricing Table shortcode

    add_shortcode('bdt_pricing_table', 'bdthemes_pricing_table');
}

if (!function_exists('bdthemes_pricing_table_item')) {
    function bdthemes_pricing_table_item( $atts, $content = null) {
        extract(shortcode_atts(array(
            //'service_name'        => esc_html__( 'Full Body Spa Therapy', 'parlour-core'),
            'price'               => '$29.99',
            'icon_type'           => 'uikit', // uikit, fontawesome, image
            'uikit_icon'          => '',
            'fontawesome_icon'    => '',
            'image_icon'          => '',
            'icon_size'           => '24',
            'icon_color'          => '#333333',
            'icon_bg'             => 'default',
            'icon_custom_bg'      => '',
            'icon_padding'        => 'small',
            'icon_custom_padding' => '',
            'icon_shadow'         => 'small',
            'icon_radius'         => 'circle',
            'css_animation'       => '',
            'class'               => ''
        ), $atts));

                

        $id        = uniqid('bdtplitem_');
        $classes   = ['bdt-pricing-table-item', 'uk-grid-small', 'uk-flex-middle', $class];
        $css_class = ['bdt-pl-icon'];
        
        $output    = [];
        $icon      = '';
        
        $icon_custom_bg      = ($icon_custom_bg) ? 'background:' . $icon_custom_bg. ';' : '';
        $icon_color          = ($icon_color) ? 'color:' . $icon_color. ';' : '';
        $icon_custom_padding = ($icon_custom_padding) ? 'padding:' . $icon_custom_padding. ';' : '';

        if (($icon_bg !=='custom') and ($icon_bg !=='none')) {
            $css_class[] = ($icon_bg) ? 'uk-background-'.$icon_bg : '';
        }
        if (($icon_padding !=='custom') and ($icon_padding !=='none')) {
            $css_class[] = ($icon_padding =='medium') ? 'uk-padding' : 'uk-padding-'.$icon_padding;
        }
        if ($icon_shadow !=='none') {
            $css_class[] = 'uk-box-shadow-'.$icon_shadow;
        }
        if ($icon_radius !=='none') {
            $css_class[] = 'uk-border-'.$icon_radius;
        }


        // icon
        if (($icon_type == 'uikit') and ($uikit_icon !='')) {
            $icon = '<span class="pricing-table-img-icon" uk-icon="icon: '.$uikit_icon.'" style="'.$icon_color.' width: '.intval($icon_size).'px;"></span>';
        } elseif (($icon_type == 'fontawesome') and ($fontawesome_icon !='')) {
            wp_enqueue_style( 'font-awesome' );
            $icon = '<i class="pricing-table-img-icon '.$fontawesome_icon . '" style="'.$icon_color.' font-size: '.intval($icon_size).'px;"></i>';
        } elseif (($icon_type == 'image') and ($image_icon !='')) {
            $image_url = wp_get_attachment_image_src( $image_icon );
            $image_url = $image_url[0];
            $icon = '<img class="pricing-table-img-icon" src="'.$image_url.'" style="width: '.intval($icon_size).'px;" alt="" />';
        }
        
        $output[] = '<li>';
            $output[] = '<div id="'.$id.'" style="" class="'.bdt_acssc($classes).' uk-flex uk-flex-middle" uk-grid>';
                if ($icon !='') {
                    $output[] = '<div>';
                        $output[] = '<div class="'.bdt_acssc($css_class).'" style="'.$icon_custom_bg.$icon_custom_padding.'">';
                            $output[] = $icon;
                        $output[] = '</div>';
                    $output[] = '</div>';
                }

                $output[] = '<div class="uk-width-expand">'. do_shortcode($content).'</div>';

                $output[] = '<div class="uk-width-auto"><strong>'.$price.'</strong></div>';
            $output[] = '</div>';
        $output[] = '</li>';

        return implode("\n", $output);
    }
    // end of Pricing Table Item shortcode

    add_shortcode('bdt_pricing_table_item', 'bdthemes_pricing_table_item');
}




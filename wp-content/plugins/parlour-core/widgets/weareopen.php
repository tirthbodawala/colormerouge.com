<?php

class widget_weareopen extends WP_Widget { 
	
	// Widget Settings
	function __construct() {
		$widget_ops  = array('description' => esc_html__('Display your open close time widget to any widget position beautifully.', 'parlour-core'));
		$control_ops = array('width' => 300, 'height' => 350, 'id_base' => 'weareopen');
		parent::__construct('weareopen', esc_html__('BdThemes We Are Open', 'parlour-core'), $widget_ops, $control_ops );
	}
	
	// Widget Output
	function widget($args, $instance) {
		extract($args);
		$title     = apply_filters('widget_title', esc_html($instance['title']));
		$show_icon = $instance['show_icon'];
		$day_slot_1  = $instance['day_slot_1'];
		$time_slot_1 = $instance['time_slot_1'];

		$day_slot_2 = $instance['day_slot_2'];
		$time_slot_2 = $instance['time_slot_2'];

		$day_slot_3 = $instance['day_slot_3'];
		$time_slot_3 = $instance['time_slot_3'];

		$day_slot_4 = $instance['day_slot_4'];
		$time_slot_4 = $instance['time_slot_4'];

		$day_slot_5 = $instance['day_slot_5'];
		$time_slot_5 = $instance['time_slot_5'];

		$day_slot_6 = $instance['day_slot_6'];
		$time_slot_6 = $instance['time_slot_6'];

		echo $before_widget;
		echo $before_title . esc_html($title) . $after_title;

		echo '<ul class="we-are-open uk-list">';
			if ($day_slot_1 or $time_slot_1) {
				echo '<li class="uk-grid-small" uk-grid>';
					echo ($show_icon) ? '<div class="uk-width-auto"><span uk-icon="icon: clock; ratio: 0.9"></span></div>' : '';
					echo '<div class="uk-width-expand">'.$day_slot_1.'<br><span>'.$time_slot_1.'</span></div>';
				echo '</li>';
			}if ($day_slot_2 or $time_slot_2) {
				echo '<li class="uk-grid-small" uk-grid>';
					echo ($show_icon) ? '<div class="uk-width-auto"><span uk-icon="icon: clock; ratio: 0.9"></span></div>' : '';
					echo '<div class="uk-width-expand">'.$day_slot_2.'<br><span>'.$time_slot_2.'</span></div>';
				echo '</li>';
			}if ($day_slot_3 or $time_slot_3) {
				echo '<li class="uk-grid-small" uk-grid>';
					echo ($show_icon) ? '<div class="uk-width-auto"><span uk-icon="icon: clock; ratio: 0.9"></span></div>' : '';
					echo '<div class="uk-width-expand">'.$day_slot_3.'<br><span>'.$time_slot_3.'</span></div>';
				echo '</li>';
			}if ($day_slot_4 or $time_slot_4) {
				echo '<li class="uk-grid-small" uk-grid>';
					echo ($show_icon) ? '<div class="uk-width-auto"><span uk-icon="icon: clock; ratio: 0.9"></span></div>' : '';
					echo '<div class="uk-width-expand">'.$day_slot_4.'<br><span>'.$time_slot_4.'</span></div>';
				echo '</li>';
			}if ($day_slot_5 or $time_slot_5) {
				echo '<li class="uk-grid-small" uk-grid>';
					echo ($show_icon) ? '<div class="uk-width-auto"><span uk-icon="icon: clock; ratio: 0.9"></span></div>' : '';
					echo '<div class="uk-width-expand">'.$day_slot_5.'<br><span>'.$time_slot_5.'</span></div>';
				echo '</li>';
			}if ($day_slot_6 or $time_slot_6) {
				echo '<li class="uk-grid-small" uk-grid>';
					echo ($show_icon) ? '<div class="uk-width-auto"><span uk-icon="icon: clock; ratio: 0.9"></span></div>' : '';
					echo '<div class="uk-width-expand">'.$day_slot_6.'<br><span>'.$time_slot_6.'</span></div>';
				echo '</li>';
			}
			
		echo '</ul>';


		echo $after_widget;
	}
	
	// Update
	function update($new_instance, $old_instance) {  
		$instance              = $old_instance; 
		$instance['title']     = strip_tags( $new_instance['title'] );
		$instance['show_icon']  = strip_tags( $new_instance['show_icon'] );

		$instance['day_slot_1']  = strip_tags( $new_instance['day_slot_1'] );
		$instance['time_slot_1'] = strip_tags( $new_instance['time_slot_1'] );

		$instance['day_slot_2']  = strip_tags( $new_instance['day_slot_2'] );
		$instance['time_slot_2'] = strip_tags( $new_instance['time_slot_2'] );

		$instance['day_slot_3']  = strip_tags( $new_instance['day_slot_3'] );
		$instance['time_slot_3'] = strip_tags( $new_instance['time_slot_3'] );

		$instance['day_slot_4']  = strip_tags( $new_instance['day_slot_4'] );
		$instance['time_slot_4'] = strip_tags( $new_instance['time_slot_4'] );

		$instance['day_slot_5']  = strip_tags( $new_instance['day_slot_5'] );
		$instance['time_slot_5'] = strip_tags( $new_instance['time_slot_5'] );

		$instance['day_slot_6']  = strip_tags( $new_instance['day_slot_6'] );
		$instance['time_slot_6'] = strip_tags( $new_instance['time_slot_6'] );
		

		return $instance;
	}
	
	// Backend Form
	function form($instance) {
		
		$defaults = array(
			'title'       => 'We Are Open Widget', 
			'phone'       => '', 
			'email'       => '', 
			'show_icon'   => 1,
			'day_slot_1'  => '',
			'time_slot_1' => '',
			'day_slot_2'  => '',
			'time_slot_2' => '',
			'day_slot_3'  => '',
			'time_slot_3' => '',
			'day_slot_4'  => '',
			'time_slot_4' => '',
			'day_slot_5'  => '',
			'time_slot_5' => '',
			'day_slot_6'  => '',
			'time_slot_6' => '',
		); // Default Values

		$instance = wp_parse_args( (array) $instance, $defaults ); 
?>
        
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>">Title:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
        <p>
			<label for="<?php echo esc_attr($this->get_field_id('day_slot_1')); ?>">Day/Time Slot 1:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('day_slot_1')); ?>" name="<?php echo esc_attr($this->get_field_name('day_slot_1')); ?>" value="<?php echo esc_attr($instance['day_slot_1']); ?>" placeholder="Monday" />
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('time_slot_1')); ?>" name="<?php echo esc_attr($this->get_field_name('time_slot_1')); ?>" value="<?php echo esc_attr($instance['time_slot_1']); ?>" placeholder="10:00AM - 07:30PM" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('day_slot_2')); ?>">Day/Time Slot 2:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('day_slot_2')); ?>" name="<?php echo esc_attr($this->get_field_name('day_slot_2')); ?>" value="<?php echo esc_attr($instance['day_slot_2']); ?>" placeholder="Monday" />
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('time_slot_2')); ?>" name="<?php echo esc_attr($this->get_field_name('time_slot_2')); ?>" value="<?php echo esc_attr($instance['time_slot_2']); ?>" placeholder="10:00AM - 07:30PM" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('day_slot_3')); ?>">Day/Time Slot 3:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('day_slot_3')); ?>" name="<?php echo esc_attr($this->get_field_name('day_slot_3')); ?>" value="<?php echo esc_attr($instance['day_slot_3']); ?>" placeholder="Monday" />
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('time_slot_3')); ?>" name="<?php echo esc_attr($this->get_field_name('time_slot_3')); ?>" value="<?php echo esc_attr($instance['time_slot_3']); ?>" placeholder="10:00AM - 07:30PM" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('day_slot_4')); ?>">Day/Time Slot 4:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('day_slot_4')); ?>" name="<?php echo esc_attr($this->get_field_name('day_slot_4')); ?>" value="<?php echo esc_attr($instance['day_slot_4']); ?>" placeholder="Monday" />
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('time_slot_4')); ?>" name="<?php echo esc_attr($this->get_field_name('time_slot_4')); ?>" value="<?php echo esc_attr($instance['time_slot_4']); ?>" placeholder="10:00AM - 07:30PM" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('day_slot_5')); ?>">Day/Time Slot 5:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('day_slot_5')); ?>" name="<?php echo esc_attr($this->get_field_name('day_slot_5')); ?>" value="<?php echo esc_attr($instance['day_slot_5']); ?>" placeholder="Monday" />
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('time_slot_5')); ?>" name="<?php echo esc_attr($this->get_field_name('time_slot_5')); ?>" value="<?php echo esc_attr($instance['time_slot_5']); ?>" placeholder="10:00AM - 07:30PM" />
		</p>
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('day_slot_6')); ?>">Day/Time Slot 6:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('day_slot_6')); ?>" name="<?php echo esc_attr($this->get_field_name('day_slot_6')); ?>" value="<?php echo esc_attr($instance['day_slot_6']); ?>" placeholder="Monday" />
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('time_slot_6')); ?>" name="<?php echo esc_attr($this->get_field_name('time_slot_6')); ?>" value="<?php echo esc_attr($instance['time_slot_6']); ?>" placeholder="10:00AM - 07:30PM" />
		</p>
		<p>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'show_icon' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'show_icon' )); ?>" type="checkbox" value="1" <?php checked('1', $instance['show_icon'] ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'show_icon' )); ?>">Show Icon</label>
		</p>
		
    <?php }
}

// Add Widget
function widget_weareopen_init() {
	register_widget('widget_weareopen');
}
add_action('widgets_init', 'widget_weareopen_init');

?>
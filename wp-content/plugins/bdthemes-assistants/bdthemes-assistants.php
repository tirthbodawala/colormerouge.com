<?php
/*
Plugin Name: BdThemes Assistants
Plugin URI: http://themeforest.net/user/bdthemes
Description: This plugin will create a assistants custom post type for bdthemes wordpress theme.
Version: 1.0.1
Author: bdthemes
Author URI: http://bdthemes.com
License: Custom
License URI: http://themeforest.net/licenses 
*/

/* ----------------------------------------------------- */
/* Add Assistants Custom Post Type
/* ----------------------------------------------------- */
function bdthemes_assistants_register() {  

	$assistants_slug = get_theme_mod('bdt_assistants_slug');

	if(isset($assistants_slug) && $assistants_slug != ''){
		$assistants_slug = $assistants_slug;
	} else {
		$assistants_slug = 'assistant';
	}
	
	$labels = array(
		'name'               => esc_html__( 'Assistants', 'bdthemes-assistants' ),
		'singular_name'      => esc_html__( 'Assistant', 'bdthemes-assistants' ),
		'add_new'            => esc_html__( 'Add New Assistant', 'bdthemes-assistants' ),
		'add_new_item'       => esc_html__( 'Add New Assistant', 'bdthemes-assistants' ),
		'edit_item'          => esc_html__( 'Edit Assistant', 'bdthemes-assistants' ),
		'new_item'           => esc_html__( 'Add New Assistant', 'bdthemes-assistants' ),
		'view_item'          => esc_html__( 'View Assistant', 'bdthemes-assistants' ),
		'search_items'       => esc_html__( 'Search Assistants', 'bdthemes-assistants' ),
		'not_found'          => esc_html__( 'No assistant found', 'bdthemes-assistants' ),
		'not_found_in_trash' => esc_html__( 'No assistant found in trash', 'bdthemes-assistants' )
	);
	
    $args = array(  
		'labels'          => $labels,
		'public'          => true,  
		'show_ui'         => true,  
		'capability_type' => 'post',  
		'hierarchical'    => false,  
		'menu_icon'       => 'dashicons-groups',
		'rewrite'         => array('slug' => $assistants_slug), // Permalinks format
		'supports'        => array('title', 'editor', 'thumbnail', 'comments', 'excerpt')  
       );  
  
    register_post_type( 'assistants' , $args );  
}
add_action('init', 'bdthemes_assistants_register', 1);   

/* ----------------------------------------------------- */
/* Register Taxonomy
/* ----------------------------------------------------- */
function bdthemes_assistants_taxonomy() {
	
	register_taxonomy("experiences", array("assistants"), array(
			"hierarchical"   => true, 
			"label"          => "Assistant by Experiences", 
			"singular_label" => "Assistant by Experience", 
			"rewrite"        => true
		));

}
add_action('init', 'bdthemes_assistants_taxonomy', 1);   


function bdthemes_assistants_edit_columns( $assistants_columns ) {
	$assistants_columns = array(
		"cb"          => "<input type=\"checkbox\" />",
		"title"       => esc_html__('Title', 'bdthemes-assistants'),
		"thumbnail"   => esc_html__('Thumbnail', 'bdthemes-assistants'),
		"experiences" => esc_html__('Experiences', 'bdthemes-assistants'),
		"author"      => esc_html__('Author', 'bdthemes-assistants'),
		"comments"    => esc_html__('Comments', 'bdthemes-assistants'),
		"date"        => esc_html__('Date', 'bdthemes-assistants'),
	);
	$assistants_columns['comments'] = '<div class="vers"><img alt="Comments" src="' . esc_url( admin_url( 'images/comment-grey-bubble.png' ) ) . '" /></div>';
	return $assistants_columns;
}
add_filter( 'manage_edit-assistants_columns', 'bdthemes_assistants_edit_columns' );

/* ----------------------------------------------------- */

function bdthemes_assistants_column_display( $columns, $post_id ) {
	
	switch ( $columns ) {

		//Display the thumbnail in the column view
		case "thumbnail":
			$width           = (int) 80;
			$height          = (int) 80;
			$thumbnail_id    = get_post_meta( $post_id, '_thumbnail_id', true );
			
			// Display the featured image in the column view if possible
			if ( $thumbnail_id ) {
				$thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );
			}
			if ( isset( $thumb ) ) {
				echo $thumb; // No need to escape
			} else {
				echo esc_html__('None', 'bdthemes-services');
			}
		break;
				
		// Display the assistants tags in the column view
		case "experiences":
		
			if ( $category_list = get_the_term_list( $post_id, 'experiences', '', ', ', '' ) ) {
				echo $category_list; // No need to escape
			} else {
				echo esc_html__('None', 'bdthemes-assistants');
			}
		break;			
	}
}
add_action( 'manage_assistants_posts_custom_column', 'bdthemes_assistants_column_display', 10, 2 );
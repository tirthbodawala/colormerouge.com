<?php
namespace ElementorExtend\Modules\ThirdPartyPlugins;

use ElementorExtend\Base\Module_Base;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); }

class Module extends Module_Base {

	public function get_name() {
		return 'third-party-plugins';
	}

	public function get_widgets() {

		$widgets = [];

		if(is_plugin_active('instagram-feed/instagram-feed.php')) {
			$widgets[] = 'Instagram_Feed';
		}
		if(is_plugin_active('contact-form-7/wp-contact-form-7.php')) {
			$widgets[] = 'Contact_Form_7';
		}

		if(is_plugin_active('booked/booked.php')) {
			$widgets[] = 'Booked';
		}

		if(is_plugin_active('mailchimp-for-wp/mailchimp-for-wp.php')) {
			$widgets[] = 'Mailchimp';
		}

		if(is_plugin_active('revslider/revslider.php')) {
			$widgets[] = 'Revolution_Slider';
		}

		return $widgets;
	}
}

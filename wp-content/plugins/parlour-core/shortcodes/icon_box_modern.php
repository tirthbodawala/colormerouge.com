<?php
// BdThemes icon_list_modern Shortcode

if (!function_exists('bdthemes_icon_list_modern')) {
    function bdthemes_icon_list_modern($atts = null, $content = null) {

        $atts = shortcode_atts(array(
            'id'     => uniqid('bdtilm_'),
            'column' => '3',
            'style'  => '1',
            'class'  => ''
        ), $atts);

		$id      = $atts['id'];
		$class   = $atts['class'];
		$classes = [$class, 'icon-list-modern-wrap', 'style-'.$atts['style']];
		$output  = [];

        $output[] = '
            <div id="'.$id.'" class="'.bdt_acssc($classes).'">
                <ul class="icon-list-modern one-third-width-elements uk-child-width-1-'.$atts['column'].'@m uk-grid-collapse" uk-grid uk-height-match="target: > li > .icon-list-modern-item">';       
        $output[] = do_shortcode($content);
        $output[] = '</ul></div>';

        return implode("\n", $output);
    }


    // end of icon_list_modern shortcode
    add_shortcode('bdt_icon_list_modern', 'bdthemes_icon_list_modern');
}


if (!function_exists('bdthemes_icon_list_modern_item')) {
    function bdthemes_icon_list_modern_item($atts = null, $content = null) {

        $atts = shortcode_atts(array(
			'title'             => 'Icon List Modern Heading',
			'subtitle'          => 'Subtitle goes here',
			'icon_type'         => 'uikit', // uikit, fontawesome, image
			'uikit_icon'        => 'home',
			'fontawesome_icon'  => 'fa fa-home',
			'image_icon'        => '',
			'icon_color'        => '#666666',
			'title_color'       => '#666666',
			'subtitle_color'    => '#b5b5b5',
			'background'        => 'muted',
			'custom_background' => '',
			'color'             => 'dark',
            'css_animation'     => '',
            'class'             => ''
        ), $atts);


		$output            = [];
		$css_class         = ['skew-bg'];
		$classes           = ['icon-list-modern-item', $atts['class']];
		
		$icon_color        = ($atts['icon_color']) ? 'color:' . $atts['icon_color'] . ';' : '';
		$title_color       = ($atts['title_color']) ? 'color:' . $atts['title_color'] . ';' : '';
		$subtitle_color    = ($atts['subtitle_color']) ? 'color:' . $atts['subtitle_color'] . ';' : '';
		$custom_background = ($atts['custom_background']) ? 'background:' . $atts['custom_background']. ';' : '';


        if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $css_class[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if ($atts['color']) {
            $classes[] = 'uk-'.$atts['color'];
        }

        // icon
        if ($atts['icon_type'] == 'uikit') {
            $icon = '<span class="list-img-icon" uk-icon="icon: '.$atts['uikit_icon'].'; ratio: 2" style="'.$icon_color.'"></span>';
        } elseif ($atts['icon_type'] == 'fontawesome') {
            $icon = '<i class="list-img-icon '.$atts['fontawesome_icon'] . '" style="'.$icon_color.'"></i>';
        } else {
            $image_url = wp_get_attachment_image_src( $atts['image_icon'] );
            $image_url = $image_url[0];
            $icon = '<img class="list-img-icon" src="'.$image_url.'" alt="" />';
        }

        $output[] = '<li>
                        <div class="'.bdt_acssc($classes).'">
                            <span class="'.bdt_acssc($css_class).'" style="'.$custom_background.'"></span>
                            <div class="head-container">
                                <div class="icon-wrap">
                                    '.$icon.'
                                </div>
                                <div class="title-wrap">
                                    <div class="dfd-content-title-big" style="'.$title_color.'">'.$atts['title'].'</div>
                                    <div class="dfd-content-subtitle block-subtitle" style="'.$subtitle_color.'">'.$atts['subtitle'].'</div>
                                </div>
                            </div>
                            <div class="description">'.do_shortcode($content).'</div>
                        </div>
                    </li>';

        return implode("\n", $output);
    }


    // end of icon_list_modern shortcode
    add_shortcode('bdt_icon_list_modern_item', 'bdthemes_icon_list_modern_item');
}
<?php
namespace ElementorExtend\Modules\ThirdPartyPlugins\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Booked extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'booked';
	}

	public function get_title() {
		return __( 'Booked', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-table';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_design_header',
			[
				'label' => __( 'Header', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'header_background',
			[
				'label' => __( 'Header Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar thead th' => 'background-color: {{VALUE}} !important;',
					'{{WRAPPER}} table.booked-calendar tr.days' => 'background-color: transparent !important',
					'{{WRAPPER}} table.booked-calendar thead' => 'background-color: transparent !important',
				],
			]
		);

		$this->add_control(
			'header_color',
			[
				'label' => __( 'Header Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar thead th' => 'color: {{VALUE}} !important;',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_date',
			[
				'label' => __( 'Date', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'date_background',
			[
				'label' => __( 'Date Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar td .date' => 'background-color: {{VALUE}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'date_color',
			[
				'label' => __( 'Date Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar td' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'date_hover_background',
			[
				'label' => __( 'Date Hover Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar td:hover .date span' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'date_hover_color',
			[
				'label' => __( 'Date Hover Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar td:hover .date span' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'current_date_color',
			[
				'label' => __( 'Current Date Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar td.today .date span' => 'color: {{VALUE}} !important;',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'current_date_border_color',
			[
				'label' => __( 'Current Date Border Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar td.today .date span' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'current_date_hover_background',
			[
				'label' => __( 'Current Date Hover Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar td.today:hover .date span' => 'background-color: {{VALUE}} !important;',
				],
			]
		);

		$this->add_control(
			'current_date_hover_color',
			[
				'label' => __( 'Current Date Hover Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar td.today:hover .date span' => 'color: {{VALUE}} !important;',
				],
			]
		);

		$this->add_control(
			'prev_next_date_background',
			[
				'label' => __( 'Prev Date/Next Month Date Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar td.prev-month .date' => 'background-color: {{VALUE}} !important;',
					'{{WRAPPER}} table.booked-calendar td.next-month .date' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} table.booked-calendar td.prev-date:hover .date' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} table.booked-calendar td.prev-date .date' => 'background-color: {{VALUE}} !important;',
					'{{WRAPPER}} table.booked-calendar td.prev-date:hover .date span' => 'background-color: {{VALUE}} !important;',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'prev_next_date_color',
			[
				'label' => __( 'Prev/Next Date Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar td.prev-date .date' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} table.booked-calendar td.prev-month .date span' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} table.booked-calendar td.next-month .date span' => 'color: {{VALUE}} !important;',
					'{{WRAPPER}} table.booked-calendar td.prev-date:hover .date span' => 'color: {{VALUE}} !important;',
				],
			]
		);


		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_border',
			[
				'label' => __( 'Border', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'border_color',
			[
				'label' => __( 'Border Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar thead th' => 'border-color: {{VALUE}} !important;',
					'{{WRAPPER}} table.booked-calendar tr.days th' => 'border-color: {{VALUE}} !important;',
					'{{WRAPPER}} table.booked-calendar td:first-child' => 'border-left-color: {{VALUE}} !important;',
					'{{WRAPPER}} table.booked-calendar td' => 'border-color: {{VALUE}} !important;',
					'{{WRAPPER}} table.booked-calendar' => 'border-bottom-color: {{VALUE}} !important;',
					'{{WRAPPER}} .booked-calendar-wrap .booked-appt-list .timeslot' => 'border-color: {{VALUE}} ;',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_apointments',
			[
				'label' => __( 'Appointments', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'background',
			[
				'label' => __( 'Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar .booked-appt-list' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .booked-calendar-wrap .booked-appt-list .timeslot:hover' => 'background-color: rgba(255, 255, 255, 0.3);',
				],
			]
		);

		$this->add_control(
			'text_color',
			[
				'label' => __( 'Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .booked-calendar-wrap .booked-appt-list h2' => 'color: {{VALUE}};',
					'{{WRAPPER}} .booked-calendar-wrap .booked-appt-list .timeslot .timeslot-time' => 'color: {{VALUE}};',
					'{{WRAPPER}} .booked-calendar-wrap .booked-appt-list .timeslot .timeslot-time i.booked-icon' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'active_date_background_color',
			[
				'label' => __( 'Active Date Background Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} table.booked-calendar tr.week td.active .date' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} table.booked-calendar tr.week td.active:hover .date' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} table.booked-calendar tr.entryBlock' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .booked-calendar-wrap .booked-appt-list .timeslot .spots-available' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}

	private function get_shortcode() {

		$shortcode = [];
		$shortcode[] = sprintf( '[booked-calendar]' );

		return implode("", $shortcode);
	}

	public function render() {
		echo do_shortcode( $this->get_shortcode() );
	}

	public function render_plain_content() {
		echo $this->get_shortcode();
	}
}

<?php
//Creating TinyMCE buttons
//********************************************************************
//check user has correct permissions and hook some functions into the tiny MCE architecture.
function add_editor_button() {
   //Check if user has correct level of privileges + hook into Tiny MC methods.
   if ( current_user_can('edit_posts') && current_user_can('edit_pages') )
   {
     //Check if Editor is in Visual, or rich text, edior mode.
     if (get_user_option('rich_editing')) {
        //Called when tiny MCE loads plugins - 'add_custom' is defined below.
        add_filter('mce_external_plugins', 'add_custom');
        //Called when buttons are loading. -'register_button' is defined below.
        add_filter('mce_buttons', 'register_button');
     }
   }
}

//add action is a wordpress function, it adds a function to a specific action...
//in this case the function is added to the 'init' action. Init action runs after wordpress is finished loading!
add_action('init', 'add_editor_button');

//Add button to the button array.
function register_button($buttons) {
   //Use PHP 'array_push' function to add the columnThird button to the $buttons array
   array_push($buttons, "columnThird");
   //Return buttons array to TinyMCE
   return $buttons;
} 
 
//Add custom plugin to TinyMCE - returns associative array which contains link to JS file. The JS file will contain your plugin when created in the following step.
function add_custom($plugin_array) {
    $plugin_array['columnThird'] = plugin_dir_url( __file__ ).'js/bdt-sc-admin.js';
    return $plugin_array;
}
function add_shortcode_menu() {
?>	

<div class="hidden" style="display:none">
<div class="shortcode-html">
	<a href="javascript:;" class="button-add-shortcode">[+]</a>
	<ul>
		<li class="parent">
			<a href="javascript:;" onclick="return false;">Elements</a>
			<ul>
				<li><a href="#" data-all='[bdt_calltoaction title="Call to action title" button_text="Button Text" align="left" button_link="#" target="self" background="default/muted/primary/secondary/none" color="dark/light" padding="small/medium/large/none" shadow="small/medium/large/xlarge/none" hover_shadow="small/medium/large/xlarge/none" radius="yes/no" class=""]Calltoaction Content[/bdt_calltoaction]'>Call to Action</a></li>

				<li><a href="#" data-all='[bdt_divider align="left/center/right" icon="yes/no" margin="small/medium/large"]'>Divider</a></li>

				<li><a href="#" data-all='[bdt_dropcap]Dropcap is a easy things with our theme, you can even directly call uk-dropcap class by wrapping with a paragraph. we used latest version of uikit for that reason it is so much easy and fun all user.[/bdt_dropcap]'>Dropcap</a></li>

				<li><a href="#" data-all='[bdt_flickr flickr_id="95572727@N00" limit="9" lightbox="yes/no" radius="rounded/circle" class=""]'>Flickr</a></li>


				<li><a href="#" data-all='[bdt_list icon="uk-icon-star" icon_color="#333333"]<ul><li>List item</li><li>List item</li><li>List item</li></ul>[/bdt_list]'>List</a></li>

				<li><a href="#" data-all="[bdt_alert type='primary/success/warning/danger' radius='yes/no' close='yes/no' animation='yes/no']Heads up! This alert needs your attention, but it's not super important.[/bdt_alert]">Note</a></li>

				<li><a href="#" data-all='[bdt_progress_bar show_percent="yes/no" text="HTML" bar_color="#f8f8f8" fill_color="#F44336" text_color="#ffffff" animation="easeInOutExpo" duration="1.5" delay="0.3"]'>Progress Bar</a></li>

				<li><a href="#" data-all='[bdt_progress_pie percent="75" before="Before Text" after="After Text" progress_pie_title="pia title" line_width="10" line_cap="butt/square/round" background="default/muted/primary/secondary/none" bar_color="#F8F8F8" fill_color="#F44336" padding="small/medium/large/none" shadow="small/medium/large/xlarge/none" hover_shadow="small/medium/large/xlarge/none" radius="yes/no"]'>Progress Pie</a></li>

				<li><a href="#" data-all='[bdt_spacer size="20"]'>Spacer</a></li>

				<li><a href="#" data-all='[bdt_team_member background="default/muted/primary/secondary/none" color="dark/light" padding="small/medium/large/none" shadow="small/medium/large/xlarge/none" hover_shadow="small/medium/large/xlarge/none" radius="yes/no" text_align="left/center/right" photo="http://192.168.1.100/shortcode/plugins/system/bdthemes_shortcodes/images/sample/member.svg" name="John Doe" role="Designer" facebook_url="#" twitter_url="#" googleplus_url="#" url="#"]Type here some info about this team member[/bdt_team_member]'>Team Member</a></li>

				<li><a href="#" data-all='[bdt_testimonial style="1" name="John Doe" title="Developer" photo="http://192.168.1.100/shortcode/plugins/system/bdthemes_shortcodes/images/sample/member.svg" company="Company name" url="Company website url" target="self/blank" italic="yes/no"]Testimonial text[/bdt_testimonial]'>Testimonial</a></li>
			</ul>	
		</li>
		<li class="parent">
			<a href="javascript:;" onclick="return false;">Row-Column</a>
			<ul>
				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/1" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[/bdt_row]'>1/1</a></li>

				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/2" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/2" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[/bdt_row]'>1/2, 1/2</a></li>

				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/3" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/3" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/3" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[/bdt_row]'>1/3, 1/3, 1/3</a></li>

				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/4" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/4" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/4" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/4" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[/bdt_row]'>1/4, 1/4, 1/4, 1/4</a></li>

				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/3" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="2/3" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[/bdt_row]'>1/3, 2/3</a></li>

				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/4" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/4" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/2" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[/bdt_row]'>1/4, 1/4, 1/2</a></li>

				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/4" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="3/4" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[/bdt_row]'>1/4, 3/4</a></li>

				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/5" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/5" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/5" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/5" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/5" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[/bdt_row]'>1/5, 1/5, 1/5, 1/5, 1/5</a></li>

				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/6" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/6" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/6" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/6" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/6" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[bdt_column size="1/6" medium="" small="" visible="" hidden="" center="" class=""]Content[/bdt_column]<br>[/bdt_row]'>1/6, 1/6, 1/6, 1/6, 1/6, 1/6</a></li>
			</ul>	
		</li>
		<li class="parent">
			<a href="javascript:;" onclick="return false;">Pricing Plan</a>
			<ul>
				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/2"]<br>[bdt_pricing_plan name="BASIC" price="29.99" period="6 month" featured="" btn_text="SIGN UP NOW" icon="fa fa-calendar-check-o"]<ul>
					<li><span style="color: #999999; margin: 10px; display: block;">Vitae adipiscing turpis. Aenean ligula nibh, molestie id vivide.</span></li>
					<li><strong>10GB</strong> Space amount</li>
					<li><strong>Unlimited</strong> users</li>
					<li><strong>30GB</strong> Bandwidth</li>
					<li>Basic Security</li>
					<li><strong>20</strong> MySQL Databases</li>
				</ul>[/bdt_pricing_plan]<br>[/bdt_column]<br>[bdt_column size="1/2"]<br>[bdt_pricing_plan name="STANDARD" price="49.99" period="1 Year" featured="" btn_text="SIGN UP NOW" icon="fa fa-balance-scale"]
				<ul>
					<li><span style="color: #999999; margin: 10px; display: block;">Vitae adipiscing turpis. Aenean ligula nibh, molestie id vivide.</span></li>
					<li><strong>30GB</strong> Space amount</li>
					<li><strong>Unlimited</strong> users</li>
					<li><strong>60GB</strong> Bandwidth</li>
					<li>Basic Security</li>
					<li><strong>20</strong> MySQL Databases</li>
				</ul>
				[/bdt_pricing_plan]<br>[/bdt_column]<br>[/bdt_row]'>2 Column</a></li>

				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/3"]<br>[bdt_pricing_plan name="BASIC" price="29.99" period="6 month" featured="" btn_text="SIGN UP NOW" icon="fa fa-calendar-check-o"]<ul>
				 	<li><span style="color: #999999; margin: 10px; display: block;">Vitae adipiscing turpis. Aenean ligula nibh, molestie id vivide.</span></li>
				 	<li><strong>10GB</strong> Space amount</li>
				 	<li><strong>Unlimited</strong> users</li>
				 	<li><strong>30GB</strong> Bandwidth</li>
				 	<li>Basic Security</li>
				 	<li><strong>20</strong> MySQL Databases</li>
				</ul>[/bdt_pricing_plan]<br>[/bdt_column]<br>[bdt_column size="1/3"]<br>[bdt_pricing_plan name="STANDARD" price="49.99" period="1 Year" featured="true" btn_text="SIGN UP NOW" icon="fa fa-balance-scale"]
					<ul>
					 	<li><span style="color: #999999; margin: 10px; display: block;">Vitae adipiscing turpis. Aenean ligula nibh, molestie id vivide.</span></li>
					 	<li><strong>30GB</strong> Space amount</li>
					 	<li><strong>Unlimited</strong> users</li>
					 	<li><strong>60GB</strong> Bandwidth</li>
					 	<li>Basic Security</li>
					 	<li><strong>20</strong> MySQL Databases</li>
					</ul>
				[/bdt_pricing_plan]<br>[/bdt_column]<br>[bdt_column size="1/3"]<br>[bdt_pricing_plan name="PREMIMUM" price="99.99" period="5 Years" featured="" btn_text="SIGN UP NOW" icon="fa fa-money"]
					<ul>
					 	<li><span style="color: #999999; margin: 10px; display: block;">Vitae adipiscing turpis. Aenean ligula nibh, molestie id vivide.</span></li>
					 	<li><strong>Unlimited</strong> Space amount</li>
					 	<li><strong>Unlimited</strong> users</li>
					 	<li><strong>60GB</strong> Bandwidth</li>
					 	<li>Basic Security</li>
					 	<li><strong>20</strong> MySQL Databases</li>
					</ul>
				[/bdt_pricing_plan]<br>[/bdt_column]<br>[/bdt_row]'>3 Column</a>
				</li>

				<li><a href="#" data-all='[bdt_row]<br>[bdt_column size="1/4"]<br>[bdt_pricing_plan name="BASIC" price="29.99" period="6 month" featured="" btn_text="SIGN UP NOW" icon="fa fa-calendar-check-o"]<ul>
				 	<li><span style="color: #999999; margin: 10px; display: block;">Vitae adipiscing turpis. Aenean ligula nibh, molestie id vivide.</span></li>
				 	<li><strong>10GB</strong> Space amount</li>
				 	<li><strong>Unlimited</strong> users</li>
				 	<li><strong>30GB</strong> Bandwidth</li>
				 	<li>Basic Security</li>
				 	<li><strong>20</strong> MySQL Databases</li>
				</ul>[/bdt_pricing_plan]<br>[/bdt_column]<br>[bdt_column size="1/4"]<br>[bdt_pricing_plan name="STANDARD" price="49.99" period="1 Year" featured="true" btn_text="SIGN UP NOW" icon="fa fa-balance-scale"]
					<ul>
					 	<li><span style="color: #999999; margin: 10px; display: block;">Vitae adipiscing turpis. Aenean ligula nibh, molestie id vivide.</span></li>
					 	<li><strong>30GB</strong> Space amount</li>
					 	<li><strong>Unlimited</strong> users</li>
					 	<li><strong>60GB</strong> Bandwidth</li>
					 	<li>Basic Security</li>
					 	<li><strong>20</strong> MySQL Databases</li>
					</ul>
				[/bdt_pricing_plan]<br>[/bdt_column]<br>[bdt_column size="1/4"]<br>[bdt_pricing_plan name="PREMIMUM" price="99.99" period="5 Years" featured="" btn_text="SIGN UP NOW" icon="fa fa-money"]
					<ul>
					 	<li><span style="color: #999999; margin: 10px; display: block;">Vitae adipiscing turpis. Aenean ligula nibh, molestie id vivide.</span></li>
					 	<li><strong>Unlimited</strong> Space amount</li>
					 	<li><strong>Unlimited</strong> users</li>
					 	<li><strong>60GB</strong> Bandwidth</li>
					 	<li>Basic Security</li>
					 	<li><strong>20</strong> MySQL Databases</li>
					</ul>
				[/bdt_pricing_plan]<br>[/bdt_column]<br>
				[bdt_column size="1/4"]<br>[bdt_pricing_plan name="EXCLUSIVE" price="199.99" period="10 Years" featured="" btn_text="SIGN UP NOW" icon="fa fa-diamond"]
					<ul>
					 	<li><span style="color: #999999; margin: 10px; display: block;">Donec adipiscing turpis. Aenean ligula nibh, molestie id nec.</span></li>
					 	<li><strong>Unlimited</strong> Space amount</li>
					 	<li><strong>Unlimited</strong> users</li>
					 	<li><strong>100GB</strong> Bandwidth</li>
					 	<li>Basic Security</li>
					 	<li><strong>50</strong> MySQL Databases</li>
					</ul>
				[/bdt_pricing_plan]<br>[/bdt_column]<br>[/bdt_row]'>4 Coulums</a></li>
			</ul>	
		</li>
		<li class="parent">
			<a href="javascript:;" onclick="return false;">Contents</a>
			<ul>				
				<li><a href="#" data-all='[bdt_counter count_start="1" count_end="5001" counter_speed="6" prefix="pre text" suffix="suffix text" separator="yes" add_icon="yes" icon="uk-icon-rocket" align="left" icon_color="#000000" icon_size="22px" background="" padding="25px" border="1px solid #cccccc"]Description text[/bdt_counter]'>Counter</a></li>

				<li><a href="#" data-all='[bdt_countdown datetime="2020/04/20 10:20:20" count_size="36" text_size="13" border="" timer_bg_color="" prefix="" suffix=""]'>Countdown</a></li>

				<li><a href="#" data-all='[bdt_custom_carousel large="4" small="1" medium="3" animation="fade" duration="200" delay="100" autoplay="false" pauseOnHover="false" slidenav="false" dotnav="false" gutter=""]Carousel content here[/bdt_custom_carousel]'>Custom Carousel</a></li>

				<li><a href="#" data-all='[bdt_event_carousel categories="" month="" limit="5" eventdetails="true" time="null" past="null" venue="false" author="null" message="There are no upcoming events at this time." key="End Date" order="ASC" viewall="false" excerpt="true" thumb="true" thumbwidth="" thumbheight="" event_tax="" large="3" medium="2" small="1" scroll="1" arrows="true" arrow_position="default" pagination="true" autoplay="true" delay="4000" speed="350" hoverpause="false" lazyload="false" loop="true" gutter="" style="white"]'>Event Carousel</a></li>

				<li><a href="#" data-all='[bdt_featured_donation title="Featured Donation Title" title_color="#ffffff" title_size="20px" image="" goal="50000" achieve="40000" bar_color="#E8E8E8" fill_color="#F39C12" class=""]Featured donation text here.[/bdt_featured_donation]'>Featured Donation</a></li>

				<li><a href="#" data-all='[gallery ids="" size="medium" link="none" columns="4" gutter="15"]'>Gallery</a></li>

				<li><a href="#" data-all='[bdt_post_carousel posts="6" categories="all" style="white" large="3" medium="2" small="1" scroll="1" arrows="true" arrow_position="default" pagination="true" autoplay="true" delay="4000" speed="350" hoverpause="false" lazyload="false" loop="false" gutter=""]'>Post Carousel</a></li>
			</ul>	
		</li>
		<li class="parent">
			<a href="javascript:;" onclick="return false;">Icons</a>
			<ul>
				<li><a href="#" data-all='[bdt_icon_list_item title="Icon List Heading" title_color="#444444" title_size="16px" color="#333333" icon="uk-icon-heart" icon_color="#333333" icon_background="rgba(0,0,0,0)" icon_size="24" icon_animation="" icon_border="" icon_shadow="" icon_radius="" icon_align="left" icon_padding="20px" icon_gap="" url="" target="self" class=""]Icon description text here[/bdt_icon_list_item]'>Icon List Item</a></li>

				<li><a href="#" data-all='[bdt_inline_icon icon="uk-icon-heart" background="transparent" color="#333333" size="16" radius="0px" square_size="yes" border="0px solid #cccccc" margin="0px" padding="15px" url="" target="blank" inline_text="" class=""]'>Inline Icon</a></li>
			</ul>	
		</li>
		<li class="boder"></li>

		<li><a href="#" data-all='[bdt_heading style="default" size="24" align="center" margin="" width="" heading="h3" color=""]This is a heading[/bdt_heading]'>Heading</a></li>	
	</ul>
</div>
</div>

<?php 
}
add_action('admin_head', 'add_shortcode_menu');

?>
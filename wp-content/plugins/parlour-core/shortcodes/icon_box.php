<?php
// BdThemes icon_box Shortcode

if (!function_exists('bdthemes_icon_box_shortcode')) {
    function bdthemes_icon_box_shortcode($atts = null, $content = null) {
        $atts = shortcode_atts(array(
            'title'             => 'Icon List Heading',
            'title_size'        => '16px',
            'icon_type'         => 'uikit', // fontawesome, image
            'uikit_icon'        => 'home',
            'fontawesome_icon'  => 'fa fa-home',
            'image_icon'        => '',
            'icon_color'        => '#333333',
            'icon_background'   => 'rgba(0,0,0,0)',
            'icon_size'         => 24,
            'icon_animation'    => '',
            'icon_border'       => '',
            'icon_shadow'       => '',
            'icon_radius'       => '',
            'icon_align'        => 'left',
            'icon_padding'      => '20px',
            'icon_gap'          => '',
            'background'        => 'none',
            'custom_background' => '',
            'color'             => 'dark',
            'custom_color'      => '',
            'padding'           => 'none',
            'custom_padding'    => '',
            'shadow'            => 'none',
            'hover_shadow'      => 'none',
            'border'            => '',
            'radius'            => 'no',
            'url'               => '',
            'linkto'            => 'all',
            'target'            => 'self',
            'class'             => ''
        ), $atts, 'bdt_icon_box');

        $id   = uniqid('suil');
        $icon = '';
        $output = [];
        if (is_rtl()) {
            if ($atts['icon_align'] === 'left') {
                $atts['icon_align'] = 'right';
            } elseif ($atts['icon_align'] === 'right') {
                $atts['icon_align'] = 'left';
            }
        }

        $icon_animation    = ($atts['icon_animation']) ? 'bdt-il-animation-'.$atts['icon_animation'].'' : '';
        $title_size        = ($atts['title_size']) ? 'font-size: '.$atts['title_size'].';' : '';
        $radius            = ($atts['icon_radius']) ? '-webkit-border-radius:' . $atts['icon_radius'] . '; border-radius:' . $atts['icon_radius'] . ';' : '';
        $icon_shadow       = ($atts['icon_shadow']) ? '-webkit-box-shadow:' . $atts['icon_shadow'] . '; box-shadow:' . $atts['icon_shadow'] . ';' : '';
        $icon_padding      = ($atts['icon_padding']) ? 'padding:' . $atts['icon_padding'] . ';' : '';
        $icon_border       = ($atts['icon_border']) ? 'border:' . $atts['icon_border'] . ';' : '';
        $icon_color        = ($atts['icon_color']) ? 'color:' . $atts['icon_color'] . ';' : '';
        $icon_size         = ($atts['icon_size']) ? 'font-size: '.intval($atts['icon_size']).'px;' : '';
        
        $classes           = array('bdt-icon-box', 'bdt-icon-align-'. str_replace('_', '-', $atts['icon_align']), $icon_animation);
        
        $custom_background = ($atts['custom_background']) ? 'background:' . $atts['custom_background']. ';' : '';
        $custom_color      = ($atts['custom_color']) ? 'color:' . $atts['custom_color']. ';' : '';
        $custom_padding    = ($atts['custom_padding']) ? 'padding:' . $atts['custom_padding']. ';' : '';
        $border            = ($atts['border']) ? 'border:' . $atts['border']. ';' : '';
        $classes[]         = ($atts['radius'] =='yes') ? 'uk-border-rounded' : '';


        if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $classes[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if ($atts['color']) {
            $classes[] = 'uk-'.$atts['color'];
        }
        if (($atts['padding'] !=='custom') and ($atts['padding'] !=='none')) {
            $classes[] = ($atts['padding'] =='medium') ? 'uk-padding' : 'uk-padding-'.$atts['padding'];
        }
        if ($atts['shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-'.$atts['shadow'];
        }
        if ($atts['hover_shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$atts['hover_shadow'];
        }


        // icon
        if ($atts['icon_type'] == 'uikit') {
            $icon = '<span class="list-img-icon" uk-icon="icon: '.$atts['uikit_icon'].'" style="'.$icon_color.'"></span>';
        } elseif ($atts['icon_type'] == 'fontawesome') {
            wp_enqueue_style( 'font-awesome' );
            $icon = '<i class="list-img-icon '.$atts['fontawesome_icon'] . '" style="'.$icon_color.$icon_size.'"></i>';
        } else {
            $image_url = wp_get_attachment_image_src( $atts['image_icon'], 'large' );
            $image_url = $image_url[0];
            $icon = '<img class="list-img-icon" src="'.$image_url.'" style="width:'.$atts['icon_size'].'px" alt="" />';
        }
 
        if ($atts['icon_align'] == 'right') {
            if (($atts['icon_background']=='transparent' || $atts['icon_background']=='') and ($atts['icon_border']=='' || substr($atts['icon_border'],0,1) =='0')) {
                $description_margin = 'margin-right: '.(intval($atts['icon_size'])+30).'px;';
            }
            else {
                $description_margin = 'margin-right: '.(intval($atts['icon_size'])+(intval($atts['icon_padding'])*2)+30+(intval($atts['icon_border'])*2)).'px;';
            }

            $description_margin = ($atts['icon_gap']) ? 'margin-right: '.intval($atts['icon_gap']).'px;' : $description_margin;

        } elseif ($atts['icon_align'] == 'top') {
           $description_margin = ($atts['icon_gap']) ? 'margin-top: '.intval($atts['icon_gap']).'px;' : '';
        } elseif ($atts['icon_align'] == 'title') {
           $description_margin = ($atts['icon_gap']) ? 'margin-right: '.intval($atts['icon_gap']).'px;' : '';
           $icon_padding = 'padding: 0;';
        } elseif ($atts['icon_align'] == 'top_left') {
           $description_margin = ($atts['icon_gap']) ? 'margin-right: '.intval($atts['icon_gap']).'px;' : '';
           if (($atts['icon_background']=='transparent' || $atts['icon_background']=='') and ($atts['icon_border']=='' || substr($atts['icon_border'],0,1) =='0')) {
               $icon_padding = 'padding: 0;';
           }
        } elseif ($atts['icon_align'] == 'top_right') {
           $description_margin = ($atts['icon_gap']) ? 'margin-right: '.intval($atts['icon_gap']).'px;' : '';
           if (($atts['icon_background']=='transparent' || $atts['icon_background']=='') and ($atts['icon_border']=='' || substr($atts['icon_border'],0,1) =='0')) {
               $icon_padding = 'padding: 0;';
           }
        } else {
            if (($atts['icon_background']=='transparent' || $atts['icon_background']=='') and ($atts['icon_border']=='' || substr($atts['icon_border'],0,1) =='0')) {
                $description_margin = 'margin-left: '.(intval($atts['icon_size'])+20).'px;';
                $icon_padding = 'padding: 0;';
            }
            else {
                $description_margin = 'margin-left: '.(intval($atts['icon_size'])+(intval($atts['icon_padding'])*2)+30+(intval($atts['icon_border'])*2)).'px;';
            }
            $description_margin = ($atts['icon_gap']) ? 'margin-left: '.intval($atts['icon_gap']).'px;' : $description_margin;
        }

        $atts['icon_background'] = ($atts['icon_background']) ? 'background:'.$atts['icon_background'].';' : '' ;

        if ( ( $atts['url'] !='' ) and ( $atts['linkto'] ==='icon' ) ) {
            $icon = '<a href="'.$atts['url'].'" target="_'.$atts['target'].'" title="'.$atts['title'].'" class="uk-link-reset">'.$icon.'</a>';
        }
        
        $title = '<h3 style="' .$title_size.'">'.$atts['title'].'</h3>';

        if ( ( $atts['url'] !='' ) and ( $atts['linkto'] ==='title' ) ) {
            $title = '<a href="'.$atts['url'].'" target="_'.$atts['target'].'" title="'.$atts['title'].'" class="uk-link-reset">'.$title.'</a>';
        }

        $item = '<div class="icon-box">
                    <div class="icon-box-wrapper">
                        <div class="icon-box-icon" style="'.$atts['icon_background'].'width:' . intval($atts['icon_size']) . 'px; height:' . intval($atts['icon_size']) . 'px;' .$icon_border.$icon_padding.$radius.$icon_shadow.'">'
                            . $icon . '
                        </div>
                    </div>
                    <div class="icon-description" style="'.$description_margin.'">
                        <h3 style="' .$title_size.'">'.$atts['title'].'</h3>
                        <div class="icon-description-text" style="color:' . $atts['color'] . ';">'
                         . do_shortcode($content) .
                        '</div>
                    </div>
                    <div class="clearfix"></div>
                </div>';

        if ( ( $atts['url'] !='' ) and ( $atts['linkto'] ==='all' ) ) {
            $item = '<a href="'.$atts['url'].'" target="_'.$atts['target'].'" title="'.$atts['title'].'" class="uk-link-reset">'.$item.'</a>';
        }

        $output[] = '<div id="'.$id.'" style="'.$custom_background.$custom_color.$custom_padding.$border.'" class="'.bdt_acssc($classes).'">'.$item.'</div>';

        return implode("\n", $output);
    }


    // end of icon-box shortcode
    add_shortcode('bdt_icon_box', 'bdthemes_icon_box_shortcode');
}
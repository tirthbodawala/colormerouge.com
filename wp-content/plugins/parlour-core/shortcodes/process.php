<?php
// BdThemes Process Shortcode

if (!function_exists('bdthemes_process')) {
    
    function bdthemes_process( $atts, $content = null) {
        extract(shortcode_atts(array(
            'id'    => uniqid('bdtprocess_'),
            'color' => 'dark',
            'class' => ''
        ), $atts)); 


        $output    = [];
        $classes   = ['bdt-process', 'uk-text-center', 'uk-child-width-1-4@m', 'uk-grid-large', 'uk-'.$color, $class];
        
        $output[]      = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" uk-grid>';
        $output[]      = do_shortcode( $content ); 
        $output[]      = '</div>';

        return implode("", $output);
    }
    // end of Process shortcode

    add_shortcode('bdt_process', 'bdthemes_process');
}

if (!function_exists('bdthemes_process_item')) {
    function bdthemes_process_item( $atts, $content = null) {
        extract(shortcode_atts(array(
            'icon_type'        => 'uikit', // uikit, fontawesome, image
            'uikit_icon'       => 'home',
            'fontawesome_icon' => 'fa fa-home',
            'image_icon'       => '',
            'icon_color'       => '#333333',
            'icon_background'  => 'default',
            'icon_custom_bg'   => '',
            'shadow'           => 'small',
            'hover_shadow'     => 'medium',
            'icon_radius'      => 'yes',
            'icon_size'        => 50,
            'css_animation'    => '',
            'class'            => ''
        ), $atts));

        
        $id             = uniqid('bdtpitem_');
        $classes        = array('bdt-process-item', $class);
        $css_class      = array('uk-padding-large', 'bdt-pi-icon');
        
        $output         = [];
        $icon           = '';
        
        $icon_color     = ($icon_color) ? 'color:' . $icon_color . ';' : '';
        
        $icon_custom_bg = ($icon_custom_bg) ? 'background:' . $icon_custom_bg. ';' : '';
        $css_class[]    = ($icon_radius =='yes') ? 'uk-border-circle' : '';

        if (($icon_background !=='custom') and ($icon_background !=='none')) {
            $css_class[] = ($icon_background) ? 'uk-background-'.$icon_background : '';
        }
        if ($shadow !=='none') {
            $css_class[] = 'uk-box-shadow-'.$shadow;
        }
        if ($hover_shadow !=='none') {
            $css_class[] = 'uk-box-shadow-hover-'.$hover_shadow;
        }

        // icon
        if ($icon_type == 'uikit') {
            $icon = '<span class="process-img-icon" uk-icon="icon: '.$uikit_icon.'" style="'.$icon_color.'"></span>';
        } elseif ($icon_type == 'fontawesome') {
            wp_enqueue_style( 'font-awesome' );
            $icon = '<i class="process-img-icon '.$fontawesome_icon . '" style="'.$icon_color.'"></i>';
        } else {
            $image_url = wp_get_attachment_image_src( $image_icon );
            $image_url = $image_url[0];
            $icon = '<img class="process-img-icon" src="'.$image_url.'" style="max-width: '.intval($icon_size).'px" alt="" />';
        }
        
        $output[] = '
            <div id="'.$id.'" style="" class="'.bdt_acssc($classes).'">
                <div class="'.bdt_acssc($css_class).'" style="'.$icon_custom_bg.'">'
                    . $icon . '
                    <span class="bdt-pi-icon-num uk-border-circle uk-text-center"></span>
                </div>
                <div class="bdt-pi-text uk-text-center uk-margin-medium-top uk-margin-small-bottom">'
                 . do_shortcode($content) .
                '</div>
            </div>';

        return implode("\n", $output);
    }
    // end of Process Item shortcode

    add_shortcode('bdt_process_item', 'bdthemes_process_item');
}




<?php
namespace ElementorExtend\Modules\ThirdPartyPlugins\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Instagram_Feed extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'instagram-feed';
	}

	public function get_title() {
		return __( 'Instagram Feed', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-gallery-grid';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'type',
			[
				'label'   => __( 'Type', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'user',
				'options' => [
					'user'        => __( 'User', 'parlour-core' ),
					'hashtag'     => __( 'Hashtag', 'parlour-core' ),
					'location'    => __( 'Location', 'parlour-core' ),
					'coordinates' => __( 'Coordinates', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'limit',
			[
				'label' => __( 'Limit (Upper Limit 33)', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 12,
			]
		);

		$this->add_control(
			'cols',
			[
				'label' => __( 'Column (Upper Limit 10)', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 4,
			]
		);

		$this->add_control(
			'imageres',
			[
				'label'   => __( 'Image Size', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'full',
				'options' => [
					'auto'   => __( 'Auto', 'parlour-core' ),
					'full'   => __( 'Full', 'parlour-core' ),
					'medium' => __( 'Medium', 'parlour-core' ),
					'thumb'  => __( 'Thumb', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'showheader',
			[
				'label'     => __( 'Show Header', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'default'   => 'no',
				'label_off' => __( 'no', 'parlour-core' ),
				'label_on'  => __( 'yes', 'parlour-core' ),
			]
		);

		$this->add_control(
			'showbutton',
			[
				'label'     => __( 'Show Button', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'default'   => 'no',
				'label_off' => __( 'no', 'parlour-core' ),
				'label_on'  => __( 'yes', 'parlour-core' ),
			]
		);

		$this->add_control(
			'buttontext',
			[
				'label'       => __( 'Button Text', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Load More...', 'parlour-core' ),
				'default'     => __( 'Load More...', 'parlour-core' ),
				'label_block' => true,
				'condition' => [
					'showbutton' => [ 'yes' ],
				],
			]
		);

		$this->add_control(
			'showfollow',
			[
				'label'     => __( 'Show Follow', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'default'   => 'no',
				'label_off' => __( 'no', 'parlour-core' ),
				'label_on'  => __( 'yes', 'parlour-core' ),
			]
		);

		$this->add_control(
			'followtext',
			[
				'label'       => __( 'Follow Text', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Follow on Instagram', 'parlour-core' ),
				'default'     => __( 'Follow on Instagram', 'parlour-core' ),
				'label_block' => true,
				'condition' => [
					'showfollow' => [ 'yes' ],
				],
			]
		);
	
		$this->end_controls_section();


		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Style', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'imagepadding',
			[
				'label' => __( 'Image Padding', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 10,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
			]
		);

		$this->add_control(
			'headercolor',
			[
				'label' => __( 'Header Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'showheader' => [ 'yes' ],
				],
			]
		);

		$this->add_control(
			'buttoncolor',
			[
				'label' => __( 'Button Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'showbutton' => [ 'yes' ],
				],
			]
		);

		$this->add_control(
			'buttontextcolor',
			[
				'label' => __( 'Button Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'showbutton' => [ 'yes' ],
				],
			]
		);

		$this->add_control(
			'followcolor',
			[
				'label' => __( 'Follow Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'showfollow' => [ 'yes' ],
				],
			]
		);

		$this->add_control(
			'followtextcolor',
			[
				'label' => __( 'Follow Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'condition' => [
					'showfollow' => [ 'yes' ],
				],
			]
		);

		$this->end_controls_section();
	}

	private function get_shortcode() {
		$settings = $this->get_settings();

		$attributes = [
			'type'             => $settings['type'],
			'num'              => $settings['limit'],
			'cols'             => $settings['cols'],
			'imageres'         => $settings['imageres'],
			'imagepadding'     => $settings['imagepadding']['size'],
			'imagepaddingunit' =>'px',
			'showheader'       => ($settings['showheader'] =='yes') ? 'true' : 'false',
			'showbutton'       => ($settings['showbutton'] =='yes') ? 'true' : 'false',
			'showfollow'       => ($settings['showfollow'] =='yes') ? 'true' : 'false',
			'headercolor'      => $settings['headercolor'],
			'buttoncolor'      => $settings['buttoncolor'],
			'buttontextcolor'  => $settings['buttontextcolor'],
			'buttontext'       => $settings['buttontext'],
			'followcolor'      => $settings['followcolor'],
			'followtextcolor'  => $settings['followtextcolor'],
			'followtext'       => $settings['followtext'],
		];

		$this->add_render_attribute( 'shortcode', $attributes );

		$shortcode = [];
		$shortcode[] = sprintf( '[instagram-feed %s]', $this->get_render_attribute_string( 'shortcode' ) );

		return implode("", $shortcode);
	}

	public function render() {
		echo do_shortcode( $this->get_shortcode() );
	}

	public function render_plain_content() {
		echo $this->get_shortcode();
	}
}

<?php
// Bdthemes progress bar shortcode

if (!function_exists('bdthemes_progress_bar')) {
	function bdthemes_progress_bar($atts) {
		$atts = shortcode_atts(array(
			'height'        => 'small',
			'percent'       => 75,
			'show_percent'  => 'no',
			'text'          => 'HTML',
			'text_color'    => 'dark',
			
			'class'         => ''
		), $atts, 'bdt_progress_bar');


		$id         = uniqid('supb');
		$css        = array();
		$output     = [];

		$classes    = array('bdt-progress-bar', 'uk-margin', $atts['class']);
		$classes [] = 'progress-bar-height-'.$atts['height'];
		$classes [] = 'uk-'.$atts['text_color'];


		$text = ($atts['text']) ? '<span class="bdt-pb-text uk-margin-small-right">' . $atts['text'] . '</span>' : '';
		$show_percent = ($atts['show_percent'] !== 'no') ? '<span class="bdt-pb-percent">'. $atts['percent'] . '%</span>' : '';

		$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'">';
		$output[] = '<progress class="uk-progress uk-margin-remove-bottom" value="'.number_format( $atts['percent'], 2 ).'" max="100"></progress>';
		$output[] = $text.$show_percent;
		$output[] = '</div>';
		return implode("", $output);
	}
	add_shortcode('bdt_progress_bar', 'bdthemes_progress_bar');
}
<?php
// BdThemes video lightbox shortcode

    if (!function_exists('bdthemes_video_lightbox')) {
        function bdthemes_video_lightbox($atts) {
            extract(shortcode_atts(array(
                'source'                => 'https://www.youtube.com/watch?v=za6LKKF0R5M',
                'poster'                => '',
                'title'                 => '',
                'subtitle'              => '',
                'shadow'                => 'medium',
                'content_align'         => 'center',
                'button_bg'             => 'default',
                'button_custom_bg'      => '',
                'button_color'          => '#666666',
                'button_padding'        => 'small',
                'button_custom_padding' => '',
                'button_radius'         => 'circle',
                'css_animation'         => '',
                'class'                 => ''
            ), $atts));

            

            $id                 = uniqid('bdtvl_');
            $classes            = ['bdt-video-lightbox', 'uk-position-relative', $class];
            $css_class          = ['bdt-video-lightbox-button', 'uk-box-shadow-small', 'uk-display-inline-block'];
            $output             = [];
            
            $button_custom_bg      = ($button_custom_bg) ? 'background:' . $button_custom_bg. ';' : '';
            $button_color          = ($button_color) ? 'color:' . $button_color. ';' : '';
            $button_custom_padding = ($button_custom_padding) ? 'padding:' . $button_custom_padding. ';' : '';

            if (($button_bg !=='custom') and ($button_bg !=='none')) {
                $css_class[] = ($button_bg) ? 'uk-background-'.$button_bg : '';
            }
            if (($button_padding !=='custom') and ($button_padding !=='none')) {
                $css_class[] = ($button_padding =='medium') ? 'uk-padding' : 'uk-padding-'.$button_padding;
            }
            if ($button_radius !=='none') {
                $css_class[] = 'uk-border-'.$button_radius;
            }

            $poster = wp_get_attachment_image_src( $poster, 'large' );

            if($poster != '') {
                $poster = $poster[0];
                $poster = '<img src="'.$poster.'" alt="poster">';

                $classes[] = 'uk-display-inline-block';

                if ($shadow !=='none') {
                    $classes[] = 'uk-box-shadow-'.$shadow;
                }
            }

            $title    = ($title) ? '<h4 class="bdt-video-lightbox-title uk-margin-remove">'.$title.'</h4>' : '';
            $subtitle = ($subtitle) ? '<p class="bdt-video-lightbox-subtitle uk-margin-remove-bottom">'.$subtitle.'</p>' : '';

            $output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'">';              
                $output[] = $poster;             
                $output[] = '<div class="bdt-video-lightbox-content uk-position-'.$content_align.'">';             
                    $output[] = '<div class="uk-grid-medium uk-flex-middle" uk-grid uk-lightbox>';             
                        $output[] = '<div><a href="'.esc_url($source).'" class="'.bdt_acssc($css_class).'" style="'.$button_custom_bg.$button_color.$button_custom_padding.'" uk-icon="icon: play; ratio: 1.5;"></a></div>';
                        $output[] = '<div>';                        
                            $output[] = $title;            
                            $output[] = $subtitle;                         
                        $output[] = '</div>';             
                    $output[] = '</div>';             
                $output[] = '</div>';             
            $output[] = '</div>';               

            return implode("\n", $output);
        }
        // end of video lightbox shortcode

        add_shortcode('bdt_video_lightbox', 'bdthemes_video_lightbox');
    }

<?php
namespace ElementorExtend\Modules\Carousel\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Testimonial_Carousel extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'testimonial-carousel';
	}

	public function get_title() {
		return __( 'Testimonial Carousel', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-testimonial-carousel';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_responsive_control(
			'columns',
			[
				'label' => __( 'Columns', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'show_image',
			[
				'label'     => __( 'Testimonial Image', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'show_title',
			[
				'label'     => __( 'Title', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'show_address',
			[
				'label'     => __( 'Address', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'show_text',
			[
				'label'     => __( 'Text', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'text_limit',
			[
				'label'     => __( 'Text Limit', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 80,
				'condition' => [
					'show_text' => 'yes',
				],
			]
		);

		$this->add_control(
			'show_rating',
			[
				'label'     => __( 'Rating', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'navigation',
			[
				'label'   => __( 'Navigation', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'both',
				'options' => [
					'both'   => __( 'Arrows and Dots', 'parlour-core' ),
					'arrows' => __( 'Arrows', 'parlour-core' ),
					'dots'   => __( 'Dots', 'parlour-core' ),
					'none'   => __( 'None', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_query',
			[
				'label' => __( 'Query', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'source',
			[
				'label'   => __( 'Source', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'bdthemes-testimonial',
				'options' => [
					'jetpack-testimonial'  => __( 'Jetpack', 'parlour-core' ),
					'bdthemes-testimonial' => __( 'BdThemes', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'posts',
			[
				'label' => __( 'Posts Per Page', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 10,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => __( 'Order by', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'date',
				'options' => [
					'date'     => __( 'Date', 'parlour-core' ),
					'title'    => __( 'Title', 'parlour-core' ),
					'category' => __( 'Category', 'parlour-core' ),
					'rand'     => __( 'Random', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label'   => __( 'Order', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'DESC',
				'options' => [
					'DESC' => __( 'Descending', 'parlour-core' ),
					'ASC'  => __( 'Ascending', 'parlour-core' ),
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_carousel_settings',
			[
				'label' => __( 'Carousel Settings', 'parlour-core' ),
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'     => __( 'Auto Play', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'no',
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label'              => __( 'Autoplay Speed', 'parlour-core' ),
				'type'               => Controls_Manager::NUMBER,
				'default'            => 5000,
				'frontend_available' => true,
				'condition' => [
					'autoplay' => 'yes',
				],
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'   => __( 'Infinite Loop', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'parlour-core' ),
					'no'  => __( 'No', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'speed',
			[
				'label'              => __( 'Animation Speed', 'parlour-core' ),
				'type'               => Controls_Manager::NUMBER,
				'default'            => 500,
				'frontend_available' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_item',
			[
				'label' => __( 'Item', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'item_background',
			[
				'label'     => esc_html__( 'Background', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-item-wrapper' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'        => 'item_border',
				'label'       => esc_html__( 'Border Color', 'parlour-core' ),
				'placeholder' => '1px',
				'default'     => '1px',
				'selector'    => '{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-item-wrapper',
				'separator' => 'before',
			]
		);
		
		$this->add_control(
			'item_hover_border_color',
			[
				'label' => esc_html__( 'Hover Border Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'item_border_border!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-item-wrapper:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'item_border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-item-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'item_box_shadow',
				'selector' => '{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-item-wrapper',
			]
		);

		$this->add_responsive_control(
			'item_padding',
			[
				'label' => esc_html__( 'Padding', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-item-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'item_gap',
			[
				'label'   => __( 'Item Gap', 'parlour-core' ),
				'type'    => Controls_Manager::SLIDER,
				'default' => [
					'size' => 35,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'frontend_available' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_image',
			[
				'label'     => esc_html__( 'Image', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_image' => 'yes',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'        => 'image_border',
				'label'       => esc_html__( 'Border Color', 'parlour-core' ),
				'placeholder' => '1px',
				'default'     => '1px',
				'selector'    => '{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-img-wrapper',
				'separator' => 'before',
			]
		);
		
		$this->add_control(
			'image_hover_border_color',
			[
				'label' => esc_html__( 'Hover Border Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'image_border_border!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-img-wrapper:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'image_border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-img-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_title',
			[
				'label'     => esc_html__( 'Title', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_title' => 'yes',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-title',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_address',
			[
				'label'     => esc_html__( 'Address', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_address' => 'yes',
				],
			]
		);

		$this->add_control(
			'address_color',
			[
				'label' => __( 'Company Name/Address Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-address' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'address_typography',
				'label' => esc_html__( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-address',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_text',
			[
				'label'     => esc_html__( 'Text', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_text' => 'yes',
				],
			]
		);

		$this->add_control(
			'text_color',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-text' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'text_typography',
				'label' => esc_html__( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-testimonial-carousel .bdt-testimonial-carousel-text',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_rating',
			[
				'label'     => esc_html__( 'Rating', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_rating' => 'yes',
				],
			]
		);

		$this->add_control(
			'rating_color',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating'         => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating polygon' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'active_rating_color',
			[
				'label' => __( 'Active Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default'   => '#FFCC00',
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating.bdt-rating-1 .bdt-rating-item:nth-child(1)'            => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating.bdt-rating-2 .bdt-rating-item:nth-child(-n+2)'         => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating.bdt-rating-3 .bdt-rating-item:nth-child(-n+3)'         => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating.bdt-rating-4 .bdt-rating-item:nth-child(-n+4)'         => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating.bdt-rating-5 .bdt-rating-item:nth-child(-n+5)'         => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating.bdt-rating-1 .bdt-rating-item:nth-child(1) polygon'    => 'fill: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating.bdt-rating-2 .bdt-rating-item:nth-child(-n+2) polygon' => 'fill: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating.bdt-rating-3 .bdt-rating-item:nth-child(-n+3) polygon' => 'fill: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating.bdt-rating-4 .bdt-rating-item:nth-child(-n+4) polygon' => 'fill: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-carousel .bdt-rating.bdt-rating-5 .bdt-rating-item:nth-child(-n+5) polygon' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_navigation',
			[
				'label'     => __( 'Navigation', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'navigation' => [ 'arrows', 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_arrows',
			[
				'label'     => __( 'Arrows', 'parlour-core' ),
				'type'      => Controls_Manager::HEADING,
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_style',
			[
				'label'   => __( 'Arrows Style', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'dark',
				'options' => [
					'dark'  => __( 'Dark', 'parlour-core' ),
					'light' => __( 'Light', 'parlour-core' ),
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_position',
			[
				'label' => __( 'Arrows Position', 'parlour-core' ),
				'type'  => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -100,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .swiper-button-next' => 'right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .bdt-testimonial-carousel .swiper-button-prev' => 'left: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_size',
			[
				'label' => __( 'Arrows Size', 'parlour-core' ),
				'type'  => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 25,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .swiper-button-prev, {{WRAPPER}} .bdt-testimonial-carousel .swiper-button-next' => 'height: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}}',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_dots',
			[
				'label' => __( 'Dots', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);


		$this->add_control(
			'dots_position',
			[
				'label' => __( 'Dots Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -80,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .swiper-pagination-bullets' => 'left: {{SIZE}}{{UNIT}} !important;',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_size',
			[
				'label' => __( 'Dots Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 5,
						'max' => 10,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-carousel .swiper-pagination-bullet' => 'height: {{SIZE}}{{UNIT}};width: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->end_controls_section();

	}

	public function render() {
		$settings  = $this->get_settings();

		global $post;
		$id           = $this->get_id();

		$args = array(
			'post_type'      => $settings['source'],
			'posts_per_page' => $settings['posts'],
			'orderby'        => $settings['orderby'],
			'order'          => $settings['order'],
			'post_status'    => 'publish'
		);

		$wp_query = new \WP_Query($args);

		if( $wp_query->have_posts() ) : ?>

			<div id="<?php echo $id; ?>" class="bdt-testimonial-carousel">
				<div class="swiper-container">
					<div class="swiper-wrapper" uk-height-match="target: > div > .bdt-testimonial-carousel-item-wrapper">

						<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
					  		<div class="swiper-slide">
						  		<div class="bdt-testimonial-carousel-item-wrapper uk-box-shadow-small">
							  		<div class="testimonial-item-header">
							  			<div class="uk-grid-small uk-flex-middle" uk-grid>
	            							<div class="uk-width-auto">
								               <?php if( 'yes' == $settings['show_image'] ) :
								                   $testimonial_thumb= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
								                   if($testimonial_thumb[0] != '') : ?>
							                   			<div class="bdt-testimonial-carousel-img-wrapper uk-overflow-hidden uk-border-circle uk-background-cover"><img src="<?php echo esc_url($testimonial_thumb[0]); ?>" alt="<?php echo esc_attr(get_the_title()); ?>" /></div>
								                  	<?php endif; ?>
								               	<?php endif; ?>
							            	</div>
							            	<div class="uk-width-expand">
								            	<?php if( 'yes' == $settings['show_title'] ) : ?>
						                            <h4 class="bdt-testimonial-carousel-title uk-margin-remove-bottom"><?php echo esc_attr(get_the_title()); ?></h4>
						                        <?php endif; ?>
						                        
						                        <?php if( 'yes' == $settings['show_address'] ) : ?>
						                            <p class="bdt-testimonial-carousel-address uk-text-meta uk-margin-remove-top"><?php echo get_post_meta( get_the_ID(), 'bdthemes_tm_company_name', true ); ?></p>
						                        <?php endif; ?>
									        </div>
						            	</div>
						            </div>

					            	<?php if( 'yes' == $settings['show_text'] ) : ?>
				                        <div class="bdt-testimonial-carousel-text"><?php echo wp_kses_post(parlour_core_custom_excerpt($settings['text_limit'])); ?></div>
				                    <?php endif; ?>

			                    	<div class="uk-padding uk-padding-remove-top">
					                    <?php if(( 'yes' == $settings['show_title'] ) || ( 'yes' == $settings['show_address'] ) || ( 'yes' == $settings['show_rating'] )) : ?>
										    <div class="bdt-testimonial-carousel-meta">
						                        
						                        <?php if ( 'yes' == $settings['show_rating'] ) : ?>
						                            <ul class="bdt-rating bdt-rating-<?php echo get_post_meta( get_the_ID(), 'bdthemes_tm_rating', true ); ?> uk-text-muted uk-grid-collapse" uk-grid>
									                    <li class="bdt-rating-item"><span uk-icon="icon: star; ratio: 0.80;"></span></li>
														<li class="bdt-rating-item"><span uk-icon="icon: star; ratio: 0.80;"></span></li>
														<li class="bdt-rating-item"><span uk-icon="icon: star; ratio: 0.80;"></span></li>
														<li class="bdt-rating-item"><span uk-icon="icon: star; ratio: 0.80;"></span></li>
														<li class="bdt-rating-item"><span uk-icon="icon: star; ratio: 0.80;"></span></li>
									                </ul>
						                        <?php endif; ?>
						                    </div>
						               	<?php endif; ?>

					                </div>
				                </div>
			                </div>
						<?php endwhile;
						wp_reset_postdata(); ?>

					</div>
				</div>

		        <?php if ( 'none' !== $settings['navigation'] ) : ?>
					<?php if ( 'arrows' !== $settings['navigation'] ) : ?>
						<div class="swiper-pagination"></div>
					<?php endif; ?>
					
					<?php if ( 'dots' !== $settings['navigation'] ) : 
						$nav_style = ($settings['arrows_style'] == 'light') ? 'swiper-button-white' : 'swiper-button-black'; 
					?>
						<div class="swiper-button-next <?php echo esc_attr($nav_style); ?>"></div>
						<div class="swiper-button-prev <?php echo esc_attr($nav_style); ?>"></div>
					<?php endif; ?>
				<?php endif; ?>
			    
			</div>

		 	<script>
				jQuery(document).ready(function($) {
				    "use strict";				    
				    var swiper = new Swiper(".elementor-element-<?php echo $id; ?> .swiper-container", {
				        navigation: {
							nextEl: ".elementor-element-<?php echo $id; ?> .swiper-button-nextt",
							prevEl: ".elementor-element-<?php echo $id; ?> .swiper-button-prev",
						},
						pagination: {
							el        : ".elementor-element-<?php echo $id; ?> .swiper-pagination",
							type      : 'bullets',
							clickable : true,
						},
				        "autoplay"      : <?php echo 'yes' === $settings['autoplay'] ? '{ "delay": ' . $settings['autoplay_speed'] . ' }' : 'false'; ?>,
				        "paginationClickable":true,
				        "loop": <?php echo ($settings['infinite'] == 'yes') ? 'true' : 'false'; ?>,
				        "speed": <?php echo $settings['speed']; ?>,
				        "slidesPerView": <?php echo $settings['columns']; ?>,
				        "spaceBetween":  <?php echo $settings['item_gap']['size']; ?>,
				        "breakpoints" : {
				            "1024" : {
				            	"slidesPerView": <?php echo $settings['columns']; ?>,
				            	"spaceBetween": <?php echo $settings['item_gap']['size']; ?>,
				            },
				            "768" : {
				            	"slidesPerView": <?php echo $settings['columns_tablet']; ?>,
				            	"spaceBetween": <?php echo $settings['item_gap']['size']; ?>,
				            },
				            "640" : {
				            	"slidesPerView": <?php echo $settings['columns_mobile']; ?>,
				            	"spaceBetween": <?php echo $settings['item_gap']['size']; ?>,
				            }
				        }
				    });
				});
			</script>
	  
		<?php endif;
	}
}



<article id="post-<?php the_ID() ?>" <?php post_class('uk-article post-format-gallery uk-text-'.get_theme_mod('parlour_blog_align', 'center')) ?> data-permalink="<?php the_permalink() ?>" typeof="Article">

    <?php get_template_part( 'template-parts/post-format/schema-meta' ); ?>
    
    <?php 
    //$images = get_post_meta( get_the_ID(), 'parlour_blog_gallery', true );
    $images = rwmb_meta( 'parlour_blog_gallery', 'type=image_advanced&size=parlour_blog' );
    if (!empty($images)) : ?>

    <div class="post-image-gallery uk-position-relative uk-overflow-hidden tm-blog-thumbnail uk-margin-large-bottom">
        <div class="swiper-wrapper" uk-lightbox>
            <?php if (has_post_thumbnail()) : ?>
                <div class="swiper-slide">
                    <?php echo  the_post_thumbnail('parlour_blog', array('class' => 'uk-width-1-1'));  ?>
                </div>
            <?php endif; ?>
            
            <?php foreach ( $images as $image) : ?> 
                <div class="swiper-slide">
                <a href="<?php echo esc_url($image['full_url']); ?>" title="<?php echo esc_attr($image['title']); ?>">
                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" width="<?php echo esc_attr($image['width']); ?>" height="<?php echo esc_attr($image['height']); ?>" class="" />
                </a>
                </div>
            <?php endforeach; ?>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>

        <img class="tm-blog-entry-overlay" src="<?php echo get_template_directory_uri(); ?>/images/blog-entry-overlay.svg" alt="">
    </div>

    <?php endif ?>
    
    


    <div class="uk-margin-medium-bottom uk-container uk-container-small">
        <?php get_template_part( 'template-parts/post-format/title' ); ?>

        <?php if(get_theme_mod('parlour_blog_meta', 1)) :?>
        <?php get_template_part( 'template-parts/post-format/meta' ); ?>
        <?php endif; ?>
    </div>
    
    <div class="uk-container uk-container-small">
        <?php get_template_part( 'template-parts/post-format/content' ); ?>

        <?php get_template_part( 'template-parts/post-format/read-more' ); ?>
    </div>

</article>
<?php
/**
 * Set up the WordPress core custom header feature.
 * @uses parlour_header_style() fuzzy function for validation
 */
function parlour_custom_header_setup() {
	add_theme_support( 'custom-header');
}
add_action( 'after_setup_theme', 'parlour_custom_header_setup' );

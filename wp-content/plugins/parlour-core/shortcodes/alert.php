<?php
// BdThemes Note Shortcode

if (!function_exists('bdthemes_alert_shortcode')) {
    function bdthemes_alert_shortcode($atts = null, $content = null) {
        extract(shortcode_atts(array(
            'type'      => 'primary', // primary, success, warning, danger
            'radius'    => 'no',
            'close'     => 'no',
            'animation' => 'no',
            'class'     => ''
        ), $atts));

        $output    = [];
        $classes   = [];
        $classes[] = $type ? 'uk-alert-'.$type : '';
        $classes[] = ($radius == 'yes') ? 'uk-border-rounded' : '';
        $animation = ($animation == 'yes') ? '{animation: true}' : '';

        $output[] = '<div class="'. bdt_acssc($classes) .'" uk-alert="'.$animation.'">';
        if ($close == 'yes') {
            $output[] = '<a class="uk-alert-close" uk-close></a>';
        }
        $output[] = do_shortcode($content);
        $output[] = '</div>';

        return implode("", $output);
    }
    // end of note shortcode

    add_shortcode('bdt_alert', 'bdthemes_alert_shortcode');
}

<?php
// BdThemes faq Shortcode
if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); } // load is_plugin_active() function if no available
if(is_plugin_active('bdthemes-faq/bdthemes-faq.php')){ 
    if (!function_exists('bdthemes_faq')) {
        function bdthemes_faq($atts) {
            $atts = shortcode_atts(array(
                'id'                => uniqid('bdtf_'),
                'categories'        => 'all',
                'limit'             => 10,
                'order'             => 'title',
                'orderby'           => 'ASC',
                'faq_icon'          => 'no',
                'read_more'         => 'no',
                'class'             => ''
            ), $atts, 'bdt_faq');

            $intro_text = '';
            $title      = '';
            $icon       = '';

            global $post;

            $args = array(
                'post_type'      => 'faq',
                'posts_per_page' => $atts['limit'],
                'orderby'        => $atts['orderby'], 
                'order'          => $atts['order'],
                'post_status'    => 'publish'
            );
            
            if($atts['categories'] != 'all'){
                $str = $atts['categories'];
                $arr = explode(',', $str); // string to array

                $args['tax_query'][] = array(
                    'taxonomy' => 'faq_filter',
                    'field'    => 'slug',
                    'terms'    => $arr
                );
            }            

            $wp_query = new WP_Query($args);


            ob_start(); // start buffer

            if ($wp_query->have_posts()) : ?>
                
                <div id="<?php echo $atts['id']; ?>" class="bdt-faq">
                        
                        <ul class="" uk-accordion>

                            <?php 
                            $limit = 1;

                            while ( $wp_query->have_posts() ) : $wp_query->the_post();
                                
                                // Title condition
                                $title = esc_attr(get_the_title());

                                // Icon condition
                                if ($atts['faq_icon'] =='yes') {
                                    if (get_post_meta( get_the_ID(), 'bdthemes_faq_icon', true)) {
                                        $icon = '<span class="uk-margin-small-right" uk-icon="icon: '.get_post_meta( get_the_ID(), 'bdthemes_faq_icon', true).'; ratio: 0.85;"></span>';
                                    } else {
                                        $icon  = '<span class="uk-margin-small-right" uk-icon="icon: question; ratio: 0.85;"></span>';
                                    }
                                }

                                // Readmore condition
                                $isReadmore = ($atts['read_more'] == 'yes') ? '<div class="bdt-readmore uk-margin-small-top"><a href="'.esc_url(get_permalink()).'" class="uk-button uk-button-text">Read More<span uk-icon="icon: arrow-right; ratio: 1.25;"></span></a></div>' : '';

                                $terms       = get_the_terms( get_the_ID(), 'faq_filter' );

                                ?>

                                <li class="bdt-faq-item">
                                    <h3 class="uk-accordion-title">
                                        <?php echo $icon. $title; ?>
                                    </h3>
                                    <div class="uk-accordion-content">
                                        <div class="tm-accordion-body">
                                           <?php echo wp_kses_post(parlour_core_custom_excerpt(50)).$isReadmore; ?>
                                        </div>
                                    </div>

                                </li>

                                <?php
                                if ($limit++ == $atts['limit']) break;
                            endwhile;
                            wp_reset_postdata(); ?>
                    </ul>
                </div>

                

                <?php

                $faq_output = ob_get_contents(); // get buffered content

                ob_end_clean(); // clean buffer

                return $faq_output;

            else:
                return __('Please select right setting for faq', 'warp');
            endif;

        }
        // end of faq shortcode

        add_shortcode('bdt_faq', 'bdthemes_faq');
    }
}

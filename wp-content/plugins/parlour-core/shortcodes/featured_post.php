<?php
// Bdthemes Post Slider 3D

if (!function_exists('bdthemes_featured_post')) {
	function bdthemes_featured_post($atts){
		extract(shortcode_atts(array(
			'post_type'     => 'post',
			'posts'         => '8',
			'categories'    => 'all',
			'orderby'       => 'date',
			'order'         => 'DESC',
			'title'         => 'yes',
			'meta'          => 'yes',
			'navigation'    => 'yes',
			'autoplay'      => 'no',
			'loop'          => 'yes',
			'style'         => 'cube',
			
			'class'         => ''
		), $atts));

		

		global $post;
		$id       = uniqid('bdtfp_');
		$classes  = ['bdt-featured-post', $class];
		$output   = [];

		$autoplay = ($autoplay =='yes') ? 2500 : 'false';
		$loop     = ($loop =='yes') ? 'true' : 'false';

		if ($post_type == 'services') {
			$taxonomy = 'service-categories';
		} else {
			$taxonomy = 'category';
		}

		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => $posts,
			'orderby'        => $orderby,
			'order'          => $order,
			'post_status'    => 'publish',
			//'meta_key'    => 'featured_post'
		);
		
		if($categories != 'all' && $categories != ''){
			$str = $categories;
			$arr = explode(',', $str); // string to array

			$args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		$wp_query = new WP_Query($args);
		$return = '';

		if( $wp_query->have_posts() ) :
    
			$output[] = '<div id ="'.$id.'" class="'.bdt_acssc($classes).'">';
		
			    $output[] = '<div class="swiper-container">';
				    $output[] = '<div class="swiper-wrapper">';

						while ( $wp_query->have_posts() ) : $wp_query->the_post();

					  		$blog_thumbnail= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

					  		if($blog_thumbnail[0] != ''){
					  
					  			$output[] = '<div class="bdt-fp-thumb swiper-slide" style="background-image:url('. esc_url($blog_thumbnail[0]) .')">';

						  			$output[] = '<div class="uk-transition-fade uk-position-cover uk-overlay uk-overlay-gradient"></div>';
						  			$output[] = '<div class="bdt-fp-desc uk-position-small uk-position-bottom-left uk-transition-slide-bottom">';

						            	if ( $title == 'yes' ) {
											$output[] = '<h4 class="uk-margin-remove-bottom"><a href="'.esc_url(get_permalink()).'" class="uk-link-reset" title="' . esc_attr(get_the_title()) . '">'.esc_html(get_the_title()) .'</a></h4>';
										}

										if ( $meta == 'yes' ) {
											if ($post_type == 'campaign') {
												$campaign = charitable_get_current_campaign();

												$meta_list = '<li>'.esc_html( 'Raised: ', 'parlour-core' ).charitable_format_money( $campaign->get_donated_amount()).'</span></li><li>'.esc_html( 'Goal: ', 'parlour-core' ).charitable_format_money( $campaign->get_goal()).'</li>';

											} elseif ($post_type == 'tribe_events') {
												$meta_list = '<li>'.tribe_get_start_date( null, false, 'M d, Y' ).'</li><li>
															'.tribe_get_venue().'</li>';
															
											} else {
												$meta_list = '<li>'.esc_attr(get_the_date('M d, Y')).'</li><li>
															'.get_the_category_list(', ').'</li>';
											}
											
											$output[] = '<ul class="bdt-fp-meta uk-subnav uk-margin-remove-bottom uk-margin-remove-top uk-margin-small-bottom uk-text-bold uk-text-muted uk-text-uppercase uk-link-reset">'.$meta_list.'</ul>';
										}

							  		$output[] = '</div>';


			        			$output[] = '</div>';
					  		}

						endwhile;
		 				wp_reset_postdata();

			        $output[] = '</div>';

			        /* Add Arrows */
			        if ( $navigation == 'yes' ) {
				        $output[] = '<div class="swiper-button-next swiper-button-black"></div><div class="swiper-button-prev swiper-button-black"></div>';
				    }

		        $output[] = '</div>';

			$output[] ='</div>';

			$js = ($style == 'cube') ? 'cube' : 'flip';

		    /* Initialize Swiper */
		    $output[] = "<script>
		    	 jQuery(document).ready(function ($) {
				    var swiper = new Swiper('#$id .swiper-container', {
				        navigation: {
							nextEl: '#$id .swiper-button-nextt',
							prevEl: '#$id .swiper-button-prev',
						},
				        autoplay      : $autoplay ? '{ 'delay' : 4500 }' : 'false',
				        centeredSlides: true,
				        slidesPerView: 'auto',
				        loop: $loop,
        				autoplayDisableOnInteraction: false,
						grabCursor: true,
				        effect: '".$js."',
				        cube: {
				            shadow: true,
				            slideShadows: true,
				            shadowOffset: 20,
				            shadowScale: 0.94
				        }
				    });
			    });
		    </script>";
			  

		 endif;

		return implode("\n", $output);
	}
	add_shortcode('bdt_featured_post', 'bdthemes_featured_post');
}
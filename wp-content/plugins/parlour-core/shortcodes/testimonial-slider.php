<?php
// Bdthemes Testimonial Slider

if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); } // load is_plugin_active() function if no available
if(is_plugin_active('jetpack/jetpack.php')){
	if (!function_exists('bdthemes_testimonial_slider')) {
		function bdthemes_testimonial_slider($atts){
			extract(shortcode_atts(array(
				'posts'             => '6',
				'nav'               => 'yes',
				'dots'              => 'no',
				'autoplay'          => 'no',
				'loop'              => 'yes',
				'background'        => 'none',
				'custom_background' => '',
				'color'             => 'dark',
				'orderby'           => 'date',
				'order'             => 'DESC',
				'thumb'             => 'yes',
				'title'             => 'yes',
				'address'           => 'yes',
				'rating'            => 'yes',
				'css_animation'     => '',
				'class'             => '',
			), $atts));

			
			
			global $post;
			$id                = uniqid('bdtts_');
			$classes           = ['bdt-testimonial-slider', $class];
			
			$output            = [];
			$autoplay          = ($autoplay =='yes') ? 4500 : 'false';
			$loop              = ($loop =='yes') ? 'true' : 'false';
			
			$custom_background = ($custom_background) ? 'background:' . $custom_background. ';' : '';

			if (($background !=='custom') and ($background !=='none')) {
	            $classes[] = ($background) ? 'uk-background-'.$background : '';
	        }
	        if ($color) {
	            $classes[] = 'uk-'.$color;
	        }

			$args = array(
				'post_type'      => 'jetpack-testimonial',
				'posts_per_page' => $posts,
				'orderby'        => $orderby,
				'order'          => $order,
				'post_status'    => 'publish'
			);

			$wp_query = new WP_Query($args);

			if( $wp_query->have_posts() ) :

				$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" style="'.$custom_background.'">';
					$output[] = '<div class="swiper-container">';
						$output[] = '<div class="swiper-wrapper">';
					
							while ( $wp_query->have_posts() ) : $wp_query->the_post();

						  		$output[] = '<div class="swiper-slide">';

			                        $output[] = '<div class="bdt-testimonial-text uk-text-center uk-margin-medium-bottom">'.wp_kses_post(parlour_core_custom_excerpt(80)).'</div>';
			                    	$output[] = '<div class="uk-flex uk-flex-center uk-flex-middle">';

					                    if( $thumb == 'yes') {
					                        $testimonial_thumb= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );

					                        if($testimonial_thumb[0] != ''){
					                  
					                        $output[] = '<div class="bdt-testimonial-thumb uk-margin-medium-right uk-overflow-hidden uk-border-circle uk-background-cover"><img src="'.esc_url($testimonial_thumb[0]).'" alt="' . esc_attr(get_the_title()) . '" /></div>';
					                        
					                        }
					                    }
					                    if (( $title == 'yes' ) or ( $address == 'yes' ) or ( $rating == 'yes' )) {
										    $output[] = '<div class="bdt-testimonial-meta">';
						                        if ( $title == 'yes' ) {
						                            $output[] = '<div class="uk-h4 uk-display-inline-block uk-margin-remove-bottom">'.esc_html(get_the_title()) .'</div>';
						                        }
						                        
						                        if ( $address == 'yes' ) {
						                            $output[] = '<span class="uk-text-small">, '.get_post_meta( get_the_ID(), 'jetpack_tm_address', true ).'</span>';
						                        }
						                        
						                        if ( $rating == 'yes' ) {
						                            $output[] = '<ul class="tm-rating tm-rating-'.get_post_meta( get_the_ID(), 'jetpack_tm_rating', true ).' uk-text-muted uk-grid-collapse" uk-grid>
												                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
												                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
												                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
												                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
												                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
												                </ul>';
						                        }
						                    $output[] = '</div>';
						                }

					                $output[] = '</div>';
				                $output[] = '</div>';
						  
							endwhile;

						$output[] ='</div>';
				
					$output[] ='</div>';
					
					/* Add Arrows */
			        if ( $nav == 'yes' ) {
			        	$nav_class = ($color == 'light') ? ' swiper-button-white' : ' swiper-button-black';
				        $output[] = '<div class="swiper-button-next'.$nav_class.'"></div><div class="swiper-button-prev'.$nav_class.'"></div>';
				    }
				    /* Add Pagination */
			        if ( $dots == 'yes' ) {
			        	$dots_class = ($color == 'light') ? ' swiper-pagination-white' : '';
				        $output[] = '<div class="swiper-pagination'.$dots_class.'"></div>';
				    }
				    
				$output[] ='</div>';

			    /* Initialize Swiper */
			    $output[] = "<script>
			    	 jQuery(document).ready(function ($) {
					    var swiper = new Swiper('#$id .swiper-container', {
							navigation: {
								nextEl: '#$id .swiper-button-nextt',
								prevEl: '#$id .swiper-button-prev',
							},
							pagination: {
								el        : '#$id .swiper-pagination',
								type      : 'bullets',
								clickable : true,
							},
					        autoplay      : $autoplay ? '{ 'delay' : 4500 }' : 'false',
					        paginationClickable: true,
					        centeredSlides: true,
					        slidesPerView: 'auto',
					        loop: $loop,
	        				autoplayDisableOnInteraction: false,
					    });
				    });
			    </script>";
		  
			endif;

			wp_reset_postdata();

			return implode("\n", $output);
		}
		add_shortcode('bdt_testimonial_slider', 'bdthemes_testimonial_slider');
	}
}
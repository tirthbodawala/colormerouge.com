<?php
// Bdthemes Post Accordion

if (!function_exists('bdthemes_post_accordion')) {
	function bdthemes_post_accordion($atts){
		extract(shortcode_atts(array(
			'post_type'     => 'post',
			'posts'         => '6',
			'categories'    => 'all',
			'orderby'       => 'date',
			'order'         => 'DESC',
			'thumb'         => 'yes',
			'meta'          => 'yes',
			'excerpt'       => 'yes',
			'excerpt_limit' => '50',
			'read_more'     => 'yes',
			'active'        => 1,
			'collapsible'   => 'yes',
			'multiple'      => 'no',
			
			'class'         => ''
		), $atts));

		

		global $post;
		$id      = uniqid('bdtpa_');
		$classes = ['bdt-post-accordion', $class];
		$output  = [];
		$media   = '';

		if ($post_type == 'services') {
			$taxonomy = 'service-categories';
		} else {
			$taxonomy = 'category';
		}

		$settings = [
            'collapsible' => ($collapsible == 'yes') ? true : false,
            'multiple'    => ($multiple == 'yes') ? true : false,
            'active'      => intval($active)-1,
        ];

		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => $posts,
			'orderby'        => $orderby,
			'order'          => $order,
			'post_status'    => 'publish'
		);
		
		if($categories != 'all' && $categories != ''){
			$str = $categories;
			$arr = explode(',', $str); // string to array

			$args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		$content_width = ($thumb == 'yes') ? ' uk-width-2-3@m' : ' uk-width-expand';

		$wp_query = new WP_Query($args);

		if( $wp_query->have_posts() ) : 

			$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'">';
				$output[] = '<ul uk-accordion=\''.json_encode($settings).'\'>';
			
				while ( $wp_query->have_posts() ) : $wp_query->the_post();

					if( $thumb == 'yes') {
				  		$blog_thumbnail= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

				  		if($blog_thumbnail[0] != ''){
				  
				  			$media = '<div class="uk-width-1-3@m"><a href="'.esc_url(get_permalink()).'" title="' . esc_attr(get_the_title()) . '" class="bdt-pa-thumb"><img src="'.esc_url($blog_thumbnail[0]).'" alt="' . esc_attr(get_the_title()) . '" /></a></div>';
				  		
				  		}
				  	}

			  		$output[] = '<li class="uk-open">';

						$output[] = '<h3 class="uk-accordion-title">'.esc_html(get_the_title()).'</h3>';
				  		$output[] = '<div class="uk-accordion-content">';
					  		$output[] = '<div class="bdt-post-accordion-item" uk-grid>';
						  
								$output[] = $media;
						  		
						  		$output[] = '<div class="bdt-pa-desc'.$content_width.'">';



									if ( $meta == 'yes' ) {
							  			if ($post_type == 'campaign') {
											$campaign = charitable_get_current_campaign();

											$meta_list = '<li>'.esc_html( 'Raised: ', 'parlour-core' ).charitable_format_money( $campaign->get_donated_amount()).'</span></li><li>'.esc_html( 'Goal: ', 'parlour-core' ).charitable_format_money( $campaign->get_goal()).'</li>';

										} elseif ($post_type == 'tribe_events') {
											$meta_list = '<li>'.tribe_get_start_date( null, false, 'M d, Y' ).'</li><li>
														'.tribe_get_venue().'</li>';
														
										} else {
											$meta_list = '<li>'.esc_attr(get_the_date('M d, Y')).'</li><li>
														'.get_the_category_list(', ').'</li>';
										}
										
										$output[] = '<ul class="bdt-pa-meta uk-subnav uk-margin-remove-top uk-margin-small-bottom uk-text-bold uk-text-muted uk-text-uppercase uk-link-reset">'.$meta_list.'</ul>';
									}

									if ( $excerpt == 'yes' ) {
										$output[] = '<div>'.wp_kses_post(parlour_core_custom_excerpt(intval($excerpt_limit))).'</div>';
									}

									if ( $read_more == 'yes') {
										$output[] = '<a href="'.esc_url(get_permalink()).'" class="uk-button uk-padding-remove-horizontal uk-margin-small-top uk-margin-small-right uk-text-bold">'.esc_html('Read More', 'parlour-core').'<span uk-icon="icon: chevron-right"></span></a>';
									}

						  		$output[] = '</div>';

							$output[] = '</div>';
						$output[] = '</div>';
					$output[] = '</li>';
			  
				endwhile;
			
				$output[] ='</ul>';
			$output[] ='</div>';
		
		 	wp_reset_postdata();

		 endif;

		return implode("\n", $output);
	}
	add_shortcode('bdt_post_accordion', 'bdthemes_post_accordion');
}
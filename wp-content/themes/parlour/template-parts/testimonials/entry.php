<?php 
    $address    = get_post_meta( get_the_ID(), 'jetpack_tm_address', true ); 
    $rating     = get_post_meta( get_the_ID(), 'jetpack_tm_rating', true ); 
    $background = get_post_meta( get_the_ID(), 'jetpack_tm_bg_style', true );
    $color      = get_post_meta( get_the_ID(), 'jetpack_tm_color_style', true );
 ?>


<div class="bdt-testimonial-item">

    <div class="uk-box-shadow-medium">
        <div class="bdt-testimonial-text uk-position-relative uk-<?php echo  ($color != null) ? $color : 'dark';  ?> uk-background-<?php echo  ($background != null) ? $background : 'default';  ?> uk-padding">

            <?php get_template_part( 'template-parts/testimonials/content' ); ?>

        </div>
    </div>

    <div class="uk-flex uk-flex-middle uk-margin-medium-top">

       <?php if (has_post_thumbnail()) : ?>
           <div class="bdt-testimonial-thumb uk-margin-medium-right uk-display-block uk-overflow-hidden uk-border-circle uk-background-cover">

               <?php echo  the_post_thumbnail('thumbnail'); ?>

           </div>
       <?php endif; ?>

        <div>
            <?php get_template_part( 'template-parts/testimonials/title' ); ?>
            <?php if($address !='') : ?>
                <span class="uk-text-small">, <?php echo esc_html($address); ?></span>
            <?php endif; ?>

            <?php if($rating !='') : ?>
                <ul class="tm-rating tm-rating-<?php echo esc_attr($rating); ?> uk-text-muted uk-grid-collapse" uk-grid>
                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
                </ul>
            <?php endif; ?>
        </div>

   </div>
    
</div>

<?php if(is_single() and empty($author_desc)) : ?>
    <div class="uk-margin-large-top"></div>
<?php endif ?>
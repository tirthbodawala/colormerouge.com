<?php

class widget_flickr extends WP_Widget { 
	
	// Widget Settings
	function __construct() {
		$widget_ops = array('description' => esc_html__('Display your latest flickr photos', 'parlour-core') );
		$control_ops = array( 'width' => 300, 'height' => 350, 'id_base' => 'flickr' );
		parent::__construct( 'flickr', esc_html__('BdThemes Flickr', 'parlour-core'), $widget_ops, $control_ops );
	}
	
	// Widget Output
	function widget($args, $instance) {
		extract($args);
		$title    = apply_filters('widget_title', esc_html($instance['title']));
		$flickr_id = $instance['flickr_id'];
		$limit     = $instance['limit'];
		$radius     = $instance['radius'];
		$lightbox = ($instance['lightbox'] == 1) ? 'yes' : 'no';
		
		// ------
		echo $before_widget;
		if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		echo do_shortcode('[bdt_flickr flickr_id="'.$flickr_id.'" limit="'.$limit.'" lightbox="'.$lightbox.'" radius="'.$radius.'"]');
		echo $after_widget;
	}
	
	// Update
	function update($new_instance, $old_instance) {  
		$instance = $old_instance; 
		
		$instance['title']    = strip_tags( $new_instance['title'] );
		$instance['flickr_id'] = strip_tags( $new_instance['flickr_id'] );
		$instance['limit']     = strip_tags( $new_instance['limit'] );
		$instance['radius'] = strip_tags($new_instance['radius']);
		$instance['lightbox'] = strip_tags($new_instance['lightbox']);

		return $instance;
	}
	
	// Backend Form
	function form($instance) {
		
		$defaults = array( 'title' => 'Flickr Widget', 'limit' => '8', 'flickr_id' => '95572727@N00', 'radius' => '0px', 'lightbox' => '1' ); // Default Values
		$instance = wp_parse_args( (array) $instance, $defaults ); 
?>
        
		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'title' )); ?>">Widget Title:</label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'title' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'title' )); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>
        <p>
			<label for="<?php echo esc_attr($this->get_field_id( 'flickr_id' )); ?>">Flickr ID:</label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'flickr_id' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'flickr_id' )); ?>" value="<?php echo esc_attr($instance['flickr_id']); ?>" /><br />Don't know how to get id? Look here: <a href="<?php echo esc_url( esc_html__('http://idgettr.com/', 'parlour-core')); ?>" target="_blank"><?php esc_html_e('Flickr idGettr', 'parlour-core'); ?></a>
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'limit' )); ?>">Number of Photos:</label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'limit' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'limit' )); ?>" value="<?php echo esc_attr($instance['limit']); ?>" />
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id( 'radius' )); ?>">Radius:</label>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'radius' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'radius' )); ?>" value="<?php echo esc_attr($instance['radius']); ?>" /><br>
			Default value 0px, it's work px, %, em unit.
		</p>

		<p>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'lightbox' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'lightbox' )); ?>" type="checkbox" value="1" <?php checked('1', $instance['lightbox'] ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'lightbox' )); ?>">Show in Lightbox?</label>
		</p>

		
    <?php }
}

// Add Widget
function widget_flickr_init() {
	register_widget('widget_flickr');
}
add_action('widgets_init', 'widget_flickr_init');

?>
<?php
namespace ElementorExtend\Modules\Box\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Call_Out extends Widget_Base {

	//protected $_has_template_content = false;

	public function get_name() {
		return 'call-out';
	}

	public function get_title() {
		return __( 'Call Out', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-call-to-action';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => __( 'Title', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'This is your call to action title', 'parlour-core' ),
				'placeholder' => __( 'Call to action title', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'description',
			[
				'label'       => __( 'Description', 'parlour-core' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => __( 'A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.', 'parlour-core' ),
				'placeholder' => __( 'Call to action description', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_footer',
			[
				'label' => __( 'Button', 'parlour-core' ),
			]
		);

		$this->add_control(
			'button_text',
			[
				'label' => __( 'Button Text', 'parlour-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Click Here', 'parlour-core' ),
			]
		);

		$this->add_control(
			'link',
			[
				'label' => __( 'Link', 'parlour-core' ),
				'type' => Controls_Manager::URL,
				'placeholder' => 'http://your-link.com',
				'default' => [
					'url'         => '#',
					'is_external' => '',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Text Style', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_text_color',
			[
				'label' => __( 'Title Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-callout .bdt-callout-title' => 'color: {{VALUE}};',
				],
				'condition' => [
					'title!' => '',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .bdt-callout .bdt-callout-title',
				'scheme' => Scheme_Typography::TYPOGRAPHY_2,
				'condition' => [
					'title!' => '',
				],
			]
		);


		$this->add_control(
			'description_text_color',
			[
				'label' => __( 'Description Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-callout .bdt-callout-description' => 'color: {{VALUE}};',
				],
				'condition' => [
					'description!' => '',
				],
				'separator' => 'before',
			]
		);


		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'description_typography',
				'selector' => '{{WRAPPER}} .bdt-callout .bdt-callout-description',
				'scheme' => Scheme_Typography::TYPOGRAPHY_2,
				'condition' => [
					'description!' => '',
				],
			]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_button_style',
			[
				'label' => __( 'Button', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'show_label' => false,
			]
		);

		$this->add_control(
			'heading_footer_button',
			[
				'label' => __( 'Button', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->add_control(
			'button_size',
			[
				'label' => __( 'Size', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'md',
				'options' => [
					'xs' => __( 'Extra Small', 'parlour-core' ),
					'sm' => __( 'Small', 'parlour-core' ),
					'md' => __( 'Medium', 'parlour-core' ),
					'lg' => __( 'Large', 'parlour-core' ),
					'xl' => __( 'Extra Large', 'parlour-core' ),
				],
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->start_controls_tabs( 'tabs_button_style' );

		$this->start_controls_tab(
			'tab_button_normal',
			[
				'label' => __( 'Normal', 'parlour-core' ),
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->add_control(
			'button_text_color',
			[
				'label' => __( 'Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-callout a.uk-button' => 'color: {{VALUE}};',
				],
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'button_typography',
				'label' => __( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-callout a.uk-button',
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->add_control(
			'button_background_color',
			[
				'label' => __( 'Background Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_4,
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-callout a.uk-button' => 'background-color: {{VALUE}};',
				],
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(), [
				'name' => 'button_border',
				'label' => __( 'Border', 'parlour-core' ),
				'placeholder' => '1px',
				'default' => '1px',
				'selector' => '{{WRAPPER}} .bdt-callout a.uk-button',
				'condition' => [
					'button_text!' => '',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'button_border_radius',
			[
				'label' => __( 'Border Radius', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-callout a.uk-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->add_control(
			'button_text_padding',
			[
				'label' => __( 'Text Padding', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-callout a.uk-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_button_hover',
			[
				'label' => __( 'Hover', 'parlour-core' ),
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->add_control(
			'button_hover_color',
			[
				'label' => __( 'Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-callout a.uk-button:hover' => 'color: {{VALUE}};',
				],
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->add_control(
			'button_background_hover_color',
			[
				'label' => __( 'Background Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-callout a.uk-button:hover' => 'background-color: {{VALUE}};',
				],
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->add_control(
			'button_hover_border_color',
			[
				'label' => __( 'Border Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-callout a.uk-button:hover' => 'border-color: {{VALUE}};',
				],
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->add_control(
			'button_hover_animation',
			[
				'label' => __( 'Animation', 'parlour-core' ),
				'type' => Controls_Manager::HOVER_ANIMATION,
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	public function render() {
		$settings = $this->get_settings();
		$external = ($settings['link']['is_external']) ? "_blank" : "_self";
		$link_url = empty( $settings['link']['url'] ) ? '#' : $settings['link']['url'];
		?>
            <div class="bdt-callout uk-grid-large uk-flex-middle" uk-grid>
                <div class="uk-width-expand uk-first-column">
                	<?php if ($settings['title']) : ?>
                    	<h3 class="bdt-callout-title uk-text-large uk-margin-remove-bottom"><?php echo esc_html($settings['title']); ?></h3>
                	<?php endif; ?>
					<?php if ($settings['description']) : ?>
                    	<div class="bdt-callout-description uk-margin-small-top"><?php echo strip_tags($settings['description']); ?></div>
					<?php endif; ?>
               </div>

                <div class="uk-width-auto@m">
                    <a class="uk-button uk-button-default uk-button-large" href="<?php echo $link_url; ?>" target="<?php echo $external; ?>">
                    <?php echo esc_html( $settings['button_text'] ); ?></a>
                </div>
            </div>

		<?php
	}

	public function _content_template() {
		?>

        <div class="bdt-callout uk-grid-large uk-flex-middle" uk-grid>
            <div class="uk-width-expand uk-first-column">
            	<# 
	            	if ('' !== settings.title) { 
	                	print('<h3 class="bdt-callout-title uk-text-large uk-margin-remove-bottom">' + settings.title +'</h3>');
	            	}
					if ('' !== settings.description) {
	                	print('<div class="bdt-callout-description uk-margin-small-top">' + settings.description + '</div>');
					}
				#>
           </div>

            <div class="uk-width-auto@m">
                <a class="uk-button uk-button-default uk-button-large" href="{{ settings.link }}" target="{{ settings.link.is_external }}">{{ settings.button_text }}</a>
            </div>
        </div>

        <?php
	}
}

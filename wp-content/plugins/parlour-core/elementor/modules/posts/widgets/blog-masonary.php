<?php
namespace ElementorExtend\Modules\Posts\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class Service Grid
 */
class Blog_Masonary extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'blog-masonary';
	}

	public function get_title() {
		return __( 'Blog Masonary', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-gallery-grid';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_responsive_control(
			'columns',
			[
				'label' => __( 'Columns', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'column_gap',
			[
				'label' => __( 'Column Gap', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'collapse' => __( 'Collapse', 'parlour-core' ),
					'small'    => __( 'Small', 'parlour-core' ),
					'medium'   => __( 'Medium', 'parlour-core' ),
					'large'    => __( 'Large', 'parlour-core' ),
				],
				'default' => 'medium',
			]
		);

		$this->add_control(
			'posts_per_page',
			[
				'label' => __( 'Posts Per Page', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 6,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_query',
			[
				'label' => __( 'Query', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'source',
			[
				'label' => _x( 'Source', 'Posts Query Control', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Show All', 'parlour-core' ),
					'by_name' => __( 'Manual Selection', 'parlour-core' ),
				],
				'label_block' => true,
			]
		);

		$post_categories = get_terms( 'category' );

		$options = [];
		foreach ( $post_categories as $category ) {
			$post_options[ $category->slug ] = $category->name;
		}

		$this->add_control(
			'post_categories',
			[
				'label'       => __( 'Categories', 'parlour-core' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => $post_options,
				'default'     => [],
				'label_block' => true,
				'multiple'    => true,
				'condition'   => [
					'source'    => 'by_name',
				],
			]
		);

		$this->add_control(
			'advanced',
			[
				'label' => __( 'Advanced', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => __( 'Order By', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'post_date',
				'options' => [
					'post_date'  => __( 'Date', 'parlour-core' ),
					'post_title' => __( 'Title', 'parlour-core' ),
					'menu_order' => __( 'Menu Order', 'parlour-core' ),
					'rand'       => __( 'Random', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label' => __( 'Order', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'desc',
				'options' => [
					'asc' => __( 'ASC', 'parlour-core' ),
					'desc' => __( 'DESC', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'thumb',
			[
				'label'     => __( 'Testimonial Image', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'title',
			[
				'label'     => __( 'Title', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'meta',
			[
				'label'     => __( 'Meta Data', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'excerpt',
			[
				'label'     => __( 'Excerpt', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'excerpt_limit',
			[
				'label'   => __( 'Excerpt Limit', 'parlour-core' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 15,
			]
		);

		$this->add_control(
			'read_more',
			[
				'label'     => __( 'Read More', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'filter_bar',
			[
				'label' => __( 'Filter Bar', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'show_filter_bar',
			[
				'label'     => __( 'Show', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'parlour-core' ),
				'label_on'  => __( 'On', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'filter_bar_align',
			[
				'label'   => __( 'Filter Bar Align', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'center',
				'options' => [
					'left'   => __( 'Left', 'parlour-core' ),
					'center' => __( 'center', 'parlour-core' ),
					'right'  => __( 'right', 'parlour-core' ),
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Items', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'padding',
			[
				'label' => __( 'Padding', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 40,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-container .bdtp-desc-wrap' => 'padding: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'shadow',
			[
				'label'     => __( 'Shadow', 'parlour-core' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'small',
				'options'   => [
					'small'  => __( 'Small', 'parlour-core' ),
					'medium' => __( 'Medium', 'parlour-core' ),
					'large'  => __( 'Large', 'parlour-core' ),
					'xlarge' => __( 'Extra Large', 'parlour-core' ),
					'none'   => __( 'No', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'hover_shadow',
			[
				'label'     => __( 'Hover Shadow', 'parlour-core' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'medium',
				'options'   => [
					'small'  => __( 'Small', 'parlour-core' ),
					'medium' => __( 'Medium', 'parlour-core' ),
					'large'  => __( 'Large', 'parlour-core' ),
					'xlarge' => __( 'Extra Large', 'parlour-core' ),
					'none'   => __( 'No', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'radius',
			[
				'label' => __( 'Border Radius', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 0,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 30,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-item > div' => 'border-radius: {{SIZE}}{{UNIT}}; overflow: hidden;',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_filter',
			[
				'label' => __( 'Filter Bar', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_filter_bar' => 'yes',
				],
			]
		);

		$this->add_control(
			'color_filter',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-filter-item' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'color_filter_active',
			[
				'label' => __( 'Active Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-filter-item.bdt-filter-item-active' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_filter',
				'label' => __( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .bdt-filter-item',
			]
		);

		$this->add_responsive_control(
			'filter_item_spacing',
			[
				'label' => __( 'Space Between', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 40,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-filter.uk-subnav-divider > :nth-child(n+2):not(.uk-first-column)::before' => 'margin-right: calc({{SIZE}}{{UNIT}}/2)',
					'{{WRAPPER}} .bdt-post-filter.uk-subnav' => 'margin-left: calc({{SIZE}}{{UNIT}}/2)',
					'{{WRAPPER}} .bdt-post-filter.uk-subnav > *' => 'padding-left: calc({{SIZE}}{{UNIT}}/2)',
				],
			]
		);

		$this->add_responsive_control(
			'filter_spacing',
			[
				'label' => __( 'Bottom Spacing', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 40,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-filter-wrapper .bdt-post-filter' => 'margin-bottom: {{SIZE}}{{UNIT}} !important',
				],
			]
		);

		$this->end_controls_section();
	}


	private function get_shortcode() {
		$settings = $this->get_settings();

		$attributes = [
			'limit'         => $settings['posts_per_page'],
			'orderby'       => $settings['orderby'],
			'order'         => $settings['order'],
			'filter'        => ($settings['show_filter_bar']) ? 'yes' : 'no',
			'align'         => $settings['filter_bar_align'],
			'shadow'        => $settings['shadow'],
			'hover_shadow'  => $settings['hover_shadow'],
			'thumb'         => $settings['thumb'],
			'title'         => $settings['title'],
			'meta'          => $settings['meta'],
			'excerpt'       => $settings['excerpt'],
			'excerpt_limit' => $settings['excerpt_limit'],
			'read_more'     => $settings['read_more'],
			'large'         => $settings['columns'],
			'medium'        => $settings['columns_tablet'],
			'small'         => $settings['columns_mobile'],
			'column_gap'    => $settings['column_gap'],
		];

		if ( 'by_name' === $settings['source'] ) {
			$attributes['categories'] = implode( ',', $settings['post_categories'] );
		}

		$this->add_render_attribute( 'shortcode', $attributes );

		$shortcode = sprintf( '[bdt_blog %s]', $this->get_render_attribute_string( 'shortcode' ) );

		return $shortcode;
	}

	public function render() {
		echo do_shortcode( $this->get_shortcode() );
	}

	public function render_plain_content() {
		echo $this->get_shortcode();
	}
}

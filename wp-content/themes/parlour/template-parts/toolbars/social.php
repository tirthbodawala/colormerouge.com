<?php 

$attrs['class']        = get_theme_mod( 'parlour_toolbar_social_style' ) ? 'uk-icon-button' : 'uk-icon-link';
$attrs['target']       = get_theme_mod( 'parlour_toolbar_social_target' ) ? '_blank' : '';

// Grid
$attrs_grid            = [];
$attrs_grid['class'][] = 'uk-grid-small uk-flex-middle';
$attrs_grid['uk-grid'] = true;

$links = (get_theme_mod( 'parlour_toolbar_social' )) ? explode(',', get_theme_mod( 'parlour_toolbar_social' )) : null;
if (count($links)) : ?>
	<div class="social-link">
		<ul<?php echo parlour_helper::attrs($attrs_grid) ?>>
		    <?php foreach ($links as $link) : ?>
		    <li>
		        <a<?php echo parlour_helper::attrs(['href' => $link], $attrs); ?> uk-icon="icon: <?php echo parlour_helper::icon($link); ?>; ratio: 0.8" title="<?php echo ucfirst(parlour_helper::icon($link)); ?>" uk-tooltip="pos: bottom"></a>
		    </li>
		    <?php endforeach ?>
		</ul>
	</div>
<?php endif; ?>

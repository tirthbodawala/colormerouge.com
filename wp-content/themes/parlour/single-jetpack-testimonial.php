<?php get_header(); 

// Layout
$position = (get_post_meta( get_the_ID(), 'parlour_page_layout', true )) ? get_post_meta( get_the_ID(), 'parlour_page_layout', true ) : get_theme_mod( 'parlour_page_layout', 'sidebar-right' );
$width = '1-3';
?>

<div<?php echo parlour_helper::section(); ?>>
	<div<?php echo parlour_helper::container(); ?>>
		<div<?php echo parlour_helper::grid(); ?>>
			<div class="uk-width-expand">
				<main class="tm-content">
			
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
						<?php get_template_part( 'template-parts/testimonials/entry' ); ?>
						
						
						<?php if(get_theme_mod('parlour_service_next_prev', 1)) { ?>

							<hr>	

							<ul class="uk-pagination">
							    <li>
							    	<?php
							        	$pre_btn_txt = '<span class="uk-margin-small-right" uk-pagination-previous></span> '. esc_html__('Previous', 'parlour'); 
							        	previous_post_link('%link', "{$pre_btn_txt}", FALSE); 
							        ?>
							        
							    </li>
							    <li class="uk-margin-auto-left">
							    	<?php $next_btn_txt = esc_html__('Next', 'parlour') . ' <span class="uk-margin-small-left" uk-pagination-next></span>';
		                    			next_post_link('%link', "{$next_btn_txt}", FALSE); ?>
		                    	</li>
							</ul>
						<?php } ?>
				
					<?php endwhile; endif; ?>
				</main> <!-- end main -->
			</div> <!-- end expand -->

			<?php if($position == 'sidebar-left' || $position == 'sidebar-right' || ) : ?>
				<aside<?php echo parlour_helper::sidebar($position, $width); ?>>
					<?php get_sidebar(); ?>
				</aside> <!-- end aside -->
			<?php endif; ?>
			
		</div> <!-- end grid -->
	</div> <!-- end container -->
</div> <!-- end tm main -->
	
<?php get_footer(); ?>
<article id="post-<?php the_ID() ?>" <?php post_class('uk-article post-format-audio') ?> data-permalink="<?php the_permalink() ?>" typeof="Article">

    <?php get_template_part( 'template-parts/post-format/schema-meta' ); ?>

    <?php 
    $audio = get_post_meta( get_the_ID(), 'parlour_blog_audio', true );
    if (!empty($audio)) : ?>

    <div class="post-audio<?php echo (is_single()) ? ' uk-margin-large-bottom' : ' uk-margin-bottom'; ?>">

        <?php echo wp_kses($audio, parlour_allowed_tags()); ?>

    </div>

    <?php endif ?>
    
    <div class="uk-margin-medium-bottom uk-container uk-container-small uk-text-center">
        <?php get_template_part( 'template-parts/post-format/title' ); ?>

        <?php if(get_theme_mod('parlour_blog_meta', 1)) :?>
        <?php get_template_part( 'template-parts/post-format/meta' ); ?>
        <?php endif; ?>
    </div>
    
    <div class="uk-container uk-container-small">
        <?php get_template_part( 'template-parts/post-format/content' ); ?>

        <?php get_template_part( 'template-parts/post-format/read-more' ); ?>
    </div>

</article>
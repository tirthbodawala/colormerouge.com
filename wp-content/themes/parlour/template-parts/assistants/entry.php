<?php
$author_desc = get_the_author_meta('description');
$monday      = get_post_meta( get_the_ID(), 'bdthemes_assistant_monday', true );
$tuesday     = get_post_meta( get_the_ID(), 'bdthemes_assistant_tuesday', true );
$wednesday   = get_post_meta( get_the_ID(), 'bdthemes_assistant_wednesday', true );
$thursday    = get_post_meta( get_the_ID(), 'bdthemes_assistant_thursday', true );
$friday      = get_post_meta( get_the_ID(), 'bdthemes_assistant_friday', true );
$saturday    = get_post_meta( get_the_ID(), 'bdthemes_assistant_saturday', true );
$sunday      = get_post_meta( get_the_ID(), 'bdthemes_assistant_sunday', true );


?>

<article id="post-<?php the_ID() ?>" <?php post_class('uk-article uk-box-shadow-small') ?> data-permalink="<?php the_permalink() ?>" typeof="Article">

    <?php get_template_part( 'template-parts/assistants/schema-meta' ); ?>
    
    <?php if (!is_single()) : ?>
        <?php get_template_part( 'template-parts/assistants/media' ); ?>    
    <?php endif; ?>

    <div class="uk-card uk-padding-large">

        <?php get_template_part( 'template-parts/assistants/title' ); ?>

        <div class="uk-container uk-container-small">
            <?php get_template_part( 'template-parts/assistants/content' ); ?>
            
            <div class="uk-subnav">
                <?php edit_post_link(esc_html__('Edit this post', 'parlour'), '<div class="uk-padding-remove uk-margin-left uk-button uk-button-text">','</div>'); ?>
                <?php get_template_part( 'template-parts/assistants/read-more' ); ?>
            </div>
        </div>

        <?php if (!empty($monday) or !empty($tuesday) or !empty($wednesday) or !empty($thursday) or !empty($friday) or !empty($saturday) or !empty($sunday)) : ?>
            <h3 class="uk-heading-bullet"><?php esc_html_e('Available at:', 'parlour'); ?></h3>
            
            <table class="uk-table uk-table-divider">
                <thead>
                    <tr>
                        <th>Day</th>
                        <th>Time</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($monday)) : ?>
                    <tr>
                        <td><?php esc_html_e('Monday', 'parlour'); ?></td>
                        <td><?php echo esc_attr($monday); ?></td>
                    </tr>
                    <?php endif; ?>

                    <?php if (!empty($tuesday)) : ?>
                    <tr>
                        <td><?php esc_html_e('Tuesday', 'parlour'); ?></td>
                        <td><?php echo esc_attr($tuesday); ?></td>
                    </tr>
                    <?php endif; ?>

                    <?php if (!empty($wednesday)) : ?>
                    <tr>
                        <td><?php esc_html_e('Wednesday', 'parlour'); ?></td>
                        <td><?php echo esc_attr($wednesday); ?></td>
                    </tr>
                    <?php endif; ?>

                    <?php if (!empty($thursday)) : ?>
                    <tr>
                        <td><?php esc_html_e('Thursday', 'parlour'); ?></td>
                        <td><?php echo esc_attr($thursday); ?></td>
                    </tr>
                    <?php endif; ?>

                    <?php if (!empty($friday)) : ?>
                    <tr>
                        <td><?php esc_html_e('Friday', 'parlour'); ?></td>
                        <td><?php echo esc_attr($friday); ?></td>
                    </tr>
                    <?php endif; ?>

                    <?php if (!empty($saturday)) : ?>
                    <tr>
                        <td><?php esc_html_e('Saturday', 'parlour'); ?></td>
                        <td><?php echo esc_attr($saturday); ?></td>
                    </tr>
                    <?php endif; ?>

                    <?php if (!empty($sunday)) : ?>
                    <tr>
                        <td><?php esc_html_e('Sunday', 'parlour'); ?></td>
                        <td><?php echo esc_attr($sunday); ?></td>
                    </tr>
                    <?php endif; ?>

                </tbody>
            </table>
        <?php else : ?>
            <div class="uk-alert-warning" uk-alert>
                <a class="uk-alert-close" uk-close></a>
                <p><?php esc_html_e('Sorry I am not available at this moment!', 'parlour'); ?></p>
            </div>
        <?php endif; ?>

    </div>
</article>

<?php if(is_single() and empty($author_desc)) : ?>
    <div class="uk-margin-large-top"></div>
<?php endif ?>
<?php
// Bdthemes progress pie shortcode

if (!function_exists('bdthemes_progress_pie')) {
	function bdthemes_progress_pie($atts) {
		$atts = shortcode_atts(array(
			'id'                => uniqid('bdtpp'),
			'percent'           => 75,
			'before'            => '',
			'text'              => '',
			'after'             => '',
			'title'             => 'Pie Title',
			'step'              => 1,
			'line_width'        => 8,
			'text_size'         => '',
			'background'        => 'default',
			'custom_background' => '',
			'text_color'        => 'dark',
			'padding'           => 'medium',
			'custom_padding'    => '',
			'shadow'            => 'small',
			'hover_shadow'      => 'none',
			'border'            => '',
			'radius'            => 'no',
			'align'             => 'center',
			'line_cap'          => 'round', //butt/square/round
			'dash_array'        => '', //5,5/20,10,5,5,5,10/10,10
			'duration'          => 1,
			'delay'             => 1,
			'css_animation'     => '',
			'class'             => ''
		), $atts, 'bdt_progress_pie');

		
		$id                = $atts['id'];
		$classes           = array('bdt-progress-pie-wrapper', 'uk-'.$atts['text_color'], $atts['class']);
		$css_class         = array('bdt-progress-pie', 'bdt-pp-align-' . $atts['align'], 'bdt-pp-lc-'.$atts['line_cap']);
		$js                = array();
		$output            = array();
		
		$custom_background = ($atts['custom_background']) ? 'background:' . $atts['custom_background']. ';' : '';
		$custom_padding    = ($atts['custom_padding']) ? 'padding:' . $atts['custom_padding']. ';' : '';
		$border            = ($atts['border']) ? 'border:' . $atts['border']. ';' : '';
		$classes[]         = ($atts['radius'] == 'yes') ? 'uk-border-rounded' : '';
		$text_size         = ($atts['text_size']) ? 'font-size:' . $atts['text_size']. ';' : '';

		if (!$atts['text']) {
		    $css_class[] = 'bdt-pp-percent';
		}
		if ($atts['before'] !== '') {
		    $atts['before'] = '<div class="bdt-progress-pie-before"> '.$atts['before'].'</div>';
		}
		if ($atts['text'] !== '') {
		    $atts['text'] = '<div class="bdt-progress-pie-text" style="'.$text_size.'"> '.$atts['text'].'</div>';
		}
		if ($atts['after'] !== '') {
		    $atts['after'] = '<div class="bdt-progress-pie-after"> '.$atts['after'].'</div>';
		}
		if ($atts['title'] !== '') {
		    $atts['title'] = '<h4 class="bdt-progress-pie-after-title"> '.$atts['title'].'</h4>';
		}
		if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $classes[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
		if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $classes[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if (($atts['padding'] !=='custom') and ($atts['padding'] !=='none')) {
            $css_class[] = ($atts['padding'] =='medium') ? 'uk-padding' : 'uk-padding-'.$atts['padding'];
        }
        if ($atts['shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-'.$atts['shadow'];
        }
        if ($atts['hover_shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$atts['hover_shadow'];
        } 

		$output[] = '<div id="'.$id.'_container" class="'.bdt_acssc($classes).'" style="'.$custom_background.$custom_padding.$border.'">';
			$output[] = '<div id="'.$id.'" class="'.bdt_acssc($css_class).'" role="progressbar" data-goal="'.$atts['percent'].'" aria-valuemin="0" data-step="'.$atts['step'].'" data-speed="'.($atts['duration']*15).'" data-delay="'.($atts['delay']*1000).'" data-barsize="'.intval($atts['line_width']).'" aria-valuemax="100">';
			    $output[] = '<div class="bdt-progress-pie-label">';
			        $output[] = $atts['before'];
			        if ($atts['text']) {
			            $output[] = $atts['text'];
			        } else {
			            $output[] = '<div class="bdt-progress-pie-number" style="'.$text_size.'"></div>';
			        }
			        $output[] = $atts['after'];
			    $output[] ='</div>';
			$output[] = '</div>';
			$output[] = $atts['title'];
		$output[] = '</div>';

		$output[] = '
			<script>
			jQuery(document).ready(function($) {
	    		"use strict";
			    //$(".bdt-progress-pie").each(function () {
			        var progressPie = "#'.$id.'";

			        $(progressPie).asPieProgress({
			            namespace: "pieProgress",
			            classes: {
			                svg: "bdt-progress-pie-svg",
			                number: "bdt-progress-pie-number",
			                content: "bdt-progress-pie-content"
			            }
			        })
			        $(progressPie).asPieProgress("start");
			    //});
			});
			</script>
			';


		return implode("\n", $output);
	}
	add_shortcode('bdt_progress_pie', 'bdthemes_progress_pie');
}
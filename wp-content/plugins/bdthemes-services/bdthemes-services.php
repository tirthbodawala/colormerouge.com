<?php
/*
Plugin Name: BdThemes Services
Plugin URI: http://themeforest.net/user/bdthemes
Description: This plugin will create a services custom post type for bdthemes wordpress theme.
Version: 1.0.0
Author: bdthemes
Author URI: http://bdthemes.com
License: Custom
License URI: http://themeforest.net/licenses 
*/

/* ----------------------------------------------------- */
/* Add Services Custom Post Type
/* ----------------------------------------------------- */
function bdthemes_services_register() {  

	$services_slug = get_theme_mod('bdt_services_slug');

	if(isset($services_slug) && $services_slug != ''){
		$services_slug = $services_slug;
	} else {
		$services_slug = 'service';
	}
	
	$labels = array(
		'name'               => esc_html__( 'Services', 'bdthemes-services' ),
		'singular_name'      => esc_html__( 'Service', 'bdthemes-services' ),
		'add_new'            => esc_html__( 'Add New Item', 'bdthemes-services' ),
		'add_new_item'       => esc_html__( 'Add New Service', 'bdthemes-services' ),
		'edit_item'          => esc_html__( 'Edit Service', 'bdthemes-services' ),
		'new_item'           => esc_html__( 'Add New Service', 'bdthemes-services' ),
		'view_item'          => esc_html__( 'View Item', 'bdthemes-services' ),
		'search_items'       => esc_html__( 'Search Services', 'bdthemes-services' ),
		'not_found'          => esc_html__( 'No service found', 'bdthemes-services' ),
		'not_found_in_trash' => esc_html__( 'No service found in trash', 'bdthemes-services' )
	);
	
    $args = array(  
		'labels'          => $labels,
		'public'          => true,  
		'show_ui'         => true,  
		'capability_type' => 'post',  
		'hierarchical'    => false,  
		'menu_icon'       => 'dashicons-portfolio',
		'rewrite'         => array('slug' => $services_slug), // Permalinks format
		'supports'        => array('title', 'editor', 'thumbnail', 'comments', 'excerpt')  
       );  
  
    register_post_type( 'services' , $args );  
}
add_action('init', 'bdthemes_services_register', 1);   

/* ----------------------------------------------------- */
/* Register Taxonomy
/* ----------------------------------------------------- */
function bdthemes_services_taxonomy() {
	
	register_taxonomy("service-categories", array("services"), array("hierarchical" => true, "label" => "Service Categories", "singular_label" => "Service Category", "rewrite" => true));

}
add_action('init', 'bdthemes_services_taxonomy', 1);   

function bdthemes_services_edit_columns( $services_columns ) {
	$services_columns = array(
		"cb"               => "<input type=\"checkbox\" />",
		"title"            => esc_html__('Title', 'bdthemes-services'),
		"thumbnail"        => esc_html__('Thumbnail', 'bdthemes-services'),
		"service-categories" => esc_html__('Category', 'bdthemes-services'),
		"author"           => esc_html__('Author', 'bdthemes-services'),
		"comments"         => esc_html__('Comments', 'bdthemes-services'),
		"date"             => esc_html__('Date', 'bdthemes-services'),
	);
	$services_columns['comments'] = '<div class="vers"><img alt="Comments" src="' . esc_url( admin_url( 'images/comment-grey-bubble.png' ) ) . '" /></div>';
	return $services_columns;
}
add_filter( 'manage_edit-services_columns', 'bdthemes_services_edit_columns' );

/* ----------------------------------------------------- */

function bdthemes_services_column_display( $services_columns, $post_id ) {
	
	switch ( $services_columns ) {
		
		//Display the thumbnail in the column view
		case "thumbnail":
			$width = (int) 80;
			$height = (int) 80;
			$thumbnail_id = get_post_meta( $post_id, '_thumbnail_id', true );
			
			// Display the featured image in the column view if possible
			if ( $thumbnail_id ) {
				$thumb = wp_get_attachment_image( $thumbnail_id, array($width, $height), true );
			}
			if ( isset( $thumb ) ) {
				echo $thumb; // No need to escape
			} else {
				echo esc_html__('None', 'bdthemes-services');
			}
		break;	
			
		// Display the services tags in the column view
		case "service-categories":
		
		if ( $category_list = get_the_term_list( $post_id, 'service-categories', '', ', ', '' ) ) {
			echo $category_list; // No need to escape
		} else {
			echo esc_html__('None', 'bdthemes-services');
		}
		break;			
	}
}
add_action( 'manage_services_posts_custom_column', 'bdthemes_services_column_display', 10, 2 );
<?php
namespace ElementorExtend\Modules\Posts\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Faq extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'faq';
	}

	public function get_title() {
		return __( 'Faq', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-toggle';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);


		$this->add_control(
			'source',
			[
				'label' => _x( 'Source', 'Posts Query Control', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Show All', 'parlour-core' ),
					'by_name' => __( 'Manual Selection', 'parlour-core' ),
				],
				'label_block' => true,
			]
		);

		$categories = get_terms( 'faq_filter' );

		$options = [];
		foreach ( $categories as $category ) {
			$options[ $category->slug ] = $category->name;
		}

		$this->add_control(
			'categories',
			[
				'label'       => __( 'Categories', 'parlour-core' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => $options,
				'default'     => [],
				'label_block' => true,
				'multiple'    => true,
				'condition'   => [
					'source'    => 'by_name',
				],
			]
		);

		$this->add_control(
			'posts',
			[
				'label' => __( 'Posts Limit', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 10,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => __( 'Order by', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'date',
				'options' => [
					'date'     => __( 'Date', 'parlour-core' ),
					'title'    => __( 'Title', 'parlour-core' ),
					'category' => __( 'Category', 'parlour-core' ),
					'rand'     => __( 'Random', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label'   => __( 'Order', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'DESC',
				'options' => [
					'DESC' => __( 'Descending', 'parlour-core' ),
					'ASC'  => __( 'Ascending', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'faq_icon',
			[
				'label'     => __( 'Faq Icon', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'no',
			]
		);

		$this->add_control(
			'read_more',
			[
				'label'     => __( 'Read More', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'no',
			]
		);		
		
		$this->end_controls_section();

	}

	private function get_shortcode() {
		$settings = $this->get_settings();
		$categories = '';

		if ($settings['source'] === 'by_name') {
			$categories = implode(",", $settings['categories']);
		} else {
			$categories = 'all';
		}

		$attributes = [
			'categories' => $categories,
			'limit'      => $settings['posts'],
			'orderby'    => $settings['orderby'],
			'order'      => $settings['order'],
			'faq_icon'   => $settings['faq_icon'],
			'read_more'  => $settings['read_more'],
		];

		$this->add_render_attribute( 'shortcode', $attributes );

		$shortcode = [];
		$shortcode[] = sprintf( '[bdt_faq %s]', $this->get_render_attribute_string( 'shortcode' ) );

		return implode("", $shortcode);
	}

	public function render() {
		echo do_shortcode( $this->get_shortcode() );
	}

	public function render_plain_content() {
		echo $this->get_shortcode();
	}
}

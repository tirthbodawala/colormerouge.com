<?php
namespace ElementorExtend\Modules\Carousel\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class Service Carousel
 */
class Service_Carousel extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'service-carousel';
	}

	public function get_title() {
		return esc_html__( 'Service Carousel', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-slider-push';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->register_query_section_controls();
	}

	private function register_query_section_controls() {
		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => esc_html__( 'Layout', 'parlour-core' ),
			]
		);

		$this->add_responsive_control(
			'columns',
			[
				'label' => esc_html__( 'Columns', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'item_gap',
			[
				'label' => __( 'Item Gap', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 10,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'posts_per_page',
			[
				'label' => esc_html__( 'Limit', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 6,
			]
		);

		$this->add_control(
			'show_title',
			[
				'label' => esc_html__( 'Show Title', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_off' => esc_html__( 'Off', 'parlour-core' ),
				'label_on' => esc_html__( 'On', 'parlour-core' ),
			]
		);

		$this->add_control(
			'show_meta',
			[
				'label' => esc_html__( 'Show Meta', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_off' => esc_html__( 'Off', 'parlour-core' ),
				'label_on' => esc_html__( 'On', 'parlour-core' ),
			]
		);

		$this->add_control(
			'show_order_link',
			[
				'label'        => esc_html__( 'Order Link Button', 'parlour-core' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Show', 'parlour-core' ),
				'label_off'    => esc_html__( 'Hide', 'parlour-core' ),
				'return_value' => 'yes',
				'default'      => 'yes',
			]
		);

		$this->add_control(
			'show_read_more',
			[
				'label'        => esc_html__( 'Read More Button', 'parlour-core' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => esc_html__( 'Show', 'parlour-core' ),
				'label_off'    => esc_html__( 'Hide', 'parlour-core' ),
				'return_value' => 'yes',
				'default'      => 'yes',
			]
		);

		$this->add_control(
			'navigation',
			[
				'label'   => __( 'Navigation', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'both',
				'options' => [
					'both'   => __( 'Arrows and Dots', 'parlour-core' ),
					'arrows' => __( 'Arrows', 'parlour-core' ),
					'dots'   => __( 'Dots', 'parlour-core' ),
					'none'   => __( 'None', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_query',
			[
				'label' => esc_html__( 'Query', 'parlour-core' ),
			]
		);

		$this->add_control(
			'source',
			[
				'label' => _x( 'Source', 'Posts Query Control', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => esc_html__( 'Show All', 'parlour-core' ),
					'by_name' => esc_html__( 'Manual Selection', 'parlour-core' ),
				],
				'label_block' => true,
			]
		);

		$categories = get_terms( 'service-categories' );

		$options = [];
		foreach ( $categories as $category ) :
			$options[ $category->slug ] = $category->name;
		endforeach;

		$this->add_control(
			'categories',
			[
				'label' => esc_html__( 'Categories', 'parlour-core' ),
				'type' => Controls_Manager::SELECT2,
				'options' => $options,
				'default' => [],
				'label_block' => true,
				'multiple' => true,
				'condition' => [
					'source' => 'by_name',
				],
			]
		);

		$this->add_control(
			'hide_empty',
			[
				'label' => esc_html__( 'Hide Empty', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => 'Hide',
				'label_off' => 'Show',
				'return_value' => 'yes',
			]
		);

		$this->add_control(
			'advanced',
			[
				'label' => esc_html__( 'Advanced', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => esc_html__( 'Order By', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'post_date',
				'options' => [
					'post_date'  => esc_html__( 'Date', 'parlour-core' ),
					'post_title' => esc_html__( 'Title', 'parlour-core' ),
					'menu_order' => esc_html__( 'Menu Order', 'parlour-core' ),
					'rand'       => esc_html__( 'Random', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label' => esc_html__( 'Order', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'desc',
				'options' => [
					'asc' => esc_html__( 'ASC', 'parlour-core' ),
					'desc' => esc_html__( 'DESC', 'parlour-core' ),
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'parlour-core' ),
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label' => __( 'Autoplay', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'parlour-core' ),
					'no' => __( 'No', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => __( 'Autoplay Speed', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
				'frontend_available' => true,
				'condition' => [
					'autoplay' => 'yes',
				],
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'   => __( 'Infinite Loop', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'parlour-core' ),
					'no'  => __( 'No', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'speed',
			[
				'label' => __( 'Animation Speed', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 500,
				'frontend_available' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_items',
			[
				'label' => esc_html__( 'Items', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'item_background',
			[
				'label'     => esc_html__( 'Background', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel .bdt-service-content-wrapper' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .bdt-service-carousel .bdt-service-img-wrapper:before' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .bdt-service-carousel .bdt-service-img-wrapper:after' => 'background-color: {{VALUE}};',

					'{{WRAPPER}} .bdt-service-carousel .uk-overlay-gradient' => 'background: -moz-linear-gradient(top, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%); background: -webkit-linear-gradient(top, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%); background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%);',
				],
			]
		);

		$this->add_responsive_control(
			'border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'elementor' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .bdt-service-carousel .bdt-service-content-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
					'{{WRAPPER}} .bdt-service-carousel .bdt-service-img .bdt-service-img-design' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} 0 0;',


				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'        => 'border',
				'label'       => esc_html__( 'Border Color', 'parlour-core' ),
				'placeholder' => '1px',
				'default'     => '1px',
				'selector'    => '{{WRAPPER}} .bdt-service-carousel .bdt-service-content-wrapper',
				'condition'   => [
					'border_radius' => true,
				],
			]
		);
		
		$this->add_control(
			'item_hover_border_color',
			[
				'label' => esc_html__( 'Hover Border Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'border_border!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel .bdt-service-content-wrapper:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'item_box_shadow',
				'selector' => '{{WRAPPER}} .bdt-service-carousel .bdt-service-content-wrapper',
			]
		);

		$this->add_responsive_control(
			'desc_padding',
			[
				'label' => esc_html__( 'Description Padding', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel .bdt-service-desc' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'alignment',
			[
				'label'       => esc_html__( 'Alignment', 'parlour-core' ),
				'type'        => Controls_Manager::CHOOSE,
				'separator' => 'before',
				'label_block' => false,
				'options'     => [
					'left'        => [
						'title'       => esc_html__( 'Left', 'parlour-core' ),
						'icon'        => 'fa fa-align-left',
					],
				'center'      => [
					'title'       => esc_html__( 'Center', 'parlour-core' ),
					'icon'        => 'fa fa-align-center',
					],
				'right'       => [
					'title'       => esc_html__( 'Right', 'parlour-core' ),
					'icon'        => 'fa fa-align-right',
					],
				],
				'default'     => 'center',
				'separator' => 'before',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_title',
			[
				'label'     => esc_html__( 'Title', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_title' => 'yes',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => esc_html__( 'Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel .bdt-service-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-service-carousel .bdt-service-title',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_meta',
			[
				'label'     => esc_html__( 'Meta', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_meta' => 'yes',
				],
			]
		);

		$this->add_control(
			'meta_color',
			[
				'label' => esc_html__( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel .bdt-service-meta li span' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'meta_separator_color',
			[
				'label' => esc_html__( 'Separator Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel  .bdt-service-meta li:not(:last-child)::after' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'meta_typography',
				'label' => esc_html__( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-service-carousel .bdt-service-meta li span',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_icon',
			[
				'label' => esc_html__( 'Icon', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_order_link, show_read_more' => 'yes',
				],
			]
		);

		$this->add_control(
			'icon_background',
			[
				'label' => esc_html__( 'Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel .bdt-service-icon' => 'background: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel .bdt-service-icon svg' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_navigation',
			[
				'label' => __( 'Navigation', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'navigation' => [ 'arrows', 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_arrows',
			[
				'label' => __( 'Arrows', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_style',
			[
				'label' => __( 'Arrows Style', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'dark',
				'options' => [
					'dark' => __( 'Dark', 'parlour-core' ),
					'light' => __( 'Light', 'parlour-core' ),
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_position',
			[
				'label' => __( 'Arrows Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -100,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel .swiper-button-next' => 'right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .bdt-service-carousel .swiper-button-prev' => 'left: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_size',
			[
				'label' => __( 'Arrows Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 25,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel .swiper-button-prev, {{WRAPPER}} .bdt-service-carousel .swiper-button-next' => 'height: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}}',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_dots',
			[
				'label' => __( 'Dots', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_position',
			[
				'label' => __( 'Dots Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -80,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel .swiper-pagination-bullets' => 'bottom: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_size',
			[
				'label' => __( 'Dots Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 5,
						'max' => 10,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-service-carousel .swiper-pagination-bullet' => 'height: {{SIZE}}{{UNIT}};width: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->end_controls_section();
	}

	public function render() {
		$settings  = $this->get_settings();

		global $post;
		$id         = $this->get_id();

		$args = array(
			'post_type'      => 'services',
			'posts_per_page' => ($settings['posts_per_page']),
			'orderby'        => $settings['orderby'],
			'order'          => $settings['order'],
			'post_status'    => 'publish',
		);
			
		if('by_name' === $settings['source']) :
			$args['tax_query'][] = array(
				'taxonomy' => 'service-categories',
				'field'    => 'slug',
				'terms'    => $settings['categories'],
			);
		endif;

		$wp_query = new \WP_Query($args);

		if( $wp_query->have_posts() ) : ?>
			<div id="<?php echo $id; ?>" class="bdt-service-carousel">
				<div class="swiper-container">
					<div class="swiper-wrapper">
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
						<div class="bdt-service-item swiper-slide bdt-service-align-<?php echo $settings['alignment']; ?>">
							<div class="bdt-service-content-wrapper uk-background-default uk-box-shadow-small">
						    	<?php if (has_post_thumbnail()) : ?>
							       <?php $order_link   = get_post_meta( get_the_ID(), 'bdthemes_service_order_link', true ); ?>

							        <div class="bdt-service-img-wrapper uk-position-relative">
							            <div class="bdt-service-img-design">
							                <div class="bdt-service-img">
									            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
									                <?php echo  the_post_thumbnail('medium', array('class' => ''));  ?>
									            </a>
										    </div>
							                <div class="bdt-service-overlay uk-position-cover uk-overlay-gradient uk-position-z-index"></div>
							            </div>  
							        </div>
							    <?php endif; ?>

							    <?php if(( 'yes' == $settings['show_title'] ) || ( 'yes' == $settings['show_meta'] )) : ?>
							        <div class="bdt-service-desc uk-padding uk-position-relative">

							            <?php if( 'yes' == $settings['show_title'] ) : ?>
							                <h3 class="bdt-service-title uk-text-<?php echo $settings['alignment']; ?> uk-margin-remove-top uk-margin-small-bottom">
											    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="uk-link-reset"><?php the_title(); ?></a>
											</h3>
							            <?php endif; ?>

							            <?php if( 'yes' == $settings['show_meta']) : ?>
							                <?php $duration = get_post_meta( get_the_ID(), 'bdthemes_service_duration', true ); ?>
							                <?php $charge   = get_post_meta( get_the_ID(), 'bdthemes_service_price', true ); ?>
							                
							                <ul class="bdt-service-meta uk-subnav uk-flex-<?php echo $settings['alignment']; ?> uk-margin-small-top uk-margin-remove-bottom" uk-margin>
							                    <li><span><?php echo esc_html('Duration: ', 'parlour').esc_html($duration); ?></span></li>
							                    <li><span><?php echo esc_html('Charge: ', 'parlour').esc_html($charge); ?></span></li>
							                </ul>
							            <?php endif; ?>
							        </div>
							    <?php endif; ?>

						        <?php if( 'yes' == $settings['show_order_link'] ) : ?>
						            <a href="<?php echo esc_url($order_link); ?>" class="bdt-service-icon bdt-service-cart-link uk-icon-button uk-margin-small-right" title="<?php echo esc_html('Order Service', 'parlour'); ?>"><span uk-icon="icon: cart"></span></a>
						        <?php endif; ?>

						        <?php if( 'yes' == $settings['show_read_more'] ) : ?>
						        	<a href="<?php echo esc_url(get_permalink()); ?>" class="bdt-service-icon bdt-service-read-more uk-icon-button" title="<?php echo esc_attr(get_the_title()); ?>"><span uk-icon="icon: arrow-right"></span></a>
								<?php endif; ?>
							</div>
						</div>
					<?php endwhile; ?>					
					<?php wp_reset_postdata(); ?> 
					</div>
				</div>

		        <?php if ( 'none' !== $settings['navigation'] ) : ?>
					<?php if ( 'arrows' !== $settings['navigation'] ) : ?>
						<div class="swiper-pagination"></div>
					<?php endif; ?>
					
					<?php if ( 'dots' !== $settings['navigation'] ) : 
						$nav_style = ($settings['arrows_style'] == 'light') ? 'swiper-button-white' : 'swiper-button-black'; 
					?>
						<div class="swiper-button-next <?php echo esc_attr($nav_style); ?>"></div>
						<div class="swiper-button-prev <?php echo esc_attr($nav_style); ?>"></div>
					<?php endif; ?>
				<?php endif; ?>
			    
			</div>

			<script>
				jQuery(document).ready(function($) {
				    "use strict";				    
				    var swiper = new Swiper(".elementor-element-<?php echo $id; ?> .swiper-container", {
				        navigation: {
							nextEl: ".elementor-element-<?php echo $id; ?> .swiper-button-nextt",
							prevEl: ".elementor-element-<?php echo $id; ?> .swiper-button-prev",
						},
						pagination: {
							el        : ".elementor-element-<?php echo $id; ?> .swiper-pagination",
							type      : 'bullets',
							clickable : true,
						},
				        "autoplay"      : <?php echo 'yes' === $settings['autoplay'] ? '{ "delay": ' . $settings['autoplay_speed'] . ' }' : 'false'; ?>,
				        "paginationClickable":true,
				        "loop": <?php echo ($this->get_settings('infinite') == 'yes') ? 'true' : 'false'; ?>,
				        "speed": <?php echo $this->get_settings('speed'); ?>,
				        "slidesPerView": <?php echo $this->get_settings('columns'); ?>,
				        "spaceBetween":  <?php echo $this->get_settings('item_gap')['size']; ?>,
				        "breakpoints" : {
				            "1024" : {
				            	"slidesPerView": <?php echo $this->get_settings('columns'); ?>,
				            	"spaceBetween": <?php echo $this->get_settings('item_gap')['size']; ?>,
				            },
				            "768" : {
				            	"slidesPerView": <?php echo $this->get_settings('columns_tablet'); ?>,
				            	"spaceBetween": <?php echo $this->get_settings('item_gap')['size']; ?>,
				            },
				            "640" : {
				            	"slidesPerView": <?php echo $this->get_settings('columns_mobile'); ?>,
				            	"spaceBetween": <?php echo $this->get_settings('item_gap')['size']; ?>,
				            }
				        }
				    });
				});
			</script>
		<?php endif;
	}
}

<?php
// Bdthemes Post Card

if (!function_exists('bdthemes_post_card')) {
	function bdthemes_post_card($atts){
		extract(shortcode_atts(array(
			'post_type'          => 'post',
			'posts'              => '3',
			'categories'         => 'all',
			'service_categories' => 'all',
			'background'         => 'default',
			'custom_background'  => '',
			'color'              => 'dark',
			'padding'            => 'medium',
			'custom_padding'     => '',
			'orderby'            => 'date',
			'order'              => 'DESC',
			'thumb'              => 'yes',
			'tags'               => 'yes',
			'title'              => 'yes',
			'meta'               => 'yes',
			'excerpt'            => 'yes',
			'excerpt_limit'      => 15,
			'read_more'          => 'yes',
			'css_animation'      => '',
			'class'              => ''
		), $atts));

		

		global $post;
		$media     = '';
		$id        = uniqid('bdtpc_');
		$classes   = ['bdt-post-card', 'uk-grid-collapse', 'uk-child-width-1-1@s', 'uk-child-width-1-3@m', $class];
		$css_class = ['bdt-pc-desc'];
		$output    = [];

		if ($post_type == 'services') {
			$taxonomy = 'service-categories';
			$categories = $service_categories;
		} else {
			$taxonomy = 'category';
		}


		$custom_background = ($custom_background) ? 'background:' . $custom_background. ';' : '';
		$custom_padding    = ($custom_padding) ? 'padding:' . $custom_padding. ';' : '';

		if (($background !=='custom') and ($background !=='none')) {
            $css_class[] = ($background) ? 'uk-background-'.$background : '';
        }
        if ($color) {
            $css_class[] = 'uk-'.$color;
        }
        if (($padding !=='custom') and ($padding !=='none')) {
            $css_class[] = ($padding =='medium') ? 'uk-padding' : 'uk-padding-'.$padding;
        }

		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => $posts,
			'orderby'        => $orderby,
			'order'          => $order,
			'post_status'    => 'publish'
		);
		
		if($categories != 'all' && $categories != ''){
			$str = $categories;
			$arr = explode(',', $str); // string to array

			$args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		$wp_query = new WP_Query($args);

		if( $wp_query->have_posts() ) : 

			$output[] = '<div class="'.bdt_acssc($classes).'" uk-grid>';
		
			while ( $wp_query->have_posts() ) : $wp_query->the_post();

				if( $thumb == 'yes') {
			  		$blog_thumbnail= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

			  		if($blog_thumbnail[0] != ''){
			  
			  			$media = '<a href="'.esc_url(get_permalink()).'" title="' . esc_attr(get_the_title()) . '" class="bdt-pc-thumb"><img src="'.esc_url($blog_thumbnail[0]).'" alt="' . esc_attr(get_the_title()) . '" /></a>';
			  		
			  		}
			  	}

		  		$output[] = '<div>';
			  		$output[] = '<div class="bdt-post-card-item">';
				  
						$output[] = $media;
				  		
				  		$output[] = '<div class="'.bdt_acssc($css_class).'" style="'.$custom_background.$custom_padding.'">';

				  			if ( $tags == 'yes' ) {
					  			$tags_list = get_the_tag_list( '<span>', '</span> <span>', '</span>');

				                if ( $tags_list ) {
				                    $output[] = '<p class="bdt-pc-tag uk-margin-small-bottom" >'. wp_kses_post($tags_list) . '</p>'; // WPCS: XSS OK.
				                }
			            	}

			            	if ( $title == 'yes' ) {
								$output[] = '<h4 class="uk-margin-remove-vertical"><a href="'.esc_url(get_permalink()).'" class="uk-link-reset" title="' . esc_attr(get_the_title()) . '">'.esc_html(get_the_title()) .'</a></h4>';
							}

							if ( $meta == 'yes' ) {
								if ($post_type == 'services') {
									$duration = get_post_meta( get_the_ID(), 'bdthemes_service_duration', true );
									$charge   = get_post_meta( get_the_ID(), 'bdthemes_service_price', true );

									$meta_list = '<li><span>'.esc_html('DURATION: ', 'parlour-core').$duration.'</span></li>
									<li><span>'.esc_html('CHARGE: ', 'parlour-core').$charge.'</span></li>';
												
								} else {
									$meta_list = '<li>'.esc_attr(get_the_date('M d, Y')).'</li><li>
												'.get_the_category_list(', ').'</li>';
								}
								
								$output[] = '<ul class="bdt-pc-meta uk-subnav uk-margin-remove-top uk-margin-small-bottom uk-text-bold uk-text-muted uk-text-uppercase uk-link-reset" uk-margin>'.$meta_list.'</ul>';
							}

							if ( $excerpt == 'yes' ) {
								$output[] = '<div>'.wp_kses_post(parlour_core_custom_excerpt($excerpt_limit)).'</div>';
							}

							if ( $read_more == 'yes') {
								$output[] = '<a href="'.esc_url(get_permalink()).'" class="uk-button uk-padding-remove-horizontal uk-margin-top uk-margin-small-right uk-text-bold">'.esc_html('Read More', 'parlour-core').'<span uk-icon="icon: chevron-right"></span></a>';
							}	
				  		$output[] = '</div>';

					$output[] = '</div>';
				$output[] = '</div>';
		  
			endwhile;
		
			$output[] ='</div>';
		
		 	wp_reset_postdata();

		 endif;

		return implode("\n", $output);
	}
	add_shortcode('bdt_post_card', 'bdthemes_post_card');
}
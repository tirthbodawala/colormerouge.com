<?php
// Bdthemes services

if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); } // load is_plugin_active() function if no available

if(is_plugin_active('bdthemes-services/bdthemes-services.php')) { 
	if (!function_exists('bdthemes_services')) {
		function bdthemes_services($atts){
			extract(shortcode_atts(array( 
				'limit'             => '8',
				'orderby'           => 'date',
				'order'             => 'DESC',
				'categories'        => 'all',
				'filter'            => 'yes',
				'filter_align'      => 'center',
				'large'             => 4,
				'medium'            => 3,
				'small'             => 1,
				'column_gap'        => 'medium',
				'class'             => '',
			), $atts));
			
			global $post;
			
			$id         = uniqid('bdtsg_');
			
			$large      = ($large) ? 'uk-width-1-'.$large.'@l' : '';
			$medium     = ($medium) ? 'uk-width-1-'.$medium.'@m' : '';
			$small      = ($small) ? 'uk-width-1-'.$small.'@s' : '';
			$responsive = [$small, $medium, $large];


			$args = array(
				'post_type'      => 'services',
				'posts_per_page' => intval($limit),
				'orderby'        => $orderby,
				'order'          => $order,
				'post_status'    => 'publish',
			);
				
			if($categories != 'all' && $categories != '') {
				// string to array
				$str = $categories;
				$arr = explode(',', $str);
				  
				$args['tax_query'][] = array(
					'taxonomy' => 'service-categories',
					'field'    => 'slug',
					'terms'    => $arr
				);
			}

			$wp_query = new WP_Query($args);

			ob_start(); // start buffer

			if( $wp_query->have_posts() ) { 


				if($filter == 'yes'){ 

					// Get Filters from Shortcode Options
					if($categories != '' && $categories != 'all') {
						$service_categories = explode(',', $categories);
					} else {
						$service_categories = get_terms('service-categories');
						$arraytostring = '';
						foreach($service_categories as $service_categories){
							$arraytostring .= $service_categories->slug . ',';
						}
						$arraytostring = rtrim($arraytostring,','); // Remove last commata;
						$service_categories = explode(',', $arraytostring); // Create array
					} 
				?>

				<div class="bdt-filter-wrapper">
					<ul id="<?php echo $id; ?>-filter" class="bdt-services-filter uk-flex-<?php echo esc_attr($filter_align);?> uk-subnav uk-subnav-divider uk-margin-large-bottom" >
						<?php if($service_categories): ?>
								<li>
									<a data-filter="*" class="bdt-filter-item-active bdt-filter-item" href="javascript:;">
										<?php esc_html_e('All Service', 'parlour-core'); ?>
									</a>
								</li>	
							<?php foreach($service_categories as $service_categories => $value) { ?>
								<?php $filter_name = get_term_by('slug',$value,'service-categories'); ?>
								<li>
									<a class="bdt-filter-item" data-filter=".sci-<?php echo esc_attr(trim($value)); ?>" href="javascript:;">
										<?php echo esc_html($filter_name->name); ?>
									</a>
								</li>
							<?php } ?>
						<?php endif; ?>
					</ul>
				</div>

				<?php } //end if filter ?>
				


				<div id="<?php echo $id; ?>" class="bdt-service-container uk-grid-<?php echo $column_gap; ?>" uk-grid>
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
						<?php $terms = get_the_terms( get_the_ID(), 'service-categories' ); ?>
						<div class="bdt-service-item <?php echo implode(" ", $responsive);  ?> <?php if($terms and $filter == 'yes') : foreach ($terms as $term) { echo 'sci-'.esc_attr($term->slug).' '; } endif; ?>">
							<?php get_template_part( 'template-parts/services/category/entry' ); ?>
						</div>
					<?php endwhile; ?>					
				</div>
				
				<?php if ($filter == 'yes') : ?>
					<script>
						jQuery(document).ready(function($) {
						    'use strict';
						    var $grid = $('#<?php echo $id; ?>').isotope({
							  itemSelector: '.bdt-service-item',
							  percentPosition: true,
							  layoutMode: 'masonry',
							});

							$('#<?php echo $id; ?>-filter').on( 'click', 'a', function() {
							  var filterValue = $(this).attr('data-filter');
							  $grid.isotope({ filter: filterValue });
							});

							$('#<?php echo $id; ?>-filter > li > .bdt-filter-item').click(function(){
							    $('#<?php echo $id; ?>-filter > li > .bdt-filter-item').removeClass('bdt-filter-item-active');
							    $(this).addClass('bdt-filter-item-active');
							});


						});
					</script>
				<?php endif; ?>
				
				<?php wp_reset_postdata(); 
			};

			$services_output = ob_get_contents(); // get buffered content

			ob_end_clean(); // clean buffer



			return $services_output;
		}
		add_shortcode('bdt_services', 'bdthemes_services');
	}
}
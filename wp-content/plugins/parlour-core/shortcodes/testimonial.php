<?php
// BdThemes testimonial Shortcode

if (!function_exists('bdthemes_testimonial_shortcode')) {
    function bdthemes_testimonial_shortcode($atts = null, $content = null) {
        $atts = shortcode_atts(array(
            'style'        => '1',
            'name'         => 'Jhon Doe',
            'title'        => '',
            'photo'        => '',
            'company'      => '',
            'url'          => '',
            'target'       => 'blank',
            'italic'       => 'no',
            'thumb_radius' => 'circle',
            'class'        => '',
        ), $atts, 'bdt_testimonial');

        $id      = uniqid('sutm');
        $cite    = '';
        $title   = '';
        $name    = '';
        $company = '';
        $photo   = '';



        $italic       = ($atts['italic'] == 'yes') ? 'bdt-testimonial-italic' : '';
        $thumb_radius = ($atts['thumb_radius'] !='none') ? 'uk-border-'.$atts['thumb_radius'] : '';

        // Get Photo form the wordpress with checking
        if ($atts['photo'] != '') {
            if (strpos($atts['photo'], 'http://') !== false || strpos($atts['photo'], 'https://') !== false) {

                $multi_photo = array();
                $multi_photo = explode(',',$atts['photo'], 2);
                $photo = '<img src="' . ($multi_photo[0]) . '" alt="" class="'.$thumb_radius.'" />';

                if(isset($multi_photo[1]) )
                    $photo .= '<img src="' . ($multi_photo[1]) . '" alt="" class="'.$thumb_radius.'" />';

            } else {
                $post = get_post( $atts['photo'] );
                $caption = $post->post_excerpt;

                $post_image = wp_get_attachment_image_src( $atts['photo'], 'large' );
                $post_image = $post_image[0];

                if($post_image == '') {
                    $post_image = BDTCJS.'../images/member.svg';
                }
                $photo .= '<img src="'.$post_image.'" alt="'.$caption.'" class="'.$thumb_radius.'" />';
            }            

        }

        if (!$atts['title'] && !$atts['name'] && !$atts['photo'] && !$atts['company']) {
          $atts['class'] .= ' bdt-testimonial-no-cite';
        }
        else {
            if ($atts['photo']) {
                $atts['class'] .= ' bdt-testimonial-has-photo';
                $photo = '<div class="bdt-testimonial-photo">'.$photo.'</div>';
            }

            if ($atts['title']) {
                $title = '<span class="bdt-testimonial-title">' . $atts['title'] . '</span>';
            }
            if ($atts['name']) {
                $name = '<span class="bdt-testimonial-name">' . $atts['name'] . '</span>';
            }
            if ($atts['company']) {
                $company = ( $atts['url'] ) ? '<a href="' . $atts['url'] . '" class="bdt-testimonial-company" target="_' . $atts['target'] . '">' . $atts['company'] . '</a>' : '<span class="bdt-testimonial-company">' . $atts['company'] . '</span>';
                if ($atts['title'])
                    $company = ' - ' . $company;
            } 
            
            $cite = "<div class='bdt-testimonial-cite'>{$name}{$title}{$company}</div>";
        }
        
        // add class of custom css for visual compser
        $classes[] = $atts['class'];

        $output = '<div id="'.$id.'" class="bdt-testimonial ' . $atts['class'] . ' bdt-testimonial-style-'.$atts['style']. ' '.$italic.'">';
            $output .= '<div class="bdt-testimonial-text bdt-content-wrap">';
                if ($atts['style'] == 4) {
                    $output .= $photo;
                }
                $output .= '<span class="quote"></span>' . do_shortcode($content);
            $output .= '</div>';
                if ($atts['style'] != 4) {
                    $output .= $photo;
                }
            $output .= $cite;
        $output .= '</div>';
                
        return $output;
    }
    // end of testimonial shortcode

    add_shortcode('bdt_testimonial', 'bdthemes_testimonial_shortcode');
}

<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package parlour
 */

get_header(); ?>



<div<?php echo parlour_helper::section(); ?>>
	<div<?php echo parlour_helper::container(); ?>>
		<div<?php echo parlour_helper::grid('uk-flex uk-flex-middle'); ?>>
			
			<div class="uk-width-expand">
				<main class="tm-content">

					<section class="error-404-section not-found">


						<div class="uk-vertical-align-middle uk-margin-large-bottom uk-margin-large-top uk-background-default uk-padding-large uk-margin-auto">

							<h1><?php esc_html_e("404", "parlour") ?></h1>
							<h3><?php esc_html_e("Page Doesn't Exists", "parlour") ?></h3>

							<p class="uk-margin-medium-top"><?php 
								$err_history_link = '<a href="javascript:history.go(-1)">'.esc_html__("Go back", "parlour").'</a>';
								$err_home_link = '<a href="'.home_url('/').'">'.get_bloginfo('name').'</a>';

								printf(esc_html__("The Page you are looking for doesn't exist or an other error occurred. %s or head over to %s %s homepage to choose a new direction.", "parlour"), $err_history_link , '<br class="uk-visible@l">' , $err_home_link ); ?></p>

						</div>

					</section><!-- .error-404 -->

				</main><!-- #main -->
			</div><!-- #primary -->
		</div>
	</div>
</div>

<?php
get_footer();

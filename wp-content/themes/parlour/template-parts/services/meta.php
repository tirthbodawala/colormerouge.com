<?php if(is_single()) :?>
    <?php 
        $duration   = get_post_meta( get_the_ID(), 'bdthemes_service_duration', true );
        $price      = get_post_meta( get_the_ID(), 'bdthemes_service_price', true );
        $order_link = get_post_meta( get_the_ID(), 'bdthemes_service_order_link', true );
        $badge      = get_post_meta( get_the_ID(), 'bdthemes_service_badge', true );
    ?>
    <div class="uk-card uk-card-default">
        <div class="uk-card-header">
            <h3 class="bdt-service-title uk-card-title"><?php esc_html_e( 'Service Info', 'parlour' ); ?></h3>
        </div>

        <div class="uk-card-body">    
            <?php if($badge != null) : ?>
                <div class="uk-card-badge uk-label"><?php echo esc_html($badge); ?></div>
            <?php endif; ?>

            <ul class="bdt-service-meta uk-list uk-list-divider uk-margin-small-bottom uk-padding-remove">

                <?php if(get_the_category_list()) : ?>
                    <li>
                        <?php printf(esc_html__('Service Type %s', 'parlour'), get_the_category_list(', ')); ?>
                    </li>
                <?php endif; ?>

                <?php if($duration != null) : ?>
                    <li class="">
                        <div class="uk-grid-small uk-flex-bottom" uk-grid>
                            <div class="uk-width-expand" uk-leader><?php esc_html_e('Duration:', 'parlour'); ?></div>
                            <div class="uk-width-auto uk-text-bold"><?php echo esc_html($duration); ?></div>
                        </div>
                       
                    </li>
                <?php endif; ?>

                <?php if($price != null) : ?>
                    <li class="">
                        <div class="uk-grid-small uk-flex-bottom" uk-grid>
                            <div class="uk-width-expand" uk-leader><?php esc_html_e('Service Charge:', 'parlour'); ?></div>
                            <div class="uk-width-auto uk-text-bold"><?php echo esc_html($price); ?></div>
                        </div>
                    </li>
                <?php endif; ?>

            </ul>
        </div>

        <?php if($order_link != null) : ?>
            <div class="uk-card-footer">
                <a href="<?php echo esc_url($order_link); ?>" class="bdt-service-icon bdt-service-cart-link uk-button uk-button-primary uk-border-rounded uk-text-bold uk-width-1-1"><?php esc_html_e( 'Order Now', 'parlour' ); ?></a>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
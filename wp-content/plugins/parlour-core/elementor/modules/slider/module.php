<?php
namespace ElementorExtend\Modules\Slider;

use ElementorExtend\Base\Module_Base;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Module extends Module_Base {

	public function get_name() {
		return 'slider';
	}

	public function get_widgets() {

		$widgets = [
			'Featured_Post',
			'Panel_Slider',
			'Post_Slider_3D',
			'Slider',
		];

		if(is_plugin_active( 'bdthemes-testimonials/bdthemes-testimonials.php' ) || is_plugin_active( 'jetpack/jetpack.php' )) {
			$widgets[] = 'Testimonial_Slider';
		}
		
		return $widgets;
	}
}

<?php if(is_single()) :?>
    <?php 
        $email            = get_post_meta( get_the_ID(), 'bdthemes_assistant_email', true );
        $phone            = get_post_meta( get_the_ID(), 'bdthemes_assistant_phone', true );
        $appointment_link = get_post_meta( get_the_ID(), 'bdthemes_assistant_appointment_link', true );
        $link_title       = get_post_meta( get_the_ID(), 'bdthemes_assistant_appointment_link_title', true );
        $badge            = get_post_meta( get_the_ID(), 'bdthemes_assistant_badge', true );
        $social_link      = get_post_meta( get_the_ID(), 'bdthemes_assistant_social_link', true );
    ?>
    <div class="uk-card uk-card-default">
        
        <div class="uk-position-relative">
            <?php get_template_part( 'template-parts/assistants/media' ); ?>
            <div class="uk-position-cover uk-overlay uk-overlay-gradient uk-position-z-index"></div>

            <?php if($social_link != null) : ?>
                <ul class="uk-list uk-position-medium uk-position-bottom-left uk-position-z-index uk-margin-remove-bottom">
                <?php foreach ($social_link as $link) : ?>
                    <?php $tooltip = ucfirst(parlour_helper::icon($link)); ?>
                    <li class="uk-display-inline-block">
                        <a<?php echo parlour_helper::attrs(['href' => $link, 'class' => 'uk-margin-small-right']); ?> uk-icon="icon: <?php echo parlour_helper::icon($link); ?>" title="<?php echo esc_attr($tooltip); ?>" uk-tooltip></a>
                    </li>
                <?php endforeach ?>
                </ul>
            <?php endif; ?>

            
        </div>

        <div class="uk-card-header">
            <h3 class="uk-card-title"><?php echo get_the_title( ) . ' '; esc_html_e( 'Info', 'parlour' ); ?></h3>
        </div>

        <div class="uk-card-body">    
            <?php if($badge != null) : ?>
                <div class="uk-card-badge uk-label"><?php echo esc_attr($badge); ?></div>
            <?php endif; ?>

            <ul class="uk-list uk-list-divider uk-margin-small-bottom uk-padding-remove">

                <?php if($email != null) : ?>
                    <li class="">
                        <div class="uk-grid-small uk-flex-bottom" uk-grid>
                            <div class="uk-width-expand" uk-leader><?php echo esc_html_e ('Email: ', 'parlour'); ?></div>
                            <div class="uk-width-auto uk-text-bold"><?php echo esc_html($email); ?></div>
                        </div>
                       
                    </li>
                <?php endif; ?>

                <?php if($phone != null) : ?>
                    <li class="">
                        <div class="uk-grid-small uk-flex-bottom" uk-grid>
                            <div class="uk-width-expand" uk-leader><?php echo esc_html_e ('Phone Number: ', 'parlour'); ?></div>
                            <div class="uk-width-auto uk-text-bold"><?php echo esc_html($phone); ?></div>
                        </div>
                    </li>
                <?php endif; ?>
            </ul>
        </div>

        <?php if($appointment_link != null) : ?>
            <div class="uk-card-footer">
                <a href="<?php echo esc_url($appointment_link); ?>" class="uk-button uk-button-primary uk-border-rounded uk-text-bold uk-width-1-1"><?php echo ($link_title) ? $link_title : esc_html__( 'Appointment Now', 'parlour' ); ?></a>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
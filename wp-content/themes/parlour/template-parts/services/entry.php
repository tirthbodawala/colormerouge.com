<?php
$author_desc = get_the_author_meta('description');
?>

<article id="post-<?php the_ID() ?>" <?php post_class('uk-article uk-box-shadow-small') ?> data-permalink="<?php the_permalink() ?>" typeof="Article">

    <?php get_template_part( 'template-parts/services/schema-meta' ); ?>

    <?php get_template_part( 'template-parts/services/media' ); ?>    

    <div class="uk-card uk-padding-large">

        <?php get_template_part( 'template-parts/services/title' ); ?>

        <div class="uk-container uk-container-small">
            <?php get_template_part( 'template-parts/services/content' ); ?>
            
            <div class="uk-subnav">
                <?php edit_post_link(esc_html__('Edit this post', 'parlour'), '<div class="uk-padding-remove uk-margin-left uk-button uk-button-text">','</div>'); ?>
                <?php get_template_part( 'template-parts/services/read-more' ); ?>
            </div>
        </div>
    </div>
</article>

<?php if(is_single() and empty($author_desc)) : ?>
    <div class="uk-margin-large-top"></div>
<?php endif ?>
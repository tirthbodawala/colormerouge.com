<?php

include 'helper.php';

function parlour_core_demo_intro_text( $default_text ) {
	?>

	<div class="parlour-core-intro-box">

		<div class="info-block">
			<h4><?php esc_html_e( 'When you import the data, the following things might happen:', 'parlour-core' ); ?></h4>

			<ul>
				<li><?php esc_html_e( 'No existing posts, pages, categories, images, custom post types or any other data will be deleted or modified.', 'parlour-core' ); ?></li>
				<li><?php esc_html_e( 'Posts, pages, images, widgets, menus and other theme settings will get imported.', 'parlour-core' ); ?></li>
				<li><?php esc_html_e( 'Please click on the Import button only once and wait, it can take a couple of minutes.', 'parlour-core' ); ?></li>
			</ul>

			<?php $reset_link = '<a href="https://wordpress.org/plugins/wordpress-reset/" target="_blank">'.esc_html__('reset data', 'parlour-core').'</a>'; ?>
			<div class="reset-info"><?php printf( __( 'We recommend you to %s &amp; clean wp-content/uploads before import to prevent duplicate content!.', 'parlour-core' ), $reset_link ); ?></div>
		</div>

		<div class="system-block">
			<?php echo parlour_core_system_requirement(); ?>			
		</div>


		<div class="clear"></div>

	</div>

	<?php
}
add_filter( 'pt-ocdi/plugin_intro_text', 'parlour_core_demo_intro_text' );

// All demo import content array
function parlour_import_files() {
	return array(
		array(
			'import_file_name'           => 'Main',
			'categories'                 => array( 'Fullscreen', 'Revolution Slider' ),
			'import_file_url'            => PARLOUR_CORE_DURL . 'contents/main.xml',
			'import_widget_file_url'     => PARLOUR_CORE_DURL . 'contents/main.json',
			'import_customizer_file_url' => PARLOUR_CORE_DURL . 'contents/main.dat',
			'import_preview_image_url'   => PARLOUR_CORE_DURL . 'images/main.jpg',
			'import_notice'              => __( 'Make sure you installed and activate all neccessary plugins correctly otherwise demo import not success for that plugin.', 'parlour-core' ),
			'preview_url'                => 'http://bdthemes.net/demo/wordpress/parlour',
		),
		array(
			'import_file_name'           => 'Glory',
			'categories'                 => array( 'Fullscreen', 'Classic Slider' ),
			'import_file_url'            => PARLOUR_CORE_DURL . 'contents/glory.xml',
			'import_widget_file_url'     => PARLOUR_CORE_DURL . 'contents/glory.json',
			'import_customizer_file_url' => PARLOUR_CORE_DURL . 'contents/glory.dat',
			'import_preview_image_url'   => PARLOUR_CORE_DURL . 'images/glory.jpg',
			'import_notice'              => __( 'Make sure you installed and activate all neccessary plugins correctly otherwise demo import not success for that plugin.', 'parlour-core' ),
			'preview_url'                => 'http://bdthemes.net/demo/wordpress/parlour/glory/',
		),
		array(
			'import_file_name'           => 'Pride',
			'categories'                 => array( 'Fullscreen', 'Revolution Slider' ),
			'import_file_url'            => PARLOUR_CORE_DURL . 'contents/pride.xml',
			'import_widget_file_url'     => PARLOUR_CORE_DURL . 'contents/pride.json',
			'import_customizer_file_url' => PARLOUR_CORE_DURL . 'contents/pride.dat',
			'import_preview_image_url'   => PARLOUR_CORE_DURL . 'images/pride.jpg',
			'import_notice'              => __( 'Make sure you installed and activate all neccessary plugins correctly otherwise demo import not success for that plugin.', 'parlour-core' ),
			'preview_url'                => 'http://bdthemes.net/demo/wordpress/parlour/pride/',
		),
		array(
			'import_file_name'           => 'Bride',
			'categories'                 => array( 'Fullscreen', 'Revolution Slider' ),
			'import_file_url'            => PARLOUR_CORE_DURL . 'contents/bride.xml',
			'import_widget_file_url'     => PARLOUR_CORE_DURL . 'contents/bride.json',
			'import_customizer_file_url' => PARLOUR_CORE_DURL . 'contents/bride.dat',
			'import_preview_image_url'   => PARLOUR_CORE_DURL . 'images/bride.jpg',
			'import_notice'              => __( 'Make sure you installed and activate all neccessary plugins correctly otherwise demo import not success for that plugin.', 'parlour-core' ),
			'preview_url'                => 'http://bdthemes.net/demo/wordpress/parlour/bride/',
		),
		array(
			'import_file_name'           => 'Fringe',
			'categories'                 => array( 'Fullscreen', 'Revolution Slider' ),
			'import_file_url'            => PARLOUR_CORE_DURL . 'contents/fringe.xml',
			'import_widget_file_url'     => PARLOUR_CORE_DURL . 'contents/fringe.json',
			'import_customizer_file_url' => PARLOUR_CORE_DURL . 'contents/fringe.dat',
			'import_preview_image_url'   => PARLOUR_CORE_DURL . 'images/fringe.jpg',
			'import_notice'              => __( 'Make sure you installed and activate all neccessary plugins correctly otherwise demo import not success for that plugin.', 'parlour-core' ),
			'preview_url'                => 'http://bdthemes.net/demo/wordpress/parlour/fringe/',
		),
		array(
			'import_file_name'           => 'Hair Craft',
			'categories'                 => array( 'Classic Slider' ),
			'import_file_url'            => PARLOUR_CORE_DURL . 'contents/haircraft.xml',
			'import_widget_file_url'     => PARLOUR_CORE_DURL . 'contents/haircraft.json',
			'import_customizer_file_url' => PARLOUR_CORE_DURL . 'contents/haircraft.dat',
			'import_preview_image_url'   => PARLOUR_CORE_DURL . 'images/haircraft.jpg',
			'import_notice'              => __( 'Make sure you installed and activate all neccessary plugins correctly otherwise demo import not success for that plugin.', 'parlour-core' ),
			'preview_url'                => 'http://bdthemes.net/demo/wordpress/parlour/haircraft/',
		),
		array(
			'import_file_name'           => 'Lucent',
			'categories'                 => array( 'Fullscreen', 'Revolution Slider' ),
			'import_file_url'            => PARLOUR_CORE_DURL . 'contents/lucent.xml',
			'import_widget_file_url'     => PARLOUR_CORE_DURL . 'contents/lucent.json',
			'import_customizer_file_url' => PARLOUR_CORE_DURL . 'contents/lucent.dat',
			'import_preview_image_url'   => PARLOUR_CORE_DURL . 'images/lucent.jpg',
			'import_notice'              => __( 'Make sure you installed and activate all neccessary plugins correctly otherwise demo import not success for that plugin.', 'parlour-core' ),
			'preview_url'                => 'http://bdthemes.net/demo/wordpress/parlour/lucent/',
		),
		array(
			'import_file_name'           => 'Mirror',
			'categories'                 => array( 'Revolution Slider', 'Blacked' ),
			'import_file_url'            => PARLOUR_CORE_DURL . 'contents/mirror.xml',
			'import_widget_file_url'     => PARLOUR_CORE_DURL . 'contents/mirror.json',
			'import_customizer_file_url' => PARLOUR_CORE_DURL . 'contents/mirror.dat',
			'import_preview_image_url'   => PARLOUR_CORE_DURL . 'images/mirror.jpg',
			'import_notice'              => __( 'Make sure you installed and activate all neccessary plugins correctly otherwise demo import not success for that plugin.', 'parlour-core' ),
			'preview_url'                => 'http://bdthemes.net/demo/wordpress/parlour/mirror/',
		),
		array(
			'import_file_name'           => 'Serenity',
			'categories'                 => array( 'Revolution Slider' ),
			'import_file_url'            => PARLOUR_CORE_DURL . 'contents/serenity.xml',
			'import_widget_file_url'     => PARLOUR_CORE_DURL . 'contents/serenity.json',
			'import_customizer_file_url' => PARLOUR_CORE_DURL . 'contents/serenity.dat',
			'import_preview_image_url'   => PARLOUR_CORE_DURL . 'images/serenity.jpg',
			'import_notice'              => __( 'Make sure you installed and activate all neccessary plugins correctly otherwise demo import not success for that plugin.', 'parlour-core' ),
			'preview_url'                => 'http://bdthemes.net/demo/wordpress/parlour/serenity/',
		),
		array(
			'import_file_name'           => 'Blossom',
			'categories'                 => array( 'Classic Slider' ),
			'import_file_url'            => PARLOUR_CORE_DURL . 'contents/blossom.xml',
			'import_widget_file_url'     => PARLOUR_CORE_DURL . 'contents/blossom.json',
			'import_customizer_file_url' => PARLOUR_CORE_DURL . 'contents/blossom.dat',
			'import_preview_image_url'   => PARLOUR_CORE_DURL . 'images/blossom.jpg',
			'import_notice'              => __( 'Make sure you installed and activate all neccessary plugins correctly otherwise demo import not success for that plugin.', 'parlour-core' ),
			'preview_url'                => 'http://bdthemes.net/demo/wordpress/parlour/blossom/',
		),
	);
}
add_filter( 'pt-ocdi/import_files', 'parlour_import_files' );


add_filter( 'pt-ocdi/regenerate_thumbnails_in_content_import', '__return_false' );


function parlour_before_content_import( $selected_import ) {

	// Delete Hello World! if exist
	wp_delete_post(1);

	//Elementor Predefine Settings
	update_option( 'thumbnail_size_w', '150' );
	update_option( 'thumbnail_size_h', '150' );

	update_option( 'medium_size_w', '400' );
	update_option( 'medium_size_h', '400' );

	update_option( 'large_size_w', '1200' );
	update_option( 'large_size_h', '800' );

	update_option( 'elementor_disable_color_schemes', 'yes' );
	update_option( 'elementor_disable_typography_schemes', 'yes' );
	update_option( 'elementor_container_width', '1280' );
	update_option( 'elementor_space_between_widgets', '35' );
	update_option( 'elementor_page_title_selector', '.tm-titlebar' );

}
add_action( 'pt-ocdi/before_content_import', 'parlour_before_content_import' );



function parlour_after_import( $selected_import ) {

	// Demo specific settings and content import
	if ( 'Main' === $selected_import['import_file_name'] ) {
		parlour_rev_slider_import('main');		
	}
	elseif ( 'Bride' === $selected_import['import_file_name'] ) {
		parlour_rev_slider_import('bride');		
	}
	elseif ( 'Glory' === $selected_import['import_file_name'] ) {
		parlour_rev_slider_import('glory');
	}
	elseif ( 'Hair Craft' === $selected_import['import_file_name'] ) {
		parlour_rev_slider_import('haircraft');
	}
	elseif ( 'Lucent' === $selected_import['import_file_name'] ) {
		parlour_rev_slider_import('lucent');
	}
	elseif ( 'Mirror' === $selected_import['import_file_name'] ) {
		parlour_rev_slider_import('mirror');
	}
	elseif ( 'Serenity' === $selected_import['import_file_name'] ) {
		parlour_rev_slider_import('serenity');
	}


	// Set Menu Locations
	$main_menu = get_term_by( 'name', 'Primary Menu', 'nav_menu' );
	set_theme_mod( 'nav_menu_locations', array( 'primary' => $main_menu->term_id));
	
	// Set Front Page and Blog
	$front_page = get_page_by_title('Homepage');
	$blog_page = get_page_by_title('Blog');

	// Update front page as static
	update_option('show_on_front', 'page');
	
	// Set static page as home page and blog page
	if(isset($front_page->ID)) { update_option('page_on_front',  $front_page->ID); }
	if(isset($blog_page->ID)) { update_option('page_for_posts',  $blog_page->ID); }

}
add_action( 'pt-ocdi/after_import', 'parlour_after_import' );


function parlour_before_widgets_import( $selected_import ) {

	update_option( 'sidebars_widgets', null );

}
add_action( 'pt-ocdi/before_widgets_import', 'parlour_before_widgets_import' );

function parlour_confirmation_dialog_options ( $options ) {
	return array_merge( $options, array(
		'width'       => 400,
		'dialogClass' => 'wp-dialog',
		'resizable'   => false,
		'height'      => 'auto',
		'modal'       => true,
	) );
}
add_filter( 'pt-ocdi/confirmation_dialog_options', 'parlour_confirmation_dialog_options', 10, 1 );


//remove pt branding
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );


//Import Revolution Slider
function parlour_rev_slider_import($slider) {

	$slider_zip = PARLOUR_CORE_DURI . 'sliders/' . $slider . '.zip';

	if (file_exists($slider_zip)) {
		if ( class_exists( 'RevSlider' ) ) {
			$rev_slider = new RevSlider();
			$rev_slider->importSliderFromPost(true,true,$slider_zip);  
			echo 'Slider Imported!';
		} else {
			echo 'Revolution Slider Not Installed or Activated!';
		}
	} else {
		echo 'Slider zip file not found!';
	}
}
<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package parlour
 */


$parlour_show_rev_slider = get_post_meta( get_the_ID(), 'parlour_show_rev_slider', true );
$parlour_rev_slider = get_post_meta( get_the_ID(), 'parlour_rev_slider', true );

if(shortcode_exists("rev_slider") && ($parlour_show_rev_slider == 'yes') && !is_search()) : ?>

<div class="slider-wrapper" id="tmSlider">
	<div>
		<section class="tm-slider uk-child-width-expand@s" uk-grid>
			<div>
				<?php echo(do_shortcode('[rev_slider '.$parlour_rev_slider.']')); ?>
			</div>
		</section>
	</div>
</div>

<?php endif; ?>

<?php
    $title           = 'yes';  // TODO
    $meta            = 'yes';  // TODO
    $excerpt         = 'no';  // TODO
    $align           = 'center';  // TODO

?>

<div class="bdt-service-item bdt-service-align-<?php echo esc_attr($align); ?>">
    <div class="bdt-service-content-wrapper uk-background-default uk-box-shadow-small">

        <?php if (has_post_thumbnail()) : ?>
           <?php
            $show_order_link = 'yes'; // TODO
            $show_link       = 'yes';  // TODO
            

            $order_link   = get_post_meta( get_the_ID(), 'bdthemes_service_order_link', true ); 

           ?>
            <div class="bdt-service-img-wrapper uk-position-relative uk-overflow-hidden">
                <div class="bdt-service-img-design">
                    <?php get_template_part( 'template-parts/services/media' ); ?>
                    <div class="bdt-service-overlay uk-position-cover uk-overlay uk-overlay-gradient uk-position-z-index"></div>
                </div>  
            </div>
        <?php endif; ?>

        <?php if(( $title==='yes') or ( $meta==='yes') or ( $excerpt==='yes')) { ?>
            <div class="uk-background-default uk-padding uk-position-relative">

                <?php if( $title==='yes') { ?>
                    <?php get_template_part( 'template-parts/services/title' ); ?>
                <?php }; ?>

                <?php if( $meta==='yes') { ?>
                    <?php $duration = get_post_meta( get_the_ID(), 'bdthemes_service_duration', true ); ?>
                    <?php $charge   = get_post_meta( get_the_ID(), 'bdthemes_service_price', true ); ?>
                    
                    <ul class="bdt-service-meta uk-subnav <?php echo 'uk-flex-'.$align; ?> uk-margin-small-top uk-margin-remove-bottom" uk-margin>
                        <li><span><?php echo esc_html('Duration: ', 'parlour').esc_html($duration); ?></span></li>
                        <li><span><?php echo esc_html('Charge: ', 'parlour').esc_html($charge); ?></span></li>
                    </ul>
                <?php }; ?>

                <?php if( $excerpt==='yes') { ?>
                    <div class="uk-container uk-text-<?php echo esc_attr($align); ?> uk-container-small">
                            <?php get_template_part( 'template-parts/services/content' ); ?>
                    </div>
                <?php }; ?>
                
                <?php if ($order_link) : ?>
                    <a href="<?php echo esc_url($order_link); ?>" class="bdt-service-cart-link bdt-service-icon uk-icon uk-icon-button uk-margin-small-right" title="<?php echo esc_html('Order Service', 'parlour'); ?>"><span uk-icon="icon: cart"></span></a>
                <?php endif; ?>

                <a href="<?php echo esc_url(get_permalink()); ?>" class="bdt-service-read-more bdt-service-icon uk-icon uk-icon-button" title="<?php echo esc_attr(get_the_title()); ?>"><span uk-icon="icon: arrow-right"></a>
            </div>
        <?php }; ?>
    </div>
</div>
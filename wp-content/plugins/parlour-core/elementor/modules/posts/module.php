<?php
namespace ElementorExtend\Modules\Posts;

use ElementorExtend\Base\Module_Base;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); }

class Module extends Module_Base {

	public function get_name() {
		return 'posts';
	}

	public function get_widgets() {

		$widgets = [
			'Posts',
			'Post_Accordion',
			'Post_Block',
			'Post_Block_Modern',
			'Post_Card',
			'Blog_Masonary',
		];

		if(is_plugin_active('bdthemes-faq/bdthemes-faq.php')) {
			$widgets[] = 'Faq';
		}
		
		return $widgets;
	}
}

<?php 

if(get_theme_mod('parlour_blog_next_prev', 1)) { ?>
	<ul class="uk-pagination">
	    <li>
	    	<?php
	    		$pre_btn_txt = '<span class="uk-margin-small-right" uk-pagination-previous></span> '. esc_html__('Previous', 'parlour'); 
	    		previous_post_link('%link', "{$pre_btn_txt}", FALSE); 
	    	?>
	    </li>
	    <li class="uk-margin-auto-left">
	    	<?php 
	    		$next_btn_txt = esc_html__('Next', 'parlour') . ' <span class="uk-margin-small-left" uk-pagination-next></span>';
    			next_post_link('%link', "{$next_btn_txt}", FALSE); 
    		?>
	    </li>
	</ul>
<?php }
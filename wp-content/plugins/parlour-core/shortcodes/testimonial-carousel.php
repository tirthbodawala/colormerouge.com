<?php
// Bdthemes Testimonial Slider

if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); } // load is_plugin_active() function if no available
if(is_plugin_active('jetpack/jetpack.php')){
	if (!function_exists('bdthemes_testimonial_carousel')) {
		function bdthemes_testimonial_carousel($atts){
			extract(shortcode_atts(array(
				'source'             => 'bdthemes-testimonial',
				'posts'             => '6',
				'navigation'        => 'no',
				'pagination'        => 'yes',
				'autoplay'          => 'no',
				'loop'              => 'yes',
				'background'        => 'none',
				'custom_background' => '',
				'color'             => 'dark',
				'orderby'           => 'date',
				'order'             => 'DESC',
				'excerpt'           => 'yes',
				'thumb'             => 'yes',
				'title'             => 'yes',
				'address'           => 'yes',
				'rating'            => 'yes',
				'shadow'            => 'small',
				'hover_shadow'      => 'none',
				'radius'            => 'no',
				'large'             => 3,
				'medium'            => 2,
				'small'             => 1,
				'gutter'            => 'medium',
				'css_animation'     => '',
				'class'             => '',
			), $atts));

			
			
			global $post;
			$id                = uniqid('bdtts_');
			$classes           = ['bdt-testimonial-carousel', $class];
			
			$output            = [];

			if ($gutter        == 'large') { $gutter = 50; }
			elseif ($gutter    == 'medium') { $gutter = 35;}
			elseif ($gutter    == 'small') { $gutter = 10;}
			
			$custom_background = ($custom_background) ? 'background:' . $custom_background. ';' : '';
			$card_class = ['testimonial-item-wrapper'];
			$card_class[]       = ($radius == 'yes') ? 'uk-border-rounded uk-overflow-hidden' : '';

			if (($background !=='custom') and ($background !=='none')) {
	            $classes[] = ($background) ? 'uk-background-'.$background : '';
	        }
	        if ($color) {
	            $classes[] = 'uk-'.$color;
	        }
    		if ($shadow !=='none') {
                $card_class[] = 'uk-box-shadow-'.$shadow;
            }		
    		if ($hover_shadow !=='none') {
                $card_class[] = 'uk-box-shadow-hover-'.$hover_shadow;
            }

			$args = array(
				'post_type'      => $source,
				'posts_per_page' => $posts,
				'orderby'        => $orderby,
				'order'          => $order,
				'post_status'    => 'publish'
			);

			$wp_query = new WP_Query($args);

			if( $wp_query->have_posts() ) :

				$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" style="'.$custom_background.'">';
					$output[] = '<div class="swiper-container">';
						$output[] = '<div class="swiper-wrapper" uk-height-match="target: > div > .testimonial-item-wrapper">';
					
							while ( $wp_query->have_posts() ) : $wp_query->the_post();

						  		$output[] = '<div class="swiper-slide">';
						  		$output[] = '<div class="'.implode(" ", $card_class).'">';
							  		$output[] = '<div class="testimonial-item-header">';
							  			$output[] = '<div class="uk-grid-small uk-flex-middle" uk-grid>';
	            							$output[] = '<div class="uk-width-auto">';
								               if( $thumb == 'yes') {
								                   $testimonial_thumb= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );

								                   if($testimonial_thumb[0] != ''){
								               
								                   $output[] = '<div class="bdt-testimonial-thumb uk-overflow-hidden uk-border-circle uk-background-cover"><img src="'.esc_url($testimonial_thumb[0]).'" alt="' . esc_attr(get_the_title()) . '" /></div>';
								                   
								                   }
								               }
							            	$output[] = '</div>';
							            	$output[] = '<div class="uk-width-expand">';
								            	if ( $title == 'yes' ) {
						                            $output[] = '<h4 class="testimonial-item-title uk-margin-remove-bottom">'.esc_html(get_the_title()) .'</h4>';
						                        }
						                        
						                        if ( $address == 'yes' ) {
						                            $output[] = '<p class="uk-text-meta uk-margin-remove-top">'.get_post_meta( get_the_ID(), 'jetpack_tm_address', true ).'</p>';
						                        }
									        $output[] = '</div>';
						            	$output[] = '</div>';
						            $output[] = '</div>';

					            	if ( $excerpt == 'yes' ) {
				                        $output[] = '<div class="testimonial-item-body">'.wp_kses_post(parlour_core_custom_excerpt(80)).'</div>';
				                    }
			                    	$output[] = '<div class="uk-padding uk-padding-remove-top">';

					                    
					                    if (( $title == 'yes' ) or ( $address == 'yes' ) or ( $rating == 'yes' )) {
										    $output[] = '<div class="bdt-testimonial-meta">';
						                        
						                        if ( $rating == 'yes' ) {
						                            $output[] = '<ul class="tm-rating tm-rating-'.get_post_meta( get_the_ID(), 'jetpack_tm_rating', true ).' uk-text-muted uk-grid-collapse" uk-grid>
												                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
												                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
												                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
												                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
												                    <li class="tm-rating-item"><span uk-icon="star"></span></li>
												                </ul>';
						                        }
						                    $output[] = '</div>';
						                }

					                $output[] = '</div>';
					                $output[] = '</div>';
				                $output[] = '</div>';
						  
							endwhile;
							wp_reset_postdata();

						$output[] ='</div>';
				
					$output[] ='</div>';
					
					/* Add Arrows */
			        if ( $navigation == 'yes' ) {
			        	$nav_class = ($color == 'light') ? ' swiper-button-white' : ' swiper-button-black';
				        $output[] = '<div class="swiper-button-next'.$nav_class.'"></div><div class="swiper-button-prev'.$nav_class.'"></div>';
				    }
				    /* Add Pagination */
			        if ( $pagination == 'yes' ) {
			        	$dots_class = ($color == 'light') ? ' swiper-pagination-white' : ' swiper-button-black';
				        $output[] = '<div class="swiper-pagination'.$dots_class.'"></div>';
				    }
				    
				$output[] ='</div>';


			 	$settings = [
					'navigation' => [
						'nextEl' => '#'.$id.' .swiper-button-nextt',
						'prevEl' => '#'.$id.' .swiper-button-prev',
					],
					'pagination' => [
						'el'        => '#'.$id.' .swiper-pagination',
						'type'      => 'bullets',
						'clickable' => true,
					],
			        'autoplay'      => $autoplay ? [ 'delay' => 4500 ] : false,
					'paginationClickable'          => true,
					'loop'                         => ($loop == 'yes') ? true : false,
					'autoplayDisableOnInteraction' => false,
					'slidesPerView'                => $large,
					'spaceBetween'                 => $gutter,
					'breakpoints'                  => [
			            1024 => [
			                'slidesPerView' => $large,
			                'spaceBetween' => $gutter
			            ],
			            768 => [
			                'slidesPerView' => $medium,
			                'spaceBetween' => $gutter
			            ],
			            640 => [
			                'slidesPerView' => $small,
			                'spaceBetween' => $gutter
			            ]
					]

			 	];

 			 	$output[] = '<script type="text/javascript">';
 			 	$output[] = '
 			 	jQuery(document).ready(function($) {
 	    			"use strict";
 				 	var swiper = new Swiper("#'.$id.' .swiper-container", '.json_encode($settings).');
 				});';
 	    		$output[] = '</script>';
		  
			endif;


			return implode("\n", $output);
		}
		add_shortcode('bdt_testimonial_carousel', 'bdthemes_testimonial_carousel');
	}
}
<?php
// Bdthemes gallery

if (!function_exists('bdthemes_gallery')) {
	function bdthemes_gallery($atts){
		extract(shortcode_atts(array( 
			'post_type'      => 'post',
			'limit'          => '12',
			'lightbox'       => 'yes',
			'orderby'        => 'date',
			'order'          => 'DESC',
			'horizontal_gap' => 10,
			'vertical_gap'   => 10,
			'large'          => 4,
			'medium'         => 3,
			'small'          => 2,
			'images'         => '',
			'img_size'		 => 'medium',
			'parallax'		 => 'no',
			'class'          => ''
		), $atts));

		$gallery_id = uniqid('bdtg_'); 
		$classes = ['bdt-gallery-grid', $class];

		global $post;

		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => intval($limit),
			'orderby'        => $orderby,
			'order'          => $order,
			'post_status'    => 'publish',
		);

		$wp_query = new WP_Query($args);

		ob_start(); // start buffer
			
		$value        = '';
		$large        = ($large) ? 'uk-width-1-'.$large.'@l' : '';
		$medium       = ($medium) ? 'uk-width-1-'.$medium.'@m' : '';
		$small        = ($small) ? 'uk-width-1-'.$small.'' : '';
		$responsive   = [$small, $medium, $large];
		
		$lightbox_tag = ($lightbox == 'yes') ? ' uk-lightbox' : ''; 
			 
	?>
	
	<div id="<?php echo $gallery_id; ?>" class="<?php echo bdt_acssc($classes); ?>">
		<div class="" uk-grid <?php echo ($parallax == 'yes') ? 'uk-grid-parallax' : '';?><?php echo esc_attr( $lightbox_tag );?> uk-lightbox>
			<?php if ($post_type != 'custom') : ?>
				<?php if( $wp_query->have_posts() ) : ?>
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

					<?php 
						if ( has_post_thumbnail()) { 
							$gallery_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
						}

						$terms       = get_the_terms( get_the_ID(), 'gallery_category' ); 
						$filter_name = get_term_by('slug',$value,'gallery_category'); 
					?> 

						<div class="bdt-gallery-item <?php echo implode(" ", $responsive);  ?>">
							<div class="uk-position-relative" uk-toggle="target: > .gallery-overlay; mode: hover; animation: uk-animation-fade">
								<?php if($lightbox == 'yes') : ?>
									<a href="<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id() )); ?>" title="<?php echo esc_attr(get_the_title()); ?>" caption="<?php echo esc_attr(get_the_title()); ?>" class="uk-overlay-default uk-position-cover gallery-overlay" hidden> 
									<?php else : ?>
									<a href="<?php echo esc_url(get_permalink()); ?>" title="<?php echo esc_attr(get_the_title()); ?>" class="uk-overlay-default uk-position-cover gallery-overlay" hidden>
								<?php endif; ?>
					                <div class="uk-position-center">
					                    <span uk-overlay-icon></span>
					                </div>
			            		</a>
						          
								<img src="<?php echo esc_url($gallery_thumbnail[0]); ?>" alt="<?php echo esc_attr(get_the_title()); ?>" />
							</div>
						</div>
					<?php endwhile; ?>
					<?php wp_reset_postdata(); 
				endif;
			endif;?>

		</div>
		
		<?php if ($post_type != 'custom') : ?>
			<?php parlour_core_pagination(); ?>
		<?php endif; ?>

	</div>
		
	<?php 

	$gallery_output = ob_get_contents(); // get buffered content

	ob_end_clean(); // clean buffer

	return $gallery_output;
	
	}
	add_shortcode('bdt_gallery', 'bdthemes_gallery');
}
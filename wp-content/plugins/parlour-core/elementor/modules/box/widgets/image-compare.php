<?php
namespace ElementorExtend\Modules\Box\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Background;


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Image_Compare extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'image-compare';
	}

	public function get_title() {
		return __( 'Image Compare', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-image-before-after';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
			]
		);


		$this->add_control(
			'before_image',
			[
				'label' => __( 'Before Image (Same Size of Both Image)', 'parlour-core' ),
				'type'  => Controls_Manager::MEDIA,
			]
		);

		$this->add_control(
			'after_image',
			[
				'label' => __( 'After Image (Same Size of Both Image)', 'parlour-core' ),
				'type'  => Controls_Manager::MEDIA,
			]
		);

		$this->add_control(
			'before_label',
			[
				'label'       => __( 'Before Label', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Before Label', 'parlour-core' ),
				'default'     => __( 'Before', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'after_label',
			[
				'label'       => __( 'After Label', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'After Label', 'parlour-core' ),
				'default'     => __( 'After', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'orientation',
			[
				'label'   => __( 'Orientation', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'horizontal',
				'options' => [
					'horizontal' => __( 'Horizontal', 'parlour-core' ),
					'vertical'   => __( 'Vertical', 'parlour-core' ),
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_layout',
			[
				'label' => __( 'Style', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'before_background',
			[
				'label' => __( 'Before Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-image-compare .twentytwenty-before-label:before' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'before_color',
			[
				'label' => __( 'Before Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-image-compare .twentytwenty-before-label:before' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'after_background',
			[
				'label' => __( 'After Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-image-compare .twentytwenty-after-label:before' => 'background-color: {{VALUE}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'after_color',
			[
				'label' => __( 'After Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-image-compare .twentytwenty-after-label:before' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'bar_color',
			[
				'label' => __( 'Bar Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-image-compare .twentytwenty-handle' => 'border-color: {{VALUE}};',
					'{{WRAPPER}} .bdt-image-compare .twentytwenty-handle:before' => 'background-color: {{VALUE}}; -webkit-box-shadow: 0 3px 0 {{VALUE}}, 0px 0px 12px rgba(51, 51, 51, 0.5); box-shadow: 0 3px 0 {{VALUE}}, 0px 0px 12px rgba(51, 51, 51, 0.5);',
					'{{WRAPPER}} .bdt-image-compare .twentytwenty-handle:after' => 'background-color: {{VALUE}}; -webkit-box-shadow: 0 3px 0 {{VALUE}}, 0px 0px 12px rgba(51, 51, 51, 0.5); box-shadow: 0 3px 0 {{VALUE}}, 0px 0px 12px rgba(51, 51, 51, 0.5);',
					'{{WRAPPER}} .bdt-image-compare .twentytwenty-handle span.twentytwenty-left-arrow' => 'border-right-color: {{VALUE}};',
					'{{WRAPPER}} .bdt-image-compare .twentytwenty-handle span.twentytwenty-right-arrow' => 'border-left-color: {{VALUE}};',
				],
				'separator' => 'before',
			]
		);

		$this->end_controls_section();

	}

	public function render() {
		$settings   = $this->get_settings();

		$before_img_src = wp_get_attachment_image_src( $settings['before_image']['id'], 'full' );
		$before_image     =  ($before_img_src) ? $before_img_src[0] : '';

		$after_img_src = wp_get_attachment_image_src( $settings['after_image']['id'], 'full' );
		$after_image     =  ($after_img_src) ? $after_img_src[0] : '';

		?>

		<div class="bdt-image-compare uk-position-relative">
			<div class="twentytwenty-container">
				<img src="<?php echo $before_image; ?>" alt="">
				<img src="<?php echo $after_image; ?>" alt="">
			</div>
		</div>

		<script>
			jQuery(document).ready(function($) {
				"use strict";
				$(".elementor-element-<?php echo $this->get_id(); ?> .twentytwenty-container").twentytwenty({
					default_offset_pct: 0.7, 
					orientation: '<?php echo $settings['orientation']; ?>',
					before_label: '<?php echo $settings['before_label']; ?>',
					after_label: '<?php echo $settings['after_label']; ?>',
				});
			});
		</script>

		<?php
	}
}

<?php
namespace ElementorExtend\Modules\Box\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Featured_Icon_Box extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'featured-icon-box';
	}

	public function get_title() {
		return __( 'Featured Icon Box', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-icon-box';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'icon_type',
			[
				'label'   => __( 'Icon Type', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'uikit',
				'options' => [
					'uikit'       => __( 'UIKit Icon', 'parlour-core' ),
					'fontawesome' => __( 'Fontawesome Icon', 'parlour-core' ),
					'image'       => __( 'Image Icon', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'uikit_icon',
			[
				'label'       => __( 'Type UIKit Icon Name', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => 'home',
				'description' => __( 'Icon source: https://getuikit.com/docs/icon#library', 'parlour-core' ),
				'condition'   => [
					'icon_type' => 'uikit',
				],
			]
		);

		$this->add_control(
			'fontawesome_icon',
			[
				'label'     => __( 'Choose Icon', 'parlour-core' ),
				'type'      => Controls_Manager::ICON,
				'default'   => 'fa fa-home',
				'condition' => [
					'icon_type' => 'fontawesome',
				],
			]
		);

		$this->add_control(
			'image_icon',
			[
				'label'     => __( 'Select Image Icon', 'parlour-core' ),
				'type'      => Controls_Manager::MEDIA,
				'condition' => [
					'icon_type' => 'image',
				],
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => __( 'Title', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'Featured Icon Box Title', 'parlour-core' ),
				'placeholder' => __( 'Your Title', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'content',
			[
				'label'      => __( 'Description', 'parlour-core' ),
				'type'       => Controls_Manager::TEXTAREA,
				'default'    => __( 'Featured icon box description goes here.', 'parlour-core' ),
				'rows'       => 10,
				'show_label' => false,
			]
		);

		$this->add_control(
			'url',
			[
				'label'       => __( 'Link', 'parlour-core' ),
				'type'        => Controls_Manager::URL,
				'placeholder' => __( 'http://your-link.com', 'parlour-core' ),
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Style', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);		

		$this->add_control(
			'color',
			[
				'label'   => __( 'Text Color', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'dark',
				'options' => [
					'dark'  => __( 'Default', 'parlour-core' ),
					'light' => __( 'Light', 'parlour-core' ),
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_top_icon',
			[
				'label' => __( 'Top Icon', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);		

		$this->start_controls_tabs( 'tabs_top_icon_style' );

		$this->start_controls_tab(
			'top_icon_normal',
			[
				'label' => esc_html__( 'Normal', 'parlour-core' ),
			]
		);

		$this->add_control(
			'icon_bg',
			[
				'label'     => __( 'Background', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box .icon-list-icon-wrapper .icon-list-icon' => 'background: {{SIZE}};',
				],
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box .icon-list-icon-wrapper .icon-list-icon' => 'color: {{SIZE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'icon_border_color',
				'selector' => '{{WRAPPER}} .bdt-featured-icon-box .icon-list-icon-wrapper .icon-list-icon',
			]
		);

		$this->add_control(
			'icon_radius',
			[
				'label'     => __( 'Radius', 'parlour-core' ),
				'type'      => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box .icon-list-icon-wrapper .icon-list-icon' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
			]
		);

		$this->add_control(
			'icon_padding',
			[
				'label'     => __( 'Padding', 'parlour-core' ),
				'type'      => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box .icon-list-icon-wrapper .icon-list-icon' => 'padding: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 40,
					],
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'top_icon_hover',
			[
				'label' => esc_html__( 'Hover', 'parlour-core' ),
			]
		);

		$this->add_control(
			'icon_hover_bg',
			[
				'label'     => __( 'Background', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box:hover .icon-list-icon-wrapper .icon-list-icon' => 'background: {{SIZE}};',
				],
			]
		);

		$this->add_control(
			'icon_hover_color',
			[
				'label'     => __( 'Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box:hover .icon-list-icon-wrapper .icon-list-icon' => 'color: {{SIZE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'icon_hover_border_color',
				'selector' => '{{WRAPPER}} .bdt-featured-icon-box:hover .icon-list-icon-wrapper .icon-list-icon',
			]
		);

		$this->add_control(
			'icon_hover_radius',
			[
				'label'     => __( 'Radius', 'parlour-core' ),
				'type'      => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box:hover .icon-list-icon-wrapper .icon-list-icon' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_bottom_icon',
			[
				'label'     => __( 'Bottom Icon', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'url!' => '',
				],
			]
		);

		$this->start_controls_tabs( 'tabs_bottom_icon_style' );

		$this->start_controls_tab(
			'bottom_icon_normal',
			[
				'label' => esc_html__( 'Normal', 'parlour-core' ),
			]
		);

		$this->add_control(
			'bottom_icon_bg',
			[
				'label'     => __( 'Background', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box .icon_list_item .icon-list-link' => 'background: {{SIZE}};',
				],
			]
		);

		$this->add_control(
			'bottom_icon_color',
			[
				'label'     => __( 'Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box .icon_list_item .icon-list-link' => 'color: {{SIZE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'bottom_icon_border_color',
				'selector' => '{{WRAPPER}} .bdt-featured-icon-box:hover .icon_list_item .icon-list-link',
			]
		);

		$this->add_control(
			'bottom_icon_radius',
			[
				'label'     => __( 'Radius', 'parlour-core' ),
				'type'      => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box .icon_list_item .icon-list-link' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
			]
		);

		$this->add_control(
			'bottom_icon_padding',
			[
				'label'     => __( 'Padding', 'parlour-core' ),
				'type'      => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box .icon_list_item .icon-list-link' => 'padding: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 40,
					],
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'bottom_icon_hover',
			[
				'label' => esc_html__( 'Hover', 'parlour-core' ),
			]
		);

		$this->add_control(
			'bottom_icon_hover_bg',
			[
				'label'     => __( 'Background', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box:hover .icon_list_item .icon-list-link' => 'background: {{SIZE}};',
				],
			]
		);

		$this->add_control(
			'bottom_icon_hover_color',
			[
				'label'     => __( 'Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box:hover .icon_list_item .icon-list-link' => 'color: {{SIZE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name'     => 'bottom_icon_hover_border_color',
				'selector' => '{{WRAPPER}} .bdt-featured-icon-box:hover .icon_list_item .icon-list-link:hover',
			]
		);

		$this->add_control(
			'bottom_icon_hover_radius',
			[
				'label'     => __( 'Radius', 'parlour-core' ),
				'type'      => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .bdt-featured-icon-box:hover .icon_list_item .icon-list-link' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	public function render() {
		$settings = $this->get_settings();
		$icon = '';

		if ($settings['icon_type'] == 'uikit') {
			$icon = '<span class="list-img-icon" uk-icon="icon: '.$settings['uikit_icon'].'"></span>';
		} elseif ($settings['icon_type'] == 'fontawesome') {
			$icon = '<i class="list-img-icon '.$settings['fontawesome_icon'] . '"></i>';
		} else {
			$image_url = wp_get_attachment_image_src( $settings['image_icon']['id'] );
			$image_url = $image_url[0];
			$icon = '<img class="list-img-icon" src="'.$image_url.'" alt="" />';
		}

		?>

		<div class="bdt-featured-icon-box uk-text-center">
		    <div class="icon_list_item">
		        <div class="icon-list-icon-wrapper">
		            <div class="icon-list-icon">
						<?php echo $icon; ?>
		            </div>
		        </div>
		        <div class="icon-list-desc uk-<?php echo $settings['color']; ?>">
		            <h3><?php echo $settings['title']; ?></h3>
		            <div class="icon_description_text">
		             <?php echo  esc_html($settings['content']); ?>
		             <?php if ($settings['url']['url']) : 
		                $rel =  ($settings['url']['nofollow']) ? ' rel=nofollow' : '';
		                ?>
		                <a href="<?php echo $settings['url']['url']; ?>" target="<?php echo ($settings['url']['is_external']) ? '_blank' : '_self'; ?>"<?php echo esc_attr($rel); ?> class="icon-list-link">
		                	<span uk-icon="icon: arrow-right"></span>
		                </a>
		             <?php endif; ?>
		            </div>
		        </div>
		        <div class="clearfix"></div>
		    </div>
		</div>

		<?php
	}
}

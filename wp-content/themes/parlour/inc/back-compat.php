<?php

/**
 * Prevent switching to Parlour on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Parlour 1.0
 */
function parlour_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );

	unset( $_GET['activated'] );

	add_action( 'admin_notices', 'parlour_upgrade_notice' );
}
add_action( 'after_switch_theme', 'parlour_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Parlour on WordPress versions prior to 4.4.
 *
 * @since Parlour 1.0
 *
 * @global string $wp_version WordPress version.
 */
function parlour_upgrade_notice() {
	$message = sprintf( esc_html__( 'Parlour requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again.', 'parlour' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.4.
 *
 * @since Parlour 1.0
 *
 * @global string $wp_version WordPress version.
 */
function parlour_customize() {
	wp_die( sprintf( esc_html__( 'Parlour requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again.', 'parlour' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'parlour_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.4.
 *
 * @since Parlour 1.0
 *
 * @global string $wp_version WordPress version.
 */
function parlour_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( esc_html__( 'Parlour requires at least WordPress version 4.4. You are running version %s. Please upgrade and try again.', 'parlour' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'parlour_preview' );

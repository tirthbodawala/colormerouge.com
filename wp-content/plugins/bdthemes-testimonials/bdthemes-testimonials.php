<?php
/*
Plugin Name: BdThemes Testimonials
Plugin URI: http://themeforest.net/user/bdthemes
Description: This plugin will create a testimonial custom post type for bdthemes wordpress theme.
Version: 1.0.0
Author: bdthemes
Author URI: http://bdthemes.com
License: Custom
License URI: http://themeforest.net/licenses 
*/

/* ----------------------------------------------------- */
/* Add Testimonial Custom Post Type
/* ----------------------------------------------------- */

define( 'BDTTM_PATH', plugin_dir_path( __FILE__ ));

function bdthemes_testimonial_register() {  

	$testimonial_slug = get_theme_mod('bdthemes_testimonial_slug');

	if(isset($testimonial_slug) && $testimonial_slug != ''){
		$testimonial_slug = $testimonial_slug;
	} else {
		$testimonial_slug = 'testimonial';
	}
	
	$labels = array(
		'name'               => esc_html__( 'Testimonials', 'bdthemes-testimonials' ),
		'singular_name'      => esc_html__( 'Testimonial', 'bdthemes-testimonials' ),
		'add_new'            => esc_html__( 'Add New', 'bdthemes-testimonials' ),
		'add_new_item'       => esc_html__( 'Add New Testimonial', 'bdthemes-testimonials' ),
		'all_items'             => esc_html__( 'All Testimonials',               'jetpack' ),
		'edit_item'          => esc_html__( 'Edit Testimonial', 'bdthemes-testimonials' ),
		'new_item'           => esc_html__( 'Add New Testimonial', 'bdthemes-testimonials' ),
		'view_item'          => esc_html__( 'View Item', 'bdthemes-testimonials' ),
		'search_items'       => esc_html__( 'Search Testimonial', 'bdthemes-testimonials' ),
		'not_found'          => esc_html__( 'No testimonial(s) found', 'bdthemes-testimonials' ),
		'not_found_in_trash' => esc_html__( 'No testimonial(s) found in trash', 'bdthemes-testimonials' )
	);
	
    $args = array(  
		'labels'          => $labels,
		'public'          => true,  
		'show_ui'         => true,  
		'capability_type' => 'post',  
		'hierarchical'    => false,  
		'menu_icon'       => 'dashicons-testimonial',
		'rewrite'         => array('slug' => $testimonial_slug), // Permalinks format
		'supports'        => array('title', 'editor', 'thumbnail')  
       );

    add_filter( 'enter_title_here',  'change_default_title'); 
  
    register_post_type( 'bdthemes-testimonial' , $args );  
}
add_action('init', 'bdthemes_testimonial_register', 1);


function change_default_title( $title ) {
	$screen = get_current_screen();

	if ( 'bdthemes-testimonial' == $screen->post_type )
		$title = esc_html__( "Enter the customer's name here", 'jetpack' );

	return $title;
}   

/* ----------------------------------------------------- */

function bdthemes_testimonial_column_display( $testimonial_columns, $post_id ) {
	
	switch ( $testimonial_columns ) {
			
		// Display the testimonial tags in the column view
		case "testimonial_categories":
		
		if ( $category_list = get_the_term_list( $post_id, 'testimonial_categories', '', ', ', '' ) ) {
			echo $category_list; // No need to escape
		} else {
			echo esc_html__('None', 'bdthemes-testimonials');
		}
		break;			
	}
}
add_action( 'manage_posts_custom_column', 'bdthemes_testimonial_column_display', 10, 2 );

require_once BDTTM_PATH . "meta-box.php";
<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package parlour
 */

?>

<?php if (is_active_sidebar('fixed-left')) : ?>
<div id="tmFixedLeft" class="uk-position-center-left">
	<div class="uk-fixed-l-wrapper">
		<?php dynamic_sidebar('fixed-left'); ?>
	</div>
</div>
<?php endif; ?>
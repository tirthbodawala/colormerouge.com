<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package parlour
 */

?>

<?php if (is_active_sidebar('fixed-right')) : ?>
<div id="tmFixedRight" class="uk-position-center-right">
	<div class="uk-fixed-r-wrapper">
		<?php dynamic_sidebar('fixed-right'); ?>
	</div>
</div>
<?php endif; ?>
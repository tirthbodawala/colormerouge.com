<?php
namespace ElementorExtend\Modules\ThirdPartyPlugins\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Revolution_Slider extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'revolution-slider';
	}

	public function get_title() {
		return __( 'Revolution Slider', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-post-slider';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}


	protected function rev_slider_list() {
        if(shortcode_exists("rev_slider")){
            $return = [];
            $slider = new \RevSlider();
            $revolution_sliders = $slider->getArrSliders();

            foreach ( $revolution_sliders as $revolution_slider ) {
                   $alias = $revolution_slider->getAlias();
                   $title = $revolution_slider->getTitle();
                   $return[$alias] = $title;
            }
            return $return;
        }
    }

	protected function _register_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);


		$slider_list = $this->rev_slider_list();

		$this->add_control(
			'Slider_name',
			[
				'label'     => __( 'Select Slider', 'parlour-core' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => $slider_list,
			]
		);

		
		$this->end_controls_section();

	}

	private function get_shortcode() {
		$settings = $this->get_settings();

		$attributes = [
			'alias'             => $settings['Slider_name'],
		];

		$this->add_render_attribute( 'shortcode', $attributes );

		$shortcode = [];
		$shortcode[] = sprintf( '[rev_slider %s]', $this->get_render_attribute_string( 'shortcode' ) );

		return implode("", $shortcode);
	}

	public function render() {
		echo do_shortcode( $this->get_shortcode() );
	}

	public function render_plain_content() {
		echo $this->get_shortcode();
	}
}

<?php
namespace ElementorExtend\Modules\Slider\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Testimonial_Slider extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'testimonial-slider';
	}

	public function get_title() {
		return __( 'Testimonial Slider', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-testimonial-carousel';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
			]
		);

		$this->add_control(
			'source',
			[
				'label'   => __( 'Source', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'bdthemes-testimonial',
				'options' => [
					'jetpack-testimonial'  => __( 'Jetpack', 'parlour-core' ),
					'bdthemes-testimonial' => __( 'BdThemes', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'posts',
			[
				'label' => __( 'Posts Per Page', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 6,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => __( 'Order by', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'date',
				'options' => [
					'date'     => __( 'Date', 'parlour-core' ),
					'title'    => __( 'Title', 'parlour-core' ),
					'category' => __( 'Category', 'parlour-core' ),
					'rand'     => __( 'Random', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label'   => __( 'Order', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'DESC',
				'options' => [
					'DESC' => __( 'Descending', 'parlour-core' ),
					'ASC'  => __( 'Ascending', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'thumb',
			[
				'label'     => __( 'Testimonial Image', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'title',
			[
				'label'     => __( 'Title', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'company_name',
			[
				'label'     => __( 'Company Name/Address', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
				'condition' => [
					'source' => 'bdthemes-testimonial',
				],
			]
		);

		$this->add_control(
			'rating',
			[
				'label'     => __( 'Rating', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
				'condition' => [
					'source' => 'bdthemes-testimonial',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_slider_settins',
			[
				'label' => __( 'Slider Settings', 'parlour-core' ),
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'     => __( 'Auto Play', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'no',
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => __( 'Autoplay Speed', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
				'frontend_available' => true,
				'condition' => [
					'autoplay' => 'yes',
				],
			]
		);

		$this->add_control(
			'loop',
			[
				'label'     => __( 'Loop', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);


		$this->add_control(
			'navigation',
			[
				'label'   => __( 'Navigation', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'arrows',
				'options' => [
					'both'   => __( 'Arrows and Dots', 'parlour-core' ),
					'arrows' => __( 'Arrows', 'parlour-core' ),
					'dots'   => __( 'Dots', 'parlour-core' ),
					'none'   => __( 'None', 'parlour-core' ),
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_style',
			[
				'label' => __( 'Style', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'text_color',
			[
				'label' => __( 'Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-text' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Title Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-meta .bdt-testimonial-title' => 'color: {{VALUE}};',
				],
				'condition' => ['title' => 'yes'],
			]
		);

		$this->add_control(
			'address_color',
			[
				'label' => __( 'Company Name/Address Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-meta .bdt-testimonial-address' => 'color: {{VALUE}};',
				],
				'condition' => [
					'company_name' => 'yes',
					'source' => 'bdthemes-testimonial',
				],
			]
		);

		$this->add_control(
			'rating_color',
			[
				'label' => __( 'Rating Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating'         => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating polygon' => 'fill: {{VALUE}};',
				],
				'condition' => [
					'rating' => 'yes',
					'source' => 'bdthemes-testimonial',
				],
			]
		);

		$this->add_control(
			'active_rating_color',
			[
				'label' => __( 'Active Rating Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default'   => '#FFCC00',
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating.bdt-rating-1 .bdt-rating-item:nth-child(1)'            => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating.bdt-rating-2 .bdt-rating-item:nth-child(-n+2)'         => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating.bdt-rating-3 .bdt-rating-item:nth-child(-n+3)'         => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating.bdt-rating-4 .bdt-rating-item:nth-child(-n+4)'         => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating.bdt-rating-5 .bdt-rating-item:nth-child(-n+5)'         => 'color: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating.bdt-rating-1 .bdt-rating-item:nth-child(1) polygon'    => 'fill: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating.bdt-rating-2 .bdt-rating-item:nth-child(-n+2) polygon' => 'fill: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating.bdt-rating-3 .bdt-rating-item:nth-child(-n+3) polygon' => 'fill: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating.bdt-rating-4 .bdt-rating-item:nth-child(-n+4) polygon' => 'fill: {{VALUE}};',
					'{{WRAPPER}} .bdt-testimonial-slider .bdt-rating.bdt-rating-5 .bdt-rating-item:nth-child(-n+5) polygon' => 'fill: {{VALUE}};',
				],
				'condition' => [
					'rating' => 'yes',
					'source' => 'bdthemes-testimonial',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_navigation',
			[
				'label' => __( 'Navigation', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'navigation' => [ 'arrows', 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_arrows',
			[
				'label' => __( 'Arrows', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_style',
			[
				'label' => __( 'Arrows Style', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'dark',
				'options' => [
					'dark' => __( 'Dark', 'parlour-core' ),
					'light' => __( 'Light', 'parlour-core' ),
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_position',
			[
				'label' => __( 'Arrows Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -100,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-slider .swiper-button-next' => 'right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .bdt-testimonial-slider .swiper-button-prev' => 'left: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_size',
			[
				'label' => __( 'Arrows Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 25,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-slider .swiper-button-prev, {{WRAPPER}} .bdt-testimonial-slider .swiper-button-next' => 'height: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}}',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_dots',
			[
				'label' => __( 'Dots', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);


		$this->add_control(
			'dots_position',
			[
				'label' => __( 'Dots Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -80,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-slider .swiper-pagination-bullets' => 'bottom: {{SIZE}}{{UNIT}} !important;',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_size',
			[
				'label' => __( 'Dots Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 5,
						'max' => 10,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-testimonial-slider .swiper-pagination-bullet' => 'height: {{SIZE}}{{UNIT}};width: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->end_controls_section();

	}

	protected function render_loop_header() {
		?>
		<div class="bdt-testimonial-slider">
			<div class="swiper-container">
				<div class="swiper-wrapper">
		<?php
	}

	protected function render_loop_footer() {

		$settings = $this->get_settings();
		?>
				</div>
			</div>

			<?php if ( 'none' !== $this->get_settings('navigation') ) : ?>
				<?php if ( 'arrows' !== $this->get_settings('navigation') ) : ?>
					<div class="swiper-pagination"></div>
				<?php endif; ?>
				
				<?php if ( 'dots' !== $this->get_settings('navigation') ) : 
					$nav_style = ($this->get_settings('arrows_style') == 'light') ? 'swiper-button-white' : 'swiper-button-black'; 
				?>
					<div class="swiper-button-next <?php echo esc_attr($nav_style); ?>"></div>
					<div class="swiper-button-prev <?php echo esc_attr($nav_style); ?>"></div>
				<?php endif; ?>
			<?php endif; ?>

		</div>

		<script>
			jQuery(document).ready(function($) {
			    "use strict";				    
			    var swiper = new Swiper(".elementor-element-<?php echo $this->get_id(); ?> .swiper-container", {
		        	navigation: {
						nextEl: ".elementor-element-<?php echo $this->get_id(); ?> .swiper-button-nextt",
						prevEl: ".elementor-element-<?php echo $this->get_id(); ?> .swiper-button-prev",
					},
					pagination: {
						el        : ".elementor-element-<?php echo $this->get_id(); ?> .swiper-pagination",
						type      : 'bullets',
						clickable : true,
					},
			        "autoplay"      : <?php echo 'yes' === $settings['autoplay'] ? '{ "delay": ' . $settings['autoplay_speed'] . ' }' : 'false'; ?>,
			        "paginationClickable":true,
			        "centeredSlides": true,
			        "loop": <?php echo ($this->get_settings('loop') == 'yes') ? 'true' : 'false'; ?>,
    				"autoplayDisableOnInteraction": false,
			        "slidesPerView": 'auto',
			    });
			});
		</script>

		<?php
	}

	public function query_posts() {

		$settings = $this->get_settings();

		$args = array(
			'post_type'      => $settings['source'],
			'posts_per_page' => $settings['posts'],
			'orderby'        => $settings['orderby'],
			'order'          => $settings['order'],
			'post_status'    => 'publish'
		);

		$this->_query = new \WP_Query( $args );
	}

	public function get_query() {
		return $this->_query;
	}

	public function render() {
		$settings  = $this->get_settings();

		$this->query_posts();

		$wp_query = $this->get_query();

		if ( ! $wp_query->found_posts ) {
			return;
		}
			$this->render_loop_header();
		?>
			<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

		  		<div class="swiper-slide">

                	<div class="bdt-testimonial-text"><?php the_excerpt(); ?></div>
                	
                		<div class="uk-flex uk-flex-center uk-flex-middle">

	                    <?php if('yes' == $settings['thumb']) : ?>
                        	<div class="bdt-testimonial-thumb"><?php echo  the_post_thumbnail('medium', array('class' => ''));  ?></div>
	                    <?php endif ?>

	                    <?php if (('yes' == $settings['title']) or ('bdthemes-testimonial' == $settings['source'])) : ?>
		                    <?php if (('yes' == $settings['title']) or ('yes' == $settings['company_name']) or ('yes' == $settings['rating'])) : ?>
							    <div class="bdt-testimonial-meta">
			                        <?php if ('yes' == $settings['title']) : ?>
			                            <div class="bdt-testimonial-title"><?php echo esc_attr(get_the_title()); ?></div>
			                        <?php endif ?>

									<?php if ( 'bdthemes-testimonial' == $settings['source']) : ?>
				                        <?php if ( 'yes' == $settings['company_name']) : ?>
				                        	<?php $separator = (( 'yes' == $settings['title'] ) and ( 'yes' == $settings['company_name'] )) ? ', ' : ''?>
				                            <span class="bdt-testimonial-address"><?php echo esc_attr( $separator ).get_post_meta(get_the_ID(), 'bdthemes_tm_company_name', true); ?></span>
				                        <?php endif ?>
				                        
				                        <?php if ('yes' == $settings['rating']) : ?>
				                            <ul class="bdt-rating bdt-rating-<?php echo get_post_meta(get_the_ID(), 'bdthemes_tm_rating', true); ?> uk-grid uk-grid-collapse">
							                    <li class="bdt-rating-item"><span uk-icon="icon: star; ratio: 0.80;"></span></li>
												<li class="bdt-rating-item"><span uk-icon="icon: star; ratio: 0.80;"></span></li>
												<li class="bdt-rating-item"><span uk-icon="icon: star; ratio: 0.80;"></span></li>
												<li class="bdt-rating-item"><span uk-icon="icon: star; ratio: 0.80;"></span></li>
												<li class="bdt-rating-item"><span uk-icon="icon: star; ratio: 0.80;"></span></li>
							                </ul>
				                        <?php endif ?>
									<?php endif ?>

			                    </div>
			                <?php endif ?>
		                <?php endif ?>

	                </div>
                </div>
		  
			<?php endwhile;
			wp_reset_postdata();

		$this->render_loop_footer();
	}
}

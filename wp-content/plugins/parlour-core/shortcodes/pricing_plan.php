<?php
// Bdthemes Pricing Plan
if (!function_exists('bdthemes_pricing_plan')) {
	function bdthemes_pricing_plan($atts=null, $content=null){
		$atts = shortcode_atts(array(
			'name'              => 'Standard',
			'price'             => '19.99',
			'before'            => '$',
			'after'             => '',
			'period'            => '',
			'featured'          => 'no',
			'icon_type'         => 'uikit', // fontawesome, image
			'uikit_icon'        => '',
			'fontawesome_icon'  => '',
			'image_icon'        => '',
			'icon_color'        => '',
			'icon_size'         => '48',
			'btn_text'          => 'Sign up Now',
			'btn_url'           => '#',
			'btn_target'        => 'self',
			'btn_style'         => 'default',
			'badge'             => '',
			'background'        => 'default',
			'custom_background' => '',
			'color'             => 'dark',
			'custom_color'      => '',
			'shadow'            => 'small',
			'hover_shadow'      => 'none',
			'border'            => '',
			'radius'            => 'no',
            'css_animation'     => '',
            'class'             => ''
		), $atts, 'plan');

		
		$id                = uniqid('bdtplan_');
		$classes           = array('bdt-plan', $atts['class']);
		
		$output            = [];
		$badge             = '';
		$featured          = '';
		
		$custom_background = ($atts['custom_background']) ? 'background:' . $atts['custom_background']. ';' : '';
		$custom_color      = ($atts['custom_color']) ? 'color:' . $atts['custom_color']. ';' : '';
		$border            = ($atts['border']) ? 'border:' . $atts['border']. ';' : '';
		$classes[]         = ($atts['radius'] == 'yes') ? 'uk-border-rounded' : '';


        if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $classes[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if ($atts['color']) {
            $classes[] = 'uk-'.$atts['color'];
        }
        if ($atts['shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-'.$atts['shadow'];
        }
        if ($atts['hover_shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$atts['hover_shadow'];
        }
		
		// icon

		$icon            = '';
		$icon_color      = ($atts['icon_color']) ? 'color:' . $atts['icon_color'] . ';' : '';
		$icon_size       = ($atts['icon_size']) ? 'font-size: '.intval($atts['icon_size']).'px;' : 'font-size: '.$atts['count_size'].';';
		$uikit_icon_size = ($atts['icon_size']) ? 'width: '.intval($atts['icon_size']).'px;' : 'width: '.$atts['count_size'].';';

        if (($atts['icon_type'] == 'uikit') and ($atts['uikit_icon'] != '')) {
            $icon = '<span uk-icon="icon: '.$atts['uikit_icon'].'" style="'.esc_attr($icon_color).esc_attr($uikit_icon_size).'"></span>';
        } 
        if (($atts['icon_type'] == 'fontawesome') and ($atts['fontawesome_icon'] != '')) {
            wp_enqueue_style( 'font-awesome' );
            $icon = '<i class="list-img-icon '.$atts['fontawesome_icon'] . '" style="' . esc_attr($icon_color) .esc_attr($icon_size) .'"></i>';
        } 
        if (($atts['icon_type'] == 'image') and ($atts['image_icon'] != '')) {
            $image_url = wp_get_attachment_image_src( $atts['image_icon'] );
            $image_url = $image_url[0];
            $icon = '<img src="'.$image_url.'" style="max-width:'.$atts['icon_size'].'px" alt="" />';
        }

        $icon = ($icon) ? '<div class="bdt-plan-icon uk-margin-medium-top">'. $icon .'</div>' : '';

		if ($atts['before'])
		    $atts['before'] = '<span class="bdt-plan-price-before">' . $atts['before'] . '</span>';
		if ($atts['after'])
		    $atts['after']  = '<span class="bdt-plan-price-after">' . $atts['after'] . '</span>';
		if ($atts['period'])
		    $atts['period'] = '<div class="bdt-plan-period">' . $atts['period'] . '</div>';
		if ($atts['featured'] == 'yes')
		    $classes[] = ' bdt-plan-featured';
		if ($atts['badge']) {
		    $badge = '<div class="bdt-plan-badge">'.$atts['badge'].'</div>';
		}

		$content = trim(do_shortcode($content), '<br><ul><li><a><b><strong><i><em><span>');

		$button = ( $atts['btn_text'] && $atts['btn_url'] ) ? '<a href="' . $atts['btn_url'] . '" class="uk-button uk-button-'.$atts['btn_style'].'" target="_' . $atts['btn_target'] . '">' . $atts['btn_text'] . '</a>' : '';

		$footer = ( $button ) ? '<div class="bdt-plan-footer">' . $button . '</div>' : '';

		$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" style="'.$custom_background.$custom_color.$border.'">';
		    $output[] = '<div class="bdt-plan-head">';
		        $output[] = $badge;
		        $output[] = '<div class="bdt-plan-name">'.$atts['name'].'</div>';
		        $output[] = '<div class="bdt-plan-price">'.$atts['before'];
		            $output[] = '<span class="bdt-plan-price-value">'.$atts['price'].'</span>'.$atts['after'];
		        $output[] = '</div>';
		        $output[] = $atts['period'] . $icon;
		    $output[] = '</div>';
		    $output[] = '<div class="bdt-plan-options">'.$content.'</div>';
		    $output[] = $footer;
		$output[] = '</div>';
		return implode("",$output);
	}
	add_shortcode('bdt_pricing_plan', 'bdthemes_pricing_plan');
}

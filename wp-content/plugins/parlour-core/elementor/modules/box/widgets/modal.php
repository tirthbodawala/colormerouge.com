<?php
namespace ElementorExtend\Modules\Box\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Border;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Modal extends Widget_Base {

	//protected $_has_template_content = false;

	public function get_name() {
		return 'modal';
	}

	public function get_title() {
		return __( 'Modal', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-call-to-action';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => __( 'Title', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'This is a Modal', 'parlour-core' ),
				'placeholder' => __( 'Modal title here', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_responsive_control(
			'title_align',
			[
				'label' => __( 'Title Alignment', 'parlour-core' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'parlour-core' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'parlour-core' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'parlour-core' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'parlour-core' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => 'center',
			]
		);

		$this->add_control(
			'description',
			[
				'label'       => __( 'Description', 'parlour-core' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => __( 'A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.', 'parlour-core' ),
				'placeholder' => __( 'Call to action description', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'footer_caption',
			[
				'label'       => __( 'Footer Caption', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'You can add footer caption.', 'parlour-core' ),
				'placeholder' => __( 'Footer Caption', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_responsive_control(
			'footer_caption_align',
			[
				'label' => __( 'Footer Alignment', 'parlour-core' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'parlour-core' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'parlour-core' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'parlour-core' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'parlour-core' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => 'center',
			]
		);

		$this->add_control(
			'modal_size',
			[
				'label' => __( 'Modal Size', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default' => __( 'Default', 'parlour-core' ),
					'container' => __( 'Container Size', 'parlour-core' ),
					'full' => __( 'Full Screen', 'parlour-core' ),
				],
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->add_control(
			'close_button_position',
			[
				'label' => __( 'Outside Close Button', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => 'Hide',
				'label_off' => 'Show',
				'return_value' => 'yes',
			]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_footer',
			[
				'label' => __( 'Button', 'parlour-core' ),
			]
		);

		$this->add_control(
			'button_text',
			[
				'label' => __( 'Button Text', 'parlour-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Click Here', 'parlour-core' ),
			]
		);

		$this->add_control(
			'button_size',
			[
				'label'   => __( 'Button Size', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'small' => __( 'Small', 'parlour-core' ),
					''      => __( 'Default', 'parlour-core' ),
					'large' => __( 'Large', 'parlour-core' ),
				],
				'condition' => [
					'button_text!' => '',
				],
			]
		);


		$this->add_control(
			'center_modal',
			[
				'label' => __( 'Modal Center Position', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => 'Hide',
				'label_off' => 'Show',
				'return_value' => 'yes',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Text Style', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_text_color',
			[
				'label' => __( 'Title Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'#'.$this->get_id().'_modal.bdt-modal .uk-modal-title' => 'color: {{VALUE}};',
				],
				'condition' => [
					'title!' => '',
				],
			]
		);

		$this->add_control(
			'title_backgound',
			[
				'label'     => __( 'Title Background', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'#'.$this->get_id().'_modal.bdt-modal .uk-modal-header' => 'color: {{VALUE}};',
				],
				'condition' => [
					'description!' => '',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'selector' => '#'.$this->get_id().'_modal.bdt-modal .uk-modal-title',
				'scheme' => Scheme_Typography::TYPOGRAPHY_2,
				'condition' => [
					'title!' => '',
				],
			]
		);


		$this->add_control(
			'description_color',
			[
				'label'     => __( 'Description Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'#'.$this->get_id().'_modal.bdt-modal .uk-modal-body' => 'color: {{VALUE}};',
				],
				'condition' => [
					'description!' => '',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'description_backgound',
			[
				'label'     => __( 'Description Background', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'#'.$this->get_id().'_modal.bdt-modal .uk-modal-dialog' => 'color: {{VALUE}};',
				],
				'condition' => [
					'description!' => '',
				],
			]
		);


		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'      => 'description_typography',
				'selector'  => '#'.$this->get_id().'_modal.bdt-modal .uk-modal-body',
				'scheme'    => Scheme_Typography::TYPOGRAPHY_2,
				'condition' => [
					'description!' => '',
				],
			]
		);


		$this->add_control(
			'caption_color',
			[
				'label' => __( 'Caption Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'#'.$this->get_id().'_modal.bdt-modal .uk-modal-footer' => 'color: {{VALUE}};',
				],
				'condition' => [
					'title!' => '',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'caption_backgound',
			[
				'label'     => __( 'Caption Background', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'#'.$this->get_id().'_modal.bdt-modal .uk-modal-footer' => 'color: {{VALUE}};',
				],
				'condition' => [
					'description!' => '',
				],
			]
		);




		$this->end_controls_section();


		$this->start_controls_section(
			'section_button_style',
			[
				'label'      => __( 'Button', 'parlour-core' ),
				'tab'        => Controls_Manager::TAB_STYLE,
				'show_label' => false,
			]
		);

		$this->add_control(
			'button_style',
			[
				'label'   => __( 'Button Style', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default'   => __( 'Default', 'parlour-core' ),
					'primary'   => __( 'Primary', 'parlour-core' ),
					'secondary' => __( 'Secondary', 'parlour-core' ),
					'muted'     => __( 'Muted', 'parlour-core' ),
					'text'      => __( 'Text', 'parlour-core' ),
					'link'      => __( 'Link', 'parlour-core' ),
					'custom'      => __( 'Custom', 'parlour-core' ),
				],
				'condition' => [
					'button_text!' => '',
				],
			]
		);

		$this->start_controls_tabs( 'tabs_button_style' );

		$this->start_controls_tab(
			'tab_button_normal',
			[
				'label'     => __( 'Normal', 'parlour-core' ),
				'condition' => [
					'button_style' => 'custom',
				],
			]
		);

		$this->add_control(
			'button_text_color',
			[
				'label'     => __( 'Text Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-modal-button' => 'color: {{VALUE}};',
				],
				'condition' => [
					'button_style' => 'custom',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'      => 'button_typography',
				'label'     => __( 'Typography', 'parlour-core' ),
				'scheme'    => Scheme_Typography::TYPOGRAPHY_4,
				'selector'  => '{{WRAPPER}} .bdt-modal-button',
				'condition' => [
					'button_style' => 'custom',
				],
			]
		);

		$this->add_control(
			'button_background_color',
			[
				'label'  => __( 'Background Color', 'parlour-core' ),
				'type'   => Controls_Manager::COLOR,
				'scheme' => [
					'type'  => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_4,
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-modal-button' => 'background-color: {{VALUE}};',
				],
				'condition' => [
					'button_style' => 'custom',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(), [
				'name'        => 'button_border',
				'label'       => __( 'Border', 'parlour-core' ),
				'placeholder' => '1px',
				'default'     => '1px',
				'selector'    => '{{WRAPPER}} .bdt-modal-button',
				'condition'   => [
					'button_style' => 'custom',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'button_border_radius',
			[
				'label'      => __( 'Border Radius', 'parlour-core' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .bdt-modal-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'button_style' => ['custom', 'default', 'primary', 'secondary', 'muted'],
				],
			]
		);

		$this->add_control(
			'button_text_padding',
			[
				'label' => __( 'Text Padding', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-modal-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition' => [
					'button_style' => 'custom',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_button_hover',
			[
				'label'     => __( 'Hover', 'parlour-core' ),
				'condition' => [
					'button_style' => 'custom',
				],
			]
		);

		$this->add_control(
			'button_hover_color',
			[
				'label'     => __( 'Text Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-modal-button:hover' => 'color: {{VALUE}};',
				],
				'condition' => [
					'button_style' => 'custom',
				],
			]
		);

		$this->add_control(
			'button_background_hover_color',
			[
				'label'     => __( 'Background Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-modal-button:hover' => 'background-color: {{VALUE}};',
				],
				'condition' => [
					'button_style' => 'custom',
				],
			]
		);

		$this->add_control(
			'button_hover_border_color',
			[
				'label'     => __( 'Border Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-modal-button:hover' => 'border-color: {{VALUE}};',
				],
				'condition' => [
					'button_style' => 'custom',
				],
			]
		);

		$this->add_control(
			'button_hover_animation',
			[
				'label'     => __( 'Animation', 'parlour-core' ),
				'type'      => Controls_Manager::HOVER_ANIMATION,
				'condition' => [
					'button_style' => 'custom',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	public function render() {
		$settings           = $this->get_settings();
		$id                 = $this->get_id();
		
		$button_size        = ($settings['button_size']) ? 'uk-button-'.$settings['button_size'] : '';
		$btn_class          = ['bdt-modal-button', 'uk-button', 'uk-button-'.$settings['button_style']];
		$classes            = ['bdt-modal'];
		
		$classes[]          = ( $settings['modal_size'] == 'default' ) ? 'uk-modal' : 'uk-modal-'.$settings['modal_size'] ;
		$classes[]          = ( $settings['modal_size'] == 'full' ) ? 'uk-background-default' : '' ;
		$close_btn_position = ( $settings['modal_size'] !== 'full' and $settings['close_button_position'] ) ?  'uk-modal-close-outside' : 'uk-modal-close-default';
		$body_class = ['uk-modal-dialog'];
		$body_class[] = ($settings['center_modal'] == 'yes') ? 'uk-margin-auto-vertical' : '';

		?>

			<button class="<?php echo implode(" ", $btn_class); ?>" type="button" uk-toggle="target: #bdt-modal-<?php echo esc_attr($id); ?>">
				<?php echo esc_html( $settings['button_text'] ); ?>
			</button>

            <div id="bdt-modal-<?php echo esc_attr($id); ?>" class="<?php echo  implode(" ", $classes); ?>" uk-modal>
                <div class="<?php echo  implode(" ", $body_class); ?>">         
	                    <button class="<?php echo $close_btn_position; ?>" type="button" uk-close></button>
	          
	                    <?php if ($settings['title']) : ?>
	                        <div class="uk-modal-header uk-text-<?php echo $settings['title_align']; ?>">
	                        	<h2 class="uk-modal-title"><?php echo $settings['title']; ?></h2>
	                        </div>
	                    <?php endif; ?>
	                    
	                    <div class="uk-modal-body" uk-overflow-auto><?php echo do_shortcode( $settings['description'] ); ?></div>
						
	                    <?php if ($settings['footer_caption']) : ?>
	                        <div class="uk-modal-footer uk-text-<?php echo $settings['footer_caption_align']; ?>"><?php echo $settings['footer_caption']; ?></div>
	                    <?php endif; ?>

	            </div>
            </div>

		<?php
	}
}

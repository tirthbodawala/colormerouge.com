<?php
/**
 * Service Category texonomy.
 *
 * This template is only used if Charitable is active.
 *
 * @package     Quick
 */

 get_header();

 // Layout
 $position = (get_post_meta( get_the_ID(), 'parlour_page_layout', true )) ? get_post_meta( get_the_ID(), 'parlour_page_layout', true ) : get_theme_mod( 'parlour_page_layout', 'sidebar-right' );

 ?>

 <div<?php echo parlour_helper::section(); ?>>
 	<div<?php echo parlour_helper::container(); ?>>
		<div class="bdt-services" uk-grid>
 			
 			<div class="uk-width-expand">
 				<main class="tm-content">
 					<div class="" uk-grid uk-grid-margin>
 						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							
							<div class="uk-width-1-3@m">
	 							<?php get_template_part( 'template-parts/services/category/entry' ); ?>							
							</div>

 						<?php endwhile; endif; ?>
 					
 					</div>
 					<?php get_template_part( 'template-parts/pagination' ); ?>
 				</main> <!-- end main -->
 			</div> <!-- end content -->
 			

 			<?php if($position == 'sidebar-left' or $position == 'sidebar-right') : ?>
 				<aside<?php echo parlour_helper::sidebar($position); ?>>
 				    <?php get_sidebar(); ?>
 				</aside> <!-- end aside -->
 			<?php endif; ?>
 			
 		</div> <!-- end grid -->
 	</div> <!-- end container -->
 </div> <!-- end tm main -->
 	
 <?php get_footer(); ?>
<?php

$style             = get_theme_mod( 'parlour_search_style', 'default');
$search            = [];
$toggle            = ['class' => 'uk-search-icon uk-padding-remove-horizontal'];
$layout_c          = get_theme_mod('parlour_header_layout', 'horizontal-left');
$layout_m          = get_post_meta( get_the_ID(), 'parlour_header_layout', true );
$layout            = (!empty($layout_m) and $layout_m != 'default') ? $layout_m : $layout_c;
$position          = get_theme_mod( 'parlour_search_position', 'header');
$id                = esc_attr( uniqid( 'search-form-' ) );
$attrs['class']    = array_merge(['uk-search'], isset($attrs['class']) ? (array) $attrs['class'] : []);
$search            = [];
$search['class']   = [];
$search['class'][] = 'uk-search-input';

if (($layout == 'side-left' or $layout == 'side-right') and $position == 'menu') {
    $style = 'default';
}
// TODO
$navbar = [
    'dropdown_align'    => get_theme_mod( 'parlour_dropdown_align', 'left' ),
    'dropdown_click'    => get_theme_mod( 'parlour_dropdown_click' ),
    'dropdown_boundary' => get_theme_mod( 'parlour_dropdown_boundary' ),
    'dropbar'           => get_theme_mod( 'parlour_dropbar' ),
];

if ($style) {
    $search['autofocus'] = true;
}

if ($style == 'modal') {
    $search['class'][] = 'uk-text-center';
    $attrs['class'][] = 'uk-search-large';
} else {
    $attrs['class'][] = 'uk-search-default';
}

if (in_array($style, ['dropdown', 'justify'])) {
    $attrs['class'][] = 'uk-search-navbar';
    $attrs['class'][] = 'uk-width-1-1';
}

?>

<?php if ($style == 'default') : // TODO renders the default style only ?>

    <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" role="search" <?php echo parlour_helper::attrs($attrs) ?>>
        <span uk-search-icon></span>
        <input id="<?php echo esc_attr($id); ?>" name="s" placeholder="<?php esc_html_e('Search...', 'parlour'); ?>" type="search" class="uk-search-input">
    </form>

<?php elseif ($style == 'drop') : ?>

    <a<?php echo parlour_helper::attrs($toggle) ?> href="#" uk-search-icon></a>
    <div uk-drop="mode: click; pos: left-center; offset: 0">
        <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" role="search" <?php echo parlour_helper::attrs($attrs) ?>>
            <span uk-search-icon></span>
            <input id="<?php echo esc_attr($id); ?>" name="s" placeholder="<?php esc_html_e('Search...', 'parlour'); ?>" type="search" class="uk-search-input">
        </form>
    </div>

<?php elseif (in_array($style, ['dropdown', 'justify'])) :

    $drop = [
        'mode'           => 'click',
        'cls-drop'       => 'uk-navbar-dropdown',
        'boundary'       => $navbar['dropdown_align'] ? '!nav' : false,
        'boundary-align' => $navbar['dropdown_boundary'],
        'pos'            => $style == 'justify' ? 'bottom-justify' : 'bottom-right',
        'flip'           => 'x',
        'offset'         => !$navbar['dropbar'] ? 28 : 0
    ];

    ?>

    <a<?php echo parlour_helper::attrs($toggle) ?> href="#" uk-search-icon></a>
    <div class="uk-navbar-dropdown uk-width-medium" <?php echo parlour_helper::attrs(['uk-drop' => json_encode(array_filter($drop))]) ?>>
        <div class="uk-grid uk-grid-small uk-flex-middle">
            <div class="uk-width-expand">
               <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" role="search" <?php echo parlour_helper::attrs($attrs) ?>>
                   <span uk-search-icon></span>
                   <input id="<?php echo esc_attr($id); ?>" name="s" placeholder="<?php esc_html_e('Search...', 'parlour'); ?>" type="search" class="uk-search-input">
               </form>
            </div>
            <div class="uk-width-auto">
                <a class="uk-navbar-dropdown-close" href="#" uk-close></a>
            </div>
        </div>

    </div>

<?php elseif ($style == 'modal') : ?>

    <a<?php echo parlour_helper::attrs($toggle) ?> href="#<?php echo esc_attr($id).'-modal' ?>" uk-search-icon uk-toggle></a>

    <div id="<?php echo esc_attr($id).'-modal' ?>" class="uk-modal-full" uk-modal>
        <div class="uk-modal-dialog uk-modal-body uk-flex uk-flex-center uk-flex-middle" uk-height-viewport>
            <button class="uk-modal-close-full" type="button" uk-close></button>
            <div class="uk-search uk-search-large">
               <form id="search-230" action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" role="search" <?php echo parlour_helper::attrs($attrs) ?>>
                    <input id="<?php echo esc_attr($id); ?>" name="s" placeholder="<?php esc_html_e('Type Word and Hit Enter', 'parlour'); ?>" type="search" class="uk-search-input uk-text-center" autofocus="">
               </form>
            </div>
        </div>
    </div>

<?php endif ?>

<?php

class bdt_countdown_widget extends WP_Widget { 
	
	// Widget Settings
	function __construct() {
		$widget_ops  = array('description' => esc_html__('Display your countdown for any event', 'parlour-core'));
		$control_ops = array('width' => 300, 'height' => 350, 'id_base' => 'countdown');
		parent::__construct('countdown', esc_html__('BdThemes Countdown', 'parlour-core'), $widget_ops, $control_ops );
	}
	
	// Widget Output
	function widget($args, $instance) {
		extract($args);

		$title      = $custom_dt = $post_id = $prefix = $suffix = '';
		$title      = apply_filters('widget_title', esc_html($instance['title']));
		$custom_dt  = $instance['custom_dt'];
		$align      = $instance['align'];
		$post_id    = $instance['post_id'];
		$prefix     = $instance['prefix'];
		$suffix     = $instance['suffix'];
		$layout     = $instance['layout'];
		$time_name  = $instance['time_name'];
		$count_size = $instance['count_size'];

		if (strpos($prefix,'|')) {
			$prefix = explode('|', $prefix);
			$prefix = '<strong>'.$prefix[0].'</strong><br>'.$prefix[1];
		}
		if (strpos($suffix,'|')) {
			$suffix = explode('|', $suffix);
			$suffix = '<strong>'.$suffix[0].'</strong><br>'.$suffix[1];
		}
		
		// ------
		echo $before_widget;
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . $instance['title'] . $args['after_title'];
		}
		echo do_shortcode('[bdt_countdown post_id="'.$post_id.'" custom_date_time="'.$custom_dt.'" layout="'.$layout.'" count_size="'.$count_size.'" time_name="'.$time_name.'" align="'.$align.'" prefix="'.$prefix.'" suffix="'.$suffix.'"]');

		echo $after_widget;
	}
	
	// Update
	function update($new_instance, $old_instance) {  
		$instance = $old_instance; 
		
		$instance['title']      = strip_tags( $new_instance['title'] );
		$instance['post_id']    = strip_tags( $new_instance['post_id'] );
		$instance['custom_dt']  = strip_tags( $new_instance['custom_dt'] );
		$instance['align']      = strip_tags( $new_instance['align'] );
		$instance['prefix']     = strip_tags( $new_instance['prefix'] );
		$instance['suffix']     = strip_tags( $new_instance['suffix'] );
		$instance['layout']     = strip_tags( $new_instance['layout'] );
		$instance['time_name']  = strip_tags( $new_instance['time_name'] );
		$instance['count_size'] = strip_tags( $new_instance['count_size'] );

		return $instance;
	}
	
	// Backend Form
	function form($instance) {
		
		$defaults = array(
			'title'      => 'Countdown Widget', 
			'prefix'     => '', 
			'count_size' => '', 
			'time_name'  => '', 
			'align'      => '', 
			'layout'     => 'vertical', 
			'suffix'     => '', 
			'post_id'    => '', 
			'custom_dt'  => '2020-04-20T10:20:20'
			); // Default Values
		$instance = wp_parse_args( (array) $instance, $defaults ); 
		?>
        
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>">Widget Title:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>

		<?php if ( class_exists( 'Charitable' ) ) : ?>
	        <p>
				<label for="<?php echo esc_attr($this->get_field_id('post_id')); ?>">Campaign Date:</label>
				<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('post_id')); ?>" name="<?php echo esc_attr($this->get_field_name('post_id')); ?>" value="<?php echo esc_attr($instance['post_id']); ?>" placeholder="1869" /><br />Date will come from campaign ID, just enter expected campaign ID, for Example 1869
			</p>
		<?php endif; ?>
		
        <p>
			<label for="<?php echo esc_attr($this->get_field_id('custom_dt')); ?>">Custom Date and Time:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('custom_dt')); ?>" name="<?php echo esc_attr($this->get_field_name('custom_dt')); ?>" value="<?php echo esc_attr($instance['custom_dt']); ?>" placeholder="yyyy-mm-ddThh:mm:ss" /><br />For Example 2018-11-15T14:15:00, this option will work when campaign id not assigned.
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'count_size' ) ?>"><?php esc_html_e( 'Count Size', 'parlour-core' ) ?></label>
			<select class="widefat" name="<?php echo $this->get_field_name( 'count_size' ) ?>" id="<?php echo $this->get_field_id( 'count_size' ) ?>">
				<option value="default" <?php selected( 'default', $instance['count_size'] ) ?>><?php _e( 'Default', 'parlour-core' ) ?></option>
				<option value="small" <?php selected( 'small', $instance['count_size'] ) ?>><?php _e( 'Small', 'parlour-core' ) ?></option>
				<option value="medium" <?php selected( 'medium', $instance['count_size'] ) ?>><?php _e( 'Medium', 'parlour-core' ) ?></option>
				<option value="large" <?php selected( 'large', $instance['count_size'] ) ?>><?php _e( 'Large', 'parlour-core' ) ?></option>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'align' ) ?>"><?php esc_html_e( 'Alignment', 'parlour-core' ) ?></label>
			<select class="widefat" name="<?php echo $this->get_field_name( 'align' ) ?>" id="<?php echo $this->get_field_id( 'align' ) ?>">
				<option value="left" <?php selected( 'left', $instance['align'] ) ?>><?php _e( 'Left', 'parlour-core' ) ?></option>
				<option value="right" <?php selected( 'right', $instance['align'] ) ?>><?php _e( 'Right', 'parlour-core' ) ?></option>
				<option value="center" <?php selected( 'center', $instance['align'] ) ?>><?php _e( 'Center', 'parlour-core' ) ?></option>
			</select>
		</p>


		<p>
			<label for="<?php echo $this->get_field_id( 'layout' ) ?>"><?php esc_html_e( 'Layout', 'parlour-core' ) ?></label>
			<select class="widefat" name="<?php echo $this->get_field_name( 'layout' ) ?>" id="<?php echo $this->get_field_id( 'layout' ) ?>">
				<option value="horizontal" <?php selected( 'horizontal', $instance['layout'] ) ?>><?php _e( 'Horizontal', 'parlour-core' ) ?></option>
				<option value="vertical" <?php selected( 'vertical', $instance['layout'] ) ?>><?php _e( 'Vertical', 'parlour-core' ) ?></option>
			</select>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'time_name' ) ?>"><?php esc_html_e( 'Time Name Show', 'parlour-core' ) ?></label>
			<select class="widefat" name="<?php echo $this->get_field_name( 'time_name' ) ?>" id="<?php echo $this->get_field_id( 'time_name' ) ?>">
				<option value="yes" <?php selected( 'yes', $instance['time_name'] ) ?>><?php _e( 'Yes', 'parlour-core' ) ?></option>
				<option value="no" <?php selected( 'no', $instance['time_name'] ) ?>><?php _e( 'No', 'parlour-core' ) ?></option>
			</select>
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('prefix')); ?>">Prefix Text:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('prefix')); ?>" name="<?php echo esc_attr($this->get_field_name('prefix')); ?>" value="<?php echo esc_attr($instance['prefix']); ?>" /><br>Want to break text with 2 line? use | in two words.
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('suffix')); ?>">Suffix Text:</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('suffix')); ?>" name="<?php echo esc_attr($this->get_field_name('suffix')); ?>" value="<?php echo esc_attr($instance['suffix']); ?>" /><br>Want to break text with 2 line? use | in two words.
		</p>		
    <?php }
}

// Add Widget
function bdt_countdown_widget_init() {
	register_widget('bdt_countdown_widget');
}
add_action('widgets_init', 'bdt_countdown_widget_init');

?>
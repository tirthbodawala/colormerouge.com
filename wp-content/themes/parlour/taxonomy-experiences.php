<?php
/**
 * Assistant Experience texonomy.
 *
 * This template is only used if Charitable is active.
 *
 * @package     Parlour
 */

 get_header();

 // Layout
 $position = (get_post_meta( get_the_ID(), 'parlour_page_layout', true )) ? get_post_meta( get_the_ID(), 'parlour_page_layout', true ) : get_theme_mod( 'parlour_page_layout', 'sidebar-right' );

 ?>

 <div<?php echo parlour_helper::section(); ?>>
 	<div<?php echo parlour_helper::container(); ?>>
 		<div<?php echo parlour_helper::grid(); ?>>
 			
 			<div class="uk-width-expand">
 				<main class="tm-content bdt-assistant-container">
 					<div class="uk-child-width-1-2@s uk-child-width-1-3@m" uk-grid>
 						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							
							<div>
	 							<?php get_template_part( 'template-parts/assistants/experiences/entry' ); ?>							
							</div>

 						<?php endwhile; endif; ?>
 					
 						<?php get_template_part( 'template-parts/pagination' ); ?>
 					</div>
 				</main> <!-- end main -->
 			</div> <!-- end content -->
 			

 			<?php if($position == 'sidebar-left' or $position == 'sidebar-right') : ?>
 				<aside<?php echo parlour_helper::sidebar($position); ?>>
 				    <?php get_sidebar(); ?>
 				</aside> <!-- end aside -->
 			<?php endif; ?>
 			
 		</div> <!-- end grid -->
 	</div> <!-- end container -->
 </div> <!-- end tm main -->
 	
 <?php get_footer(); ?>
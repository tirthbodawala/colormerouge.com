<?php
namespace ElementorExtend\Modules\Box\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Video_Lightbox extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'video-lightbox';
	}

	public function get_title() {
		return __( 'Video Lightbox', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-youtube';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
			]
		);

		$this->add_control(
			'source',
			[
				'label'       => __( 'Video Source', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'https://www.youtube.com/watch?v=za6LKKF0R5M', 'parlour-core' ),
				'placeholder' => __( 'https://www.youtube.com/watch?v=UWdxS38FLgw', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'poster',
			[
				'label' => __( 'Poster Image', 'parlour-core' ),
				'type' => Controls_Manager::MEDIA,
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => __( 'Title', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Your title here', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'subtitle',
			[
				'label'       => __( 'Subtitle', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Your subtitle here', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_style',
			[
				'label' => __( 'Style', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'content_align',
			[
				'label'        => __( 'Content Align', 'parlour-core' ),
				'type'         => Controls_Manager::SELECT,
				'default'      => 'center',
				'options'      => [
					'center-left'  => __('Left', 'parlour-core') ,
					'center-right' => __('Right', 'parlour-core') ,
					'center'       => __('Center', 'parlour-core') ,
				],
				'condition'    => [
					'poster[url]' => '',
				],
			]
		);

		$this->add_control(
			'button_bg',
			[
				'label'     => __( 'Button Background', 'parlour-core' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'default',
				'options'   => [
					'default'   => __( 'Default', 'parlour-core' ),
					'muted'     => __( 'Muted', 'parlour-core' ),
					'primary'   => __( 'Primary', 'parlour-core' ),
					'secondary' => __( 'Secondary', 'parlour-core' ),
					'custom'    => __( 'Custom', 'parlour-core' ),
					'none'      => __( 'No', 'parlour-core' ),
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'button_custom_bg',
			[
				'label'     => __( 'Button Custom Background', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'condition' => [
				'button_bg' => 'custom',
				],
			]
		);

		$this->add_control(
			'button_color',
			[
				'label'     => __( 'Button Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '#666666',
			]
		);

		$this->add_control(
			'button_padding',
			[
				'label'     => __( 'Button Padding', 'parlour-core' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'small',
				'options'   => [
					'small'  => __( 'Small', 'parlour-core' ),
					'medium' => __( 'Medium', 'parlour-core' ),
					'large'  => __( 'Large', 'parlour-core' ),
					'custom' => __( 'Custom', 'parlour-core' ),
					'none'   => __( 'No', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'button_custom_padding',
			[
				'label'       => __( 'Button Custom Padding', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( '20px 15px', 'parlour-core' ),
				'label_block' => true,
				'condition' => [
					'button_padding' => 'custom',
				],
			]
		);

		$this->add_control(
			'button_radius',
			[
				'label'     => __( 'Button Radius', 'parlour-core' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'circle',
				'options'   => [
					'rounded' => __( 'Rounded', 'parlour-core' ),
					'circle'  => __( 'Circle', 'parlour-core' ),
					'none'    => __( 'No', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Title Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-video-lightbox .bdt-video-lightbox-title' => 'color: {{VALUE}}',
				],
				'condition'    => [
					'title!' => '',
				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .bdt-video-lightbox .bdt-video-lightbox-title',
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'condition'    => [
					'title!' => '',
				],
			]
		);

		$this->add_control(
			'subtitle_color',
			[
				'label'     => __( 'Subtitle Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-video-lightbox .bdt-video-lightbox-subtitle' => 'color: {{VALUE}}',
				],
				'condition'    => [
					'subtitle!' => '',
				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'subtitle_typography',
				'selector' => '{{WRAPPER}} .bdt-video-lightbox .bdt-video-lightbox-subtitle',
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'condition'    => [
					'subtitle!' => '',
				],
			]
		);

		$this->end_controls_section();

	}

	private function get_shortcode() {
		$settings = $this->get_settings();

		$attributes = [
			'source'                => $settings['source'],
			'poster'                => $settings['poster']['id'],
			'title'                 => $settings['title'],
			'subtitle'              => $settings['subtitle'],
			'content_align'         => $settings['content_align'],
			'button_bg'             => $settings['button_bg'],
			'button_custom_bg'      => $settings['button_custom_bg'],
			'button_color'          => $settings['button_color'],
			'button_padding'        => $settings['button_padding'],
			'button_custom_padding' => $settings['button_custom_padding'],
			'button_radius'         => $settings['button_radius'],
		];

		$this->add_render_attribute( 'shortcode', $attributes );

		$shortcode = [];
		$shortcode[] = sprintf( '[bdt_video_lightbox %s]', $this->get_render_attribute_string( 'shortcode' ) );

		return implode("", $shortcode);
	}

	public function render() {
		echo do_shortcode( $this->get_shortcode() );
	}

	public function render_plain_content() {
		echo $this->get_shortcode();
	}
}

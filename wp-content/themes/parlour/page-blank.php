<?php 
/* Template Name: Blank Page */

get_header();

$bg_style         = get_theme_mod( 'parlour_body_bg_style');
//$mainbody_width = get_theme_mod( 'parlour_body_width');
$text             = get_theme_mod( 'parlour_body_txt_style' );


$class            = ['uk-section', 'uk-padding-remove-vertical'];
$class[]          = ($bg_style) ? 'uk-section-'.$bg_style : '';
$class[]          = ($text) ? 'uk-'.$text : '';


?>



<div<?php echo parlour_helper::attrs(['id' => $id, 'class' => $class]); ?>>
	<div class="">
		<main class="tm-home">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php the_content(); ?>

			<?php endwhile; endif; ?>
		</main> <!-- end main -->

	</div> <!-- end container -->
</div> <!-- end tm main -->
	
<?php get_footer(); ?>
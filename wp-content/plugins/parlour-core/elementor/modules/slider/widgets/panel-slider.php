<?php
namespace ElementorExtend\Modules\Slider\Widgets;

use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use ElementorExtend\Modules\QueryControl\Controls\Group_Control_Posts;
use ElementorExtend\Modules\QueryControl\Module;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class Panel Slider
 */
class Panel_Slider extends Widget_Base {

	/**
	 * @var \WP_Query
	 */
	private $_query = null;

	protected $_has_template_content = false;

	public function get_name() {
		return 'panel-slider';
	}

	public function get_title() {
		return __( 'Panel Slider', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-slider-push';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	public function get_script_depends() {
		return [ 'imagesloaded' ];
	}

	public function on_import( $element ) {
		if ( ! get_post_type_object( $element['settings']['posts_post_type'] ) ) {
			$element['settings']['posts_post_type'] = 'services';
		}

		return $element;
	}

	public function on_export( $element ) {
		$element = Group_Control_Posts::on_export_remove_setting_from_element( $element, 'posts' );
		return $element;
	}

	public function get_query() {
		return $this->_query;
	}

	protected function _register_controls() {
		$this->register_query_section_controls();
		//$this->register_meta_data_controls();
	}

	private function register_query_section_controls() {

		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Sliders', 'parlour-core' ),
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => __( 'Slider Items', 'parlour-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'tab_title'   => __( 'Slide #1', 'parlour-core' ),
						'tab_content' => __( 'I am item content. Click edit button to change this text.', 'parlour-core' ),
					],
					[
						'tab_title'   => __( 'Slide #2', 'parlour-core' ),
						'tab_content' => __( 'I am item content. Click edit button to change this text.', 'parlour-core' ),
					],
					[
						'tab_title'   => __( 'Slide #3', 'parlour-core' ),
						'tab_content' => __( 'I am item content. Click edit button to change this text.', 'parlour-core' ),
					],
					[
						'tab_title'   => __( 'Slide #4', 'parlour-core' ),
						'tab_content' => __( 'I am item content. Click edit button to change this text.', 'parlour-core' ),
					],
				],
				'fields' => [
					[
						'name'        => 'tab_title',
						'label'       => __( 'Title & Content', 'parlour-core' ),
						'type'        => Controls_Manager::TEXT,
						'default'     => __( 'Slide Title' , 'parlour-core' ),
						'label_block' => true,
					],
					[
						'name'  => 'tab_image',
						'label' => __( 'Image', 'parlour-core' ),
						'type'  => Controls_Manager::MEDIA,
					],
					[
						'name'       => 'tab_content',
						'label'      => __( 'Content', 'parlour-core' ),
						'type'       => Controls_Manager::WYSIWYG,
						'default'    => __( 'Slide Content', 'parlour-core' ),
						'show_label' => false,
					],
					[
						'name'        => 'tab_link',
						'label'       => __( 'Link', 'parlour-core' ),
						'type'        => Controls_Manager::URL,
						'placeholder' => 'http://your-link.com',
						'default'     => [
							'url' => '#',
						],
					],
				],
				'title_field' => '{{{ tab_title }}}',
			]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_responsive_control(
			'columns',
			[
				'label'          => __( 'Columns', 'parlour-core' ),
				'type'           => Controls_Manager::SELECT,
				'default'        => '3',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options'        => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'show_title',
			[
				'label'     => __( 'Show Title', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'default'   => 'yes',
				'label_off' => __( 'Off', 'parlour-core' ),
				'label_on'  => __( 'On', 'parlour-core' ),
			]
		);


		$this->add_control(
			'show_read_more',
			[
				'label'       => __( 'Show Button', 'parlour-core' ),
				'type'        => Controls_Manager::SWITCHER,
				'default'     => 'yes',
				'label_on'    => __( 'Off', 'parlour-core' ),
				'label_off'   => __( 'On', 'parlour-core' ),
				'description' => 'It will work when link field no null.',
			]
		);

		$this->add_control(
			'button_text',
			[
				'label' => __( 'Button Text', 'parlour-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Read More', 'parlour-core' ),
				'placeholder' => __( 'Read More', 'parlour-core' ),
				'condition' => [
					'show_read_more' => [ 'yes' ],
				],
			]
		);

		$this->add_control(
			'navigation',
			[
				'label'   => __( 'Navigation', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'both',
				'options' => [
					'both'   => __( 'Arrows and Dots', 'parlour-core' ),
					'arrows' => __( 'Arrows', 'parlour-core' ),
					'dots'   => __( 'Dots', 'parlour-core' ),
					'none'   => __( 'None', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);


		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'parlour-core' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left'    => [
						'title' => __( 'Left', 'parlour-core' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'parlour-core' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'parlour-core' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'parlour-core' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'default' => '',
			]
		);


		$this->end_controls_section();


		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'parlour-core' ),
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label' => __( 'Autoplay', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'parlour-core' ),
					'no' => __( 'No', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => __( 'Autoplay Speed', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'   => __( 'Infinite Loop', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'parlour-core' ),
					'no'  => __( 'No', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'speed',
			[
				'label' => __( 'Animation Speed', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 500,
				'frontend_available' => true,
			]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Items', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'background_color',
			[
				'label' => __( 'Background Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_4,
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-panel-slide-item' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .bdt-panel-slide-item .uk-overlay-gradient' => 'background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%);',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Title Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-panel-slide-item .bdt-panel-slide-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => __( 'Title Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-panel-slide-item .bdt-panel-slide-title',
			]
		);

		$this->add_control(
			'text_color',
			[
				'label' => __( 'Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-panel-slide-item .bdt-panel-slide-text' => 'color: {{VALUE}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'text_typography',
				'label' => __( 'Text Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-panel-slide-item .bdt-panel-slide-text',
			]
		);

		$this->add_control(
			'button_color',
			[
				'label' => __( 'Button Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-panel-slide-item .bdt-panel-slide-link' => 'color: {{VALUE}};',
				],
				'separator' => 'before',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_navigation',
			[
				'label' => __( 'Navigation', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'navigation' => [ 'arrows', 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_arrows',
			[
				'label' => __( 'Arrows', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_style',
			[
				'label' => __( 'Arrows Style', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'dark',
				'options' => [
					'dark' => __( 'Dark', 'parlour-core' ),
					'light' => __( 'Light', 'parlour-core' ),
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_position',
			[
				'label' => __( 'Arrows Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -100,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-panel-slider .swiper-button-next' => 'right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .bdt-panel-slider .swiper-button-prev' => 'left: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_size',
			[
				'label' => __( 'Arrows Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 25,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-panel-slider .swiper-button-prev, {{WRAPPER}} .bdt-panel-slider .swiper-button-next' => 'height: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}}',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_dots',
			[
				'label' => __( 'Dots', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_position',
			[
				'label' => __( 'Dots Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -80,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-panel-slider .swiper-pagination-bullets' => 'bottom: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_size',
			[
				'label' => __( 'Dots Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 5,
						'max' => 10,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-panel-slider .swiper-pagination-bullet' => 'height: {{SIZE}}{{UNIT}};width: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->end_controls_section();

	}
	
	protected function render_loop_header() {
		?>
		<div class="bdt-panel-slider">
			<div class="swiper-container">
		<?php
	}

	protected function render_loop_footer() {

		$settings = $this->get_settings();
		?>
			</div>

			<?php if ( 'none' !== $this->get_settings('navigation') ) : ?>
				<?php if ( 'arrows' !== $this->get_settings('navigation') ) : ?>
					<div class="swiper-pagination"></div>
				<?php endif; ?>
				
				<?php if ( 'dots' !== $this->get_settings('navigation') ) : 
					$nav_style = ($this->get_settings('arrows_style') == 'light') ? 'swiper-button-white' : 'swiper-button-black'; 
				?>
					<div class="swiper-button-next <?php echo esc_attr($nav_style); ?>"></div>
					<div class="swiper-button-prev <?php echo esc_attr($nav_style); ?>"></div>
				<?php endif; ?>
			<?php endif; ?>

		</div>

		<script>
			jQuery(document).ready(function($) {
			    "use strict";				    
			    var swiper = new Swiper(".elementor-element-<?php echo $this->get_id(); ?> .swiper-container", {
			        navigation: {
						nextEl: ".elementor-element-<?php echo $this->get_id(); ?> .swiper-button-nextt",
						prevEl: ".elementor-element-<?php echo $this->get_id(); ?> .swiper-button-prev",
					},
					pagination: {
						el        : ".elementor-element-<?php echo $this->get_id(); ?> .swiper-pagination",
						type      : 'bullets',
						clickable : true,
					},
			        "autoplay"      : <?php echo 'yes' === $settings['autoplay'] ? '{ "delay": ' . $settings['autoplay_speed'] . ' }' : 'false'; ?>,
			        "paginationClickable":true,
			        "loop": <?php echo ($this->get_settings('infinite') == 'yes') ? 'true' : 'false'; ?>,
			        "speed": <?php echo $this->get_settings('speed'); ?>,
			        "slidesPerView": <?php echo $this->get_settings('columns'); ?>,
			        "spaceBetween":  0,
			        "breakpoints" : {
			            "1024" : {
			            	"slidesPerView": <?php echo $this->get_settings('columns'); ?>,
			            },
			            "768" : {
			            	"slidesPerView": <?php echo $this->get_settings('columns_tablet'); ?>,
			            },
			            "640" : {
			            	"slidesPerView": <?php echo $this->get_settings('columns_mobile'); ?>,
			            }
			        }
			    });
			});
		</script>

		<?php
	}
	public function render() {
		$settings = $this->get_settings();
		$url      = $target = $link_title = '';
		$classes  = ['bdt-panel-slide-item', 'swiper-slide', 'uk-transition-toggle'];


		$this->render_loop_header();

		?>
		<div class="swiper-wrapper">
			<?php $counter = 1; ?>
			<?php foreach ( $settings['tabs'] as $item ) : ?>

				<?php 
					$image_src = wp_get_attachment_image_src( $item['tab_image']['id'], 'full' );
					$image     =  ($image_src) ? $image_src[0] : PARLOUR_CORE_URL.'/images/no-image.jpg';

				?>
				


						<div class="<?php echo implode(" ", $classes); ?>">
				        	<div class="bdt-panel-slide-thumb">
				        		<img src="<?php echo  $image; ?>" alt="<?php echo $item['tab_title']; ?>">
				        	</div>
				        	
				        	<div class="bdt-panel-slide-desc uk-position-bottom-left uk-position-z-index uk-light">

					        	<?php if (!empty( $item['tab_link']['url']) and ('yes' == $settings['show_read_more']))  : ?>
									<a href="<?php echo $item['tab_link']['url']; ?>" target="<?php echo $item['tab_link']['is_external']; ?>" class="bdt-panel-slide-link uk-margin-small-bottom uk-transition-slide-bottom uk-display-inline-block"><?php echo $settings['button_text']; ?> <span uk-icon="icon: arrow-right; ratio: 1.2"></span></a>
								<?php endif; ?>

								<?php if ( 'yes' == $settings['show_title'] ) : ?>
									<h3 class="bdt-panel-slide-title uk-margin-remove-bottom uk-margin-small-top uk-transition-slide-bottom"><?php echo $item['tab_title']; ?></h3>
								<?php endif; ?>

								<?php if ( '' !== $item['tab_content'] ) : ?>
									<div class="bdt-panel-slide-text uk-transition-slide-bottom"><?php echo $this->parse_text_editor( $item['tab_content'] ); ?></div>
								<?php endif; ?>

					  		</div>
							<div class="uk-transition-fade uk-position-cover uk-overlay uk-overlay-gradient"></div>

						</div>





			<?php
				$counter++;
			endforeach;
			?>
		</div>
		<?php

		$this->render_loop_footer();
	}
}

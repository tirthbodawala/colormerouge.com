<?php if(!is_single() or is_single()) :?>
	<?php if(!is_single()) :?>
	<h3 class="bdt-assistant-title uk-margin-remove-top uk-margin-small-bottom">
	    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="uk-link-reset"><?php the_title(); ?></a>
	    <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
	            <?php printf( '<span class="sticky-post featured">%s</span>', esc_html__( 'Featured', 'parlour') ); ?>
	    <?php endif; ?>
	</h3>
	
	<?php elseif(is_single()) :?>
		<div class="uk-margin-medium-bottom" uk-grid>
			<div class="uk-width-auto">
			    <h1 class="uk-article-title uk-margin-remove-top uk-heading-bullet">
			    	<?php the_title(); ?>
			    </h1>
			</div>
		    <div class="uk-width-expand uk-flex uk-flex-middle">
		    	<span class="uk-margin-small-right"><?php esc_html_e( 'Experiences in:', 'parlour'); ?></span> 
		    	<?php
	                $category = get_the_term_list(get_the_ID(),'experiences', '<ul class="tm-atb-experience uk-margin-remove-top"><li>', '</li><li>', '</li></ul>' );
	                echo  $category; // WPCS: XSS OK.
	            ?>
		    </div>
		</div>
	<?php endif ?>
<?php endif ?>
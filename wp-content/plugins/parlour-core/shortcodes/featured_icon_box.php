<?php
// BdThemes Featured Icon Box Shortcode

if (!function_exists('bdthemes_featured_icon_box_shortcode')) {
    function bdthemes_featured_icon_box_shortcode($atts = null, $content = null) {
        $atts = shortcode_atts(array(
			'title'             => 'Featured Icon Box Title',
			'title_size'        => '16px',
			'icon_type'         => 'uikit', // fontawesome, image
			'uikit_icon'        => 'home',
			'fontawesome_icon'  => 'fa fa-home',
			'image_icon'        => '',
			'background'        => 'none',
			'color'             => 'dark',
			'shadow'            => 'none',
			'hover_shadow'      => 'none',
            'url'               => '',
			'rel'               => '',
			'target'            => '_self',
			'css_animation'     => '',
			'class'             => ''
        ), $atts, 'bdt_icon_list_item');

        
        $id            = uniqid('bdtil_');
        $icon          = '';
        $output        = [];

        $classes           = array('bdt-featured-icon-box', 'uk-text-center', $atts['class']);


        if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $classes[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if ($atts['color']) {
            $classes[] = 'uk-'.$atts['color'];
        }
        if ($atts['shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-'.$atts['shadow'];
        }
        if ($atts['hover_shadow'] !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$atts['hover_shadow'];
        }


        // icon
        if ($atts['icon_type'] == 'uikit') {
            $icon = '<span class="list-img-icon" uk-icon="icon: '.$atts['uikit_icon'].'"></span>';
        } elseif ($atts['icon_type'] == 'fontawesome') {
            $icon = '<i class="list-img-icon '.$atts['fontawesome_icon'] . '"></i>';
        } else {
            $image_url = wp_get_attachment_image_src( $atts['image_icon'] );
            $image_url = $image_url[0];
            $icon = '<img class="list-img-icon" src="'.$image_url.'" alt="" />';
        }
        
        $output[] = '
            <div id="'.$id.'" class="'.bdt_acssc($classes).'">
                <div class="icon_list_item">
                    <div class="icon-list-icon-wrapper">
                        <div class="icon-list-icon">'
                            . $icon . '
                        </div>
                    </div>
                    <div class="icon-list-desc">
                        <h3>'.$atts['title'].'</h3>
                        <div class="icon_description_text">';
                         $output[] = do_shortcode($content);
                         if ($atts['url']) {
                            $rel =  ($atts['rel']) ? ' rel="'.$atts['rel'].'"' : '';
                            $output[] = '<a href="'.$atts['url'].'" target="'.$atts['target'].'"'.$rel.' class="icon-list-link"><span uk-icon="icon: arrow-right"></span></a>';
                         }
                         $output[] = '</div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>';

        return implode("\n", $output);
    }


    // end of icon_list_item shortcode
    add_shortcode('bdt_featured_icon_box', 'bdthemes_featured_icon_box_shortcode');
}
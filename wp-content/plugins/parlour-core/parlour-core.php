<?php
/*
Plugin Name: Parlour Core
Plugin URI: http://bdthemes.com/
Description: Core is BdThemes WordPress themes feature extending plugin that you give some extra features like shortcode, widgets etc.
Version: 1.2.3
Author: Bdthemes
Author URI: http://bdthemes.com/
Text Domain: parlour-core
Domain Path: /languages
License: GPL2

Parlour Core is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Parlour Core is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Parlour Core. If not, see {License URI}.
*/

define( 'PARLOUR_CORE_VER', '1.2.3' );
define( 'PARLOUR_CORE__FILE__', __FILE__ );
define( 'PARLOUR_CORE_URI', plugin_dir_path( PARLOUR_CORE__FILE__ ));
define( 'PARLOUR_CORE_URL', plugin_dir_url(PARLOUR_CORE__FILE__));

define( 'PARLOUR_CORE_EURI', plugin_dir_path( PARLOUR_CORE__FILE__ ) . 'elementor/' );
define( 'PARLOUR_CORE_EURL', PARLOUR_CORE_URL . 'elementor/' );
define( 'PARLOUR_CORE_EMURI', PARLOUR_CORE_EURI . 'modules/' );
define( 'PARLOUR_CORE_EMURL', PARLOUR_CORE_EURL . 'modules/' );

define( 'PARLOUR_CORE_ASSETS_URL', PARLOUR_CORE_URL . 'assets/' );
define( 'PARLOUR_CORE_DURL', PARLOUR_CORE_URL . 'includes/demo-importer/');
define( 'PARLOUR_CORE_DURI', PARLOUR_CORE_URI. 'includes/demo-importer/');


$plugin_dir_path = dirname(__FILE__);
$is_admin        = is_admin();

// Load language file
add_action( 'plugins_loaded', 'parlour_core_load_textdomain', 1 );
function parlour_core_load_textdomain() {
    load_plugin_textdomain( 'parlour-core', false, basename( dirname( __FILE__ ) ) . '/languages' );
}

function parlour_core_empty_paragraph_fix($content){
    $array = array (
        '<p>[' => '[',
        ']</p>' => ']',
        ']<br />' => ']'
    );

    $content = strtr($content, $array);
    return $content;
}

add_filter('the_content', 'parlour_core_empty_paragraph_fix');

//Remove e.g. from values
function parlour_core_arrangement_shortcode_value($value) {
    return preg_replace('/^e.g.\s*/', '', $value);
}

function parlour_core_arrangement_shortcode_arr_value(&$value) {
    $value = preg_replace('/^e.g.\s*/', '', $value);
}

if ( get_theme_mod( 'parlour_dev_mode', 1 ) ) {
    if (get_theme_mod( 'parlour_manual_import', 0 ) == 0) {
        require_once(dirname(__FILE__).'/includes/demo-importer/import.php');
    }
    require_once(dirname(__FILE__).'/includes/plugins/one-click-demo-import/one-click-demo-import.php');
}


require_once(dirname(__FILE__).'/includes/pagination.php');
// Helper function here
include(dirname(__FILE__).'/helper.php');

// Shortcodes function here
if(file_exists(dirname(__FILE__).'/shortcodes/')){  
    $shotcode_dir = scandir(dirname(__FILE__).'/shortcodes/');
    foreach ($shotcode_dir as $file){
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if(strtolower($ext) == 'php'){
            include_once(dirname(__FILE__).'/shortcodes/'.$file);
        }     
    }
}
//Override shortcode folder
if(file_exists(get_template_directory().'/inc/overrides/')){  
    $dir = scandir(get_template_directory().'/inc/overrides/');
    foreach ($dir as $file){
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if(strtolower($ext) == 'php'){
            require_once(get_template_directory().'/inc/overrides/'.$file);
        }     
    }
}

// Widgets functions here
if(file_exists(dirname(__FILE__).'/widgets/')){  
    $shotcode_dir = scandir(dirname(__FILE__).'/widgets/');
    foreach ($shotcode_dir as $file){
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if(strtolower($ext) == 'php'){
            include_once(dirname(__FILE__).'/widgets/'.$file);
        }     
    }
}


// Custom Excerpt Length
function parlour_core_custom_excerpt($limit=50) {
    return strip_shortcodes(wp_trim_words(get_the_content(), $limit, '...'));
}


function parlour_core_admin_styles() {
    wp_enqueue_style('style', PARLOUR_CORE_URL.'css/admin-settings.css',true);
}

add_action('admin_print_styles', 'parlour_core_admin_styles');





include_once(dirname(__FILE__).'/elementor/elementor.php');

// function add_elementor_page_settings_controls(  $page ) {
//     //\Elementor\PageSettings\Page
//    // print_r($page);
//     //return;
//     //
//     if(method_exists( $page, "add_control")){
//         $page->add_control(
//             'menu_item_color',
//             [
//                 'label' => __( 'Menu Item Color 2', 'elementor' ),
//                 'type' => \Elementor\Controls_Manager::COLOR,
//                 'selectors' => [
//                     '{{WRAPPER}} .menu-item a' => 'color: {{VALUE}}',
//                 ],
//             ]
//         );
//     }
// }

// add_action( 'elementor/element/page-settings/section_page_settings/before_section_end', 'add_elementor_page_settings_controls' );
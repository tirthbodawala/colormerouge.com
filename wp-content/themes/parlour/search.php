<?php 

get_header();

// Layout
$position = (is_active_sidebar('search-results-widgets')) ? get_theme_mod( 'parlour_page_layout', 'sidebar-right' ) : '';

?>

<div<?php echo parlour_helper::section(); ?>>
	<div<?php echo parlour_helper::container(); ?>>
		<div<?php echo parlour_helper::grid(); ?>>
			<div class="uk-width-expand">
				<main class="tm-content">

					<h3><?php esc_html_e('New Search', 'parlour') ?></h3>

					<p><?php esc_html_e('If you are not happy with the results below please do another search', 'parlour') ?></p>

					<?php get_search_form(); ?>

					<div class="uk-clearfix"></div>
				
					<hr class="uk-article-divider">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						
						<article id="post-<?php the_ID(); ?>" <?php post_class('uk-article entry-search'); ?>>						        
						    <div class="entry-wrap">

						        <div class="entry-title">
						            <h3 class="uk-article-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'parlour'), the_title_attribute('echo=0') ); ?>" rel="bookmark" class="uk-link-reset"><?php the_title(); ?></a></h3>
						        </div>

						        <div class="entry-type">
						        <?php if( get_post_type($post->ID) == 'post' ){ ?>
						        	<?php echo esc_html__('Blog Post', 'parlour'); ?>
						        <?php } elseif( get_post_type($post->ID) == 'page' ){ ?>
						        	<?php echo esc_html__('Page', 'parlour'); ?>
						        <?php } elseif( get_post_type($post->ID) == 'tribe_events' ){ ?>
						        	<?php echo esc_html__('Event', 'parlour'); ?>
						        <?php } elseif( get_post_type($post->ID) == 'campaign' ){ ?>
						        	<?php echo esc_html__('Campaign', 'parlour'); ?>
						        <?php } elseif( get_post_type($post->ID) == 'product' ){ ?>
						        	<?php echo esc_html__('Product', 'parlour'); ?>
						        <?php } ?>
						        </div>

					        	<?php if (parlour_custom_excerpt(100) != '') { ?>
										<div class="entry-content">
											<?php echo wp_kses_post(parlour_custom_excerpt(100)); ?>
										</div>
					        	<?php } ?>

					        	<?php
					        		$post_title = get_the_title();
				        		if (empty($post_title)) : ?>
						        		<p class="uk-article-meta">
						        			<?php if(get_the_date()) : ?>
						        				<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'parlour'), the_title_attribute('echo=0') ); ?>" rel="bookmark" class="uk-link-reset"><time><?php printf(get_the_date()); ?></time></a>
						        			<?php endif; ?>
						        			<?php if(get_the_author()) : ?>
						        		    <?php printf(esc_html__('Written by %s.', 'parlour'), '<a href="'.get_author_posts_url(get_the_author_meta('ID')).'" title="'.get_the_author().'">'.get_the_author().'</a>'); ?>
						        		    <?php endif; ?>

						        		    <?php if(get_the_category_list()) : ?>
						        		        <?php printf(esc_html__('Posted in %s', 'parlour'), get_the_category_list(', ')); ?>
						        		    <?php endif; ?>
						        		</p>
					        	<?php endif; ?>

						    </div>

						</article><!-- #post -->
						
					<?php endwhile; ?>
		
					<?php get_template_part( 'template-parts/pagination' ); ?>
	
					<?php else : ?>
						<div class="uk-alert uk-alert-warning uk-text-large"><?php esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'parlour') ?></div>
					<?php endif; ?>
				</main> <!-- end main -->
			</div> <!-- end content -->

			<?php if($position == 'sidebar-left' || $position == 'sidebar-right') : ?>
				<aside<?php echo parlour_helper::sidebar($position); ?>>
				    <?php get_sidebar(); ?>
				</aside> <!-- end aside -->
			<?php endif; ?>

		</div> <!-- end grid -->
	</div> <!-- end container -->
</div> <!-- end tm main -->
	
<?php get_footer(); ?>
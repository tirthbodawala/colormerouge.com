<?php


// Load the main stylesheet
add_action( 'wp_enqueue_scripts', 'parlour_theme_style' );
function parlour_theme_style() {
	wp_enqueue_style( 'parlour-theme-style', get_template_directory_uri() . '/style.css' );
}


// Load child theme's textdomain.
add_action( 'after_setup_theme', 'parlour_child_textdomain' );
function parlour_child_textdomain(){
   load_child_theme_textdomain( 'parlour', get_stylesheet_directory().'/languages' );
}


// Example code loading JS in footer. Uncomment to use.
 

/* ====== REMOVE COMMENT

add_action('wp_footer', 'parlourChildFooterScript' );
function parlourChildFooterScript(){

	echo '
	<script type="text/javascript">

	// Your JS code here

	</script>';

}
 ====== REMOVE COMMENT */

/* ======================================================== */



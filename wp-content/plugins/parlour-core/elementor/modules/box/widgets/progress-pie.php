<?php
namespace ElementorExtend\Modules\Box\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Progress_Pie extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'progress-pie';
	}

	public function get_title() {
		return __( 'Progress Pie', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-counter-circle';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'percent',
			[
				'label'       => __( 'Percent', 'parlour-core' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 75,
			]
		);

		$this->add_control(
			'duration',
			[
				'label'       => __( 'Duration', 'parlour-core' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 1,
			]
		);

		$this->add_control(
			'delay',
			[
				'label'       => __( 'Delay', 'parlour-core' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 1,
			]
		);

		$this->add_control(
			'step',
			[
				'label'       => __( 'Steps', 'parlour-core' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 1,
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => __( 'Progress Pie Title', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Your title text here', 'parlour-core' ),
				'default' => __( 'Progress Pie Title', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'before',
			[
				'label'       => __( 'Before Text', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Your before text here', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'text',
			[
				'label'       => __( 'Middle Text', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Your middle text here', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'after',
			[
				'label'       => __( 'After Text', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => __( 'Your after text here', 'parlour-core' ),
				'label_block' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Style', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'progress_pie',
			[
				'label' => __( 'Progress Pie', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'progress_background',
			[
				'label' => __( 'Pie Fill Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-progress-pie-wrapper .bdt-progress-pie svg ellipse' => 'stroke: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'progress_color',
			[
				'label' => __( 'Pie Bar Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-progress-pie-wrapper .bdt-progress-pie svg path' => 'stroke: {{VALUE}};',
				],
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
			]
		);

		$this->add_control(
			'before_text_color',
			[
				'label' => __( 'Before Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-progress-pie-before' => 'color: {{VALUE}};',
				],
				'condition' => [
					'before!' => '',
				],
			]
		);

		$this->add_control(
			'middle_text_color',
			[
				'label' => __( 'Middle Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-progress-pie-text' => 'color: {{VALUE}};',
				],
				'condition' => [
					'text!' => '',
				],

			]
		);

		$this->add_control(
			'number_color',
			[
				'label' => __( 'Percentage Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-progress-pie-number' => 'color: {{VALUE}};',
				],
				'condition' => [
					'text' => '',
				],

			]
		);

		$this->add_control(
			'after_text_color',
			[
				'label' => __( 'After Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-progress-pie-after' => 'color: {{VALUE}};',
				],
				'condition' => [
					'after!' => '',
				],
			]
		);


		$this->add_responsive_control(
			'progress_padding',
			[
				'label' => __( 'Padding', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .bdt-progress-pie-wrapper .bdt-progress-pie' => 'padding: {{SIZE}}{{UNIT}};',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
			]
		);


		$this->add_control(
			'line_width',
			[
				'label'       => __( 'Line Width', 'parlour-core' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 8,
			]
		);


		$this->add_control(
			'line_cap',
			[
				'label'     => __( 'Line Cap', 'parlour-core' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'round',
				'options'   => [
					'round' => __( 'Rounded', 'parlour-core' ),
					'square'  => __( 'Square', 'parlour-core' ),
					'butt'    => __( 'Butt', 'parlour-core' ),
				],
			]
		);



		$this->add_control(
			'progress_title',
			[
				'label' => __( 'Title', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'title!' => '',
				],
			]
		);

		$this->add_control(
			'title_background',
			[
				'label' => __( 'Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-progress-pie-wrapper .bdt-progress-pie-title' => 'background-color: {{VALUE}};  border-top: none;',
				],
				'condition' => [
					'title!' => '',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-progress-pie-wrapper .bdt-progress-pie-title' => 'color: {{VALUE}};',
				],
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'condition' => [
					'title!' => '',
				],
			]
		);

		$this->add_responsive_control(
			'title_padding',
			[
				'label' => __( 'Padding', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'selectors' => [
					'{{WRAPPER}} .bdt-progress-pie-wrapper .bdt-progress-pie-title' => 'padding-top: {{SIZE}}{{UNIT}}; padding-bottom: {{SIZE}}{{UNIT}};',
				],
				'default' => [
					'unit' => 'px',
				],
				'tablet_default' => [
					'unit' => 'px',
				],
				'mobile_default' => [
					'unit' => 'px',
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 35,
					],
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'selector' => '{{WRAPPER}} .bdt-progress-pie-wrapper .bdt-progress-pie-title',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'condition' => [
					'title!' => '',
				],
			]
		);


		$this->end_controls_section();

	}

	public function render() {
		$id         = uniqid('pp');
		$settings   = $this->get_settings();
		$before_txt = $mdl_text = $after_txt = $title = '';
		$css_class  = ['bdt-progress-pie', 'bdt-pp-lc-'.$settings['line_cap']];

		if (!$settings['text']) {
		    $css_class[] = 'bdt-pp-percent';
		}
		if ($settings['before'] !== '') {
		    $before_txt = '<div class="bdt-progress-pie-before"> '.$settings['before'].'</div>';
		}
		if ($settings['text'] !== '') {
		    $mdl_text = '<div class="bdt-progress-pie-text"> '.$settings['text'].'</div>';
		}
		if ($settings['after'] !== '') {
		    $after_txt = '<div class="bdt-progress-pie-after"> '.$settings['after'].'</div>';
		}
		if ($settings['title'] !== '') {
		    $title = '<h4 class="bdt-progress-pie-title uk-margin-small-top"> '.$settings['title'].'</h4>';
		}


		?>
				<div id="<?php echo $id; ?>_container" class="bdt-progress-pie-wrapper uk-text-center">
					<div id="<?php echo $id; ?>" 
					class="<?php echo implode(" ", $css_class); ?>" 
					role="progressbar" 
					data-goal="<?php echo $settings['percent']; ?>" 
					aria-valuemin="0" 
					data-step="<?php echo $settings['step']; ?>" 
					data-speed="<?php echo ($settings['duration']*15); ?>" 
					data-delay="<?php echo ($settings['delay']*1000); ?>" 
					data-barsize="<?php echo intval($settings['line_width']); ?>" 
					aria-valuemax="100">
					    <div class="bdt-progress-pie-label">
					        <?php 

					        echo $before_txt;

					        if ($mdl_text) {
					            echo $mdl_text;
					        } else { ?>
					            <div class="bdt-progress-pie-number"></div>
				        	<?php }
					        echo $after_txt; ?>
					    </div>
					</div>
						<?php echo $title; ?>
				</div>

				<script>
					jQuery(document).ready(function($) {
			    		"use strict";
				        var progressPie = "#<?php echo esc_attr($id); ?>";

				        $(progressPie).asPieProgress({
				            namespace: "pieProgress",
				            classes: {
				                svg: "bdt-progress-pie-svg",
				                number: "bdt-progress-pie-number",
				                content: "bdt-progress-pie-content"
				            }
				        })
				        $(progressPie).asPieProgress("start");
					});
				</script>
		<?php
	}
}

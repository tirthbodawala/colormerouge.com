<?php
// Bdthemes Post Block

if (!function_exists('bdthemes_post_block')) {
	function bdthemes_post_block($atts){
		extract(shortcode_atts(array(
			'post_type'     => 'post',
			'posts'         => '4',
			'categories'    => 'all',
			'color'         => 'dark',
			'orderby'       => 'date',
			'order'         => 'DESC',
			'thumb'         => 'yes',
			'title'         => 'yes',
			'meta'          => 'yes',
			'excerpt'       => 'yes',
			'excerpt_limit' => 20,
			
			'class'         => ''
		), $atts));

		

		global $post;
		$id      = uniqid('bdtpb_');
		$classes = ['bdt-post-block', 'uk-grid', 'uk-child-width-1-1@s', 'uk-child-width-1-2@m', $class];
		$output  = [];
		$media   = '';

		if ($post_type == 'services') {
			$taxonomy = 'service-categories';
		} else {
			$taxonomy = 'category';
		}

		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => $posts,
			'orderby'        => $orderby,
			'order'          => $order,
			'post_status'    => 'publish'
		);
		
		if($categories != 'all' && $categories != ''){
			$str = $categories;
			$arr = explode(',', $str); // string to array

			$args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		$content_width = ($thumb == 'yes') ? ' uk-width-1-2@m' : ' uk-width-expand';

		$wp_query = new WP_Query($args);

		if( $wp_query->have_posts() ) : 

			$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" uk-grid>';
		
			while ( $wp_query->have_posts() ) : $wp_query->the_post();

				if( $thumb == 'yes') {
			  		$blog_thumbnail= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

			  		if($blog_thumbnail[0] != ''){
			  
			  			$media = '<div class="uk-width-1-2@m"><a href="'.esc_url(get_permalink()).'" title="' . esc_attr(get_the_title()) . '" class="bdt-pb-thumb"><img src="'.esc_url($blog_thumbnail[0]).'" alt="' . esc_attr(get_the_title()) . '" /></a></div>';
			  		
			  		}
			  	}

		  		$output[] = '<div>';
			  		$output[] = '<div class="bdt-post-block-item" uk-grid>';
				  
						$output[] = $media;
				  		
				  		$output[] = '<div class="bdt-pb-desc uk-'.$color.$content_width.'">';


			            	if ( $title == 'yes' ) {
								$output[] = '<h4><a href="'.esc_url(get_permalink()).'" class="uk-link-reset" title="' . esc_attr(get_the_title()) . '">'.esc_html(get_the_title()) .'</a></h4>';
							}

							if ( $meta == 'yes' ) {
					  			if ($post_type == 'services') {
					  				$duration   = get_post_meta( get_the_ID(), 'bdthemes_service_duration', true );
					  				$price      = get_post_meta( get_the_ID(), 'bdthemes_service_price', true );
									$meta_list = '<li>'.esc_html__('Duration: ', 'parlour-core').$duration.'</li><li>
												'.esc_html__('Price: ', 'parlour-core').$price.'</li>';
												
								} else {
									$meta_list = '<li>'.esc_attr(get_the_date('M d, Y')).'</li><li>
												'.get_the_category_list(', ').'</li>';
								}
								
								$output[] = '<ul class="bdt-pb-meta uk-subnav uk-margin-remove-top uk-margin-small-bottom uk-text-muted uk-text-uppercase uk-link-reset">'.$meta_list.'</ul>';
							}

							if ( $excerpt == 'yes' ) {
								$output[] = '<div class="uk-margin-small-top">'.wp_kses_post(parlour_core_custom_excerpt((intval($excerpt_limit)))).'</div>';
							}	
				  		$output[] = '</div>';

					$output[] = '</div>';
				$output[] = '</div>';
		  
			endwhile;
		
			$output[] ='</div>';
		
		 	wp_reset_postdata();

		 endif;

		return implode("\n", $output);
	}
	add_shortcode('bdt_post_block', 'bdthemes_post_block');
}
<div class="custom-text">
<?php
if( get_theme_mod('parlour_toolbar_right_custom') ) {
	echo wp_kses_post(get_theme_mod('parlour_toolbar_right_custom'));
} ?>
</div>
<?php
// Bdthemes Post Carousel

if (!function_exists('bdthemes_service_carousel')) {
	function bdthemes_service_carousel($atts){
		extract(shortcode_atts(array(
			'categories'        => 'all',
			'posts'             => '6',
			'gutter'            => 'medium',
			'orderby'           => 'date',
			'order'             => 'DESC',
			'thumb'             => 'yes',
			'title'             => 'yes',
			'meta'              => 'yes',
			'read_more'        => 'yes',
			'navigation'        => 'yes',
			'pagination'        => 'no',
			'autoplay'          => 'no',
			'loop'              => 'yes',
			'shadow'            => 'small',
			'hover_shadow'      => 'none',
			'radius'            => 'no',
			'large'             => 3,
			'medium'            => 2,
			'small'             => 1,
			'align'            => 'center',
			'css_animation'     => '',
			'class'             => ''
		), $atts));

		

		global $post;
		$id 			= uniqid('bdtsc_');
		$output         = [];
		$classes        = ['bdt-service-carousel', 'uk-position-relative', $class, $css_animation];
		$css_class      = ['swiper-slide', 'tm-service-item', 'service-align-'.$align, 'uk-background-default', 'uk-dark'];
		$css_class2     = ['bdt-pc-desc', 'uk-position-relative', 'uk-padding'];
		
		if ($gutter     == 'large') { $gutter = 50; }
		elseif ($gutter == 'medium') { $gutter = 25;}
		elseif ($gutter == 'small') { $gutter = 10;}
		elseif ($gutter == 'collapse') { $gutter = 0;}
		else { $gutter  = 35; }

		$css_class[]       = ($radius == 'yes') ? 'uk-border-rounded' : '';
	
		if ($shadow !=='none') {
            $css_class[] = 'uk-box-shadow-'.$shadow;
        }		
		if ($hover_shadow !=='none') {
            $css_class[] = 'uk-box-shadow-hover-'.$hover_shadow;
        }

		$args = array(
			'post_type'      => 'services',
			'posts_per_page' => $posts,
			'orderby'        => $orderby,
			'order'          => $order,
			'post_status'    => 'publish'
		);
		
		if($categories != 'all' && $categories != ''){
			$str = $categories;
			$arr = explode(',', $str); // string to array

			$args['tax_query'][] = array(
				'taxonomy' => 'service-categories',
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		$wp_query = new WP_Query($args);

		if( $wp_query->have_posts() ) :

			$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'">';
				$output[] = '<div class="swiper-container" >';
					$output[] = '<div class="swiper-wrapper">';
				
						while ( $wp_query->have_posts() ) : $wp_query->the_post();

					  		$output[] = '<div class="'.bdt_acssc($css_class).'">';
					  
						  		if( $thumb == 'yes') {
							  		$blog_thumbnail= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );

							  		if($blog_thumbnail[0] != ''){
							  			$output[] = '<div class="service-thumbnail uk-position-relative uk-overflow-hidden">';
							  				$output[] = '<div class="service-thumbnail-design">';
									  			$output[] = '<div class="">
									  				        <a href="'.get_the_permalink().'" title="'.get_the_title().'">';
									  				            $output[] = '<img src="'.$blog_thumbnail[0].'" alt="">';
									  				        $output[] = '</a>
									  								</div>';
								  				$output[] = '<div class="tm-service-overlay uk-position-cover uk-overlay uk-overlay-gradient uk-position-z-index"></div>';
								  			$output[] = '</div>';
								  		$output[] = '</div>';
							  		}
							  	}
					  		
					  		$output[] = '<div class="'.esc_attr(implode(' ', $css_class2)).'">';

								if ( $meta == 'yes' ) {

									    if( $title==='yes') { 
									        $output[] = '<h3 class="tm-service-title uk-margin-remove-top uk-margin-small-bottom"><a href="'.get_permalink().'" title="'.get_the_title().'" class="uk-link-reset">'.get_the_title().'</a></h3>';
									    }

									    if( $meta==='yes') {
									        $duration = get_post_meta( get_the_ID(), 'bdthemes_service_duration', true );
									        $charge   = get_post_meta( get_the_ID(), 'bdthemes_service_price', true );
									        
									        $output[] = '<ul class="tm-service-meta uk-subnav uk-flex-'.$align.' uk-margin-small-top uk-margin-remove-bottom" uk-margin>
									            <li><span>'.esc_html('Duration: ', 'parlour-core').$duration.'</span></li>
									            <li><span>'.esc_html('Charge: ', 'parlour-core').$charge.'</span></li>
									        </ul>';
									    }

									    $order_link   = get_post_meta( get_the_ID(), 'bdthemes_service_order_link', true ); 
									    $output[] = '<a href="'.esc_url($order_link).'" class="tm-service-item-cart-link uk-icon uk-icon-button uk-transition-slide-bottom uk-margin-small-right" title="'.esc_html('Order Service', 'parlour-core').'"><span uk-icon="icon: cart"></span></a>';

									    $output[] = '<a href="'.esc_url(get_permalink()).'" class="tm-service-item-link uk-icon uk-icon-button" title="'.esc_attr(get_the_title()).'"><span uk-icon="icon: arrow-right"></a>';
								}
							  		
						  		$output[] = '</div>';
							$output[] = '</div>';
					  
						endwhile;

					$output[] = '</div>';
				$output[] = '</div>';
					$output[] = ($pagination == 'yes') ? '<div class="swiper-pagination"></div>' : '';
					$output[] = ($navigation == 'yes') ? '<div class="swiper-button-next"></div><div class="swiper-button-prev"></div>' : '';
		
			$output[] = '</div>';
		
		 	wp_reset_postdata();

		 	$settings = [
				'navigation' => [
					'nextEl' => '#'.$id.' .swiper-button-nextt',
					'prevEl' => '#'.$id.' .swiper-button-prev',
				],
				'pagination' => [
					'el'        => '#'.$id.' .swiper-pagination',
					'type'      => 'bullets',
					'clickable' => true,
				],
		        'autoplay'      => $autoplay ? [ 'delay' => 4500 ] : false,
				'paginationClickable'          => true,
				'loop'                         => ($loop == 'yes') ? true : false,
				'autoplayDisableOnInteraction' => false,
				'slidesPerView'                => $large,
				'spaceBetween'                 => $gutter,
				'breakpoints'                  => [
		            1024 => [
		                'slidesPerView' => $large,
		                'spaceBetween' => $gutter
		            ],
		            768 => [
		                'slidesPerView' => $medium,
		                'spaceBetween' => $gutter
		            ],
		            640 => [
		                'slidesPerView' => $small,
		                'spaceBetween' => $gutter
		            ]
				]

		 	];
		 	$output[] = '<script type="text/javascript">';
		 	$output[] = '
		 	jQuery(document).ready(function($) {
    			"use strict";
			 	var swiper = new Swiper("#'.$id.' .swiper-container", '.json_encode($settings).');
			});';
    		$output[] = '</script>';
	  
		endif;

		return implode("\n", $output);
	}
	add_shortcode('bdt_service_carousel', 'bdthemes_service_carousel');
}
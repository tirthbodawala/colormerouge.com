<?php
// Bdthemes Countdown Shortcode

if (!function_exists('bdthemes_countdown')) {
	function bdthemes_countdown($atts) {
        $count_style = $datetime = $timezone = $count_color = $count_size = $count_p_color = $count_p_size = $prefix = $suffix = $class = $post_id = $background = $custom_background = $padding = $custom_padding = $shadow = $border = $radius = '';
        extract(shortcode_atts( array(
            'datetime'          => '2020-05-01T00:00:00', // example: 2020-05-01T00:00:00
            'post_id'           => '',
            'count_size'        => 'default',  // default, medium, small, large
            'count_color'       => 'dark',  // dark, light
            'time_name'         => 'yes',  // yes, no
            'prefix'            => '',
            'suffix'            => '',
            'layout'            => 'vertical', // vertical, horizontal
            'text'              => 'dark', // dark, light
            'background'        => 'none', // default, primary, secondary, muted, none
            'custom_background' => '',
            'padding'           => 'none',
            'custom_padding'    => '',
            'shadow'            => 'none',
            'hover_shadow'      => 'none',
            'border'            => '',
            'align'             => 'left', // left, right, center
            'radius'            => 'no',
            'css_animation'     => '',
            'class'             => ''
        ),$atts));

        
        
        $title             = $post = $post_end_date = $url = '';
        $output            = [];
        $btn_size          = ($count_size != 'default') ? 'uk-button-'. $count_size : '';
        
        $classes           = array('bdt-countdown', $class);
        $btn_class         = ['donate-button', 'uk-button', 'uk-button-primary', $btn_size];
        $tick_class        = ['tick'];
        $grid_class        = ['uk-grid'];
        
        $datetime          = $datetime;
        $prefix            = $prefix;        
        
        $custom_background = ($custom_background) ? 'background:' . $custom_background. ';' : '';
        $custom_padding    = ($custom_padding) ? 'padding:' . $custom_padding. ';' : '';
        $border            = ($border) ? 'border:' . $border. ';' : '';
        $classes[]         = ($radius == 'yes') ? 'uk-border-rounded' : '';
        $classes[]         = ($align) ? 'uk-text-'.$align : '';
        $classes[]         = ($text) ? 'uk-'.$text : '';
        $grid_class[]      = ($layout == 'vertical') ? 'uk-child-width-1-1' : '';
        $grid_class[]      = ($layout == 'horizontal') ? 'uk-flex-middle' : '';
        $ver_margin        = ($layout == 'vertical') ? 'uk-grid-margin' : '';
        
        $btn_class[]       = ($radius == 'yes') ? 'uk-border-rounded' : '';
        $tick_class[]      = ($count_size) ? 'tick-'.$count_size : 'tick-default';
        $tick_class[]      = ($count_color) ? 'tick-'.$count_color : '';
        $tick_class[]      = $ver_margin;


        if (($background !=='custom') and ($background !=='none')) {
            $classes[] = ($background) ? 'uk-background-'.$background : '';
        }
        if (($padding !=='custom') and ($padding !=='none')) {
            $classes[] = ($padding =='medium') ? 'uk-padding' : 'uk-padding-'.$padding;
        }
        if ($shadow !=='none') {
            $classes[] = 'uk-box-shadow-'.$shadow;
        }
        if ($hover_shadow !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$hover_shadow;
        }


        $output[] = '<div class="'.bdt_acssc($classes).'" style="'.$custom_background.$custom_padding.$border.'">';
            $output[] = '<div class="'.bdt_acssc($grid_class).'" uk-grid>';

                if($datetime!='') {
                    if ($prefix != '') {
                        $output[] ='<div class="'.$ver_margin.'"><h3 class="bdt_prefix-text">'.$prefix.'</h3></div>';
                    }

                    $output[] ='<div class="'.bdt_acssc($tick_class).'" data-did-init="handleTickInit">';
                        $output[] ='<div data-layout="horizontal '.$align.'" data-repeat="true"
                                        data-transform="rotate( 
                                            transform( plural(day, days), input() ), 
                                            transform( plural(hour, hours), input() ),
                                            transform( plural(minute, minutes), input() ),
                                            transform( plural(second, seconds), input() )
                                         ) -> map( keys( label, value ) )">';

                            $output[] ='<div class="tick-group">';
                                $output[] = ($time_name == 'yes') ? '<div data-key="label" data-view="text"></div>' : '';

                                $output[] ='<div data-key="value" data-repeat="true" data-transform="pad(00) -> split -> delay">
                                    <span data-view="flip"></span>';
                                $output[] ='</div>';
                            $output[] ='</div>';
                        $output[] ='</div>';
                    $output[] ='</div>';
                    

                    if ($post_id) {
                        $output[] ='<div class="countdown-button '.$ver_margin.'"><a title="'.esc_html__( 'Donate Now', 'parlour-core' ).'" class="'.bdt_acssc($btn_class).'" href="'.$url.'">'.esc_html__( 'Donate Now', 'parlour-core' ).' <span class="uk-margin-small-left" uk-icon="icon: heart"></span></a></div>';
                    } elseif ($suffix != '') {
                        $output[] ='<div><h3 class="bdt_suffix-text">'.$suffix.'</h3></div>';
                    }
                    
                    $output[] ='<script>
                                    
                                        function handleTickInit(tick) {
                                            var nextYear = "'.esc_attr($datetime).'";
                                            var counter = Tick.count.down(nextYear);
                                            counter.onupdate = function(value) {
                                                tick.value = value;
                                            };
                                        }
                                    
                                </script>';
                }
                else {
                    $output[] = '<strong>'.esc_html__('You did not enter any date of custom date or campaign ID', 'parlour-core').'</strong>';
                }
            $output[] ='</div>';
        $output[] ='</div>';


        wp_enqueue_style('tick');
        wp_enqueue_script('tick');

        return implode("\n", $output);
    }
    add_shortcode('bdt_countdown', 'bdthemes_countdown');
}
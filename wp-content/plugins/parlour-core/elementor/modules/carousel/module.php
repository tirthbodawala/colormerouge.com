<?php
namespace ElementorExtend\Modules\Carousel;

use ElementorExtend\Base\Module_Base;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); }

class Module extends Module_Base {

	public function get_name() {
		return 'carousel';
	}

	public function get_widgets() {

		$widgets = [
			'Post_Carousel',
		];

		if(is_plugin_active('bdthemes-assistants/bdthemes-assistants.php')) {
			$widgets[] = 'Assistant_Carousel';
		}

		if(is_plugin_active('bdthemes-services/bdthemes-services.php')) {
			$widgets[] = 'Service_Carousel';
		}

		if(is_plugin_active( 'bdthemes-testimonials/bdthemes-testimonials.php' ) || is_plugin_active( 'jetpack/jetpack.php' )) {
			$widgets[] = 'Testimonial_Carousel';
		}
		
		return $widgets;
	}
}

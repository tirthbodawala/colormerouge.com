<?php
// Bdthemes Post Carousel

if (!function_exists('bdthemes_post_carousel')) {
	function bdthemes_post_carousel($atts){
		extract(shortcode_atts(array(
			'posts'             => '6',
			'categories'        => 'all',
			'large'             => 3,
			'medium'            => 2,
			'small'             => 1,
			'navigation'        => 'no',
			'pagination'        => 'yes',
			'autoplay'          => 'no',
			'loop'              => 'yes',
			'background'        => 'default',
			'custom_background' => '',
			'color'             => 'dark',
			'padding'           => 'medium',
			'custom_padding'    => '',
			'shadow'            => 'small',
			'hover_shadow'      => 'none',
			'radius'            => 'no',
			'orderby'           => 'date',
			'order'             => 'DESC',
			'thumb'             => 'yes',
			'tags'              => 'yes',
			'title'             => 'yes',
			'meta'              => 'yes',
			'excerpt'           => 'yes',
			'read_more'         => 'yes',
			'gutter'            => 'medium',
			'css_animation'     => '',
			'class'             => ''
		), $atts));

		

		global $post;

		$id                = uniqid('bdtpc_');
		
		$classes           = ['bdt-post-carousel', 'uk-position-relative', $class, $css_animation];
		$css_class         = ['swiper-slide'];
		$css_class2        = ['bdt-pc-desc'];
		
		$output            = [];
		
		if ($gutter        == 'large') { $gutter = 50; }
		elseif ($gutter    == 'medium') { $gutter = 35;}
		elseif ($gutter    == 'small') { $gutter = 10;}
		
		$css_class[]       = ($radius == 'yes') ? 'uk-border-rounded uk-overflow-hidden' : '';
		$custom_background = ($custom_background) ? 'background:' . $custom_background. ';' : '';
		$custom_padding    = ($custom_padding) ? 'padding:' . $custom_padding. ';' : '';

		if (($background !=='custom') and ($background !=='none')) {
            $css_class[] = ($background) ? 'uk-background-'.$background : '';
        }
        if ($color) {
            $css_class[] = 'uk-'.$color;
        }		
		if ($shadow !=='none') {
            $css_class[] = 'uk-box-shadow-'.$shadow;
        }		
		if ($hover_shadow !=='none') {
            $css_class[] = 'uk-box-shadow-hover-'.$hover_shadow;
        }
        if (($padding !=='custom') and ($padding !=='none')) {
            $css_class2[] = ($padding =='medium') ? 'uk-padding' : 'uk-padding-'.$padding;
        }

		$args = array(
			'post_type'      => 'post',
			'posts_per_page' => $posts,
			'orderby'        => $orderby,
			'order'          => $order,
			'post_status'    => 'publish'
		);
		
		if($categories != 'all' && $categories != ''){
			$str = $categories;
			$arr = explode(',', $str); // string to array

			$args['tax_query'][] = array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		$wp_query = new WP_Query($args);

		if( $wp_query->have_posts() ) :

			$output[] = '<div id ="'.$id.'" class="'.bdt_acssc($classes).'">';
				$output[] = '<div class="swiper-container" >';
					$output[] = '<div class="swiper-wrapper">';
				
						while ( $wp_query->have_posts() ) : $wp_query->the_post();

					  		$output[] = '<div class="'.bdt_acssc($css_class).'" style="'.$custom_background.'">';
					  
						  		if( $thumb == 'yes') {
							  		$blog_thumbnail= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );

							  		if($blog_thumbnail[0] != ''){
							  			$output[] = '<div class="tm-blog-thumbnail">';
								  			$output[] = '<a href="'.esc_url(get_permalink()).'" title="' . esc_attr(get_the_title()) . '" class="bdt-pc-thumb"><img src="'.esc_url($blog_thumbnail[0]).'" alt="' . esc_attr(get_the_title()) . '" /></a>';
								  			$output[] = '<img class="tm-blog-entry-overlay" src="' . get_template_directory_uri() . '/images/blog-entry-overlay.svg" alt="">';
								  		$output[] = '</div>';
							  		}
							  	}
					  		
					  		$output[] = '<div class="'.esc_attr(implode(' ', $css_class2)).'" style="'.$custom_padding.'">';

					  			if ( $tags == 'yes' ) {
						  			$tags_list = get_the_tag_list( '<span class="bdt-pc-tag">', '</span> <span class="bdt-pc-tag">', '</span>');
					                if ( $tags_list ) {
					                    $output[] = '<p class="bdt-pc-tags uk-position-medium uk-position-top-right" >'. wp_kses_post($tags_list) . '</p>'; // WPCS: XSS OK.
					                }
				            	}

				            	if ( $title == 'yes' ) {
									$output[] = '<h4 class="uk-margin-small-bottom uk-margin-small-top"><a href="'.esc_url(get_permalink()).'" title="' . esc_attr(get_the_title()) . '" class="uk-link-reset">'.esc_html(get_the_title()) .'</a></h4>';
								}

								if ( $meta == 'yes' ) {
									$output[] = '<ul class="uk-subnav uk-margin-small-top uk-subnav-divider" uk-margin><li><span>'.esc_html__('Posted:', 'parlour-core').' '.esc_attr(get_the_date()).'</span></li><li><span>'.sprintf(esc_html__('By: %s', 'parlour-core'), '<a href="'.get_author_posts_url(get_the_author_meta('ID')).'" title="'.get_the_author().'" class="uk-link-reset">'.get_the_author().'</a>').'</span></li></ul>';
								}

								if ( $excerpt == 'yes' ) {
									$output[] = '<div>'.wp_kses_post(parlour_core_custom_excerpt(15)).'</div>';
								}

								if ( $read_more == 'yes') {
									$output[] = '<a href="'.esc_url(get_permalink()).'" class="uk-button uk-padding-remove-horizontal uk-margin-top uk-margin-small-right uk-button-text">'.esc_html('Read More', 'parlour-core').'<span uk-icon="icon: arrow-right"></span></a>';
								}
							  		
						  		$output[] = '</div>';
							$output[] = '</div>';
					  
						endwhile;

					$output[] = '</div>';
				$output[] = '</div>';

				$output[] = ($pagination == 'yes') ? '<div class="swiper-pagination"></div>' : '';
				$output[] = ($navigation == 'yes') ? '<div class="swiper-button-next"></div><div class="swiper-button-prev"></div>' : '';
		
			$output[] = '</div>';
		
		 	wp_reset_postdata();

		 	$settings = [
				'navigation' => [
					'nextEl' => '#'.$id.' .swiper-button-nextt',
					'prevEl' => '#'.$id.' .swiper-button-prev',
				],
				'pagination' => [
					'el'        => '#'.$id.' .swiper-pagination',
					'type'      => 'bullets',
					'clickable' => true,
				],
		        'autoplay'      => $autoplay ? [ 'delay' => 4500 ] : false,
				'paginationClickable'          => true,
				'loop'                         => ($loop == 'yes') ? true : false,
				'autoplayDisableOnInteraction' => false,
				'slidesPerView'                => $large,
				'spaceBetween'                 => $gutter,
				'breakpoints'                  => [
		            1024 => [
		                'slidesPerView' => $large,
		                'spaceBetween' => $gutter
		            ],
		            768 => [
		                'slidesPerView' => $medium,
		                'spaceBetween' => $gutter
		            ],
		            640 => [
		                'slidesPerView' => $small,
		                'spaceBetween' => $gutter
		            ]
				]

		 	];
		 	$output[] = '<script type="text/javascript">';
		 	$output[] = '
		 	jQuery(document).ready(function($) {
    			"use strict";
			 	var swiper = new Swiper("#'.$id.' .swiper-container", '.json_encode($settings).');
			});';
    		$output[] = '</script>';
	  
		endif;

		return implode("\n", $output);
	}
	add_shortcode('bdt_post_carousel', 'bdthemes_post_carousel');
}
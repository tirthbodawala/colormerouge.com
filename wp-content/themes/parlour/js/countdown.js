(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})(
    {
        1: [function(require, module, exports) {
            var ElementorExtendFrontend = function($) {
                var self = this;
                    
                this.modules = {};
    
                var handlers = {
                    countdown: require('modules/countdown/assets/js/frontend/frontend'),
                };
    
                var initModules = function() {
                    self.modules = {};
                    
                    $.each(handlers, function(moduleName) {
                        self.modules[moduleName] = new this($);
                    });
                };
    
                this.init = function() {
                    $(window).on('elementor/frontend/init', initModules);
                };
    
                this.init();
            };
    
            window.ElementorExtendFrontend = new ElementorExtendFrontend(jQuery);
    
        }, { "modules/countdown/assets/js/frontend/frontend": 2 }],
        2: [function(require, module, exports) {
            module.exports = function() {
                elementorFrontend.hooks.addAction('frontend/element_ready/countdown.default', require('./handlers/countdown'));
            };
    
        }, { "./handlers/countdown": 3 }],
        3: [function(require, module, exports) {
            var Countdown = function($countdown, endTime, $) {
                var timeInterval,
                    elements = {
                        $daysSpan: $countdown.find('.bdt-countdown-days'),
                        $hoursSpan: $countdown.find('.bdt-countdown-hours'),
                        $minutesSpan: $countdown.find('.bdt-countdown-minutes'),
                        $secondsSpan: $countdown.find('.bdt-countdown-seconds')
                    };
    
                var updateClock = function() {
                    var timeRemaining = Countdown.getTimeRemaining(endTime);
    
                    $.each(timeRemaining.parts, function(timePart) {
                        var $element = elements['$' + timePart + 'Span'],
                            partValue = this.toString();
    
                        if (1 === partValue.length) {
                            partValue = 0 + partValue;
                        }
    
                        if ($element.length) {
                            $element.text(partValue);
                        }
                    });
    
                    if (timeRemaining.total <= 0) {
                        clearInterval(timeInterval);
                    }
                };
    
                var initializeClock = function() {
                    updateClock();
    
                    timeInterval = setInterval(updateClock, 1000);
                };
    
                initializeClock();
            };
    
            Countdown.getTimeRemaining = function(endTime) {
                var timeRemaining = endTime - new Date(),
                    seconds = Math.floor((timeRemaining / 1000) % 60),
                    minutes = Math.floor((timeRemaining / 1000 / 60) % 60),
                    hours = Math.floor((timeRemaining / (1000 * 60 * 60)) % 24),
                    days = Math.floor(timeRemaining / (1000 * 60 * 60 * 24));
    
                if (days < 0 || hours < 0 || minutes < 0) {
                    seconds = minutes = hours = days = 0;
                }
    
                return {
                    total: timeRemaining,
                    parts: {
                        days: days,
                        hours: hours,
                        minutes: minutes,
                        seconds: seconds
                    }
                };
            };
    
            module.exports = function($scope, $) {
                var $element = $scope.find('.bdt-countdown-wrapper'),
                    date = new Date($element.data('date') * 1000);
    
                new Countdown($element, date, $);
            };
    
        }, {}],   
    }

,{},[1]);

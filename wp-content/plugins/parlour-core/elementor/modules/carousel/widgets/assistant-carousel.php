<?php
namespace ElementorExtend\Modules\Carousel\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class Assistant Carousel
 */
class Assistant_Carousel extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'assistant-carousel';
	}

	public function get_title() {
		return __( 'Assistant Carousel', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-gallery-grid';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->register_query_section_controls();
	}

	private function register_query_section_controls() {
		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
			]
		);

		$this->add_responsive_control(
			'columns',
			[
				'label' => __( 'Columns', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'item_gap',
			[
				'label' => __( 'Item Gap', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 35,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'posts_per_page',
			[
				'label' => __( 'Limit', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 8,
			]
		);

		$this->add_control(
			'show_title',
			[
				'label' => __( 'Show Title', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_off' => __( 'Off', 'parlour-core' ),
				'label_on' => __( 'On', 'parlour-core' ),
			]
		);

		$this->add_control(
			'show_meta',
			[
				'label' => __( 'Show Meta', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_off' => __( 'Off', 'parlour-core' ),
				'label_on' => __( 'On', 'parlour-core' ),
			]
		);

		$this->add_control(
			'show_social_link',
			[
				'label'        => __( 'Social Link Icon', 'parlour-core' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => __( 'Show', 'parlour-core' ),
				'label_off'    => __( 'Hide', 'parlour-core' ),
				'return_value' => 'yes',
				'default'      => 'yes',
			]
		);

		$this->add_control(
			'navigation',
			[
				'label'   => __( 'Navigation', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'both',
				'options' => [
					'both'   => __( 'Arrows and Dots', 'parlour-core' ),
					'arrows' => __( 'Arrows', 'parlour-core' ),
					'dots'   => __( 'Dots', 'parlour-core' ),
					'none'   => __( 'None', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_query',
			[
				'label' => __( 'Query', 'parlour-core' ),
			]
		);

		$this->add_control(
			'source',
			[
				'label' => _x( 'Source', 'Posts Query Control', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Show All', 'parlour-core' ),
					'by_name' => __( 'Manual Selection', 'parlour-core' ),
				],
				'label_block' => true,
			]
		);

		$categories = get_terms( 'experiences' );

		$options = [];
		foreach ( $categories as $category ) :
			$options[ $category->slug ] = $category->name;
		endforeach;

		$this->add_control(
			'categories',
			[
				'label' => __( 'Categories', 'parlour-core' ),
				'type' => Controls_Manager::SELECT2,
				'options' => $options,
				'default' => [],
				'label_block' => true,
				'multiple' => true,
				'condition' => [
					'source' => 'by_name',
				],
			]
		);

		$this->add_control(
			'hide_empty',
			[
				'label' => __( 'Hide Empty', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => 'Hide',
				'label_off' => 'Show',
				'return_value' => 'yes',
			]
		);

		$this->add_control(
			'advanced',
			[
				'label' => __( 'Advanced', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => __( 'Order By', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'post_date',
				'options' => [
					'post_date'  => __( 'Date', 'parlour-core' ),
					'post_title' => __( 'Title', 'parlour-core' ),
					'menu_order' => __( 'Menu Order', 'parlour-core' ),
					'rand'       => __( 'Random', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label' => __( 'Order', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'desc',
				'options' => [
					'asc' => __( 'ASC', 'parlour-core' ),
					'desc' => __( 'DESC', 'parlour-core' ),
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'parlour-core' ),
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label' => __( 'Autoplay', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'parlour-core' ),
					'no' => __( 'No', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => __( 'Autoplay Speed', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
				'frontend_available' => true,
				'condition' => [
					'autoplay' => 'yes',
				],
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'   => __( 'Infinite Loop', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'parlour-core' ),
					'no'  => __( 'No', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'speed',
			[
				'label' => __( 'Animation Speed', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 500,
				'frontend_available' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_items',
			[
				'label' => __( 'Items', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'item_background',
			[
				'label' => __( 'Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-content-wrapper' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-content-wrapper .uk-overlay-gradient' => 'background: -moz-linear-gradient(top, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%); background: -webkit-linear-gradient(top, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%); background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%);',
				],
			]
		);

		$this->add_responsive_control(
			'border_radius',
			[
				'label' => __( 'Border Radius', 'elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel-item .bdt-assistant-carousel-content-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
					'{{WRAPPER}} .bdt-assistant-carousel-item .bdt-assistant-carousel-content-wrapper .assistant-thumbnail .assistant-thumbnail-design' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} 0 0;',


				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'border',
				'label' => __( 'Border Color', 'parlour-core' ),
				'placeholder' => '1px',
				'default' => '1px',
				'selector' => '{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-content-wrapper',
				'condition' => [
					'border_radius!' => null,
				],
			]
		);
		
		$this->add_control(
			'item_hover_border_color',
			[
				'label' => __( 'Hover Border Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'border_border!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-content-wrapper:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'item_box_shadow',
				'selector' => '{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-content-wrapper',
			]
		);

		$this->add_responsive_control(
			'desc_padding',
			[
				'label' => __( 'Description Padding', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-desc' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'alignment',
			[
				'label'       => __( 'Alignment', 'parlour-core' ),
				'type'        => Controls_Manager::CHOOSE,
				'separator' => 'before',
				'label_block' => false,
				'options'     => [
					'left'        => [
						'title'       => __( 'Left', 'parlour-core' ),
						'icon'        => 'fa fa-align-left',
					],
				'center'      => [
					'title'       => __( 'Center', 'parlour-core' ),
					'icon'        => 'fa fa-align-center',
					],
				'right'       => [
					'title'       => __( 'Right', 'parlour-core' ),
					'icon'        => 'fa fa-align-right',
					],
				],
				'default'     => 'center',
				'separator' => 'before',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_title',
			[
				'label'     => __( 'Title', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_title' => 'yes',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-item .bdt-assistant-carousel-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => __( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-item .bdt-assistant-carousel-title',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_meta',
			[
				'label'     => __( 'Meta', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_meta' => 'yes',
				],
			]
		);

		$this->add_control(
			'meta_color',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-item .bdt-assistant-carousel-meta li a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'meta_separator_color',
			[
				'label' => __( 'Separator Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-item .bdt-assistant-carousel-content-wrapper .bdt-assistant-carousel-meta li:not(:last-child)::after' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'meta_typography',
				'label' => __( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-item .bdt-assistant-carousel-meta li a',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_icon',
			[
				'label' => __( 'Icon', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_social_link' => 'yes',
				],
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-icon a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'hover_icon_color',
			[
				'label' => __( 'Hover Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .bdt-assistant-carousel-icon a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_navigation',
			[
				'label' => __( 'Navigation', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'navigation' => [ 'arrows', 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_arrows',
			[
				'label' => __( 'Arrows', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_style',
			[
				'label' => __( 'Arrows Style', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'dark',
				'options' => [
					'dark' => __( 'Dark', 'parlour-core' ),
					'light' => __( 'Light', 'parlour-core' ),
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_position',
			[
				'label' => __( 'Arrows Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -100,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .swiper-button-next' => 'right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .bdt-assistant-carousel .swiper-button-prev' => 'left: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_size',
			[
				'label' => __( 'Arrows Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 25,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .swiper-button-prev, {{WRAPPER}} .bdt-assistant-carousel .swiper-button-next' => 'height: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}}',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_dots',
			[
				'label' => __( 'Dots', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_position',
			[
				'label' => __( 'Dots Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -80,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .swiper-pagination-bullets' => 'bottom: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_size',
			[
				'label' => __( 'Dots Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 5,
						'max' => 10,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-carousel .swiper-pagination-bullet' => 'height: {{SIZE}}{{UNIT}};width: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->end_controls_section();
	}

	public function render() {
		$settings  = $this->get_settings();

		global $post;
		$id         = $this->get_id();

		$args = array(
			'post_type'      => 'assistants',
			'posts_per_page' => ($settings['posts_per_page']),
			'orderby'        => $settings['orderby'],
			'order'          => $settings['order'],
			'post_status'    => 'publish',
		);
			
		if('by_name' === $settings['source']) :
			$args['tax_query'][] = array(
				'taxonomy' => 'experiences',
				'field'    => 'slug',
				'terms'    => $settings['categories'],
			);
		endif;

		$wp_query = new \WP_Query($args);

		if( $wp_query->have_posts() ) : ?>
			<div id="<?php echo $id; ?>" class="bdt-assistant-carousel">
				<div class="swiper-container">
					<div class="bdt-assistant-carousel-container swiper-wrapper">
						<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
							<div class="bdt-assistant-carousel-item-wrapper swiper-slide">
								<div class="bdt-assistant-carousel-item assistant-align-<?php echo $settings['alignment']; ?>">
									<div class="bdt-assistant-carousel-content-wrapper uk-background-default uk-box-shadow-small">
									
								    	<?php if (has_post_thumbnail()) : ?>
									        <div class="bdt-assistant-carousel-thumbnail-wrapper assistant-thumbnail uk-position-relative uk-overflow-hidden">
									            <div class="bdt-assistant-carousel-thumbnail assistant-thumbnail-design">
													<?php $images = rwmb_meta( 'bdthemes_assistant_altimg', 'type=image_advanced&size=medium' ); ?>

													<?php if (has_post_thumbnail() and empty($images) ) : ?>
													    <div class="">
												            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
												                <?php echo  the_post_thumbnail('medium', array('class' => 'bdt-assistant-carousel-thumbnail'));  ?>
												            </a>
													    </div>
													<?php elseif (!empty($images)) : ?>
												        <div class="assistant-image-gallery uk-position-relative uk-overflow-hidden" uk-toggle="target: > .assistant-img-flip; mode: hover; animation: uk-animation-fade; queued: true; duration: 300">
											                <div class="assistant-img-flip uk-position-absolute uk-position-z-index">
											                    <?php echo  the_post_thumbnail('medium', array('class' => 'bdt-assistant-carousel-thumbnail'));  ?>
											                </div>
											                <?php foreach ( $images as $image) : ?> 
											                    <div class="assistant-img">
											                    <a href="<?php echo esc_url($image['full_url']); ?>" title="<?php echo esc_attr($image['title']); ?>">
											                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" width="<?php echo esc_attr($image['width']); ?>" height="<?php echo esc_attr($image['height']); ?>" class="bdt-assistant-carousel-thumbnail" />
											                    </a>
											                    </div>
											                <?php endforeach; ?>
												        </div>
													<?php endif ?>

									                <div class="bdt-assistant-carousel-overlay uk-position-cover uk-overlay uk-overlay-gradient uk-position-z-index"></div>
									            </div>  
									        </div>
									    <?php endif; ?>

									    <?php if(( 'yes' == $settings['show_title'] ) || ( 'yes' == $settings['show_meta'] ) || ( 'yes' == $settings['show_social_link'] )) : ?>
									        <div class="bdt-assistant-carousel-desc uk-padding uk-position-relative">

									            <?php if( 'yes' == $settings['show_title'] ) : ?>
									                <h3 class="bdt-assistant-carousel-title tm-assistant-title uk-text-<?php echo $settings['alignment']; ?> uk-margin-remove-top uk-margin-small-bottom">
													    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="uk-link-reset"><?php the_title(); ?></a>
													</h3>
									            <?php endif; ?>

									            <?php if( 'yes' == $settings['show_meta']) :
									               echo get_the_term_list(get_the_ID(),'experiences', '<ul class="bdt-assistant-carousel-meta uk-text-'.$settings['alignment'].' uk-margin-small-top uk-margin-remove"><li>', '</li><li>', '</li></ul>' );
									            endif; ?>

									            <?php if( 'yes' == $settings['show_social_link'] ) :
									                $social_link = get_post_meta( get_the_ID(), 'bdthemes_assistant_social_link', true );
									                if($social_link != null and is_array($social_link)) : ?>
									                	<div class="bdt-assistant-carousel-icon uk-position-absolute uk-position-medium uk-margin-remove-vertical uk-position-bottom-<?php echo $settings['alignment']; ?>">
									                		<ul class="uk-list uk-margin-remove-bottom">
									                	    <?php foreach ($social_link as $link) : ?>
									                	        <?php $tooltip = ucfirst(\bdthemes_helper::icon($link)); ?>
									                	        <li class="uk-display-inline-block">
									                	            <a<?php echo \bdthemes_helper::attrs(['href' => $link, 'class' => 'uk-margin-small-right']); ?> uk-icon="icon: <?php echo \bdthemes_helper::icon($link); ?>" title="<?php echo esc_html($tooltip); ?>" uk-tooltip></a>
									                	        </li>
									                	    <?php endforeach ?>
									                	    </ul>
									                	</div>
									                <?php endif;
									            endif; ?>
									        </div>
									    <?php endif; ?>
									</div>
								</div>
							</div>
						<?php endwhile; ?>					
						<?php wp_reset_postdata(); ?> 
					</div>
				</div>

				<?php if ( 'none' !== $this->get_settings('navigation') ) : ?>
					<?php if ( 'arrows' !== $this->get_settings('navigation') ) : ?>
						<div class="swiper-pagination"></div>
					<?php endif; ?>
					
					<?php if ( 'dots' !== $this->get_settings('navigation') ) : 
						$nav_style = ($this->get_settings('arrows_style') == 'light') ? 'swiper-button-white' : 'swiper-button-black'; 
					?>
						<div class="swiper-button-next <?php echo esc_attr($nav_style); ?>"></div>
						<div class="swiper-button-prev <?php echo esc_attr($nav_style); ?>"></div>
					<?php endif; ?>
				<?php endif; ?>

			</div>
			
			<script>
			jQuery(document).ready(function($) {
			    "use strict";				    
			    var swiper = new Swiper(".elementor-element-<?php echo $id; ?> .swiper-container", {
			        navigation: {
						nextEl: ".elementor-element-<?php echo $id; ?> .swiper-button-nextt",
						prevEl: ".elementor-element-<?php echo $id; ?> .swiper-button-prev",
					},
					pagination: {
						el        : ".elementor-element-<?php echo $id; ?> .swiper-pagination",
						type      : 'bullets',
						clickable : true,
					},
			        "autoplay"      : <?php echo 'yes' === $settings['autoplay'] ? '{ "delay": ' . $settings['autoplay_speed'] . ' }' : 'false'; ?>,
			        "paginationClickable":true,
			        "loop": <?php echo ($this->get_settings('infinite') == 'yes') ? 'true' : 'false'; ?>,
			        "speed": <?php echo $this->get_settings('speed'); ?>,
			        "slidesPerView": <?php echo $this->get_settings('columns'); ?>,
			        "spaceBetween":  <?php echo $this->get_settings('item_gap')['size']; ?>,
			        "breakpoints" : {
			            "1024" : {
			            	"slidesPerView": <?php echo $this->get_settings('columns'); ?>,
			            	"spaceBetween": <?php echo $this->get_settings('item_gap')['size']; ?>,
			            },
			            "768" : {
			            	"slidesPerView": <?php echo $this->get_settings('columns_tablet'); ?>,
			            	"spaceBetween": <?php echo $this->get_settings('item_gap')['size']; ?>,
			            },
			            "640" : {
			            	"slidesPerView": <?php echo $this->get_settings('columns_mobile'); ?>,
			            	"spaceBetween": <?php echo $this->get_settings('item_gap')['size']; ?>,
			            }
			        }
			    });
			});
		</script>
	<?php endif;
	}
}

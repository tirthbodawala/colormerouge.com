<?php

// Options
$logo            = get_theme_mod('parlour_logo_mobile');;
$mobile          = get_theme_mod('mobile', []);
$logo_align      = get_theme_mod('parlour_mobile_logo_align', 'center');
$menu_align      = get_theme_mod('parlour_mobile_menu_align', 'left');
$search_align    = get_theme_mod('parlour_mobile_search_align', 'right');
$offcanvas_style = get_theme_mod('parlour_mobile_offcanvas_style', 'offcanvas');
$offcanvas_mode  = get_theme_mod('parlour_mobile_offcanvas_mode', 'slide');
$menu_text       = get_theme_mod('parlour_mobile_menu_text');
$shadow          = get_theme_mod('parlour_header_shadow', 'special');
$break_point     = 'uk-hidden@'.get_theme_mod('parlour_mobile_break_point', 'm');
$class           = ['tm-header-mobile', $break_point];
$class[]         = ($shadow) ? 'uk-box-shadow-'.$shadow : '';


$offcanvas_color = get_theme_mod( 'parlour_offcanvas_color', 'dark' );
$offcanvas_color = ($offcanvas_color !== 'custom') ? 'uk-'.$offcanvas_color : 'custom-color';


$search_align = false; // TODO

?>
<div<?php echo parlour_helper::attrs(['class' => $class]) ?>>
    <nav class="uk-navbar-container" uk-navbar>

        <?php if ($logo_align == 'left' || $menu_align == 'left' || $search_align == 'left') : ?>
        <div class="uk-navbar-left">

            <?php if ($menu_align == 'left') : ?>
            <a class="uk-navbar-toggle" href="#tm-mobile" uk-toggle<?php echo ($offcanvas_style == 'dropdown') ? '="animation: true"' : '' ?>>
                <span uk-navbar-toggle-icon></span>
                <?php if ($menu_text) : ?>
                <span class="uk-margin-small-left"><?php esc_html_e('Menu', 'parlour') ?></span>
                <?php endif ?>
            </a>
            <?php endif ?>

            <?php if ($search_align == 'left') : ?>
            <a class="uk-navbar-item"><?php esc_html_e('Search', 'parlour') ?></a>
            <?php endif ?>

            <?php if ($logo_align == 'left') : ?>
            <?php get_template_part( 'template-parts/logo-mobile' ); ?>
            <?php endif ?>

        </div>
        <?php endif ?>

        <?php if ($logo_align == 'center') : ?>
        <div class="uk-navbar-center">
            <?php get_template_part( 'template-parts/logo-mobile' ); ?>
        </div>
        <?php endif ?>

        <?php if ($logo_align == 'right' || $menu_align == 'right' || $search_align == 'right') : ?>
        <div class="uk-navbar-right">

            <?php if ($logo_align == 'right') : ?>
            <?php get_template_part( 'template-parts/logo-mobile' ); ?>
            <?php endif ?>

            <?php if ($search_align == 'right') : ?>
            <a class="uk-navbar-item"><?php esc_html_e('Search', 'parlour') ?></a>
            <?php endif ?>

            <?php if ($menu_align == 'right') : ?>
            <a class="uk-navbar-toggle" href="#tm-mobile" uk-toggle<?php echo ($offcanvas_style) == 'dropdown' ? '="animation: true"' : '' ?>>
                <?php if ($menu_text) : ?>
                <span class="uk-margin-small-right"><?php esc_html_e('Menu', 'parlour') ?></span>
                <?php endif ?>
                <span uk-navbar-toggle-icon></span>
            </a>
            <?php endif ?>

        </div>
        <?php endif ?>

    </nav>

    <?php if ($shadow == 'special') : ?>
        <div class="tm-header-shadow">
            <div></div>
        </div>
    <?php endif; ?>

    <?php if (is_active_sidebar('offcanvas') or has_nav_menu('offcanvas')) :

        if ($offcanvas_style == 'offcanvas') : ?>
        <div id="tm-mobile" class="<?php echo esc_attr($offcanvas_color); ?>" uk-offcanvas mode="<?php echo esc_html($offcanvas_mode); ?>" overlay>
            <div class="uk-offcanvas-bar uk-dark">
                <?php get_template_part( 'template-parts/offcanvas' ); ?>
            </div>
        </div>
        <?php endif ?>

        <?php if ($offcanvas_style == 'modal') : ?>
        <div id="tm-mobile" class="uk-modal-full <?php echo esc_attr($offcanvas_color); ?>" uk-modal>
            <div class="uk-modal-dialog uk-modal-body">
                <button class="uk-modal-close-full" type="button" uk-close></button>
                <div class="uk-flex uk-flex-center uk-flex-middle" uk-height-viewport>
                    <?php get_template_part( 'template-parts/offcanvas' ); ?>
                </div>
            </div>
        </div>
        <?php endif ?>

        <?php if ($offcanvas_style == 'dropdown') : ?>
        <div class="uk-position-relative uk-position-z-index">
            <div id="tm-mobile" class="uk-box-shadow-medium<?php echo ($offcanvas_mode == 'slide') ? ' uk-position-top' : '' ?> <?php echo esc_attr($offcanvas_color); ?>" hidden>
                <div class="uk-background-default uk-padding">
                    <?php get_template_part( 'template-parts/offcanvas' ); ?>
                </div>
            </div>
        </div>
        <?php endif ?>

    <?php else : ?>
        <div id="tm-mobile" class="<?php echo esc_attr($offcanvas_color); ?>" uk-offcanvas mode="<?php echo esc_html($offcanvas_mode); ?>" overlay>
            <div class="uk-offcanvas-bar">
                <?php esc_html_e( 'Ops! You don\'t have any menu or widget in Off-canvas. Please add some menu in Off-canvas menu position or add some widget in Off-canvas widget position for view them here.', 'parlour' ); ?>
            </div>
        </div>
    <?php endif; ?>
</div>
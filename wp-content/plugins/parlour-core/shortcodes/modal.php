<?php
// BdThemes Modal Shortcode

if (!function_exists('bdthemes_modal')) {
    function bdthemes_modal($atts = null, $content = null) {
        extract(shortcode_atts(array(
            'id'                 => uniqid('bdtmod_'),
            'btn_style'          => 'default',
            'btn_text'           => 'Open Modal',
            'btn_icon'           => 'no',
            'btn_radius'         => 'no',
            'icon_type'          => 'uikit', // fontawesome, image
            'uikit_icon'         => 'home',
            'fontawesome_icon'   => 'fa fa-home',
            'image_icon'         => '',
            'size'               => 'default', // default, container, full
            'center'             => 'no',
            'title'              => 'Your Modal Title',
            'title_align'        => 'left', // 
            'footer_align'       => 'center', // 
            'close_btn_position' => 'default',
            'footer'             => '',
            //'background'       => 'default', // default, primary, secondary, muted
            'css_animation'      => '',
            'class'              => ''
        ), $atts));

        
        
        $btn_class     = ['bdt-modal-button', 'uk-button', 'uk-button-'.$btn_style, $css_animation];
        $btn_class[]   = ($btn_radius == 'yes') ? 'uk-border-rounded' : '';
        $classes       = ['bdt-modal', $class];
        
        $output        = [];
        $icon          = '';
        
        $settings = [
            'center' => ($center == 'yes') ? true : false,
        ];

        $classes[]          = ( $size == 'default' ) ? 'uk-modal' : 'uk-modal-'.$size ;
        $classes[]          = ( $size == 'full' ) ? 'uk-background-default' : '' ;
        $close_btn_position = ( $size == 'full' ) ? 'full' : $close_btn_position ;


        if ($btn_icon == 'yes') {
            // icon
            if ($icon_type == 'uikit') {
                $icon = '<span class="bdt-modal-btn-icon uk-margin-small-left" uk-icon="icon: '.$uikit_icon.'" ></span>';
            } elseif ($icon_type == 'fontawesome') {
                wp_enqueue_style( 'font-awesome' );
                $icon = '<i class="bdt-modal-btn-icon uk-margin-small-left '.$fontawesome_icon . '"></i>';
            } else {
                $image_url = wp_get_attachment_image_src( $image_icon, 'large' );
                $image_url = $image_url[0];
                $icon = '<img class="bdt-modal-btn-icon uk-margin-small-left" src="'.$image_url.'" alt="" />';
            }
        }

        //This is a button toggling the modal
        $output[] = '<button class="'.bdt_acssc($btn_class).'" type="button" uk-toggle="target: #'.$id.'">'.$btn_text.$icon.'</button>';

        // This is the modal
        $output[] = '<div id="'.$id.'" class="'. bdt_acssc($classes) .'" uk-modal=\''.json_encode($settings).'\'>
                        <div class="uk-modal-dialog">';
                            
                $output[] = '<button class="uk-modal-close-'.$close_btn_position.'" type="button" uk-close></button>';
                
                if ($title) {
                    $output[] = '<div class="uk-modal-header uk-text-'.$title_align.'"><h2 class="uk-modal-title">'.$title.'</h2></div>';
                }
                
                $output[] = '<div class="uk-modal-body" uk-overflow-auto>'.do_shortcode( $content ).'</div>';

                if ($footer) {
                    $output[] = '<div class="uk-modal-footer uk-text-'.$footer_align.'">'.$footer.'</div>';
                }

        $output[] = '</div></div>';

        return implode("", $output);
    }
    // end of Modal shortcode

    add_shortcode('bdt_modal', 'bdthemes_modal');
}

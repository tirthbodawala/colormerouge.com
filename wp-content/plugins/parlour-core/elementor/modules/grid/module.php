<?php
namespace ElementorExtend\Modules\Grid;

use ElementorExtend\Base\Module_Base;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); }

class Module extends Module_Base {

	public function get_name() {
		return 'grid';
	}

	public function get_widgets() {

		$widgets = [];

		if(is_plugin_active('bdthemes-assistants/bdthemes-assistants.php')) {
			$widgets[] = 'Assistant_Grid';
		}

		if(is_plugin_active('bdthemes-services/bdthemes-services.php')) {
			$widgets[] = 'Service_Grid';
		}

		return $widgets;
	}
}

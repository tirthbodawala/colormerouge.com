<?php

/**
 * Admin related stylesheets
 * @return [type] [description]
 */
function parlour_admin_style() {
	wp_register_style( 'admin-setting', get_template_directory_uri() . '/admin/css/admin-settings.css' );
	wp_enqueue_style( 'admin-setting' );
}
add_action( 'admin_enqueue_scripts', 'parlour_admin_style' );


/**
 * Admin related scripts
 * @return [type] [description]
 */
function parlour_admin_script() {
	wp_register_script('admin-setting', get_template_directory_uri() . '/admin/js/admin-settings.js', array( 'jquery' ), PARLOUR_VER, true);

	wp_enqueue_script('admin-setting');
}
add_action( 'admin_enqueue_scripts', 'parlour_admin_script' );


/**
 * Site Stylesheets
 * @return [type] [description]
 */
function parlour_styles() {

	$css_dir = get_template_directory_uri().'/css/';
	$rtl_enabled = is_rtl();

	// Load Visual composer CSS first so it's easier to override
	if ( function_exists( 'vc_map' ) ) {
		wp_enqueue_style( 'js_composer_front' );
	}
	// Deregister Composer Custom CSS
	wp_deregister_style( 'js_composer_custom_css' );

	// Load Primary Stylesheet
	if ($rtl_enabled) {
		wp_enqueue_style( 'theme-style', $css_dir.'theme.rtl.css', array(), PARLOUR_VER, 'all' );
	} else {
		wp_enqueue_style( 'theme-style', $css_dir.'theme.css', array(), PARLOUR_VER, 'all' );
	}
	wp_enqueue_style( 'parlour-style', get_stylesheet_uri(), array(), PARLOUR_VER, 'all' );

}  
add_action( 'wp_enqueue_scripts', 'parlour_styles' ); 


/**
 * Site Scripts
 * @return [type] [description]
 */
function parlour_scripts() {

	$dev_mode      = get_theme_mod('parlour_dev_mode', true);
	$preloader     = get_theme_mod('parlour_preloader');
	$js_dir        = get_template_directory_uri() . '/js/';
	$vendor_js_dir = get_template_directory_uri() . '/inc/vendor/uikit/js/';

	if ($preloader) {
		wp_enqueue_script('please-wait', $js_dir.'please-wait.min.js', array(), PARLOUR_VER, false);
	}

	if ($dev_mode) {
		// Dequeue some for double load
		wp_dequeue_script('isotope');

		// Register Script
		wp_register_script('uikit', $vendor_js_dir.'uikit.js', array( 'jquery' ), PARLOUR_VER, false);
		wp_register_script('uikit-icons', $vendor_js_dir.'uikit-icons.min.js', array( 'uikit' ), PARLOUR_VER, true);
		wp_register_script('appear', $js_dir.'jquery.appear.js', array( 'jquery' ), PARLOUR_VER, true);
		wp_register_script('fitvids', $js_dir.'fitvids.js', array( 'jquery' ), PARLOUR_VER, true);
		wp_register_script('flip-countdown', $js_dir.'flip.min.js', array( 'jquery' ), PARLOUR_VER, true);
		wp_register_script('cookie-bar', $js_dir.'jquery.cookiebar.js', array( 'jquery' ), PARLOUR_VER, true);
		
		wp_register_script('progress-pie', $js_dir.'jquery.asPieProgress.min.js', array( 'jquery' ), PARLOUR_VER, true);
		wp_register_script('countdown', $js_dir.'countdown.js', array( 'jquery' ), PARLOUR_VER, true);
		wp_register_script('count-up', $js_dir.'jquery.countup.js', array( 'jquery' ), PARLOUR_VER, true);
		wp_register_script('tm-isotope', $js_dir.'isotope.pkgd.min.js', array( 'jquery' ), PARLOUR_VER, true);
		wp_register_script('flickr', $js_dir.'jquery.flickr.js', array( 'jquery' ), PARLOUR_VER, true);
		wp_register_script('swiper', $js_dir.'swiper.jquery.min.js', array( 'jquery' ), PARLOUR_VER, true);
		wp_register_script('event-move', $js_dir.'jquery.event.move.js', array( 'jquery' ), PARLOUR_VER, true);
		wp_register_script('twentytwenty', $js_dir.'jquery.twentytwenty.js', array( 'jquery' ), PARLOUR_VER, true);
		
		wp_register_script('parlour-script', $js_dir.'theme.js', array( 'uikit' ), PARLOUR_VER, true);


		// Enqueue
		wp_enqueue_script('uikit');
		wp_enqueue_script('uikit-icons');
		wp_enqueue_script('appear');
		wp_enqueue_script('fitvids');
		wp_enqueue_script('flip-countdown');
		wp_enqueue_script('cookie-bar');
		wp_enqueue_script('progress-pie');
		wp_enqueue_script('countdown');
		wp_enqueue_script('count-up');
		wp_enqueue_script('tm-isotope');
		wp_enqueue_script('flickr');
		wp_enqueue_script('swiper');

		wp_enqueue_script('event-move');
		wp_enqueue_script('twentytwenty');
		
		wp_enqueue_script('parlour-script');
	} else {
		wp_enqueue_script('uikit', $vendor_js_dir.'uikit.min.js', array( 'jquery' ), PARLOUR_VER, false);
		wp_enqueue_script('parlour-script', $js_dir.'theme.min.js', array( 'uikit' ), PARLOUR_VER, true);
	}

  	// Load WP Comment Reply JS
  	if(is_singular()) { wp_enqueue_script( 'comment-reply' ); }
}
add_action( 'wp_enqueue_scripts', 'parlour_scripts' );  
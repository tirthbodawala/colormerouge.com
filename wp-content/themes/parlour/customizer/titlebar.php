<?php
function parlour_customize_register_titlebar($wp_customize) {
	//header section
	$wp_customize->add_section('header', array(
		'title' => esc_attr__('Titlebar', 'parlour'),
		'priority' => 31
	));

	$wp_customize->add_setting('parlour_global_header', array(
		'default' => 'title',
		'sanitize_callback' => 'parlour_sanitize_choices'
	));
	$wp_customize->add_control('parlour_global_header', array(
		'label'    => esc_attr__('Titlebar Layout', 'parlour'),
		'section'  => 'header',
		'settings' => 'parlour_global_header', 
		'type'     => 'select',
		'priority' => 1,
		'choices'  => array(
			'title'               => esc_attr__('Titlebar (Left Align)', 'parlour'),
			'featuredimagecenter' => esc_attr__('Titlebar (Center Align)', 'parlour'),
			'notitle'             => esc_attr__('No Titlebar', 'parlour')
		)
	));


	$wp_customize->add_setting('parlour_titlebar_style', array(
		'default' => 'titlebar-dark',
		'sanitize_callback' => 'parlour_sanitize_choices'
	));
	$wp_customize->add_control('parlour_titlebar_style', array(
		'label'    => esc_attr__('Titlebar Style', 'parlour'),
		'section'  => 'header',
		'settings' => 'parlour_titlebar_style', 
		'type'     => 'select',
		'priority' => 1,
		'choices'  => array(
			'titlebar-dark' => esc_attr__('Dark (for dark backgrounds)', 'parlour'),
			'titlebar-light' => esc_attr__('Light (for light backgrounds)', 'parlour')
		)
	));

	$wp_customize->add_setting( 'parlour_titlebar_bg_image' , array(
		'sanitize_callback' => 'esc_url'
	));
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'parlour_titlebar_bg_image', array(
		'priority' => 1,
	    'label'    => esc_attr__( 'Titlebar Background', 'parlour' ),
	    'section'  => 'header',
	    'settings' => 'parlour_titlebar_bg_image'
	)));

	$wp_customize->add_setting('parlour_blog_title', array(
		'default' => esc_attr__('Blog', 'parlour'),
		'sanitize_callback' => 'esc_attr'
	));
	$wp_customize->add_control('parlour_blog_title', array(
		'priority' => 2,
	    'label'    => esc_attr__('Blog Title: ', 'parlour'),
	    'section'  => 'header',
	    'settings' => 'parlour_blog_title'
	));

	if (class_exists('Woocommerce')){
		$wp_customize->add_setting('parlour_woocommerce_title', array(
			'default' => esc_attr__('Shop', 'parlour'),
			'sanitize_callback' => 'esc_attr'
		));
		$wp_customize->add_control('parlour_woocommerce_title', array(
			'priority' => 3,
		    'label'    => esc_attr__('WooCommerce Title: ', 'parlour'),
		    'section'  => 'header',
		    'settings' => 'parlour_woocommerce_title'
		));
	}
	
	$wp_customize->add_setting('parlour_right_element', array(
		'default' => 'back_button',
		'sanitize_callback' => 'parlour_sanitize_choices'
	));
	$wp_customize->add_control('parlour_right_element', array(
		'label'    => esc_attr__('Right Element', 'parlour'),
		'section'  => 'header',
		'settings' => 'parlour_right_element', 
		'type'     => 'select',
		'priority' => 4,
		'choices'  => array(
			0             => esc_attr__('Nothing', 'parlour'),
			'back_button' => esc_attr__('Back Button', 'parlour'),
			'breadcrumb'  => esc_attr__('Breadcrumb', 'parlour')
		)
	));

}

add_action('customize_register', 'parlour_customize_register_titlebar');
<?php
$social_link = get_post_meta( get_the_ID(), 'bdthemes_assistant_social_link', true );
if($social_link != null and is_array($social_link)) : ?>
	<div class="bdt-assistant-social uk-position-absolute uk-position-bottom-center">
		<ul class="uk-list uk-margin-remove-bottom">
	    <?php foreach ($social_link as $link) : ?>
	        <?php $tooltip = ucfirst(parlour_helper::icon($link)); ?>
	        <li class="uk-display-inline-block">
	            <a<?php echo parlour_helper::attrs(['href' => $link, 'class' => 'uk-margin-small-right']); ?> uk-icon="icon: <?php echo parlour_helper::icon($link); ?>" title="<?php echo esc_html($tooltip); ?>" uk-tooltip></a>
	        </li>
	    <?php endforeach ?>
	    </ul>
	</div>
<?php endif; ?>
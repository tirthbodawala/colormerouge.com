<?php
// Bdthemes assistants

if ( ! function_exists('is_plugin_active')){ include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); } // load is_plugin_active() function if no available

if(is_plugin_active('bdthemes-assistants/bdthemes-assistants.php')) { 
	if (!function_exists('bdthemes_assistants')) {
		function bdthemes_assistants($atts){
			extract(shortcode_atts(array( 
				'limit'      => '6',
				'orderby'    => 'date',
				'order'      => 'DESC',
				'categories' => 'all',
				'filter'     => 'yes',
				'align'      => 'center',
				'column_gap' => 'medium',
				'large'      => 3,
				'medium'     => 2,
				'small'      => 1,
				'class'      => '',
			), $atts));
			
			global $post;

			$id         = uniqid('bdtsg_');
			
			$large      = ($large) ? 'uk-width-1-'.$large.'@l' : '';
			$medium     = ($medium) ? 'uk-width-1-'.$medium.'@m' : '';
			$small      = ($small) ? 'uk-width-1-'.$small.'@s' : '';
			$responsive = [$small, $medium, $large];

			$title       = 'yes';  // TODO
			$meta        = 'yes';  // TODO
			$align       = 'center';  // TODO
			$show_social_link = 'yes'; // TODO

			$args = array(
				'post_type'      => 'assistants',
				'posts_per_page' => intval($limit),
				'orderby'        => $orderby,
				'order'          => $order,
				'post_status'    => 'publish',
			);
				
			if($categories != 'all' && $categories != '') {
				// string to array
				$str = $categories;
				$arr = explode(',', $str);
				  
				$args['tax_query'][] = array(
					'taxonomy' => 'experiences',
					'field'    => 'slug',
					'terms'    => $arr
				);
			}

			$wp_query = new WP_Query($args);

			ob_start(); // start buffer

			if( $wp_query->have_posts() ) { 


				if($filter == 'yes') { 

					// Get Filters from Shortcode Options
					if($categories != '' && $categories != 'all') {
						$service_categories = explode(',', $categories);
					} else {
						$service_categories = get_terms('experiences');
						$arraytostring = '';
						foreach($service_categories as $service_categories){
							$arraytostring .= $service_categories->slug . ',';
						}
						$arraytostring = rtrim($arraytostring,','); // Remove last commata;
						$service_categories = explode(',', $arraytostring); // Create array
					} 
					?>

					<div class="bdt-filter-wrapper">
						<ul id="<?php echo $id; ?>-filter" class="bdt-assistants-filter uk-flex-<?php echo esc_attr($align);?> uk-subnav uk-subnav-divider uk-margin-large-bottom" >
							<?php if($service_categories): ?>
									<li>
										<a data-filter="*" class="bdt-filter-item-active bdt-filter-item" href="javascript:;">
											<?php esc_html_e('All', 'parlour-core'); ?>
										</a>
									</li>	
								<?php foreach($service_categories as $service_categories => $value) { ?>
									<?php $filter_name = get_term_by('slug',$value,'experiences'); ?>
									<li>
										<a class="bdt-filter-item" data-filter=".sci-<?php echo esc_attr(trim($value)); ?>" href="javascript:;">
											<?php echo esc_html($filter_name->name); ?>
										</a>
									</li>
								<?php } ?>
							<?php endif; ?>
						</ul>
					</div>

				<?php } //end if filter ?>
				<div id="<?php echo $id; ?>" class="bdt-assistant-container uk-grid-<?php echo $column_gap; ?>" uk-grid>
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
						<?php $terms = get_the_terms( get_the_ID(), 'experiences' ); ?>
				    	<div class="bdt-assistant-item <?php echo implode(" ", $responsive);?><?php if($terms and $filter == 'yes') : foreach ($terms as $term) { echo ' sci-'.esc_attr($term->slug); } endif; ?>">





							<div class="tm-assistant-item uk-box-shadow-small assistant-align-<?php echo esc_attr($align); ?>">

							    <?php if (has_post_thumbnail()) : ?>
							        <div class="assistant-thumbnail uk-position-relative uk-overflow-hidden">
							            <div class="assistant-thumbnail-design">
											<?php $images = rwmb_meta( 'bdthemes_assistant_altimg', 'type=image_advanced&size=medium' ); ?>

											<?php if (has_post_thumbnail() and empty($images) ) : ?>
											    <div class="">
										            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
										                <?php echo  the_post_thumbnail('medium', array('class' => ''));  ?>
										            </a>
											    </div>
											<?php elseif (!empty($images)) : ?>
										        <div class="assistant-image-gallery uk-position-relative uk-overflow-hidden" uk-toggle="target: > .assistant-img-flip; mode: hover; animation: uk-animation-fade; queued: true; duration: 300">
									                <div class="assistant-img-flip uk-position-absolute uk-position-z-index">
									                    <?php echo  the_post_thumbnail('medium', array('class' => ''));  ?>
									                </div>
									                <?php foreach ( $images as $image) : ?> 
									                    <div class="assistant-img">
									                    <a href="<?php echo esc_url($image['full_url']); ?>" title="<?php echo esc_attr($image['title']); ?>">
									                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" width="<?php echo esc_attr($image['width']); ?>" height="<?php echo esc_attr($image['height']); ?>" class="" />
									                    </a>
									                    </div>
									                <?php endforeach; ?>
										        </div>
											<?php endif ?>

							                <div class="tm-assistant-overlay uk-position-cover uk-overlay uk-overlay-gradient uk-position-z-index"></div>
							            </div>  
							        </div>
							    <?php endif; ?>

							    <?php if(( $title === 'yes' ) or ( $meta === 'yes' )) { ?>
							        <div class="uk-background-default uk-position-relative uk-padding">

							            <?php if( $title==='yes') { ?>
							                <h3 class="tm-assistant-title uk-margin-remove-top uk-margin-small-bottom">
						                	    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="uk-link-reset"><?php the_title(); ?></a>
						                	    <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
						                	            <?php printf( '<span class="sticky-post featured">%s</span>', esc_html__( 'Featured', 'parlour') ); ?>
						                	    <?php endif; ?>
						                	</h3>
							            <?php }; 

							            if( $meta==='yes') {

							                echo get_the_term_list(get_the_ID(),'experiences', '<ul class="tm-assistant-meta uk-flex-'.$align.' uk-margin-small-top uk-margin-remove"><li>', '</li><li>', '</li></ul>' );
							            }; 

							            if( $show_social_link === 'yes') {
							                $social_link = get_post_meta( get_the_ID(), 'bdthemes_assistant_social_link', true );
							                if($social_link != null and is_array($social_link)) : ?>
							                	<div class="tm-assistant-social uk-position-absolute uk-position-bottom-center">
							                		<ul class="uk-list uk-margin-remove-bottom">
							                	    <?php foreach ($social_link as $link) : ?>
							                	        <?php $tooltip = ucfirst(parlour_helper::icon($link)); ?>
							                	        <li class="uk-display-inline-block">
							                	            <a<?php echo parlour_helper::attrs(['href' => $link, 'class' => 'uk-margin-small-right']); ?> uk-icon="icon: <?php echo parlour_helper::icon($link); ?>" title="<?php echo esc_html($tooltip); ?>" uk-tooltip></a>
							                	        </li>
							                	    <?php endforeach ?>
							                	    </ul>
							                	</div>
							                <?php endif;
							            }; ?>
							        </div>
							    <?php }; ?>
							</div>


















				    	</div>
					<?php endwhile; ?>
				</div>
				
				<?php if ($filter == 'yes') : ?>
					<script>
						jQuery(document).ready(function($) {
						    'use strict';
						    var $grid = $('#<?php echo $id; ?>').isotope({
							  // options
							  itemSelector: '.bdt-assistant-item',
							  percentPosition: true,
							  layoutMode: 'masonry',
							});

							$('#<?php echo $id; ?>-filter').on( 'click', 'a', function() {
							  var filterValue = $(this).attr('data-filter');
							  $grid.isotope({ filter: filterValue });
							});

							$('#<?php echo $id; ?>-filter > li > .bdt-filter-item').click(function(){
							    $('#<?php echo $id; ?>-filter > li > .bdt-filter-item').removeClass('bdt-filter-item-active');
							    $(this).addClass('bdt-filter-item-active');
							});
						});
					</script>
				<?php endif; ?>
				
				<?php wp_reset_postdata(); 
			};

			$assistants_output = ob_get_contents(); // get buffered content

			ob_end_clean(); // clean buffer

			return $assistants_output;
		}
		add_shortcode('bdt_assistants', 'bdthemes_assistants');
	}
}
<?php
// Bdthemes fancy text shortcode

if (!function_exists('bdthemes_column')) {
	function bdthemes_column($atts = null, $content = null) {
		$atts = shortcode_atts(array(
            'visible' => '', // xl, l, m, s
            'hidden'  => '', // xl, l, m, s
            'center'  => 'no',
            'class'   => ''
        ), $atts, 'bdt_column');

        $id             = uniqid('sucol');
        $css            = array();
        $classes        = array('bdt-column', bdt_ecssc($atts));


        if ($atts['visible']) {
            $classes[] = 'uk-hidden@' . $atts['visible'];
        }
        if ($atts['hidden']) {
            $classes[] = 'uk-visible@' . $atts['hidden'];
        }
        if ($atts['center'] == 'yes')
            $classes[] = 'uk-text-center';

        return '<div id="'.$id.'" class="' . bdt_acssc($classes) . '"><div class="bdt-column-inner"><div class="bdt-column-content">' . do_shortcode($content) . '</div></div></div>';
	}
 }
 //check override
add_filter('template_include', 'override_plugin_column');

function override_plugin_column($template) {
    if (function_exists('bdthemes_column_override')) {
        add_shortcode('bdt_column', 'bdthemes_column_override');
    }else{
        add_shortcode('bdt_column', 'bdthemes_column');
    }
    return $template;
}
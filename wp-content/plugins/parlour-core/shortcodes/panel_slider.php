<?php
// Bdthemes Panel Slider

if (!function_exists('bdthemes_panel_slider')) {
	function bdthemes_panel_slider($atts=null, $content=null){
		extract(shortcode_atts(array(
			'large'         => '3',
			'medium'        => '2',
			'small'         => '1',
			'gutter'        => 'collapse',
			'navigation'    => 'yes',
			'pagination'    => 'no',
			'autoplay'      => 'yes',
			'loop'          => 'yes',
			'color'         => 'dark',
			'css_animation' => '',
			'class'         => ''
		), $atts));

		$css_animation  = bdthemes_helper::getCSSAnimation( $css_animation );
		
		$id             = uniqid('bdtpnls_');
		$classes        = ['bdt-panel-slider', 'uk-position-relative', $css_animation, $class];
		$output         = [];
		
		if ($gutter     == 'large') { $gutter = 50; }
		elseif ($gutter == 'medium') { $gutter = 25;}
		elseif ($gutter == 'small') { $gutter = 10;}
		elseif ($gutter == 'collapse') { $gutter = 0;}

		$autoplay = ($autoplay =='yes') ? 5000 : 'false';
		$loop     = ($loop =='yes') ? 'true' : 'false';
		
		$output[] = '<div id ="'.$id.'" class="'.bdt_acssc($classes).'">';
	
		    $output[] = '<div class="swiper-container">';
			    $output[] = '<div class="swiper-wrapper">';
		  				 $output[] = do_shortcode($content);
		        $output[] = '</div>';
	        $output[] = '</div>';

	        /* Add Pagination */
	        if ( $pagination == 'yes' ) {
	        	$pagination_class = ($color == 'light') ? ' swiper-pagination-white' : '';
		        $output[] = '<div class="swiper-pagination'.$pagination_class.'"></div>';
		    }
		    
	        /* Add Arrows */
	        if ( $navigation == 'yes' ) {
		        $output[] = '<div class="swiper-button-next swiper-button-white"></div><div class="swiper-button-prev swiper-button-white"></div>';
		    }

		$output[] ='</div>';

	    /* Initialize Swiper */
	    $output[] = "<script>
	    	 jQuery(document).ready(function ($) {
			    var swiper = new Swiper('#$id .swiper-container', {
					navigation: {
						nextEl: '#$id .swiper-button-nextt',
						prevEl: '#$id .swiper-button-prev',
					},
					pagination: {
						el        : '#$id .swiper-pagination',
						type      : 'bullets',
						clickable : true,
					},
			        autoplay      : $autoplay ? '{ 'delay' : 4500 }' : 'false',
			        paginationClickable: true,
			        loop: $loop,
    				autoplayDisableOnInteraction: false,
			        slidesPerView: $large,
			        spaceBetween: $gutter,
			        breakpoints: {
			            1024: {
			                slidesPerView: $large,
			                spaceBetween: $gutter
			            },
			            768: {
			                slidesPerView: $medium,
			                spaceBetween: $gutter
			            },
			            640: {
			                slidesPerView: $small,
			                spaceBetween: $gutter
			            }
    				}
			    });
		    });
	    </script>";

	return implode("\n", $output);
	}
	add_shortcode('bdt_panel_slider', 'bdthemes_panel_slider');
}


if (!function_exists('bdthemes_panel_slide')) {
    function bdthemes_panel_slide($atts=null, $content=null) {
        extract(shortcode_atts(array(
			'image'             => '',
			'title'             => 'Panel Slide Title',
			'url'               => '#',
			'link_title'        => 'Order Now',
			'target'            => '_blank',
			'background'        => 'secondary',
			'custom_background' => '',
			'class'             => ''
        ), $atts));

		$output            = [];
		$classes           = ['bdt-panel-slide', 'swiper-slide', 'uk-transition-toggle', $class];
		
		$custom_background = ($custom_background) ? 'background:' . $custom_background. ';' : '';

		if (($background !=='custom') and ($background !=='none')) {
            $classes[] = ($background) ? 'uk-background-'.$background : '';
        }
		
		$image_src = wp_get_attachment_image_src( $image, 'large' );
		$image      =  ($image_src) ? $image_src[0] : BDTCURI.'/images/no-image.jpg';
		
		$output[] = '<div class="'.bdt_acssc($classes).'" style="'.$custom_background.'">';
        	$output[] = '<div class="bdt-pnls-thumb">';
        		$output[] = '<img src="'. esc_url($image) .'" alt="'.$title.'">';
        	$output[] = '</div>';
        	
        	$output[] = '<div class="bdt-pnls-desc uk-position-medium uk-position-bottom-left uk-position-z-index uk-light uk-transition-slide-bottom">';

	        	if ( $url != '' ) {
					$output[] = '<a href="'.$url.'" target="'.$target.'" class="uk-link-reset uk-margin-small-bottom">'.$link_title.' <span uk-icon="icon: arrow-right; ratio: 1.5"></span></a>';
				}
				if ( $title != '' ) {
					$output[] = '<h2 class="uk-margin-remove-bottom uk-margin-small-top">'.$title.'</h2>';
				}

				if ( $content != '' ) {					
					$output[] = '<div class="bdt-pnls-desc uk-text-bold uk-text-muted uk-text-uppercase uk-link-reset">'.do_shortcode($content).'</div>';
				}

	  		$output[] = '</div>';
			$output[] = '<div class="uk-transition-fade uk-position-cover uk-overlay uk-overlay-gradient"></div>';

		$output[] = '</div>';
       

        return implode("\n", $output);
    }


    // end of panel_slide shortcode
    add_shortcode('bdt_panel_slide', 'bdthemes_panel_slide');
}
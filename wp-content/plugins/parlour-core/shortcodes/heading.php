<?php
// Bdthemes heading Shortcode

if (!function_exists('bdthemes_heading')) {
	function bdthemes_heading($atts = null, $content = null) {
		extract(shortcode_atts(array(
			'subtitle'  => '',
			'size'      => '42',
			'align'     => 'left',
			'margin'    => '',
			'width'     => '',
			'weight'    => '',
			'transform' => '',
			'heading'   => 'h3',
			'color'     => ''
		), $atts));

		$id        = uniqid('suhead');
		$output    = [];
		$classes   = ['bdt-heading-wrapper', 'bdt-heading-align-'. $align, 'uk-position-relative'];
		$heading_classes = ['bdt-heading'];
		$heading_classes[] = ($transform) ? 'uk-transform-'.$transform : '';
		$weight = ($weight) ? 'font-weight:'.$weight.';' : '';
		
		$width     = ($width) ? 'width: ' . intVal($width) . '%;' : '';
		$margin    = ($margin) ? 'margin-bottom: ' . intVal($margin) . 'px;' : '';
		$size      = ($size) ? 'font-size: ' . intVal($size) . 'px;line-height: '.$size.'px;' : '';
		$color     = ($color) ? ' color: ' . $color . ';' : '';
		$content   = ($content) ? $content : 'This is a heading';
		$subtitle  = ($subtitle) ? '<span class="bdt-heading-subtitle" style="">'.$subtitle.'</span>' : '';

		$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" style="'.$width.$margin.'">';
			$output[] = $subtitle;
			$output[] = '<'.$heading.' class="'.bdt_acssc($heading_classes).'" style="'.$size.$color.$weight.'">'.do_shortcode($content).'</'.$heading.'>';
			$output[] = '<div class="bdt-heading-style"></div>';
		$output[] = '</div>';

		return implode("\n", $output);
	}
	add_shortcode('bdt_heading', 'bdthemes_heading');
}
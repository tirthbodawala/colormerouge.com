<?php
namespace ElementorExtend\Modules\Posts\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Post_Block extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'post-block';
	}

	public function get_title() {
		return __( 'Post Block', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-posts-grid';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'post_type',
			[
				'label'   => __( 'Post Type', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'post',
				'options' => [
					'post'     => __( 'Post', 'parlour-core' ),
					'services' => __( 'Services', 'parlour-core' ),
				],
			]
		);


		$this->add_control(
			'source',
			[
				'label' => _x( 'Source', 'Posts Query Control', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Show All', 'parlour-core' ),
					'by_name' => __( 'Manual Selection', 'parlour-core' ),
				],
				'label_block' => true,
			]
		);

		$post_categories = get_terms( 'category' );

		$options = [];
		foreach ( $post_categories as $category ) {
			$post_options[ $category->slug ] = $category->name;
		}

		$this->add_control(
			'post_categories',
			[
				'label'       => __( 'Categories', 'parlour-core' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => $post_options,
				'default'     => [],
				'label_block' => true,
				'multiple'    => true,
				'condition'   => [
					'post_type' => 'post',
					'source'    => 'by_name',
				],
			]
		);

		$service_categories = get_terms( 'service-categories' );

		$service_options = [];
		foreach ( $service_categories as $category ) {
			$service_options[ $category->slug ] = $category->name;
		}

		$this->add_control(
			'service_categories',
			[
				'label'       => __( 'Categories', 'parlour-core' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => $service_options,
				'default'     => [],
				'label_block' => true,
				'multiple'    => true,
				'condition'   => [
					'post_type' => 'services',
					'source'    => 'by_name',
				],
			]
		);

		$this->add_control(
			'posts',
			[
				'label' => __( 'Posts Limit', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 6,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => __( 'Order by', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'date',
				'options' => [
					'date'     => __( 'Date', 'parlour-core' ),
					'title'    => __( 'Title', 'parlour-core' ),
					'category' => __( 'Category', 'parlour-core' ),
					'rand'     => __( 'Random', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label'   => __( 'Order', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'DESC',
				'options' => [
					'DESC' => __( 'Descending', 'parlour-core' ),
					'ASC'  => __( 'Ascending', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'thumb',
			[
				'label'     => __( 'Post Thumbnail', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'title',
			[
				'label'     => __( 'Title', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'meta',
			[
				'label'     => __( 'Meta Data', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'excerpt',
			[
				'label'     => __( 'Excerpt', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'excerpt_limit',
			[
				'label'   => __( 'Excerpt Limit', 'parlour-core' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 20,
			]
		);		
		
		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Style', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'color',
			[
				'label'     => __( 'Color', 'parlour-core' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'dark',
				'options'   => [
					'dark'   => __( 'Default', 'parlour-core' ),
					'light'  => __( 'Light', 'parlour-core' ),
				],
			]
		);

		$this->end_controls_section();

	}

	private function get_shortcode() {
		$settings = $this->get_settings();

		if ($settings['post_type'] === 'post') {
			if ($settings['source'] === 'by_name') {
				$categories = implode(",", $settings['post_categories']);
			} else {
				$categories = 'all';
			}
		} elseif ($settings['post_type'] === 'services') {
			if ($settings['source'] === 'by_name') {
				$categories = implode(",", $settings['service_categories']);
			} else {
				$categories = 'all';
			}
		}

		$attributes = [
			'post_type'     => $settings['post_type'],
			'categories'    => $categories,
			'posts'         => $settings['posts'],
			'orderby'       => $settings['orderby'],
			'order'         => $settings['order'],
			'thumb'         => $settings['thumb'],
			'title'         => $settings['title'],
			'meta'          => $settings['meta'],
			'excerpt'       => $settings['excerpt'],
			'excerpt_limit' => $settings['excerpt_limit'],
			'color'         => $settings['color'],
		];

		$this->add_render_attribute( 'shortcode', $attributes );

		$shortcode = [];
		$shortcode[] = sprintf( '[bdt_post_block %s]', $this->get_render_attribute_string( 'shortcode' ) );

		return implode("", $shortcode);
	}

	public function render() {
		echo do_shortcode( $this->get_shortcode() );
	}

	public function render_plain_content() {
		echo $this->get_shortcode();
	}
}

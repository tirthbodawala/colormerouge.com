<?php
/**
 * all array css classes will output as proper space
 * @param array $classes shortcode css class as array
 * @return proper string
 */

function bdt_acssc($classes) {
    if (is_array($classes))
        $classes     = implode($classes, ' ');
    $abs_classes = trim(preg_replace('/\s\s+/', ' ', $classes));
    return $abs_classes;
}

/**
 * Extra CSS class helper
 * @param array $atts Shortcode attributes
 * @return string
 */
function bdt_ecssc($atts) {
    if (is_array($atts)) {
        return ( $atts['class'] ) ? ' ' . trim( $atts['class'] ) : '';
        //return 'hellooooo';
    }
}


if (!function_exists('bdthemes_allow_tags')) {
    function bdthemes_allow_tags($tag = null) {
        $tag_allowed = wp_kses_allowed_html('post');

        $tag_allowed['input'] = array(
            'class'   => array(),
            'id'      => array(),
            'name'    => array(),
            'value'   => array(),
            'checked' => array(),
            'type'    => array()
        );
        $tag_allowed['select'] = array(
            'class'    => array(),
            'id'       => array(),
            'name'     => array(),
            'value'    => array(),
            'multiple' => array(),
            'type'     => array()
        );
        $tag_allowed['option'] = array(
            'value'    => array(),
            'selected' => array()
        );

        if($tag == null){
            return $tag_allowed;
        }
        elseif(is_array($tag)){
            $new_tag_allow = array();
            foreach ($tag as $_tag){
                $new_tag_allow[$_tag] = $tag_allowed[$_tag];
            }

            return $new_tag_allow;
        }
        else{
            return isset($tag_allowed[$tag]) ? array($tag=>$tag_allowed[$tag]) : array();
        }
    }
}

/**
 * Color convert class
 */
class bdt_color {

    private static function lib_lighten($args) {
        list($color, $delta) = self::colorArgs($args);

        $hsl = self::toHSL($color);
        $hsl[3] = self::clamp($hsl[3] + $delta, 100);
        return self::toRGB($hsl);
    }

    private static function lib_darken($args) {
        list($color, $delta) = self::colorArgs($args);

        $hsl = self::toHSL($color);
        $hsl[3] = self::clamp($hsl[3] - $delta, 100);
        return self::toRGB($hsl);
    }

    private static function colorArgs($args) {
        if ($args[0] != 'list' || count($args[2]) < 2) {
            return array(array('color', 0, 0, 0));
        }
        list($color, $delta) = $args[2];
        if ($color[0] != 'color')
            $color = array('color', 0, 0, 0);

        $delta = floatval($delta[1]);

        return array($color, $delta);
    }

    private static function toHSL($color) {
        if ($color[0] == 'hsl') return $color;

        $r = $color[1] / 255;
        $g = $color[2] / 255;
        $b = $color[3] / 255;

        $min = min($r, $g, $b);
        $max = max($r, $g, $b);

        $L = ($min + $max) / 2;
        if ($min == $max) {
            $S = $H = 0;
        } else {
            if ($L < 0.5)
                $S = ($max - $min)/($max + $min);
            else
                $S = ($max - $min)/(2.0 - $max - $min);

            if ($r == $max) $H = ($g - $b)/($max - $min);
            elseif ($g == $max) $H = 2.0 + ($b - $r)/($max - $min);
            elseif ($b == $max) $H = 4.0 + ($r - $g)/($max - $min);

        }

        $out = array('hsl',
            ($H < 0 ? $H + 6 : $H)*60,
            $S*100,
            $L*100,
        );

        if (count($color) > 4) $out[] = $color[4]; // copy alpha
        return $out;
    }

    private static function toRGB_helper($comp, $temp1, $temp2) {
        if ($comp < 0) $comp += 1.0;
        elseif ($comp > 1) $comp -= 1.0;

        if (6 * $comp < 1) return $temp1 + ($temp2 - $temp1) * 6 * $comp;
        if (2 * $comp < 1) return $temp2;
        if (3 * $comp < 2) return $temp1 + ($temp2 - $temp1)*((2/3) - $comp) * 6;

        return $temp1;
    }

    public static function toRGB($color) {
        if ($color == 'color') return $color;

        $H = $color[1] / 360;
        $S = $color[2] / 100;
        $L = $color[3] / 100;

        if ($S == 0) {
            $r = $g = $b = $L;
        } else {
            $temp2 = $L < 0.5 ?
                $L*(1.0 + $S) :
                $L + $S - $L * $S;

            $temp1 = 2.0 * $L - $temp2;

            $r = self::toRGB_helper($H + 1/3, $temp1, $temp2);
            $g = self::toRGB_helper($H, $temp1, $temp2);
            $b = self::toRGB_helper($H - 1/3, $temp1, $temp2);
        }

        $out = array('color', round($r*255), round($g*255), round($b*255));
        if (count($color) > 4) $out[] = $color[4]; // copy alpha
        return $out;
    }

    public static function clamp($v, $max = 1, $min = 0) {
        return min($max, max($min, $v));
    }

    public static function rgbaToHex($color) {
        if ($color[0] != 'color')
            throw new exception("color expected for rgbahex");

        return sprintf("#%02x%02x%02x",
            $color[1],$color[2], $color[3]);
    }

    public static function _hexToRgb($hexStr, $returnAsString = false, $seperator = ',') {
        $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
        $rgbArray = array();
        $rgbArray[] = 'color';
        if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
            $colorVal = hexdec($hexStr);
            $rgbArray[] = 0xFF & ($colorVal >> 0x10);
            $rgbArray[] = 0xFF & ($colorVal >> 0x8);
            $rgbArray[] = 0xFF & $colorVal;
        } elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
            $rgbArray[] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
            $rgbArray[] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
            $rgbArray[] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
        } else {
            return false; //Invalid hex color code
        }

        return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
    }

    public static function _inverseHex($color) {
        
        $prependHash = FALSE;

        IF(STRPOS($color,'#')!==FALSE) {
            $prependHash = TRUE;
            $color       = STR_REPLACE('#',NULL,$color);
        }

        SWITCH($len=STRLEN($color)) {
            case 3:
                $color=PREG_REPLACE("/(.)(.)(.)/","\\1\\1\\2\\2\\3\\3",$color);
            case 6:
                break;
            default:
                TRIGGER_ERROR("Invalid hex length ($len). Must be (3) or (6)", E_USER_ERROR);
        }

        IF(!PREG_MATCH('/[a-f0-9]{6}/i',$color)) {
          $color = HTMLENTITIES($color);
          TRIGGER_ERROR( "Invalid hex string #$color", E_USER_ERROR );
        }

        $r = DECHEX(255-HEXDEC(SUBSTR($color,0,2)));
        $r = (STRLEN($r)>1)?$r:'0'.$r;
        $g = DECHEX(255-HEXDEC(SUBSTR($color,2,2)));
        $g = (STRLEN($g)>1)?$g:'0'.$g;
        $b = DECHEX(255-HEXDEC(SUBSTR($color,4,2)));
        $b = (STRLEN($b)>1)?$b:'0'.$b;

        return ($prependHash?'#':NULL).$r.$g.$b;
    }

    // static method for color change
    public static function hexToRgb($hexStr, $returnAsString = false, $seperator = ','){
        $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr);
        $rgbArray = array();

        if (strlen($hexStr) == 6){
            $colorVal = hexdec($hexStr);
            $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
            $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
            $rgbArray['blue'] = 0xFF & $colorVal;
        } elseif (strlen($hexStr) == 3){
            $rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
            $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
            $rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
        } else {
            return false;
        }

        return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray;
    }

    public static function rgba($hex, $opacity){
        return 'rgba(' . self::hexToRgb($hex, true) . ','.$opacity.')';
    }

    public static function lighten($color, $pc = '5%'){
        $pc = str_replace('%', '', $pc);
        $args = array('list', ',', array(self::_hexToRgb($color), array('%', $pc)));
        $rgb = array_slice(self::lib_lighten($args), 1);

        return self::rgbaToHex(self::lib_lighten($args));
    }

    public static function darken($color, $pc = '5%'){
        $pc = str_replace('%', '', $pc);
        $args = array('list', ',', array(self::_hexToRgb($color), array('%', $pc)));
        $rgb = array_slice(self::lib_darken($args), 1);

        return self::rgbaToHex(self::lib_darken($args));
    }

    public static function inverse($color){
        $color = self::_inverseHex($color);
        $args = self::_hexToRgb($color);
        return self::rgbaToHex($args);
    }
}


function bdthemes_get_timezone_id() {    
    $timezone = get_option( 'timezone_string' );

    /* If site timezone string exists, return it */
    if ( $timezone ) {
        return $timezone;
    }

    $utc_offset = 3600 * get_option( 'gmt_offset', 0 );

    /* Get UTC offset, if it isn't set return UTC */
    if ( ! $utc_offset ) {
        return 'UTC';
    }

    /* Attempt to guess the timezone string from the UTC offset */
    $timezone = timezone_name_from_abbr( '', $utc_offset );

    /* Last try, guess timezone string manually */
    if ( $timezone === false ) {

        $is_dst = date( 'I' );

        foreach ( timezone_abbreviations_list() as $abbr ) {
            foreach ( $abbr as $city ) {
                if ( $city['dst'] == $is_dst && $city['offset'] == $utc_offset ) {
                    return $city['timezone_id'];
                }
            }
        }
    }

    /* If we still haven't figured out the timezone, fall back to UTC */
    return 'UTC';
}


/**
 * helper functions class
 */
class bdthemes_helper {

    static $selfClosing = ['input'];


    /**
     * Renders a tag.
     *
     * @param  string $name
     * @param  array  $attrs
     * @param  string $text
     * @return string
     */
    public static function tag($name, array $attrs = [], $text = null) {
        $attrs = self::attrs($attrs);
        return "<{$name}{ $attrs }" . (in_array($name, self::$selfClosing) ? '/>' : ">$text</{$name}>");
    }

    /**
     * Renders a form tag.
     *
     * @param  array $tags
     * @param  array $attrs
     * @return string
     */
    public static function form($tags, array $attrs = []) {
        $attrs = self::attrs($attrs);
        return "<form{$attrs}>\n" . implode("\n", array_map(function($tag) {
            $output = self::tag($tag['tag'], array_diff_key($tag, ['tag' => null]));
            return $output;
        }, $tags)) . "\n</form>";
    }

    /**
     * Renders an image tag.
     *
     * @param  array|string $url
     * @param  array        $attrs
     * @return string
     */
    public static function image($url, array $attrs = []) {
        $url = (array) $url;
        $path = array_shift($url);
        $params = $url ? '?'.http_build_query(array_map(function ($value) {
            return is_array($value) ? implode(',', $value) : $value;
        }, $url)) : '';

        if (!isset($attrs['alt']) || empty($attrs['alt'])) {
            $attrs['alt'] = true;
        }

        $output = self::attrs(['src' => $path.$params], $attrs);

        return "<img{$output}>";
    }
    
    /**
     * Renders tag attributes.
     * @param  array $attrs
     * @return string
     */
    public static function attrs(array $attrs) {
        $output = [];

        if (count($args = func_get_args()) > 1) {
            $attrs = call_user_func_array('array_merge_recursive', $args);
        }

        foreach ($attrs as $key => $value) {

            if (is_array($value)) { $value = implode(' ', array_filter($value)); }
            if (empty($value) && !is_numeric($value)) { continue; }

            if (is_numeric($key)) {
               $output[] = $value;
            } elseif ($value === true) {
               $output[] = $key;
            } elseif ($value !== '') {
               $output[] = sprintf('%s="%s"', $key, htmlspecialchars($value, ENT_COMPAT, 'UTF-8', false));
            }
        }

        return $output ? ' '.implode(' ', $output) : '';
    }

    /**
     * social icon generator from link
     * @param  [type] $link [description]
     * @return [type]       [description]
     */
    public static function icon($link) {
       static $icons;
       $icons = self::social_icons();

       if (strpos($link, 'mailto:') === 0) {
           return 'mail';
       }

       $icon = parse_url($link, PHP_URL_HOST);
       $icon = preg_replace('/.*?(plus\.google|[^\.]+)\.[^\.]+$/i', '$1', $icon);
       $icon = str_replace('plus.google', 'google-plus', $icon);

       if (!in_array($icon, $icons)) {
           $icon = 'social';
       }

       return $icon;
    }

    public static function social_icons() {
       $icons = [ "behance", "dribbble", "facebook", "github-alt", "github", "foursquare", "tumblr", "whatsapp", "soundcloud", "flickr", "google-plus", "google", "linkedin", "vimeo", "instagram", "joomla", "pagekit", "pinterest", "twitter", "uikit", "wordpress", "xing", "youtube" ];

       return $icons;
    }

}

<article id="post-<?php the_ID() ?>" <?php post_class('uk-article post-format-gallery') ?> data-permalink="<?php the_permalink() ?>" typeof="Article">

    <?php get_template_part( 'template-parts/post-format/schema-meta' ); ?>
    
    <?php 
    //$images = get_post_meta( get_the_ID(), 'parlour_blog_gallery', true );
    $images = rwmb_meta( 'parlour_blog_gallery', 'type=image_advanced&size=parlour_blog' );
    if (!empty($images)) : ?>

    <div class="post-image-gallery<?php echo (is_single()) ? ' uk-margin-large-bottom' : ' uk-margin-bottom'; ?>">
        <div class="image-lightbox owl-carousel owl-theme" data-owl-carousel='{"margin": 10, "items": 1, "nav": true, "navText": "", "loop": true}'>
            <?php 
            foreach ( $images as $image) {
                echo '<div class="carousel-cell"><a href="'.esc_url($image['full_url']).'" title="'.esc_attr($image['title']).'"><img src="'.esc_url($image['url']).'" alt="'.esc_attr($image['alt']).'" width="'.esc_attr($image['width']).'" height="'.esc_attr($image['height']).'" class="uk-border-rounded" /></a></div>';
            } ?>
        </div>
    </div>

    <?php endif ?>
    



    <div class="uk-margin-medium-bottom uk-container uk-container-small uk-text-center">
        <?php get_template_part( 'template-parts/post-format/title' ); ?>

        <?php if(get_theme_mod('parlour_blog_meta', 1)) :?>
        <?php get_template_part( 'template-parts/post-format/meta' ); ?>
        <?php endif; ?>
    </div>
    
    <div class="uk-container uk-container-small">
        <?php get_template_part( 'template-parts/post-format/content' ); ?>

        <?php get_template_part( 'template-parts/post-format/read-more' ); ?>
    </div>

</article>
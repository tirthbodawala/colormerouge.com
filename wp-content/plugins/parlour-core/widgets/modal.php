<?php

class widget_modal extends WP_Widget { 
	
	// Widget Settings
	function __construct() {
		$widget_ops  = array('description' => esc_html__('Display your modal to any widget position beautifully.', 'parlour-core'));
		$control_ops = array('width' => 300, 'height' => 350, 'id_base' => 'modal');
		parent::__construct('modal', esc_html__('BdThemes Modal', 'parlour-core'), $widget_ops, $control_ops );
	}
	
	// Widget Output
	function widget($args, $instance) {
		extract($args);
		$title        = apply_filters('widget_title', esc_html($instance['title']));
		$align        = $instance['align'];
		$button_txt   = $instance['button_txt'];
		$button_style = $instance['button_style'];
		$content      = $instance['content'];
		$footer_text  = $instance['footer_text'];
		$modal_center = ( $instance['modal_center'] == 1 ) ? ' uk-margin-auto-vertical' : '';
		$close_btn    = ( $instance['close_btn'] );
		$modal_size    = ( $instance['modal_size'] == 1 ) ? ' uk-modal-full' : ' uk-modal';

		echo $before_widget;
		
		$output             = [];

        //This is a button toggling the modal
        $output[] = '<button class="bdt-modal-widget-button uk-button uk-button-'.$button_style.' uk-border-rounded" type="button" uk-toggle="target: #'.$id.'">'.$button_txt.'</button>';

        // This is the modal
        $output[] = '<div id="'.$id.'" class="bdt-modal-widget'.$modal_size.'" uk-modal>
                        <div class="uk-modal-dialog uk-text-'.$align.' '.$modal_center.'">';
                            
                if ( $close_btn !== 'none' ) {
                	$output[] = '<button class="uk-modal-close-'.$close_btn.'" type="button" uk-close></button>';
                }

                if ($title) {
                    $output[] = '<div class="uk-modal-header"><h2 class="uk-modal-title">'.$title.'</h2></div>';
                }
                
                $output[] = '<div class="uk-modal-body" uk-overflow-auto>'.do_shortcode( $content ).'</div>';

                if ($footer_text) {
                    $output[] = '<div class="uk-modal-footer">'.$footer_text.'</div>';
                }

        $output[] = '</div></div>';

        echo implode("", $output);

		echo $after_widget;
	}
	
	// Update
	function update($new_instance, $old_instance) {  
		$instance                 = $old_instance; 
		$instance['title']        = strip_tags( $new_instance['title'] );
		$instance['align']        = strip_tags( $new_instance['align'] );
		$instance['button_txt']   = strip_tags( $new_instance['button_txt'] );
		$instance['button_style'] = strip_tags( $new_instance['button_style'] );
		$instance['content']      = strip_tags( $new_instance['content'] );
		$instance['footer_text']  = strip_tags( $new_instance['footer_text'] );
		$instance['modal_center'] = strip_tags( $new_instance['modal_center'] );
		$instance['close_btn']    = strip_tags( $new_instance['close_btn'] );
		$instance['modal_size']    = strip_tags( $new_instance['modal_size'] );

		return $instance;
	}
	
	// Backend Form
	function form($instance) {
		
		$defaults = array('title' => '', 'align' => 'center', 'button_txt' => 'BOOK NOW', 'button_style' => 'secondary', 'content' => '[booked-calendar]', 'footer_text' => '', 'modal_center' => '0', 'close_btn' => 'default', 'modal_size' => '0'); // Default Values
		$instance = wp_parse_args( (array) $instance, $defaults ); 
		?>
        
		<p>
			<label for="<?php echo esc_attr($this->get_field_id('title')); ?>">Header Title</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" value="<?php echo esc_attr($instance['title']); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'align' ) ?>"><?php esc_html_e( 'Content Align', 'parlour-core' ) ?></label>
			<select class="widefat" name="<?php echo $this->get_field_name( 'align' ) ?>" id="<?php echo $this->get_field_id( 'align' ) ?>">
				<option value="left" <?php selected( 'left', $instance['align'] ) ?>><?php _e( 'Left', 'parlour-core' ) ?></option>
				<option value="right" <?php selected( 'right', $instance['align'] ) ?>><?php _e( 'Right', 'parlour-core' ) ?></option>
				<option value="center" <?php selected( 'center', $instance['align'] ) ?>><?php _e( 'Center', 'parlour-core' ) ?></option>
			</select>
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('button_txt')); ?>">Button Text</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('button_txt')); ?>" name="<?php echo esc_attr($this->get_field_name('button_txt')); ?>" value="<?php echo esc_attr($instance['button_txt']); ?>" placeholder="BOOK NOW" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'button_style' ) ?>"><?php esc_html_e( 'Button Style', 'parlour-core' ) ?></label>
			<select class="widefat" name="<?php echo $this->get_field_name( 'button_style' ) ?>" id="<?php echo $this->get_field_id( 'button_style' ) ?>">
				<option value="default" <?php selected( 'default', $instance['button_style'] ) ?>><?php _e( 'Default', 'parlour-core' ) ?></option>
				<option value="primary" <?php selected( 'primary', $instance['button_style'] ) ?>><?php _e( 'Primary', 'parlour-core' ) ?></option>
				<option value="secondary" <?php selected( 'secondary', $instance['button_style'] ) ?>><?php _e( 'Secondary', 'parlour-core' ) ?></option>
				<option value="danger" <?php selected( 'danger', $instance['button_style'] ) ?>><?php _e( 'Danger', 'parlour-core' ) ?></option>
				<option value="text" <?php selected( 'text', $instance['button_style'] ) ?>><?php _e( 'Text', 'parlour-core' ) ?></option>
			</select>
		</p>

        <p>
			<label for="<?php echo esc_attr($this->get_field_id('content')); ?>">Modal Content</label>
			<textarea class="widefat" id="<?php echo esc_attr($this->get_field_id('content')); ?>" name="<?php echo esc_attr($this->get_field_name('content')); ?>" placeholder="I am a textarea, you can write here your modal content"><?php echo esc_attr($instance['content']); ?></textarea>
		</p>

		<p>
			<label for="<?php echo esc_attr($this->get_field_id('footer_text')); ?>">Footer Text</label>
			<input class="widefat" type="text" id="<?php echo esc_attr($this->get_field_id('footer_text')); ?>" name="<?php echo esc_attr($this->get_field_name('footer_text')); ?>" value="<?php echo esc_attr($instance['footer_text']); ?>" />
		</p>
		
		<p>
			<label for="<?php echo $this->get_field_id( 'close_btn' ) ?>"><?php esc_html_e( 'Close Button', 'parlour-core' ) ?></label>
			<select class="widefat" name="<?php echo $this->get_field_name( 'close_btn' ) ?>" id="<?php echo $this->get_field_id( 'close_btn' ) ?>">
				<option value="default" <?php selected( 'default', $instance['close_btn'] ) ?>><?php _e( 'Default', 'parlour-core' ) ?></option>
				<option value="outside" <?php selected( 'Outside', $instance['close_btn'] ) ?>><?php _e( 'Outside', 'parlour-core' ) ?></option>
				<option value="none" <?php selected( 'none', $instance['close_btn'] ) ?>><?php _e( 'No', 'parlour-core' ) ?></option>
			</select>
		</p>

		<p>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'modal_center' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'modal_center' )); ?>" type="checkbox" value="1" <?php checked('1', $instance['modal_center'] ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'modal_center' )); ?>">Do you show your modal at center?</label>
		</p>

		<p>
			<input class="widefat" id="<?php echo esc_attr($this->get_field_id( 'modal_size' )); ?>" name="<?php echo esc_attr($this->get_field_name( 'modal_size' )); ?>" type="checkbox" value="1" <?php checked('1', $instance['modal_size'] ); ?> />
			<label for="<?php echo esc_attr($this->get_field_id( 'modal_size' )); ?>">Full size modal?</label>
		</p>

    <?php }
}

// Add Widget
function widget_modal_init() {
	register_widget('widget_modal');
}
add_action('widgets_init', 'widget_modal_init');

?>
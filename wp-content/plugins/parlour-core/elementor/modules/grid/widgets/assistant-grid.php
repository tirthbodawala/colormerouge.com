<?php
namespace ElementorExtend\Modules\Grid\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class Assistant Grid
 */
class Assistant_Grid extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'assistant-grid';
	}

	public function get_title() {
		return __( 'Assistant Grid', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-gallery-grid';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->register_query_section_controls();
	}

	private function register_query_section_controls() {
		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
			]
		);

		$this->add_responsive_control(
			'columns',
			[
				'label' => __( 'Columns', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => '3',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options' => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'column_gap',
			[
				'label' => __( 'Column Gap', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'collapse' => __( 'Collapse', 'parlour-core' ),
					'small'    => __( 'Small', 'parlour-core' ),
					'medium'   => __( 'Medium', 'parlour-core' ),
					'large'    => __( 'Large', 'parlour-core' ),
					'xlarge'   => __( 'Extra Large', 'parlour-core' ),
				],
				'default' => 'medium',
			]
		);

		$this->add_control(
			'posts_per_page',
			[
				'label' => __( 'Limit', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 8,
			]
		);

		$this->add_control(
			'show_filter_bar',
			[
				'label'     => __( 'Show Filter Bar', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'parlour-core' ),
				'label_on'  => __( 'On', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'show_title',
			[
				'label' => __( 'Show Title', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_off' => __( 'Off', 'parlour-core' ),
				'label_on' => __( 'On', 'parlour-core' ),
			]
		);

		$this->add_control(
			'show_meta',
			[
				'label' => __( 'Show Meta', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_off' => __( 'Off', 'parlour-core' ),
				'label_on' => __( 'On', 'parlour-core' ),
			]
		);

		$this->add_control(
			'show_social_link',
			[
				'label'        => __( 'Social Link Icon', 'parlour-core' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => __( 'Show', 'parlour-core' ),
				'label_off'    => __( 'Hide', 'parlour-core' ),
				'return_value' => 'yes',
				'default'      => 'yes',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_query',
			[
				'label' => __( 'Query', 'parlour-core' ),
			]
		);

		$this->add_control(
			'source',
			[
				'label' => _x( 'Source', 'Posts Query Control', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Show All', 'parlour-core' ),
					'by_name' => __( 'Manual Selection', 'parlour-core' ),
				],
				'label_block' => true,
			]
		);

		$categories = get_terms( 'experiences' );

		$options = [];
		foreach ( $categories as $category ) :
			$options[ $category->slug ] = $category->name;
		endforeach;

		$this->add_control(
			'categories',
			[
				'label' => __( 'Categories', 'parlour-core' ),
				'type' => Controls_Manager::SELECT2,
				'options' => $options,
				'default' => [],
				'label_block' => true,
				'multiple' => true,
				'condition' => [
					'source' => 'by_name',
				],
			]
		);

		$this->add_control(
			'hide_empty',
			[
				'label' => __( 'Hide Empty', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => 'Hide',
				'label_off' => 'Show',
				'return_value' => 'yes',
			]
		);

		$this->add_control(
			'advanced',
			[
				'label' => __( 'Advanced', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => __( 'Order By', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'post_date',
				'options' => [
					'post_date'  => __( 'Date', 'parlour-core' ),
					'post_title' => __( 'Title', 'parlour-core' ),
					'menu_order' => __( 'Menu Order', 'parlour-core' ),
					'rand'       => __( 'Random', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label' => __( 'Order', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'desc',
				'options' => [
					'asc' => __( 'ASC', 'parlour-core' ),
					'desc' => __( 'DESC', 'parlour-core' ),
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_items',
			[
				'label' => __( 'Items', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'item_background',
			[
				'label' => __( 'Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-content-wrapper' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .bdt-assistant-grid .uk-overlay-gradient' => 'background: -moz-linear-gradient(top, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%); background: -webkit-linear-gradient(top, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%); background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 40%, {{VALUE}} 100%);',
				],
			]
		);

		$this->add_responsive_control(
			'border_radius',
			[
				'label' => __( 'Border Radius', 'elementor' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-content-wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}}; overflow: hidden;',
					'{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-img .bdt-assistant-img-design' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} 0 0;',


				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'border',
				'label' => __( 'Border Color', 'parlour-core' ),
				'placeholder' => '1px',
				'default' => '1px',
				'selector' => '{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-content-wrapper',
				'condition' => [
					'border_radius!' => null,
				],
			]
		);
		
		$this->add_control(
			'item_hover_border_color',
			[
				'label' => __( 'Hover Border Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'border_border!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-content-wrapper:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'item_box_shadow',
				'selector' => '{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-content-wrapper',
			]
		);

		$this->add_responsive_control(
			'desc_padding',
			[
				'label' => __( 'Description Padding', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-desc' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'alignment',
			[
				'label'       => __( 'Alignment', 'parlour-core' ),
				'type'        => Controls_Manager::CHOOSE,
				'separator' => 'before',
				'label_block' => false,
				'options'     => [
					'left'        => [
						'title'       => __( 'Left', 'parlour-core' ),
						'icon'        => 'fa fa-align-left',
					],
				'center'      => [
					'title'       => __( 'Center', 'parlour-core' ),
					'icon'        => 'fa fa-align-center',
					],
				'right'       => [
					'title'       => __( 'Right', 'parlour-core' ),
					'icon'        => 'fa fa-align-right',
					],
				],
				'default'     => 'center',
				'separator' => 'before',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_title',
			[
				'label'     => __( 'Title', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_title' => 'yes',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => __( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-title',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_meta',
			[
				'label'     => __( 'Meta', 'parlour-core' ),
				'tab'       => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_meta' => 'yes',
				],
			]
		);

		$this->add_control(
			'meta_color',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-meta li a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'meta_separator_color',
			[
				'label' => __( 'Separator Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-meta li:not(:last-child)::after' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'meta_typography',
				'label' => __( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-meta li a',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_icon',
			[
				'label' => __( 'Icon', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_social_link' => 'yes',
				],
			]
		);

		$this->add_control(
			'icon_color',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-icon a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'hover_icon_color',
			[
				'label' => __( 'Hover Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-grid .bdt-assistant-icon a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_filter_bar',
			[
				'label' => __( 'Filter Bar', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_filter_bar' => 'yes',
				],
			]
		);

		$this->add_control(
			'color_filter',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-filter-item' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'color_filter_active',
			[
				'label' => __( 'Active Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-filter-item.bdt-assistant-filter-item-active' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'filter_separator_color',
			[
				'label' => __( 'Separator Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-grid .uk-subnav-divider > :nth-child(n+2):not(.uk-first-column)::before' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'filter_item_spacing',
			[
				'label' => __( 'Space Between', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 40,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-filter.uk-subnav-divider > :nth-child(n+2):not(.uk-first-column)::before' => 'margin-right: calc({{SIZE}}{{UNIT}}/2)',
					'{{WRAPPER}} .bdt-assistant-filter.uk-subnav' => 'margin-left: calc({{SIZE}}{{UNIT}}/2)',
					'{{WRAPPER}} .bdt-assistant-filter.uk-subnav > *' => 'padding-left: calc({{SIZE}}{{UNIT}}/2)',
				],
			]
		);

		$this->add_responsive_control(
			'filter_spacing',
			[
				'label' => __( 'Bottom Spacing', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 40,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-assistant-filter-wrapper .bdt-assistant-filter' => 'margin-bottom: {{SIZE}}{{UNIT}} !important',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography_filter',
				'label' => __( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .bdt-assistant-filter-item',
			]
		);

		$this->end_controls_section();
	}

	public function render() {
		$settings  = $this->get_settings();

		global $post;
		$id         = uniqid('bdtsg_');
		
		$large      = ($settings['columns']) ? 'uk-width-1-'.$settings['columns'].'@l' : '';
		$medium     = ($settings['columns_tablet']) ? 'uk-width-1-'.$settings['columns_tablet'].'@m' : '';
		$small      = ($settings['columns_mobile']) ? 'uk-width-1-'.$settings['columns_mobile'].'@s' : '';
		$responsive = [$small, $medium, $large];

		$args = array(
			'post_type'      => 'assistants',
			'posts_per_page' => ($settings['posts_per_page']),
			'orderby'        => $settings['orderby'],
			'order'          => $settings['order'],
			'post_status'    => 'publish',
		);
			
		if('by_name' === $settings['source']) :
			$args['tax_query'][] = array(
				'taxonomy' => 'experiences',
				'field'    => 'slug',
				'terms'    => $settings['categories'],
			);
		endif;

		$wp_query = new \WP_Query($args);

		if( $wp_query->have_posts() ) : ?>
			<div class="bdt-assistant-grid">

				<?php if( 'yes' == $settings['show_filter_bar'] ) : 

					$assistant_cat_filter = [];
					// Get Filters from Shortcode Options
					if ( 'by_name' === $settings['source'] ) :
						$assistant_cat_filter = $settings['categories'];
				 	else :
						$assistant_terms = get_terms( 'experiences' );
						foreach( $assistant_terms as $assistant_filter ) :
							$assistant_cat_filter[] = $assistant_filter->slug;
						endforeach;

					endif;
						?>

					<div class="bdt-assistant-filter-wrapper">
						<ul id="<?php echo $id; ?>-filter" class="bdt-assistant-filter uk-flex-<?php echo esc_attr($settings['alignment']);?> uk-subnav uk-subnav-divider uk-margin-large-bottom" >
							<?php if(is_array($assistant_cat_filter)): ?>
									<li>
										<a data-filter="*" class="bdt-assistant-filter-item-active bdt-assistant-filter-item" href="javascript:;">
											<?php esc_html_e('All Assistant', 'parlour-core'); ?>
										</a>
									</li>	
								<?php foreach($assistant_cat_filter as $assistant_cat_filter => $value) : ?>
									<?php $filter_name = get_term_by('slug', $value, 'experiences'); ?>
									<li>
										<a class="bdt-assistant-filter-item" data-filter=".sci-<?php echo esc_attr(trim($value)); ?>" href="javascript:;">
											<?php echo esc_html($filter_name->name); ?>
										</a>
									</li>
								<?php endforeach; ?>
							<?php endif; ?>
						</ul>
					</div>
				<?php endif //end if filter ?>
				
				<div id="<?php echo $id; ?>" class="bdt-assistant-container uk-grid-<?php echo $settings['column_gap']; ?>" uk-grid>
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
						<?php $terms = get_the_terms( get_the_ID(), 'experiences' ); ?>
						<div class="bdt-assistant-item-wrapper <?php echo implode(" ", $responsive); ?>
							 <?php if( $settings['show_filter_bar'] ) : 
				    			foreach ($terms as $term) :
				    				echo ' sci-'.esc_attr($term->slug);
				    			endforeach;
				    		endif; ?>">
							<div class="bdt-assistant-item">
								<div class="bdt-assistant-content-wrapper uk-background-default uk-box-shadow-small bdt-assistant-align-<?php echo $settings['alignment']; ?>">
								
							    	<?php if (has_post_thumbnail()) : ?>
								        <div class="bdt-assistant-img-wrapper uk-position-relative uk-overflow-hidden">
								            <div class="bdt-assistant-img-design">
												<?php $images = rwmb_meta( 'bdthemes_assistant_altimg', 'type=image_advanced&size=medium' ); ?>

												<?php if (has_post_thumbnail() and empty($images) ) : ?>
												    <div class="">
											            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
											                <?php echo  the_post_thumbnail('medium', array('class' => 'bdt-assistant-img'));  ?>
											            </a>
												    </div>
												<?php elseif (!empty($images)) : ?>
											        <div class="bdt-assistant-image-gallery uk-position-relative uk-overflow-hidden" uk-toggle="target: > .bdt-assistant-img-flip; mode: hover; animation: uk-animation-fade; queued: true; duration: 300">
										                <div class="bdt-assistant-img-flip uk-position-absolute uk-position-z-index">
										                    <?php echo  the_post_thumbnail('medium', array('class' => 'bdt-assistant-img'));  ?>
										                </div>
										                <?php foreach ( $images as $image) : ?> 
										                    <div class="bdt-assistant-img">
										                    <a href="<?php echo esc_url($image['full_url']); ?>" title="<?php echo esc_attr($image['title']); ?>">
										                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" width="<?php echo esc_attr($image['width']); ?>" height="<?php echo esc_attr($image['height']); ?>" class="bdt-assistant-img" />
										                    </a>
										                    </div>
										                <?php endforeach; ?>
											        </div>
												<?php endif ?>

								                <div class="bdt-assistant-overlay uk-position-cover uk-overlay-gradient uk-position-z-index"></div>
								            </div>  
								        </div>
								    <?php endif; ?>

								    <?php if(( 'yes' == $settings['show_title'] ) || ( 'yes' == $settings['show_meta'] ) || ( 'yes' == $settings['show_social_link'] )) : ?>
								        <div class="bdt-assistant-desc uk-padding uk-position-relative uk-position-z-index">

								            <?php if( 'yes' == $settings['show_title'] ) : ?>
								                <h3 class="bdt-assistant-title uk-text-<?php echo $settings['alignment']; ?> uk-margin-remove-top uk-margin-small-bottom">
												    <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>" class="uk-link-reset"><?php the_title(); ?></a>
												</h3>
								            <?php endif; ?>

								            <?php if( 'yes' == $settings['show_meta']) :
								               echo get_the_term_list(get_the_ID(),'experiences', '<ul class="bdt-assistant-meta bdt-assistant-meta uk-text-'.$settings['alignment'].' uk-margin-small-top uk-margin-remove"><li>', '</li><li>', '</li></ul>' );
								            endif; ?>

								            <?php if( 'yes' == $settings['show_social_link'] ) :
								                $social_link = get_post_meta( get_the_ID(), 'bdthemes_assistant_social_link', true );
								                if($social_link != null and is_array($social_link)) : ?>
								                	<div class="bdt-assistant-icon bdt-assistant-social uk-position-absolute uk-position-medium uk-margin-remove-vertical uk-position-bottom-<?php echo $settings['alignment']; ?>">
								                		<ul class="uk-list uk-margin-remove-bottom">
								                	    <?php foreach ($social_link as $link) : ?>
								                	        <?php $tooltip = ucfirst(\bdthemes_helper::icon($link)); ?>
								                	        <li class="uk-display-inline-block">
								                	            <a<?php echo \bdthemes_helper::attrs(['href' => $link, 'class' => 'uk-margin-small-right']); ?> uk-icon="icon: <?php echo \bdthemes_helper::icon($link); ?>" title="<?php echo esc_html($tooltip); ?>" uk-tooltip></a>
								                	        </li>
								                	    <?php endforeach ?>
								                	    </ul>
								                	</div>
								                <?php endif;
								            endif; ?>
								        </div>
								    <?php endif; ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>					
					<?php wp_reset_postdata(); ?> 
				</div>
			</div>
			
			<?php if ( 'yes' == $settings['show_filter_bar'] ) : ?>
				<script>
					jQuery(document).ready(function($) {
					    'use strict';
					    var $grid = $('#<?php echo $id; ?>').isotope({
						  itemSelector: '.bdt-assistant-item-wrapper',
						  percentPosition: true,
						  layoutMode: 'masonry',
						});

						$('#<?php echo $id; ?>-filter').on( 'click', 'a', function() {
						  var filterValue = $(this).attr('data-filter');
						  $grid.isotope({ filter: filterValue });
						});

						$('#<?php echo $id; ?>-filter > li > .bdt-assistant-filter-item').click(function(){
						    $('#<?php echo $id; ?>-filter > li > .bdt-assistant-filter-item').removeClass('bdt-assistant-filter-item-active');
						    $(this).addClass('bdt-assistant-filter-item-active');
						});
					});
				</script>
			<?php endif; ?>
		<?php endif;
	}
}

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" role="search" class="uk-search uk-search-default uk-width-1-1">
    <span uk-search-icon></span>
    <input id="<?php echo esc_attr($unique_id); ?>" name="s" placeholder="<?php esc_html_e('Search...', 'parlour'); ?>" type="search" class="uk-search-input">
</form>
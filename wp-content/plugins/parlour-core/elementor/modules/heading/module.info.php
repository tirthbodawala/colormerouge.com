<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

return [
	'title' => __( 'Heading', 'parlour-core' ),
	'required' => true,
	'default_activation' => true,
];

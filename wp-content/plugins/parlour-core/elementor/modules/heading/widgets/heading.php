<?php
namespace ElementorExtend\Modules\Heading\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Text_Shadow;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// remove default heading for extend heading control
add_action(
    'elementor/widgets/widgets_registered',
    function($widgetManager) {
        $widgetManager->unregister_widget_type('heading');
    }, 10,  3
);

class Heading extends Widget_Base {

	public function get_name() {
		return 'heading';
	}

	public function get_title() {
		return __( 'Heading', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-type-tool';
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_title',
			[
				'label' => __( 'Title', 'parlour-core' ),
			]
		);

		$this->add_control(
			'pre_title',
			[
				'label' => __( 'Pre Title', 'parlour-core' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => __( 'Enter your prefix title', 'parlour-core' ),
			]
		);

		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'parlour-core' ),
				'type' => Controls_Manager::TEXTAREA,
				'placeholder' => __( 'Enter your title', 'parlour-core' ),
				'default' => __( 'This is heading element', 'parlour-core' ),
			]
		);

		$this->add_control(
			'link',
			[
				'label' => __( 'Link', 'parlour-core' ),
				'type' => Controls_Manager::URL,
				'placeholder' => 'http://your-link.com',
				'default' => [
					'url' => '',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'header_size',
			[
				'label' => __( 'HTML Tag', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'h1' => __( 'H1', 'parlour-core' ),
					'h2' => __( 'H2', 'parlour-core' ),
					'h3' => __( 'H3', 'parlour-core' ),
					'h4' => __( 'H4', 'parlour-core' ),
					'h5' => __( 'H5', 'parlour-core' ),
					'h6' => __( 'H6', 'parlour-core' ),
					'div' => __( 'div', 'parlour-core' ),
					'span' => __( 'span', 'parlour-core' ),
					'p' => __( 'p', 'parlour-core' ),
				],
				'default' => 'h2',
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'parlour-core' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'parlour-core' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'parlour-core' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'parlour-core' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'parlour-core' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'default' => 'center',
				'selectors' => [
					'{{WRAPPER}}' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'view',
			[
				'label' => __( 'View', 'parlour-core' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-heading' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'hide_style',
			[
				'label' => __( 'Hide Style', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'label_on' => 'Hide',
				'label_off' => 'Show',
				'return_value' => 'yes',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .bdt-heading',
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'text_shadow',
				'selector' => '{{WRAPPER}} .bdt-heading',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_pre_title_style',
			[
				'label' => __( 'Pre Title', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'pre_title_color',
			[
				'label' => __( 'Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-heading-pre-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'pre_typography',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .bdt-heading-pre-title',
			]
		);

		$this->add_group_control(
			Group_Control_Text_Shadow::get_type(),
			[
				'name' => 'pre_text_shadow',
				'selector' => '{{WRAPPER}} .bdt-heading-pre-title',
			]
		);


		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings();

		if ( empty( $settings['title'] ) ) {
			return;
		}

		$this->add_render_attribute( 'heading', 'class', 'bdt-heading' );

		$title = $settings['title'];
		$pre_title = $settings['pre_title'];

		if ( ! empty( $settings['link']['url'] ) ) {
			$this->add_render_attribute( 'url', 'href', $settings['link']['url'] );

			if ( $settings['link']['is_external'] ) {
				$this->add_render_attribute( 'url', 'target', '_blank' );
			}

			if ( ! empty( $settings['link']['nofollow'] ) ) {
				$this->add_render_attribute( 'url', 'rel', 'nofollow' );
			}

			$title = sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( 'url' ), $title );
		}
		$title_html = [];
		$title_html[] = '<div class="bdt-heading-wrapper">';
		if ($pre_title) {
			$title_html[] = '<div class="bdt-heading-pre-title">'.$pre_title.'</div>';
		}
		$title_html[] = sprintf( '<%1$s %2$s>%3$s</%1$s>', $settings['header_size'], $this->get_render_attribute_string( 'heading' ), $title );
		if ($settings['hide_style'] !== 'yes') {
			$title_html[] = '<div class="bdt-heading-style"></div>';
		}
		$title_html[] = '</div>';

		echo implode("", $title_html);
	}

	protected function _content_template() {
		?>

		
		<div class="bdt-heading-wrapper">
			<#
				var title = settings.title;
				var pre_title = settings.pre_title;

				if ( '' !== settings.link.url ) {
					title = '<a href="' + settings.link.url + '">' + title + '</a>';
				}
				
				if ( '' !== settings.pre_title ) {
					var pre_title_html = '<div class="bdt-heading-pre-title">' + pre_title + '</div>';
				}
				var title_html = '<' + settings.header_size  + ' class="bdt-heading">' + title + '</' + settings.header_size + '>';

				print( pre_title_html  );
				print( title_html );

				if ( 'yes' !== settings.hide_style ) {
					print('<div class="bdt-heading-style"></div>');
				}
			#>
			</div>
		<?php
	}
}

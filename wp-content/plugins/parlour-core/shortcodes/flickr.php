<?php
// Bdthemes flickr shortcode

if (!function_exists('bdthemes_flickr')) {
	function bdthemes_flickr($atts) {
		extract(shortcode_atts(array(
			'flickr_id'     => '95572727@N00',
			'limit'         => '9',
			'lightbox'      => 'no',
			'radius'        => '', // rounded, circle
			
			'class'         => ''
		), $atts));

		
		
		$unique_id     = uniqid("bdtflickr_");
		$classes       = ['bdt-flickr-feed', $class];
		
		$output        = [];
		
		$radius        = ($radius) ? 'uk-border-' . $radius : '';
		$image         = ($lightbox == 'yes') ? '<a href="{{image_b}}" title="{{title}}"> ' : '';
		$image         .= '<img class="'.$radius.'" src="{{image_s}}" alt="{{title}}" />';
		$image         .= ($lightbox == 'yes') ? '</a> ' : '';

		if ($lightbox == 'yes') {
		    $classes[] = 'image-lightbox';
		}

		$output[] = "<ul id='".$unique_id."' class='".bdt_acssc($classes)."'></ul><div class='uk-clearfix'></div>";

		$output[] = "<script type='text/javascript'>
				      jQuery(document).ready(function() {
				              jQuery('#".$unique_id."').jflickrfeed({
				                limit: " . $limit . ", qstrings: {
				                  id: '" . $flickr_id . "'},
				                  itemTemplate: '<li>" . addslashes($image) . "</li>' });
				            });
				    </script> ";
		return implode("\n", $output);
	}
	add_shortcode('bdt_flickr', 'bdthemes_flickr');
}
<?php
// Bdthemes thumb gallery

if (!function_exists('bdthemes_thumb_gallery')) {
	function bdthemes_thumb_gallery($atts){
		extract(shortcode_atts(array( 
			'post_type'     => 'post',
			'images'        => '',
			'limit'         => '8',
			'orderby'       => 'date',
			'order'         => 'DESC',
			'style'         => '1',
			'autoplay'		=> 'yes',
			'speed'		    => 4500,
			'class'         => ''
		), $atts));

		

		global $post;

		$img_size = '';
		$classes  = ['bdt-thumb-gallery', 'bdt-thumb-gallery-style-'.$style, $class];

		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => intval($limit),
			'orderby'        => $orderby,
			'order'          => $order,
			'post_status'    => 'publish',
		);

		$wp_query = new WP_Query($args);
		ob_start(); // start buffer
			
		$id = uniqid('bdttg_'); 
		$value = '';
		$pagination_item = [];
			 
	?>
	
	<div class="<?php echo bdt_acssc($classes); ?>">
		<div id="<?php echo $id; ?>_container" class="swiper-container bdt-tg-container"> 
			<div class="swiper-wrapper">
			<?php if ($post_type != 'custom') : ?>
				<?php if( $wp_query->have_posts() ) : ?>
					<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

						<?php 
							if ( has_post_thumbnail()) { 
								$gallery_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
								$gallery_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'thumbnail' );
							}
						?>  
						<div class="swiper-slide">
							<div class="bdt-tg-item">
								<div class="bdt-tg-caption uk-position-absolute uk-background-default">
									<h4 class="uk-margin-remove"><?php echo esc_attr(get_the_title()); ?></h4>
									<p class="uk-text-small uk-margin-remove"><?php echo wp_kses_post(parlour_core_custom_excerpt(15)); ?></p>
									<a href="<?php echo esc_url(get_permalink()); ?>" class="bdt-tg-dbtn uk-icon uk-flex uk-flex-middle uk-position-top-right uk-light" uk-icon="icon: chevron-right; ratio: 1.15"></a>
								</div>
								<img src="<?php echo esc_url($gallery_image[0]); ?>" alt="<?php echo esc_attr(get_the_title()); ?>" />
							</div>
						</div>

						<?php
							$pagination_item[] = '<div class="swiper-slide" style="background-image: url('.esc_url($gallery_thumbnail[0]).')">
            				</div>';
            			?>

					<?php endwhile; ?>
				<?php wp_reset_postdata(); 
			endif; ?>

			<?php else : ?>
				<?php 
					$images = ($post_type == 'custom' ) ? explode( ',', $images ) : '';
					foreach ( $images as $i => $image ) : ?>
						<?php
							$gallery_image = wp_get_attachment_image_src( $image, 'large' );
							$gallery_thumbnail = wp_get_attachment_image_src( $image, 'thumbnail' );
						?>

						<div class="swiper-slide">
							<div class="bdt-tg-item">
								<img src="<?php echo esc_url($gallery_image[0]); ?>" alt="<?php echo esc_attr(get_the_title()); ?>" />
							</div>
						</div>

						<?php
							$pagination_item[] = '<div class="swiper-slide" style="background-image: url('.esc_url($gallery_thumbnail[0]).')">
            				</div>';
            			?>

					<?php endforeach; ?>

			<?php endif; ?>

			</div>
			<div class="swiper-button-next swiper-button-white"></div>
			<div class="swiper-button-prev swiper-button-white"></div>
		</div>
		<div id="<?php echo $id; ?>_nav"  class="swiper-container bdt-tgt-container">
	        <div class="swiper-wrapper">
				<?php echo implode("\n", $pagination_item); ?>
	        </div>
	    </div>
	</div>

	<?php 

	 	$settings = [
	 		'navigation' => [
				'nextEl' => '#'.$id.' .swiper-button-nextt',
				'prevEl' => '#'.$id.' .swiper-button-prev',
			],
			'autoplay'     => $autoplay ? [ 'delay' => $speed ] : false,
			'loop'         => false,
			'spaceBetween' => 10,
	 	];

	 	$thumb_settings = [
			'spaceBetween'        => 10,
			'centeredSlides'      => true,
			'centeredSlides'      => true,
			'slidesPerView'       => 'auto',
			'touchRatio'          => 0.2,
			'slideToClickedSlide' => true,
	 	];
	?>

	<script>
	 	jQuery(document).ready(function($) {
			"use strict";
			var tgContainer             = new Swiper("#<?php echo $id; ?>_container", <?php echo json_encode($settings); ?>);
			var tgtContainer            = new Swiper("#<?php echo $id; ?>_nav", <?php echo json_encode($thumb_settings); ?>);
			tgContainer.params.control  = tgtContainer;
			tgtContainer.params.control = tgContainer;
		});
	</script>
		
	<?php 

	$gallery_output = ob_get_contents(); // get buffered content

	ob_end_clean(); // clean buffer

	return $gallery_output;
	
	}
	add_shortcode('bdt_thumb_gallery', 'bdthemes_thumb_gallery');
}
<?php
$layout_c               = get_theme_mod('parlour_header_layout', 'horizontal-left');
$layout_m               = get_post_meta( get_the_ID(), 'parlour_header_layout', true );
$layout                 = (!empty($layout_m) and $layout_m != 'default') ? $layout_m : $layout_c;
$logo                   = get_theme_mod('parlour_logo_default');
$logo_width             = get_theme_mod('parlour_logo_width_default');
$logo_mode              = ($logo) ? 'tm-logo-img' : 'tm-logo-text';
$class                  = ['uk-logo'];
$class[]                = (!in_array($layout, ['stacked-left-a', 'stacked-left-b', 'stacked-center-b', 'stacked-center-a', 'side-left', 'side-right']))  ? 'uk-navbar-item' : '';
$class[]                = $logo_mode;
$width                  = ($logo_width) ? $logo_width : '';
$img_atts               = [];
$img_atts['class'][]    = 'uk-responsive-height';
$img_atts['src'][]      = esc_url($logo);
$img_atts['style'][]    = 'width:'.esc_attr($width);
$img_atts['itemprop'][] = 'logo';
$img_atts['alt'][]      = get_bloginfo( 'name' );
?>

<a href="<?php echo esc_url(home_url('/')); ?>"<?php echo parlour_helper::attrs(['class' => $class]) ?> itemprop="url">
    <?php if ($logo) : ?>
        <img<?php echo parlour_helper::attrs($img_atts) ?>>
    <?php else : ?>
        <?php bloginfo( 'name' );?>
    <?php endif; ?>
</a>
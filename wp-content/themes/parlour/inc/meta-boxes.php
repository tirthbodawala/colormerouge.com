<?php
/**
 * Registering meta boxes
 *
 * All the definitions of meta boxes are listed below with comments.
 * Please read them CAREFULLY.
 * You also should read the changelog to know what has been changed before updating.
 * For more information, please visit:
 * @link http://www.deluxeblogtips.com/meta-box/docs/define-meta-boxes
 */

/********************* Meta Box Definitions ***********************/

add_action( 'admin_init', 'rw_register_meta_boxes' );
function rw_register_meta_boxes() {
	
	// load is_plugin_active() function if no available
	if (!function_exists('is_plugin_active')) {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); 
	}
	global $meta_boxes;

	$prefix = 'parlour_';
	$meta_boxes = [];

	/* ----------------------------------------------------- */
	// FAQ Filter Array
	if(is_plugin_active('parlour-faq/parlour-faq.php')){ 

		$types = get_terms('faq_filter', 'hide_empty=0');
		$types_array[0] = 'All categories';
		if($types) {
			foreach($types as $type) {
				$types_array[$type->term_id] = $type->name;
			}
		}

	}


	// SIDEBAR ARRAY
	function get_sidebars_array() {
	    global $wp_registered_sidebars;
	    $list_sidebars = [];
	    foreach ( $wp_registered_sidebars as $sidebar ) {
	        $list_sidebars[$sidebar['id']] = $sidebar['name'];
	    }
	    // remove them from the list for better understand purpose
	    unset($list_sidebars['footer-widgets']);
	    unset($list_sidebars['footer-widgets-2']);
	    unset($list_sidebars['footer-widgets-3']);
	    unset($list_sidebars['footer-widgets-4']);
	    unset($list_sidebars['offcanvas']);
	    unset($list_sidebars['fixed-left']);
	    unset($list_sidebars['fixed-right']);
	    unset($list_sidebars['headerbar']);
	    unset($list_sidebars['drawer']);
	    unset($list_sidebars['bottom']);
	    return $list_sidebars;
	}

	// Rev Slider list generate
	function rev_slider_list() {
		if(shortcode_exists("rev_slider")){
			$output = [];
			$slider = new RevSlider();
			$revolution_sliders = $slider->getArrSliders();

			foreach ( $revolution_sliders as $revolution_slider ) {
			       $alias = $revolution_slider->getAlias();
			       $title = $revolution_slider->getTitle();
			       $output[$alias] = $title;
			}
			return $output;
		}	
	}


	/* ----------------------------------------------------- */
	// Blog Categories Filter Array
	$blog_options = []; // fixes a PHP warning when no blog posts at all.
	$blog_categories = get_categories();
	if($blog_categories) {
		foreach ($blog_categories as $category) {
			$blog_options[$category->slug] = $category->name;
		}
	}

	/* ----------------------------------------------------- */
	// Service Categories Filter Array
	$service_options = []; // fixes a PHP warning when no blog posts at all.
	$service_categories = get_terms('service-categories');
	if(isset($service_categories)) {
		foreach ($service_categories as $category) {
	           if(is_object($category))
				$service_options[$category->slug] = $category->name;
		}
	}

	/* ----------------------------------------------------- */
	// Assistant Categories Filter Array
	$assistant_options = []; // fixes a PHP warning when no blog posts at all.
	$assistant_categories = get_terms('experiences');
	if(isset($assistant_categories)) {
		foreach ($assistant_categories as $category) {
	           if(is_object($category))
				$assistant_options[$category->slug] = $category->name;
		}
	}
	
 
	/* ----------------------------------------------------- */
	// Page Settings
	/* ----------------------------------------------------- */
	
	$meta_boxes[] = array(
		'id'       => 'pagesettings',
		'title'    => 'Page Settings',
		'pages'    => array( 'page' ),
		'context'  => 'normal',
		'priority' => 'high',

		'tabs'      => array(
			'layout' => array(
                'label' => esc_html__('Layout', 'parlour'),
            ),
            'slider' => array(
                'label' => esc_html__('Slider', 'parlour'),
            ),
            'titlebar' => array(
                'label' => esc_html__('Page Titlebar', 'parlour'),
            ),
            'footer' => array(
                'label' => esc_html__('Footer', 'parlour'),
            ),
            'blog'  => array(
                'label' => esc_html__( 'Blog', 'parlour'),
            ),
            'services'  => array(
                'label' => esc_html__( 'Services', 'parlour'),
            ),
            'assistants'  => array(
                'label' => esc_html__( 'Assistants', 'parlour'),
            ),
            'testimonials'  => array(
                'label' => esc_html__( 'Testimonials', 'parlour'),
            ),
        ),

        // Tab style: 'default', 'box' or 'left'. Optional
        'tab_style' => 'default',
	
		// List of meta fields
		'fields' => array(
			array(
					'name'		=> 'Toolbar',
					'id'		=> $prefix . "toolbar",
					'type'		=> 'select',
					'options'	=> array(
						null        => esc_html_x('Default (as customizer)', 'backend', 'parlour'),
						true		=> 'Enable',
						false		=> 'Disable'
					),
					'multiple' => false,
					'std'      => null,
					'desc'     => 'Enable or disable the toolbar for this page.',
					'tab'      => 'layout',
			),

			array(
				'name'		=> esc_html_x('Page Layout', 'backend', 'parlour'),
				'id'		=> $prefix . "page_layout",
				'type'		=> 'select',
				'options'	=> array(
					'default'       => esc_html_x('Default', 'backend', 'parlour'),
					'full'          => esc_html_x('Fullwidth', 'backend', 'parlour'),
					'sidebar-right' => esc_html_x('Sidebar Right', 'backend', 'parlour'),
					'sidebar-left'  => esc_html_x('Sidebar Left', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( 'default' ),
				'desc'     => wp_kses(_x('<strong>Default:</strong> For usage normal Text Pages<br /> <strong>Full Width:</strong> For pages using Elementor (commonly used)<br /> <strong>Sidebar Left:</strong> Sidebar Left Template<br /> <strong>Sidebar Right:</strong> Sidebar Right Template', 'backend', 'parlour'), array('strong'=>array(), 'br'=>array())),
				'tab'      => 'layout',
			),

			array(
				'name'     => esc_html_x('Sidebar', 'backend', 'parlour'),
				'id'       => $prefix . "sidebar",
				'type'     => 'select',
				'options'  => get_sidebars_array(),
				'multiple' => false,
				'std'      => array( 'show' ),
				'desc'     => esc_html_x('Select the sidebar you wish to display on this page.', 'backend', 'parlour'),
				'tab'      => 'layout',
				'visible'  => array($prefix . 'page_layout', 'starts with', 'sidebar'),
			),

			array(
				'name'		=> esc_html_x('Header Layout', 'backend', 'parlour'),
				'id'		=> $prefix . "header_layout",
				'type'		=> 'image_select',
				'options'	=> array(
					null                   => get_template_directory_uri().'/admin/images/header-default.png',
					'horizontal-left'      => get_template_directory_uri().'/admin/images/header-horizontal-left.png',
					'horizontal-center'    => get_template_directory_uri().'/admin/images/header-horizontal-center.png',
					'horizontal-right'     => get_template_directory_uri().'/admin/images/header-horizontal-right.png',
					'stacked-center-a'     => get_template_directory_uri().'/admin/images/header-stacked-center-a.png',
					'stacked-center-b'     => get_template_directory_uri().'/admin/images/header-stacked-center-b.png',
					'stacked-center-split' => get_template_directory_uri().'/admin/images/header-stacked-center-split.png',
					'stacked-left-a'       => get_template_directory_uri().'/admin/images/header-stacked-left-a.png',
					'stacked-left-b'       => get_template_directory_uri().'/admin/images/header-stacked-left-b.png',
					'toggle-offcanvas'     => get_template_directory_uri().'/admin/images/header-toggle-offcanvas.png',
					'toggle-modal'         => get_template_directory_uri().'/admin/images/header-toggle-modal.png',
					'side-left'            => get_template_directory_uri().'/admin/images/header-side-left.png',
					'side-right'           => get_template_directory_uri().'/admin/images/header-side-right.png',
				),
				'multiple' => false,
				'std'      => array( null ),
				'desc'     => esc_html_x('Override the header style for this page.', 'backend', 'parlour'),
				'tab'      => 'layout',
			),


			
			array(
				'name'		=> 'Background Style',
				'id'		=> $prefix . "header_bg_style",
				'type'		=> 'select',
				'options'	=> array(
					null        => esc_html_x('Default (as customizer)', 'backend', 'parlour'),
					'default'   => esc_html_x('Default', 'backend', 'parlour'),
					'muted'     => esc_html_x('Muted', 'backend', 'parlour'),
					'primary'   => esc_html_x('Primary', 'backend', 'parlour'),
					'secondary' => esc_html_x('Secondary', 'backend', 'parlour'),
					'media'     => esc_html_x('Image', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( 'light' ),
				'desc'     => 'Select your header style from here.',
				'tab'      => 'layout',
				'hidden' => array($prefix . 'header_layout', '=', false ),
			),
			array(
				'name'             => 'Background Image',
				'id'               => $prefix . "header_bg_img",
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
				'desc'             => 'Upload header Image for the header Style.',
				'tab'              => 'layout',
				'visible'           => array($prefix . 'header_bg_style', '=', 'media'),
			),
			array(
				'name'		=> 'Header Text Style',
				'id'		=> $prefix . "header_txt_style",
				'type'		=> 'select',
				'options'	=> array(
					null    => esc_html_x('Default (as customizer)', 'backend', 'parlour'),
					'light' => esc_html_x('Light', 'backend', 'parlour'),
					'dark'  => esc_html_x('Dark', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( null ),
				'desc'     => 'Select your header text style from here.',
				'tab'      => 'layout',
				'hidden' => array($prefix . 'header_layout', '=', false ),
			),
			array(
				'name'		=> 'Header Transparent',
				'id'		=> $prefix . "header_transparent",
				'type'		=> 'select',
				'options'	=> array(
					null    => esc_html_x('Default (as customizer)', 'backend', 'parlour'),
					'no'    => esc_html_x('No', 'backend', 'parlour'),
					'light' => esc_html_x('Overlay (Light)', 'backend', 'parlour'),
					'dark'  => esc_html_x('Overlay (Dark)', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( null ),
				'desc'     => 'Select your header transparent style from here.',
				'tab'      => 'layout',
				'hidden' => array($prefix . 'header_layout', 'in', ['side-left', 'side-right', false] ),
			),
			array(
				'name'		=> esc_html_x('Header Sticky', 'backend', 'parlour'),
				'id'		=> $prefix . "header_sticky",
				'type'		=> 'select',
				'options'	=> array(
					null     => esc_html_x('Default (as customizer)', 'backend', 'parlour'),
					'no'     => esc_html_x('No', 'backend', 'parlour'),
					'sticky' => esc_html_x('Sticky', 'backend', 'parlour'),
					'smart'  => esc_html_x('Smart Sticky', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( null ),
				'desc'     => esc_html_x('Override the header type for this page.', 'backend', 'parlour'),
				'tab'      => 'layout',
				'hidden' => array($prefix . 'header_layout', 'in', ['side-left', 'side-right'] ),
			),

			

			array(
				'name'		=> 'Show Slider',
				'id'		=> $prefix . 'show_rev_slider',
				'type'		=> 'radio',
				'options'	=> array(
					'yes'		=> 'Yes',
					'no'		=> 'No'
				),
				'multiple' => false,
				'std'      => array( 'no' ),
				'desc'     => 'Select yes if you want to show slider in this page.',
				'tab'      => 'slider',
			),
			array(
				'name'     => 'Select Slider',
				'id'       => $prefix . "rev_slider",
				'type'     => 'select',
				'options'  => rev_slider_list(),
				'multiple' => false,
				'desc'     => 'Select which revolution slider you want to show this page.',
				'tab'      => 'slider',
				'hidden' => array($prefix . 'show_rev_slider', '=', 'no'),
			),

			array(
				'name'		=> 'Titlebar',
				'id'		=> $prefix . "titlebar",
				'type'		=> 'select',
				'options'	=> array(
					'show' => 'Enable',
					'hide' => 'Disable'
				),
				'multiple' => false,
				'std'      => array( true ),
				'desc'     => 'Enable or disable the Titlebar on this Page.',
				'tab'      => 'titlebar',
			),
			array(
				'name'		=> 'Layout Alignment',
				'id'		=> $prefix . "titlebar_layout",
				'type'		=> 'select',
				'options'	=> array(
					'default' => esc_html_x('Default (set in Theme Customizer)', 'backend', 'parlour'),
					'left'    => esc_html_x('Left Align', 'backend', 'parlour'),
					'center'  => esc_html_x('Center Align', 'backend', 'parlour'),
					'right'   => esc_html_x('Right Align', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( 'default' ),
				'desc'     => 'Choose your Titlebar Layout for this Page',
				'tab'      => 'titlebar',
				'hidden' => array($prefix . 'titlebar', '=', 'hide'),
			),
			array(
				'name'		=> 'Background Style',
				'id'		=> $prefix . "titlebar_bg_style",
				'type'		=> 'select',
				'options'	=> array(
					null        => esc_html_x('Default (as customizer)', 'backend', 'parlour'),
					'default'   => esc_html_x('Default', 'backend', 'parlour'),
					'muted'     => esc_html_x('Muted', 'backend', 'parlour'),
					'primary'   => esc_html_x('Primary', 'backend', 'parlour'),
					'secondary' => esc_html_x('Secondary', 'backend', 'parlour'),
					'media'     => esc_html_x('Image', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( 'light' ),
				'desc'     => 'Select your titlebar style from here.',
				'tab'      => 'titlebar',
				'hidden' => array($prefix . 'titlebar_layout', '=', 'default'),
			),
			array(
					'name'             => 'Background Image',
					'id'               => $prefix . "titlebar_bg_img",
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'desc'             => 'Upload Titlebar Image for the Titlebar Style.',
					'tab'              => 'titlebar',
					'visible'           => array($prefix . 'titlebar_bg_style', '=', 'media'),
			),
			array(
					'name'		=> 'Text Color',
					'id'		=> $prefix . "titlebar_txt_style",
					'type'		=> 'select',
					'options'	=> array(
						'0'     => esc_html_x('Default', 'backend', 'parlour'),
						'light' => esc_html_x('Light', 'backend', 'parlour'),
						'dark'  => esc_html_x('Dark', 'backend', 'parlour'),
					),
					'multiple' => false,
					'std'      => array( '0' ),
					'desc'     => 'Select your titlebar text color from here.',
					'tab'      => 'titlebar',
					'hidden' => array($prefix . 'titlebar_layout', '=', 'default'),
			),

			array(
					'name'		=> 'Footer Widgets',
					'id'		=> $prefix . "footer_widgets",
					'type'		=> 'select',
					'options'	=> array(
						null        => esc_html_x('Default (as customizer)', 'backend', 'parlour'),
						'show'		=> 'Enable',
						'hide'		=> 'Disable'
					),
					'multiple' => false,
					'std'      => null,
					'desc'     => 'Enable or disable the Footer Widgets on this Page.',
					'tab'      => 'footer',
			),
			array(
					'name'		=> 'Copyright',
					'id'		=> $prefix . "copyright",
					'type'		=> 'select',
					'options'	=> array(
						null        => esc_html_x('Default (as customizer)', 'backend', 'parlour'),
						'show'		=> 'Enable',
						'hide'		=> 'Disable'
					),
					'multiple' => false,
					'std'      => null,
					'desc'     => 'Enable or disable the Footer Copyright Section on this Page.',
					'tab'      => 'footer',
			),
			array(
				'name'       => 'Blog Categories',
				'id'         => $prefix . "blog_categories",
				'type'       => 'taxonomy_advanced',
				'taxonomy'   => 'category',
				'field_type' => 'checkbox_tree',
				'desc'       => 'If nothing is selected, it will show Items from <strong>ALL</strong> categories.',
				'tab'        => 'blog',
			),

			array(
				'name'		=> esc_html_x('Grid Column (Large)', 'backend', 'parlour'),
				'id'		=> $prefix.'blog_columns',
				'type'		=> 'select',
				'options'	=> array(
					'1'   => esc_html_x('1 Column', 'backend', 'parlour'),
					'2'   => esc_html_x('2 Columns', 'backend', 'parlour'),
					'3'   => esc_html_x('3 Columns', 'backend', 'parlour'),
					'4'   => esc_html_x('4 Columns', 'backend', 'parlour'),
					//'5' => esc_html_x('5 Columns', 'backend', 'parlour'),
					//'6' => esc_html_x('6 Columns', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( '1' ),
				'tab'      => 'blog',
				//'hidden' => array('jetpack_tm_view', '=', 'list'),
			),

			array(
				'name'		=> esc_html_x('Grid Column (Medium)', 'backend', 'parlour'),
				'id'		=> $prefix.'blog_columns_medium',
				'type'		=> 'select',
				'options'	=> array(
					'1'   => esc_html_x('1 Column', 'backend', 'parlour'),
					'2'   => esc_html_x('2 Columns', 'backend', 'parlour'),
					'3'   => esc_html_x('3 Columns', 'backend', 'parlour'),
					'4'   => esc_html_x('4 Columns', 'backend', 'parlour'),
					//'5' => esc_html_x('5 Columns', 'backend', 'parlour'),
					//'6' => esc_html_x('6 Columns', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( '1' ),
				'tab'      => 'blog',
				//'hidden' => array('jetpack_tm_view', '=', 'list'),
			),

			array(
				'name'     => esc_html_x('Grid Column (Small)', 'backend', 'parlour'),
				'id'       => $prefix.'blog_columns_small',
				'type'     => 'select',
				'std'      => array( '1' ),
				'tab'      => 'blog',
				//'hidden' => array('jetpack_tm_view', '=', 'list'),
				'options'  => array(
					'1' => esc_html_x('1 Column', 'backend', 'parlour'),
					'2' => esc_html_x('2 Columns', 'backend', 'parlour'),
					'3' => esc_html_x('3 Columns', 'backend', 'parlour'),
					//'4' => esc_html_x('4 Columns', 'backend', 'parlour'),
					//'5' => esc_html_x('5 Columns', 'backend', 'parlour'),
					//'6' => esc_html_x('6 Columns', 'backend', 'parlour'),
				),
			),

			array(
				'name'  => esc_html_x('Limit', 'backend', 'parlour' ),
				'id'    => $prefix.'blog_limit',
				'desc'  => esc_html_x('Enter limit number for how much item you want to show per page.', 'backend', 'parlour' ),
				'clone' => false,
				'type'  => 'text',
				'std'   => '10',
				'tab'   => 'blog'
			),





			// Testimonials columns control for page
			array(
				'name'		=> esc_html_x('Grid Column (Large)', 'backend', 'parlour'),
				'id'		=> 'jetpack_tm_columns',
				'type'		=> 'select',
				'options'	=> array(
					'2'		=> esc_html_x('2 Columns', 'backend', 'parlour'),
					'3'		=> esc_html_x('3 Columns', 'backend', 'parlour'),
					'4'		=> esc_html_x('4 Columns', 'backend', 'parlour'),
					'5'		=> esc_html_x('5 Columns', 'backend', 'parlour'),
					'6'		=> esc_html_x('6 Columns', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( '3' ),
				'tab'      => 'testimonials',
				//'hidden' => array('jetpack_tm_view', '=', 'list'),
			),

			array(
				'name'		=> esc_html_x('Grid Column (Medium)', 'backend', 'parlour'),
				'id'		=> 'jetpack_tm_columns_medium',
				'type'		=> 'select',
				'options'	=> array(
					'1'		=> esc_html_x('1 Column', 'backend', 'parlour'),
					'2'		=> esc_html_x('2 Columns', 'backend', 'parlour'),
					'3'		=> esc_html_x('3 Columns', 'backend', 'parlour'),
					'4'		=> esc_html_x('4 Columns', 'backend', 'parlour'),
					'5'		=> esc_html_x('5 Columns', 'backend', 'parlour'),
					'6'		=> esc_html_x('6 Columns', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( '2' ),
				'tab'      => 'testimonials',
				//'hidden' => array('jetpack_tm_view', '=', 'list'),
			),

			array(
				'name'     => esc_html_x('Grid Column (Small)', 'backend', 'parlour'),
				'id'       => 'jetpack_tm_columns_small',
				'type'     => 'select',
				'std'      => array( '1' ),
				'tab'      => 'testimonials',
				//'hidden' => array('jetpack_tm_view', '=', 'list'),
				'options'  => array(
					'1' => esc_html_x('1 Column', 'backend', 'parlour'),
					'2' => esc_html_x('2 Columns', 'backend', 'parlour'),
					'3' => esc_html_x('3 Columns', 'backend', 'parlour'),
					'4' => esc_html_x('4 Columns', 'backend', 'parlour'),
					'5' => esc_html_x('5 Columns', 'backend', 'parlour'),
					'6' => esc_html_x('6 Columns', 'backend', 'parlour'),
				),
			),

			array(
				'name'     => 'Service Categories',
				'id'       => $prefix . "service_categories",
				'type'     => 'checkbox_list',
				'multiple' => true,
				'options'  => $service_options,
				'desc'     => 'If nothing is selected, it will show Items from <strong>ALL</strong> categories.',
				'tab'      => 'services',
			),

			array(
				'name'    => 'Service Column',
				'id'      => $prefix . "service_column",
				'type'    => 'select',
				'tab'     => 'services',
				'std'     => array( '3' ),
				'options' => array(
					'1' => esc_html_x('1 Column', 'backend', 'parlour'),
					'2' => esc_html_x('2 Columns', 'backend', 'parlour'),
					'3' => esc_html_x('3 Columns', 'backend', 'parlour'),
					'4' => esc_html_x('4 Columns', 'backend', 'parlour'),
					'5' => esc_html_x('5 Columns', 'backend', 'parlour'),
					'6' => esc_html_x('6 Columns', 'backend', 'parlour'),
				),
			),
			array(
				'name'    => 'Column Gap',
				'id'      => $prefix . "service_column_gap",
				'type'    => 'select',
				'tab'     => 'services',
				'std'     => null,
				'options' => array(
					null  => esc_html_x('Default', 'backend', 'parlour'),
					'small'    => esc_html_x('Small', 'backend', 'parlour'),
					'medium'   => esc_html_x('Medium', 'backend', 'parlour'),
					'large'    => esc_html_x('Large', 'backend', 'parlour'),
					'collapse' => esc_html_x('Collapse', 'backend', 'parlour'),
				),
			),


			array(
				'name'     => 'Assistant Categories',
				'id'       => $prefix . "assistant_categories",
				'type'     => 'checkbox_list',
				'multiple' => true,
				'options'  => $assistant_options,
				'desc'     => 'If nothing is selected, it will show Items from <strong>ALL</strong> categories.',
				'tab'      => 'assistants',
			),

			array(
				'name'    => 'Assistant Column',
				'id'      => $prefix . "assistant_column",
				'type'    => 'select',
				'tab'     => 'assistants',
				'std'     => array( '3' ),
				'options' => array(
					'1' => esc_html_x('1 Column', 'backend', 'parlour'),
					'2' => esc_html_x('2 Columns', 'backend', 'parlour'),
					'3' => esc_html_x('3 Columns', 'backend', 'parlour'),
					'4' => esc_html_x('4 Columns', 'backend', 'parlour'),
					'5' => esc_html_x('5 Columns', 'backend', 'parlour'),
					'6' => esc_html_x('6 Columns', 'backend', 'parlour'),
				),
			),
			array(
				'name'    => 'Column Gap',
				'id'      => $prefix . "assistant_column_gap",
				'type'    => 'select',
				'tab'     => 'assistants',
				'std'     => null,
				'options' => array(
					null  => esc_html_x('Default', 'backend', 'parlour'),
					'small'    => esc_html_x('Small', 'backend', 'parlour'),
					'medium'   => esc_html_x('Medium', 'backend', 'parlour'),
					'large'    => esc_html_x('Large', 'backend', 'parlour'),
					'collapse' => esc_html_x('Collapse', 'backend', 'parlour'),
				),
			),


		)
	);

	/* ----------------------------------------------------- */
	// Blog Metaboxes
	/* ----------------------------------------------------- */

	$meta_boxes[] = array(
		'id'       => 'pagesettings',
		'title'    => esc_html_x('Blog Post Settings', 'backend', 'parlour' ),
		'pages'    => array( 'post' ),
		'context'  => 'normal',
		'priority' => 'high',
		'tabs'     => array(
            'blog_post'  => array(
                'label' => esc_html__( 'Post Settings', 'parlour'),
            ),
            'gallery'  => array(
                'label' => esc_html__( 'Gallery Settings', 'parlour'),
            ),
            'video'  => array(
                'label' => esc_html__( 'Video Settings', 'parlour'),
            ),
            'audio'  => array(
                'label' => esc_html__( 'Audio Settings', 'parlour'),
            ),
            'link'  => array(
                'label' => esc_html__( 'Link Settings', 'parlour'),
            ),
            'quote'  => array(
                'label' => esc_html__( 'Quote Settings', 'parlour'),
            ),
        ),
        // Tab style: 'default', 'box' or 'left'. Optional
        'tab_style' => 'default',
	
		// List of meta fields
		'fields' => array(
			array(
				'name'		=> 'Titlebar',
				'id'		=> $prefix . "titlebar",
				'type'		=> 'select',
				'options'	=> array(
					'show' => 'Enable',
					'hide' => 'Disable'
				),
				'multiple' => false,
				'std'      => array( true ),
				'desc'     => 'Enable or disable the Titlebar on this Page.',
				'tab'      => 'blog_post',
			),
			array(
				'name'		=> 'Layout Alignment',
				'id'		=> $prefix . "titlebar_layout",
				'type'		=> 'select',
				'options'	=> array(
					'default' => esc_html_x('Default (set in Theme Customizer)', 'backend', 'parlour'),
					'left'    => esc_html_x('Left Align', 'backend', 'parlour'),
					'center'  => esc_html_x('Center Align', 'backend', 'parlour'),
					'right'   => esc_html_x('Right Align', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( 'default' ),
				'desc'     => 'Choose your Titlebar Layout for this Page',
				'tab'      => 'blog_post',
				'hidden' => array($prefix . 'titlebar', '=', 'hide'),
			),
			array(
				'name'		=> 'Background Style',
				'id'		=> $prefix . "titlebar_bg_style",
				'type'		=> 'select',
				'options'	=> array(
					'default'   => esc_html_x('Default', 'backend', 'parlour'),
					'muted'     => esc_html_x('Muted', 'backend', 'parlour'),
					'primary'   => esc_html_x('Primary', 'backend', 'parlour'),
					'secondary' => esc_html_x('Secondary', 'backend', 'parlour'),
					'media'     => esc_html_x('Image', 'backend', 'parlour'),
					//'video'     => esc_html_x('Video', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( 'light' ),
				'desc'     => 'Select your titlebar style from here.',
				'tab'      => 'blog_post',
				'hidden' => array($prefix . 'titlebar_layout', '=', 'default'),
			),
			array(
				'name'             => 'Background Image',
				'id'               => $prefix . "titlebar_bg_img",
				'type'             => 'image_advanced',
				'max_file_uploads' => 1,
				'desc'             => 'Upload Titlebar Image for the Titlebar Style.',
				'tab'              => 'blog_post',
				'visible'          => array($prefix . 'titlebar_bg_style', '=', 'media'),
			),
			array(
				'name'		=> 'Text Color',
				'id'		=> $prefix . "titlebar_txt_style",
				'type'		=> 'select',
				'options'	=> array(
					'0'     => esc_html_x('Default', 'backend', 'parlour'),
					'light' => esc_html_x('Light', 'backend', 'parlour'),
					'dark'  => esc_html_x('Dark', 'backend', 'parlour'),
				),
				'multiple' => false,
				'std'      => array( '0' ),
				'desc'     => 'Select your titlebar text color from here.',
				'tab'      => 'blog_post',
				'hidden' => array($prefix . 'titlebar_layout', '=', 'default'),
			),
			array(
				'name'     => esc_html_x('Hide Featured Image?', 'backend', 'parlour' ),
				'id'       => $prefix . "hideimage",
				'type'     => 'checkbox',
				'multiple' => false,
				'desc'     => esc_html_x('Check this if you want to hide the Featured Image / Gallery on the Blog Detail Page', 'backend', 'parlour' ),
				'tab'      => 'blog_post',
			),

			// Post Format Gallery
			array(
				'name'             => esc_html_x('Gallery Images', 'backend', 'parlour' ),
				'desc'             => esc_html_x('You can upload up to 30 gallery images for a slideshow', 'backend', 'parlour' ),
				'id'               => $prefix . 'blog_gallery',
				'type'             => 'image_advanced',
				'max_file_uploads' => 30,
				'tab'              => 'gallery'
			),

			// Post Format Audio
			array(
				'name'  => esc_html_x('Audio Embed Code', 'backend', 'parlour' ),
				'id'    => $prefix . 'blog_audio',
				'desc'  => esc_html_x('Please enter the Audio Embed Code here.', 'backend', 'parlour' ),
				'clone' => false,
				'type'  => 'textarea',
				'std'   => '',
				'tab'   => 'audio'
			),

			// Post Format Link
			array(
				'name'  => esc_html_x('URL', 'backend', 'parlour' ),
				'id'    => $prefix . 'blog_link',
				'desc'  => esc_html_x('Enter a URL for your link post format. (Don\'t forget the http://)', 'backend', 'parlour' ),
				'clone' => false,
				'type'  => 'text',
				'std'   => '',
				'tab'   => 'link'
			),

			// Post Format Quote
			array(
				'name'  => esc_html_x('Quote', 'backend', 'parlour' ),
				'id'    => $prefix . 'blog_quote',
				'desc'  => esc_html_x('Please enter the text for your quote here.', 'backend', 'parlour' ),
				'clone' => false,
				'type'  => 'textarea',
				'std'   => '',
				'tab'   => 'quote'
			),
			array(
				'name'  => esc_html_x('Quote Source', 'backend', 'parlour' ),
				'id'    => $prefix . 'blog_quotesrc',
				'desc'  => esc_html_x('Please enter the Source of the Quote here.', 'backend', 'parlour' ),
				'clone' => false,
				'type'  => 'text',
				'std'   => '',
				'tab'   => 'quote'
			),


			// Post Format Video
			array(
				'name'      => esc_html_x('Video Source', 'backend', 'parlour' ),
				'id'        => $prefix . 'blog_videosrc',
				'type'      => 'select',
				'options'   => array(
					'videourl'  => esc_html_x('Video URL', 'backend', 'parlour' ),
					'embedcode' => esc_html_x('Embed Code', 'backend', 'parlour' )
				),
				'multiple'  => false,
				'std'       => array( 'videourl' ),
				'tab'       => 'video'
			),
			array(
				'name'  => esc_html_x('Video URL/Embed Code', 'backend', 'parlour' ),
				'id'    => $prefix . 'blog_video',
				'desc'  => wp_kses(_x('If you choose Video URL you can just insert the URL of the <a href="http://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F" target="_blank">Supported Video Site</a>. Otherwise insert the full embed code.', 'backend', 'parlour' ), array('a'=>array())),
				'clone' => false,
				'type'  => 'textarea',
				'std'   => '',
				'tab'   => 'video'
			),
		)
	);

	
	add_filter( 'rwmb_outside_conditions', function( $conditions ) {
	    $conditions['parlour_page_layout'] = array(
	        'visible' => array('page_template', '!=', 'page-homepage.php')
	    );
	    $conditions['.rwmb-tab-titlebar'] = array(
	        'visible' => array('page_template', '!=', 'page-homepage.php')
	    );
	    $conditions['.rwmb-tab-blog'] = array(
	        'hidden' => array('page_template', '!=', 'page-blog.php')
	    );

	    $conditions['.rwmb-tab-testimonials'] = array(
	        'hidden' => array('page_template', '!=', 'page-testimonials.php')
	    );

	    $conditions['.rwmb-tab-services'] = array(
	        'hidden' => array('page_template', '!=', 'page-services.php')
	    );

	    $conditions['.rwmb-tab-assistants'] = array(
	        'hidden' => array('page_template', '!=', 'page-assistants.php')
	    );

	    $conditions['#pagesettings'] = array(
	        'visible' => array('page_template', '!=', 'page-blank.php')
	    );

	    $conditions['.rwmb-tab-gallery'] = array(
	        'hidden' => array('post_format', '!=', 'gallery')
	    );
	    $conditions['.rwmb-tab-video'] = array(
	        'hidden' => array('post_format', '!=', 'video')
	    );
	    $conditions['.rwmb-tab-audio'] = array(
	        'hidden' => array('post_format', '!=', 'audio')
	    );
	    $conditions['.rwmb-tab-quote'] = array(
	        'hidden' => array('post_format', '!=', 'quote')
	    );
	    $conditions['.rwmb-tab-link'] = array(
	        'hidden' => array('post_format', '!=', 'link')
	    );
	    return $conditions;
	});
	

	/* ----------------------------------------------------- */
	// Services Metabox
	/* ----------------------------------------------------- */
	if(is_plugin_active('bdthemes-services/bdthemes-services.php')) { 

		$meta_boxes[] = array(
			'id'      => 'service_fields',
			'title'   => esc_html_x( 'Additional Settings', 'backend', 'parlour'),
			'pages'   => array( 'services' ),
			'context' => 'normal',
			'tabs'      => array(
				'settings' => array(
	                'label' => esc_html__('Settings', 'parlour'),
	            ),
	            'media' => array(
	                'label' => esc_html__('Media Gallery', 'parlour'),
	            ),
	        ),

	        // Tab style: 'default', 'box' or 'left'. Optional
	        'tab_style' => 'default',
			'fields'  => array(
				array(
					'name'  => esc_html_x('Duration', 'backend', 'parlour' ),
					'id'    => 'bdthemes_service_duration',
					'desc'  => esc_html_x('Please enter duration of your service.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'time',
					'std'   => '',
					'tab'   => 'settings'
				),
				array(
					'name'  => esc_html_x('Price', 'backend', 'parlour' ),
					'id'    => 'bdthemes_service_price',
					'desc'  => esc_html_x('Please enter price of your service.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'settings'
				),
				array(
					'name'  => esc_html_x('Order Link', 'backend', 'parlour' ),
					'id'    => 'bdthemes_service_order_link',
					'desc'  => esc_html_x('Please enter your service order form link here.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'settings'
				),
				array(
					'name'  => esc_html_x('Badge', 'backend', 'parlour' ),
					'id'    => 'bdthemes_service_badge',
					'desc'  => esc_html_x('If you want to add any badge in your service so you can add here. For example: hot or popular etc.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'settings'
				),
				array(
					'name'             => esc_html_x('Gallery Images', 'backend', 'parlour' ),
					'desc'             => esc_html_x('You can upload up to 25 gallery images for a slideshow', 'backend', 'parlour' ),
					'id'               => 'bdthemes_service_gallery',
					'type'             => 'image_advanced',
					'max_file_uploads' => 25,
					'tab'              => 'media'
				),			
			)
		);
	}

	// Assistant metabox
	if(is_plugin_active('bdthemes-assistants/bdthemes-assistants.php')) { 

		$meta_boxes[] = array(
			'id'      => 'assistant_fields',
			'title'   => esc_html_x( 'Additional Settings', 'backend', 'parlour'),
			'pages'   => array( 'assistants' ),
			'context' => 'normal',
			'tabs'      => array(
				'settings' => array(
	                'label' => esc_html__('Settings', 'parlour'),
	            ),
	            'availability' => array(
	                'label' => esc_html__('Availability', 'parlour'),
	            ),
	            'social' => array(
	                'label' => esc_html__('Social Link', 'parlour'),
	            ),
	            'alternate_image' => array(
	                'label' => esc_html__('Alternate Image', 'parlour'),
	            ),
	        ),

	        // Tab style: 'default', 'box' or 'left'. Optional
	        'tab_style' => 'default',
			'fields'  => array(
				array(
					'name'  => esc_html_x('Email Address', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_email',
					'desc'  => esc_html_x('Enter assistant email address.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'email',
					'std'   => '',
					'tab'   => 'settings'
				),
				array(
					'name'  => esc_html_x('Phone Number', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_phone',
					'desc'  => esc_html_x('Enter your assistant phone number.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'settings'
				),	
				array(
					'name'  => esc_html_x('Appointment Link', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_appointment_link',
					'desc'  => esc_html_x('Enter your assistant appointment link here.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'settings'
				),
				array(
					'name'  => esc_html_x('Appointment Link Title', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_appointment_link_title',
					'desc'  => esc_html_x('Enter your assistant appointment title here.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'settings'
				),
				array(
					'name'  => esc_html_x('Appointment Link Target', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_appointment_link_target',
					'desc'  => esc_html_x('Enter your assistant appointment target here.', 'backend', 'parlour' ),
					'clone' => false,
					'type'      => 'select',
					'options'   => array(
						'self'  => esc_html_x('Self', 'backend', 'parlour' ),
						'blank' => esc_html_x('Blank', 'backend', 'parlour' ),
						'top'   => esc_html_x('Top', 'backend', 'parlour' )
					),
					'std'   => '',
					'tab'   => 'settings'
				),		
				array(
					'name'  => esc_html_x('Monday', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_monday',
					'desc'  => esc_html_x('Enter your assistant monday availability.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'availability'
				),
				array(
					'name'  => esc_html_x('Tuesday', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_tuesday',
					'desc'  => esc_html_x('Enter your assistant tuesday availability.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'availability'
				),	
				array(
					'name'  => esc_html_x('Wednesday', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_wednesday',
					'desc'  => esc_html_x('Enter your assistant wednesday availability.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'availability'
				),
				array(
					'name'  => esc_html_x('Thursday', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_thursday',
					'desc'  => esc_html_x('Enter your assistant thursday availability.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'availability'
				),
				array(
					'name'  => esc_html_x('Friday', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_friday',
					'desc'  => esc_html_x('Enter your assistant friday availability.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'availability'
				),
				array(
					'name'  => esc_html_x('Saturday', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_saturday',
					'desc'  => esc_html_x('Enter your assistant saturday availability.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'availability'
				),
				array(
					'name'  => esc_html_x('Sunday', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_sunday',
					'desc'  => esc_html_x('Enter your assistant sunday availability.', 'backend', 'parlour' ),
					'clone' => false,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'availability'
				),
				array(
					'name'  => esc_html_x('Social link', 'backend', 'parlour' ),
					'id'    => 'bdthemes_assistant_social_link',
					'desc'  => esc_html_x('Enter your assistant social links here.', 'backend', 'parlour' ),
					'clone' => true,
					'type'  => 'text',
					'std'   => '',
					'tab'   => 'social'
				),
				array(
					'name'             => esc_html_x('Alternate Image', 'backend', 'parlour' ),
					'id'               => 'bdthemes_assistant_altimg',
					'desc'             => esc_html_x('Enter your assistant alternate image for animation.', 'backend', 'parlour' ),
					'clone'            => false,
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'std'              => '',
					'tab'              => 'alternate_image'
				),
			)
		);
	}

	/* ----------------------------------------------------- */
	// FAQ Metabox
	/* ----------------------------------------------------- */
	if(is_plugin_active('bdthemes-faq/bdthemes-faq.php')) { 

		$meta_boxes[] = array(
			'id'      => 'faq_info',
			'title'   => esc_html_x( 'FAQ Additional', 'backend', 'parlour'),
			'pages'   => array( 'faq' ),
			'context' => 'normal',			
			'fields'  => array(
				array(
					'name'		=> esc_html_x('Show FAQ Icon', 'backend', 'parlour'),
					'id'		=> 'bdthemes_show_faq_icon',
					'type'		=> 'radio',
					'options'	=> array(
						'yes'		=> esc_html_x('Yes', 'backend', 'parlour'),
						'no'		=> esc_html_x('No', 'backend', 'parlour')
					),
					'multiple' => false,
					'std'      => array( 'no' ),
				),
				array(
					'name'		=> esc_html_x( 'FAQ Icon', 'backend', 'parlour'),
					'id'		=> 'bdthemes_faq_icon',
					'desc'		=> esc_html_x( 'Please type a fontawesome icon name for your FAQ. for example: fa fa-home', 'backend', 'parlour'),
					'clone'		=> false,
					'type'		=> 'text',
					'std'		=> '',
					'hidden' => array('bdthemes_show_faq_icon', '=', 'no'),
				),			
			)
		);
	}


	if(is_plugin_active('jetpack/jetpack.php')) {
		$meta_boxes[] = array(
			'id'      => 'testimonial_info',
			'title'   => esc_html_x( 'Additional Information', 'backend', 'parlour'),
			'pages'   => array( 'jetpack-testimonial' ),
			'context' => 'normal',			
			'fields'  => array(
				array(
					'name'		=> esc_html_x( 'Address', 'backend', 'parlour'),
					'id'		=> 'jetpack_tm_address',
					'desc'		=> esc_html_x( 'Please type address or area here. for example: Florida.', 'backend', 'parlour'),
					'clone'		=> false,
					'type'		=> 'text',
					'std'		=> '',
					'placeholder' => esc_html_x('Florida', 'backend', 'parlour' ),
					//'hidden' => array('bdthemes_show_faq_icon', '=', 'no'),
				),
				array(
					'name'		=> 'Rating',
					'id'		=> "jetpack_tm_rating",
					'type'		=> 'select',
					'options'	=> array(
						'1' => esc_html_x('1 Star', 'backend', 'parlour'),
						'2' => esc_html_x('2 Stars', 'backend', 'parlour'),
						'3' => esc_html_x('3 Stars', 'backend', 'parlour'),
						'4' => esc_html_x('4 Stars', 'backend', 'parlour'),
						'5' => esc_html_x('5 Stars', 'backend', 'parlour'),
					),
					'multiple' => false,
					'std'      => array( '' ),
					'desc'     => 'Select your client rating what he/she gives you.',
				),		
				array(
					'name'		=> 'Background Style',
					'id'		=> "jetpack_tm_bg_style",
					'type'		=> 'select',
					'options'	=> array(
						'default'   => esc_html_x('Default', 'backend', 'parlour'),
						'muted'     => esc_html_x('Muted', 'backend', 'parlour'),
						'primary'   => esc_html_x('Primary', 'backend', 'parlour'),
						'secondary' => esc_html_x('Secondary', 'backend', 'parlour'),
					),
					'multiple' => false,
					'std'      => array( 'default' ),
					'desc'     => 'Select your testimonial background style from here. This background style only works in testimonials page.',
				),		
				array(
					'name'		=> 'Color Style',
					'id'		=> "jetpack_tm_color_style",
					'type'		=> 'select',
					'options'	=> array(
						'dark'  => esc_html_x('Dark', 'backend', 'parlour'),
						'light' => esc_html_x('Light', 'backend', 'parlour'),
					),
					'multiple' => false,
					'std'      => array( 'dark' ),
					'desc'     => 'Select your testimonial text style from here. This text style only works in testimonials page.',
				),		
			)
		);
	}

	
	foreach ( $meta_boxes as $meta_box ) {
		new RW_Meta_Box( $meta_box );
	}
}


/* ----------------------------------------------------- */
// Background Styling
/* ----------------------------------------------------- */
add_action( 'admin_init', 'rw_register_meta_boxes_background' );
function rw_register_meta_boxes_background() {
	
	global $meta_boxes;

	if(get_theme_mod('parlour_global_layout', 'full') == 'boxed') {

		$prefix = 'parlour_';
		$meta_boxes = [];

		$meta_boxes[] = array(
			'id' => 'styling',
			'title' => 'Background Styling Options',
			'pages' => array( 'post', 'page', 'portfolio' ),
			'context' => 'side',
			'priority' => 'low',
		
			// List of meta fields
			'fields' => array(
				array(
					'name'             => 'Background Image URL',
					'id'               => $prefix . 'bgurl',
					'desc'             => '',
					'clone'            => false,
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'std'              => ''
				),
				array(
					'name'		=> 'Style',
					'id'		=> $prefix . "bgstyle",
					'type'		=> 'select',
					'options'	=> array(
						'stretch'		=> 'Stretch Image',
						'repeat'		=> 'Repeat',
						'no-repeat'		=> 'No-Repeat',
						'repeat-x'		=> 'Repeat-X',
						'repeat-y'		=> 'Repeat-Y'
					),
					'multiple'	=> false,
					'std'		=> array( 'stretch' )
				),
				array(
					'name'		=> 'Background Color',
					'id'		=> $prefix . "bgcolor",
					'type'		=> 'color'
				)
			)
		);
		
		foreach ( $meta_boxes as $meta_box ) {
			new RW_Meta_Box( $meta_box );
		}
	}
}
<?php
// Bdthemes Post Slider 3D

if (!function_exists('bdthemes_post_slider_3d')) {
	function bdthemes_post_slider_3d($atts){
		extract(shortcode_atts(array(
			'post_type'          => 'post',
			'posts'              => '8',
			'categories'         => 'all',
			'service_categories' => 'all',
			'orderby'            => 'date',
			'order'              => 'DESC',
			'title'              => 'yes',
			'meta'               => 'yes',
			'navigation'         => 'yes',
			'pagination'         => 'no',
			'autoplay'           => 'no',
			'loop'               => 'yes',
			'color'              => 'dark',
			'css_animation'      => '',
			'class'              => ''
		), $atts));

		

		global $post;
		$id       = uniqid('bdtps3d_');
		$classes  = ['bdt-post-slider-3d', $class];
		$output   = [];
		$autoplay = ($autoplay =='yes') ? 2500 : 'false';
		$loop     = ($loop =='yes') ? 'true' : 'false';

		if ($post_type == 'services') {
			$taxonomy = 'service-categories';
			$categories = $service_categories;
		} else {
			$taxonomy = 'category';
		}
		

		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => $posts,
			'orderby'        => $orderby,
			'order'          => $order,
			'post_status'    => 'publish'
		);
		
		if($categories != 'all' && $categories != ''){
			$str = $categories;
			$arr = explode(',', $str); // string to array

			$args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		$wp_query = new WP_Query($args);

		if( $wp_query->have_posts() ) :

			$output[] = '<div id ="'.$id.'" class="'.bdt_acssc($classes).'">';
		
			    $output[] = '<div class="swiper-container">';
				    $output[] = '<div class="swiper-wrapper">';

						while ( $wp_query->have_posts() ) : $wp_query->the_post();

					  		$blog_thumbnail= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

					  		if($blog_thumbnail[0] != ''){
					  
					  			$output[] = '<div class="bdt-ps3d-thumb swiper-slide" style="background-image:url('. esc_url($blog_thumbnail[0]) .')">';

						  			$output[] = '<div class="bdt-ps3d-desc uk-position-medium uk-position-bottom-left uk-position-z-index uk-transition-slide-bottom">';

						            	if ( $title == 'yes' ) {
											$output[] = '<h4 class="uk-margin-remove-bottom"><a href="'.esc_url(get_permalink()).'" class="uk-link-reset" title="' . esc_attr(get_the_title()) . '">'.esc_html(get_the_title()) .'</a></h4>';
										}

										if ( $meta == 'yes' ) {
											if ($post_type == 'services') {
												$duration = get_post_meta( get_the_ID(), 'bdthemes_service_duration', true );
												$charge   = get_post_meta( get_the_ID(), 'bdthemes_service_price', true );

												$meta_list = '<li><span>'.esc_html('DURATION: ', 'parlour-core').$duration.'</span></li>
												<li><span>'.esc_html('CHARGE: ', 'parlour-core').$charge.'</span></li>';

											} elseif ($post_type == 'experts') {
												$designation = get_post_meta( get_the_ID(), 'bdthemes_expert_designation', true );
												$meta_list = '<li><span>'.$designation.'</span></li>';
															
											} else {
												$meta_list = '<li><span>'.esc_attr(get_the_date('M d, Y')).'</span></li><li>
															'.get_the_category_list(', ').'</li>';
											}
											
											$output[] = '<ul class="bdt-ps3d-meta uk-subnav uk-margin-remove-top uk-margin-remove-bottom uk-margin-small-bottom uk-text-bold uk-text-muted uk-text-uppercase uk-link-reset">'.$meta_list.'</ul>';
										}

							  		$output[] = '</div>';

						  			$output[] = '<div class="uk-transition-fade uk-position-cover uk-overlay-gradient"></div>';

			        			$output[] = '</div>';
					  		}

						endwhile;

			        $output[] = '</div>';

		        $output[] = '</div>';

		        /* Add Pagination */
		        if ( $pagination == 'yes' ) {
		        	$pagination_class = ($color == 'light') ? ' swiper-pagination-white' : ' swiper-pagination-black';
			        $output[] = '<div class="swiper-pagination'.$pagination_class.'"></div>';
			    }
			    
		        /* Add Arrows */
		        if ( $navigation == 'yes' ) {
		        	$navigation_class = ($color == 'light') ? ' swiper-button-white' : ' swiper-button-black';
			        $output[] = '<div class="swiper-button-next'.$navigation_class.'"></div><div class="swiper-button-prev'.$navigation_class.'"></div>';
			    }

			$output[] ='</div>';

		    /* Initialize Swiper */
		    $output[] = "<script>
		    	 jQuery(document).ready(function ($) {
				    var swiper = new Swiper('#$id .swiper-container', {						
						navigation: {
							nextEl: '#$id .swiper-button-nextt',
							prevEl: '#$id .swiper-button-prev',
						},
						pagination: {
							el        : '#$id .swiper-pagination',
							type      : 'bullets',
							clickable : true,
						},
				        autoplay      : $autoplay ? '{ 'delay' : 4500 }' : 'false',
				        paginationClickable: true,
				        effect: 'coverflow',
				        centeredSlides: true,
				        slidesPerView: 'auto',
				        loop: $loop,
        				autoplayDisableOnInteraction: false,
				        coverflow: {
				            rotate: 50,
				            stretch: 0,
				            depth: 100,
				            modifier: 1,
				            slideShadows : true
				        }
				    });
			    });
		    </script>";
			  
		 	wp_reset_postdata();

		 endif;

		return implode("\n", $output);
	}
	add_shortcode('bdt_post_slider_3d', 'bdthemes_post_slider_3d');
}
<?php
    if (is_single()) {
        $images = rwmb_meta( 'bdthemes_service_gallery', 'type=image_advanced&size=large' );
    } else {
        $images = rwmb_meta( 'bdthemes_service_gallery', 'type=image_advanced&size=medium' );
    }
?>

<?php if (has_post_thumbnail() and empty($images) ) : ?>
    <div class="bdt-service-img">
        <?php if(is_single()) : ?>
            <?php echo  the_post_thumbnail('large', array('class' => 'uk-width-1-1'));  ?>
        <?php else : ?>
            <a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>">
                <?php echo  the_post_thumbnail('medium', array('class' => 'uk-width-1-1'));  ?>
            </a>
        <?php endif; ?>           
    </div>

<?php elseif (!empty($images)) : ?>

    <div class="bdt-service-img post-image-gallery uk-position-relative uk-overflow-hidden">
        <div class="swiper-wrapper" uk-lightbox>
            <div class="swiper-slide">
                <?php if(is_single()) : ?>
                    <?php echo  the_post_thumbnail('large', array('class' => 'uk-width-1-1'));  ?>
                <?php else : ?>
                    <?php echo  the_post_thumbnail('medium', array('class' => 'uk-width-1-1'));  ?>
                <?php endif; ?>  
            </div>
            <?php foreach ( $images as $image) : ?> 
                <div class="swiper-slide">
                <a href="<?php echo esc_url($image['full_url']); ?>" title="<?php echo esc_attr($image['title']); ?>">
                    <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>"/>
                </a>
                </div>
            <?php endforeach; ?>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>

<?php endif ?>
    

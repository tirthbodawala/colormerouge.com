<?php
// Bdthemes Custom Carousel

if (!function_exists('bdthemes_custom_carousel')) {
	function bdthemes_custom_carousel($atts=null, $content=null){
		$atts = shortcode_atts(array(
			'id'                => uniqid('bdtcc_'),
			'large'             => 4,
			'medium'            => 3,
			'small'             => 1,
			'navigation'        => 'yes',
			'pagination'        => 'no',
			'autoplay'          => 'no',
			'loop'              => 'yes',
			'background'        => 'none',
			'custom_background' => '',
			'color'             => 'dark',
			'carousel_padding'  => '',
			'padding'           => 'none',
			'custom_padding'    => '',
			'shadow'            => 'none',
			'hover_shadow'      => 'none',
			'radius'            => 'no',
			'gutter'            => '',
			'css_animation'     => '',
			'class'             => '',
		), $atts, 'bdt_custom_carousel');

		
		$id                     = $atts['id'];
		$classes                = ['bdt-custom-carousel', $atts['class']];
		$css_class              = ['swiper-slide', 'uk-overflow-hidden'];
		
		if ($atts['gutter']     == 'large') { $atts['gutter'] = 50; }
		elseif ($atts['gutter'] == 'medium') { $atts['gutter'] = 25;}
		elseif ($atts['gutter'] == 'small') { $atts['gutter'] = 10;}
		elseif ($atts['gutter'] == 'collapse') { $atts['gutter'] = 0;}
		else { $atts['gutter']  = 35; }
		
		$output                 = [];
		$autoplay               = ($atts['autoplay'] =='yes') ? 4500 : 'false';
		$loop                   = ($atts['loop'] =='yes') ? 'true' : 'false';
		
		$large                  = $atts['large'];
		$medium                 = $atts['medium'];
		$small                  = $atts['small'];
		$gutter                 = $atts['gutter'];
		
		$css_class[]            = ($atts['radius'] =='yes') ? 'uk-border-rounded' : '';
		$custom_background      = ($atts['custom_background']) ? 'background:' . $atts['custom_background']. ';' : '';
		$custom_padding         = ($atts['custom_padding']) ? 'padding:' . $atts['custom_padding']. ';' : '';
		$carousel_padding       = ($atts['carousel_padding']) ? 'padding:' . $atts['carousel_padding']. ';' : '';

		if (($atts['background'] !=='custom') and ($atts['background'] !=='none')) {
            $css_class[] = ($atts['background']) ? 'uk-background-'.$atts['background'] : '';
        }
        if ($atts['color']) {
            $css_class[] = 'uk-'.$atts['color'];
        }		
		if ($atts['shadow'] !=='none') {
            $css_class[] = 'uk-box-shadow-'.$atts['shadow'];
        }		
		if ($atts['hover_shadow'] !=='none') {
            $css_class[] = 'uk-box-shadow-hover-'.$atts['hover_shadow'];
        }
        if (($atts['padding'] !=='custom') and ($atts['padding'] !=='none')) {
            $css_class[] = ($atts['padding'] =='slarge') ? 'uk-padding' : 'uk-padding-'.$atts['padding'];
        }
		
		
		$output[] = '<div id="'.$id.'"  class="'.bdt_acssc($classes).'">';
			$output[] = '<div class="swiper-container" style="'.$carousel_padding.'">';
				$output[] = '<div class="swiper-wrapper">';
	               	bdthemes_override_shortcodes($css_class, $custom_background, $custom_padding);
						$output[] = do_shortcode($content);
					bdthemes_restore_shortcodes();
		        $output[]= '</div>';
        	$output[]= '</div>';
		        
	        /* Add Arrows */
	        if ( $atts['navigation'] == 'yes' ) {
	        	$n_class = ($atts['color'] == 'light') ? ' swiper-button-white' : '';
		        $output[] = '<div class="swiper-button-next'.$n_class.'"></div><div class="swiper-button-prev'.$n_class.'"></div>';
		    }
		    /* Add Pagination */
	        if ( $atts['pagination'] == 'yes' ) {
	        	$p_class = ($atts['color'] == 'light') ? ' swiper-pagination-white' : '';
		        $output[] = '<div class="swiper-pagination'.$p_class.'"></div>';
		    }

        $output[]= '</div>';

        /* Initialize Swiper */
	    $output[] = "<script>
	    	 jQuery(document).ready(function ($) {
			    var swiper = new Swiper('#$id .swiper-container', {
					navigation: {
						nextEl: '#$id .swiper-button-nextt',
						prevEl: '#$id .swiper-button-prev',
					},
					pagination: {
						el        : '#$id .swiper-pagination',
						type      : 'bullets',
						clickable : true,
					},
			        autoplay      : $autoplay ? '{ 'delay' : 4500 }' : 'false',
			        paginationClickable: true,
			        loop: $loop,
    				autoplayDisableOnInteraction: false,
			        slidesPerView: $large,
			        spaceBetween: $gutter,
			        breakpoints: {
			            1024: {
			                slidesPerView: $large,
			                spaceBetween: $gutter
			            },
			            768: {
			                slidesPerView: $medium,
			                spaceBetween: $gutter
			            },
			            640: {
			                slidesPerView: $small,
			                spaceBetween: $gutter
			            }
    				}
			    });
		    });
	    </script>";

		return implode("\n", $output);
	}
	add_shortcode('bdt_custom_carousel', 'bdthemes_custom_carousel');

	function bdthemes_override_shortcodes($css_class, $custom_background, $custom_padding) {
	    global $shortcode_tags, $_shortcode_tags;
	    // Let's make a back-up of the shortcodes
	    $_shortcode_tags = $shortcode_tags;
	    // Add any shortcode tags that we shouldn't touch here
	    $disabled_tags = array( '' );
	    foreach ( $shortcode_tags as $tag => $cb ) {
	        if ( in_array( $tag, $disabled_tags ) ) {
	            continue;
	        }
	        // Overwrite the callback function
	        $shortcode_tags[ $tag ] = 'bdthemes_wrap_shortcode_in_div';
	        $_shortcode_tags["css_class"] = $css_class;
	        $_shortcode_tags["custom_background"] = $custom_background;
	        $_shortcode_tags["custom_padding"] = $custom_padding;
	    }
	}

	function bdthemes_restore_shortcodes() {
	    global $shortcode_tags, $_shortcode_tags;
	    // Restore the original callbacks
	    if ( isset( $_shortcode_tags ) ) {
	        $shortcode_tags = $_shortcode_tags;
	    }
	}

	function bdthemes_wrap_shortcode_in_div( $attr, $content = null, $tag ) {
	    global $_shortcode_tags;
	    return '<div class="'.bdt_acssc($_shortcode_tags["css_class"]).'" style="'.$_shortcode_tags["custom_background"].$_shortcode_tags["custom_padding"].'">' . call_user_func( $_shortcode_tags[ $tag ], $attr, $content, $tag ) . '</div>';
	}
}
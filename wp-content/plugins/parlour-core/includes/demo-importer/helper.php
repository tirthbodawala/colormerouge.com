<?php

/**
 * This function help to get current system information 
 * @return [type] [description]
 */
function parlour_core_system_requirement() {
    ob_start();
	$php_version        = phpversion();
	$max_execution_time = ini_get('max_execution_time');
	$memory_limit       = ini_get('memory_limit');
	$uploads            = wp_upload_dir();
	$upload_path        = $uploads['basedir'];
	$file_get_content   = wp_remote_get('http://bdthemes.com/wordpress/index.htm',array('decompress'  => false));
	?>
    <ul class="check-system-status">
        <li>
            <?php
            echo wp_kses(__('<span class="label1">PHP Version: </span>', 'parlour-core'), bdthemes_allow_tags('span'));
            if (version_compare($php_version,'5.6.0','<')) {
                echo '<span class="invalid"><i class="dashicons-before dashicons-no-alt"></i></span>' . sprintf(wp_kses(__('<span class="label2">Currently: %s</span>', 'parlour-core'), bdthemes_allow_tags('span')), $php_version) . '(' . esc_html__('Min: 5.4', 'parlour-core') . ')';
            } else {
                echo '<span class="valid"><i class="dashicons-before dashicons-yes"></i></span>' . sprintf(wp_kses(__('<span class="label2">Currently: %s</span>', 'parlour-core'), bdthemes_allow_tags('span')), $php_version);
            }
            ?>
        </li>
        <li>
            <?php
            echo wp_kses(__('<span class="label1">Maximum execution time: </span>', 'parlour-core'), bdthemes_allow_tags('span'));
            if ($max_execution_time < '90') {
                echo '<span class="invalid"><i class="dashicons-before dashicons-no-alt"></i></span>' . sprintf(wp_kses(__('<span class="label2">Currently: %s</span>', 'parlour-core'), bdthemes_allow_tags('span')), $max_execution_time) . '(' . esc_html__('Min: 90', 'parlour-core') . ')';
            } else {
                echo '<span class="valid"><i class="dashicons-before dashicons-yes"></i></span>' . sprintf(wp_kses(__('<span class="label2">Currently: %s</span>', 'parlour-core'), bdthemes_allow_tags('span')), $max_execution_time);
            }
            ?>
        </li>
        <li>
            <?php
            echo wp_kses(__('<span class="label1">Memory Limit: </span>', 'parlour-core'), bdthemes_allow_tags('span'));
            if (intval($memory_limit) < '128') {
                echo '<span class="invalid"><i class="dashicons-before dashicons-no-alt"></i></span>' . sprintf(wp_kses(__('<span class="label2">Currently: %s</span>', 'parlour-core'), bdthemes_allow_tags('span')), $memory_limit) . '(' . esc_html__('Min: 128M', 'parlour-core') . ')';
            } else {
                echo '<span class="valid"><i class="dashicons-before dashicons-yes"></i></span>' . sprintf(wp_kses(__('<span class="label2">Currently: %s</span>', 'parlour-core'), bdthemes_allow_tags('span')), $memory_limit);
            }
            ?>
        </li>
        <li>
            <?php
            echo wp_kses(__('<span class="label1">Uploads folder writable: </span>', 'parlour-core'), bdthemes_allow_tags('span'));
            if (!is_writable($upload_path)) {
                echo '<span class="invalid"><i class="dashicons-before dashicons-no-alt"></i></span>';
            } else {
                echo '<span class="valid"><i class="dashicons-before dashicons-yes"></i></span>';
            }
            ?>
        </li>
		<li>
			<?php
			echo wp_kses(__('<span class="label1">Connect BdThemes Server: </span>', 'parlour-core'), bdthemes_allow_tags('span'));
			if (!$file_get_content) {
				echo '<span class="invalid"><i class="dashicons-before dashicons-no-alt"></i></span>';
			} else {
				echo '<span class="valid"><i class="dashicons-before dashicons-yes"></i></span>';
			}
			?>
		</li>
        <li>
            <?php
            echo wp_kses(__('<span class="label1">Start Session: </span>', 'parlour-core'), bdthemes_allow_tags('span'));

            if (!session_id()) {
                session_start();
            }
            if(!session_id()){
                echo '<span class="invalid"><i class="dashicons-before dashicons-no-alt"></i></span>' . wp_kses(__('<span class="label2">Failed to start session. Please check session save_path in your php setting</span>', 'parlour-core'), bdthemes_allow_tags('span'));
            }else {
                echo '<span class="valid"><i class="dashicons-before dashicons-yes"></i></span>';
            }
            ?>
        </li>

    </ul>
    <?php
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
}
<?php
// BdThemes tabs Shortcode

if (!function_exists('bdthemes_tab')) {
    
    $bdt_tabs = [];
    $bdt_tab_count = 0;

    function bdthemes_tab($atts = null, $content = null) {
        extract(shortcode_atts(array(
            'id'            => uniqid('bdttab_'),
            'align'         => 'center',
            'bottom'        => 'no',
            'active'        => 1,
            'animation'     => 'no',
            'collapsible'   => 'yes',
            'swiping'       => 'yes',
            'duration'      => 200,
            
            'class'         => ''
        ), $atts));

        
        
        $tab_id        = uniqid('ti');
        $classes       = ['bdt-tabs', $class];
        $css_class     = [];
        
        $output        = [];
        $css_class[]   = ($align == 'right' or $align == 'center') ? 'uk-flex-'.$align : '';
        $css_class[]   = ($bottom == 'yes') ? 'uk-tab-bottom' : '';
        $css_class[]   = ($align == 'justify') ? 'uk-child-width-expand' : '';

        $settings = [
            'connect'     => '.'.$tab_id,
            'collapsible' => ($collapsible == 'yes') ? true : false,
            'swiping'     => ($swiping == 'yes') ? true : false,
            'active'      => intval($active)-1,
            'animation'   => ($animation == 'yes') ? true : false,
            'duration'    => $duration,

        ];

        do_shortcode( $content );

        global $bdt_tabs;
        global $bdt_tab_count;

        $tabs = $panes = [];
        if ( is_array( $bdt_tabs ) ) {
            foreach ( $bdt_tabs as $tab ) {
                $tabs[]  = '<li class="'.$tab['disable'].'"><a href="#">' . $tab['title'] . '</a></li>';
                $panes[] = '<li class="'.$tab['class'].'">' . $tab['content'] . '</li>';
            }
        }
        // Reset tabs
        $bdt_tabs      = [];
        $bdt_tab_count = 0;
        
        $ftab          = '<ul class="'. bdt_acssc($css_class) .'" uk-tab=\''.json_encode($settings).'\'>' . implode( '', $tabs ). '</ul>';
        $ftcontent     = '<ul class="'.$tab_id.' uk-switcher uk-margin">'. implode( "\n", $panes ) . '</ul>';
        
        $output[]      = '<div id="'.$id.'" class="'.bdt_acssc($classes).'">';
        $output[]      = ($bottom == 'yes') ? $ftcontent.$ftab : $ftab.$ftcontent; 
        $output[]      = '</div>';

        return implode("", $output);
    }
    // end of Tabs shortcode

    add_shortcode('bdt_tab', 'bdthemes_tab');
}

if (!function_exists('bdthemes_tab_item')) {
    function bdthemes_tab_item($atts = null, $content = null) {
        extract(shortcode_atts(array(
            'title'   => 'Tab Title',
            'disable' => 'no',
            'class'   => '',
        ), $atts));

        global $bdt_tabs;
        global $bdt_tab_count;

        $x = $bdt_tab_count;
        $bdt_tabs[$x] = array(
            'title'   => $title,
            'content' => do_shortcode( $content ),
            'disable' => ($disable == 'yes') ? 'uk-disabled' : '',
            'class'   => $class,
        );
        $bdt_tab_count++;
    }
    // end of Tab Item shortcode

    add_shortcode('bdt_tab_item', 'bdthemes_tab_item');
}




<?php
// Bdthemes callout shortcode

if (!function_exists('bdthemes_callout')) {
	function bdthemes_callout($atts = null, $content = null) {
        $title = $button_txt = $button_link = $target = $class = $background = $custom_background = $color = $custom_color = $padding = $custom_padding = $shadow = $hover_shadow = $border = $radius = '';
        extract(shortcode_atts( array(
			'title'             => esc_html__( 'We are waiting for you. Don\'t be hesitate.', 'parlour-core'),
			'button_txt'        => esc_html('Book Now', 'parlour-core'),
			'button_link'       => '#',
			'target'            => 'self',
			'background'        => 'default',
			'custom_background' => '',
			'color'             => 'dark',
			'custom_color'      => '',
			'padding'           => 'medium',
			'custom_padding'    => '',
			'shadow'            => 'none',
			'hover_shadow'      => 'none',
			'border'            => '',
			'radius'            => 'no',
			'css_animation'     => '',
			'class'             => ''
		),$atts));

		
		
		$id                = uniqid('bdtco_');
		$classes           = array('bdt-call-out uk-section', $class);
		$output            = array();
		
		$custom_background = ($custom_background) ? 'background:' . $custom_background. ';' : '';
		$custom_color      = ($custom_color) ? 'color:' . $custom_color. ';' : '';
		$custom_padding    = ($custom_padding) ? 'padding:' . $custom_padding. ';' : '';
		$border            = ($border) ? 'border:' . $border. ';' : '';
		$radius            = ($radius == 'yes') ? 'uk-border-rounded' : '';
		$classes[]         = ($radius == 'yes') ? 'uk-border-rounded' : '';


        if (($background !=='custom') and ($background !=='none')) {
            $classes[] = ($background) ? 'uk-background-'.$background : '';
        }
        if ($color) {
            $classes[] = 'uk-'.$color;
        }
        if (($padding !=='custom') and ($padding !=='none')) {
            $classes[] = ($padding =='medium') ? 'uk-padding' : 'uk-padding-'.$padding;
        }
        if ($padding =='none') {
            $classes[] = 'uk-padding-remove';
        }
        if ($shadow !=='none') {
            $classes[] = 'uk-box-shadow-'.$shadow;
        }
        if ($hover_shadow !=='none') {
            $classes[] = 'uk-box-shadow-hover-'.$hover_shadow;
        }


		$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" style="'.$custom_background.$custom_color.$custom_padding.$border.'">';
		    $output[] = '<div class="uk-container">
                <div class="uk-grid-large uk-flex-middle" uk-grid>
                    <div class="uk-width-expand uk-first-column">';
                    	if ($title) {
                        	$output[] = '<h3 class="uk-text-large uk-margin-remove-bottom">'.esc_html($title).'</h3>';
                    	}
						if ($content) {
                        	$output[] = '<div class="uk-margin-small-top">'.strip_tags($content).'</div>';
						}
                    $output[] = '</div>

                    <div class="uk-width-auto@m">
                        <a class="uk-button uk-button-default uk-button-large" href="'.esc_html( $button_link ).'" target="_'.$target.'">'.esc_html( $button_txt ).'</a>
                    </div>
                </div>
            </div>
        </div>';

		return implode("\n", $output);
	}
	add_shortcode('bdt_callout', 'bdthemes_callout');
}
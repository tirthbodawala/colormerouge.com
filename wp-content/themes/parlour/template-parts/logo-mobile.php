<?php
$logo                   = get_theme_mod('parlour_logo_mobile');
$logo_width             = get_theme_mod('parlour_logo_width_mobile');
$width                  = ($logo_width) ? $logo_width : '';
$img_atts               = [];
$img_atts['class'][]    = 'uk-responsive-height';
$img_atts['style'][]    = 'width:'.esc_attr($width);
$img_atts['src'][]      = esc_url($logo);
$img_atts['itemprop'][] = 'logo';
$img_atts['alt'][]      = get_bloginfo( 'name' );

?>

<a href="<?php echo esc_url(home_url('/')); ?>"<?php echo parlour_helper::attrs(['class' => 'uk-logo uk-navbar-item']) ?> itemprop="url">
    <?php if ($logo) : ?>
        <img<?php echo parlour_helper::attrs($img_atts) ?>>
    <?php else : ?>
        <?php bloginfo( 'name' );?>
    <?php endif; ?>
</a>
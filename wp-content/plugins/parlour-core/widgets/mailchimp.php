<?php

/**
 * Navigation Menu widget class
 *
 * @since 3.0.0
 */

if (get_option( 'mailchimp_api_key' )) {

	class BDT_Mailchimp_Widget extends WP_Widget {

		function __construct()
		{
			$widget_ops = array ( 'description' => esc_html__( 'Use this widget to add a mailchimp newsletter to your site.', 'parlour-core' ) );
			parent::__construct( 'bdt_mailchimp', esc_html__( 'BdThemes Mailchimp Newsletter', 'parlour-core' ), $widget_ops );
		}

		function widget( $args, $instance )
		{

			$mailchimp_api = get_option( 'mailchimp_api_key' );


			if ( empty( $mailchimp_api ) ) {
				echo '<div class="newsletter-signup bdt-newsletter">';
				echo '<p>' . esc_html__( 'No mailchimp list selected. Please set your mailchimp API key in the theme admin panel and then configure the widget from the widget options.', 'parlour-core' ) . '</p>';
				echo '	</div><!-- end newsletter-signup -->';
				return;
			}

			echo $args['before_widget'];

			if ( ! empty( $instance['title'] ) ) {
				echo $args['before_title'] . $instance['title'] . $args['after_title'];
			}

			echo do_shortcode('[bdt_mailchimp email_list="' . $instance['bdt_mailchimp_list'] . '" before_text="' . $instance['bdt_mailchimp_intro'] . '" after_text="' . $instance['bdt_mailchimp_outro'] . '" button_text="' . $instance['button_text'] . '"  ]');

			echo $args['after_widget'];

		}

		function update( $new_instance, $old_instance )
		{
			$instance['title']               = strip_tags( stripslashes( $new_instance['title'] ) );
			$instance['button_text']         = strip_tags( stripslashes( $new_instance['button_text'] ) );
			$instance['bdt_mailchimp_intro'] = stripslashes( $new_instance['bdt_mailchimp_intro'] );
			$instance['bdt_mailchimp_outro'] = stripslashes( $new_instance['bdt_mailchimp_outro'] );
			$instance['bdt_mailchimp_list']  = $new_instance['bdt_mailchimp_list'];
			return $instance;
		}

		function form( $instance ) {

			$title               = isset( $instance['title'] ) ? $instance['title'] : '';
			$button_text         = isset( $instance['button_text'] ) ? $instance['button_text'] : '';
			$bdt_mailchimp_intro = isset( $instance['bdt_mailchimp_intro'] ) ? $instance['bdt_mailchimp_intro'] : '';
			$bdt_mailchimp_outro = isset( $instance['bdt_mailchimp_outro'] ) ? $instance['bdt_mailchimp_outro'] : '';
			$bdt_mailchimp_list  = isset( $instance['bdt_mailchimp_list'] ) ? $instance['bdt_mailchimp_list'] : '';
			$mailchimp_api = get_option( 'mailchimp_api_key' );

			if ( ! function_exists( 'curl_init' ) ) {
				echo esc_html__( 'Curl is not enabled on your hosting environment. Please contact your hosting company and ask them to enable CURL for your account.', 'parlour-core' );
				return;
			}

			if ( empty ( $mailchimp_api ) ) {
				echo esc_html__( 'Please enter your MailChimp API KEY in the BdThemes Settings page to work this widget.', 'parlour-core' );
				return;
			}

			if ( ! empty ( $mailchimp_api ) ) {
				if ( ! class_exists( 'MCAPI' ) ) {
					
					include_once( WP_PLUGIN_DIR.'/parlour-core/includes/MCAPI.class.php');
					
				}
				$api_key = $mailchimp_api;

				$mcapi = new MCAPI( $api_key );

				$lists = $mcapi->lists();
			}
			?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:', 'parlour-core' ) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $title; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'bdt_mailchimp_list' ); ?>"><?php esc_html_e( 'Select List:', 'parlour-core' ); ?></label>
			<select id="<?php echo $this->get_field_id( 'bdt_mailchimp_list' ); ?>" name="<?php echo $this->get_field_name( 'bdt_mailchimp_list' ); ?>" class="widefat">

			<?php
			if ( isset( $lists['data'] ) && is_array( $lists['data'] ) ) {
				foreach ( $lists['data'] as $key => $value ) {
					$selected = ( isset( $bdt_mailchimp_list ) && $bdt_mailchimp_list == $value['id'] ) ? ' selected="selected" ' : '';
					?>
					<option
						<?php echo $selected; ?>value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
				<?php
				}
			}
			?>
			</select>
		</p>

		<p>
			<div><label for="<?php echo $this->get_field_id( 'bdt_mailchimp_intro' ); ?>"><?php echo esc_html__( 'Intro Text :', 'parlour-core' ); ?></label></div>
			<div><textarea id="<?php echo $this->get_field_id( 'bdt_mailchimp_intro' ); ?>" name="<?php echo $this->get_field_name( 'bdt_mailchimp_intro' ); ?>" cols="35" rows="5" class="widefat"><?php echo $bdt_mailchimp_intro; ?></textarea></div>
		</p>
		<p>
			<div><label for="<?php echo $this->get_field_id( 'bdt_mailchimp_outro' ); ?>"><?php echo esc_html__( 'After Form Text :', 'parlour-core' ); ?></label></div>
			<div><textarea id="<?php echo $this->get_field_id( 'bdt_mailchimp_outro' ); ?>" name="<?php echo $this->get_field_name( 'bdt_mailchimp_outro' ); ?>" cols="35" rows="5" class="widefat"><?php echo $bdt_mailchimp_outro; ?></textarea></div>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'button_text' ); ?>"><?php esc_html_e( 'Button text:', 'parlour-core' ) ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'button_text' ); ?>" name="<?php echo $this->get_field_name( 'button_text' ); ?>" value="<?php echo $button_text; ?>" />
		</p>
		<?php
		}
	}
	function register_widget_BDT_Mailchimp_Widget(){
		register_widget( "BDT_Mailchimp_Widget" );
	}
	add_action( 'widgets_init', 'register_widget_BDT_Mailchimp_Widget' );

	/*--------------------------------------------------------------------------------------------------
		Mailchimp ajax helper
	--------------------------------------------------------------------------------------------------*/

	function bdt_do_action() {
		// get warp config
		global $warp;
		$config = $warp['config'];

		if ( isset ( $_POST['bdt_ajax'] ) ) {
			if ( ! headers_sent() ) { //just in case...
				header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT', true, 200 );
			}

			if ( isset ( $_POST['bdt_mc_email'] ) ) {

				$mailchimp_api = ( get_theme_mod('generous_mailchimp_api') ) ? get_theme_mod('generous_mailchimp_api') : '';
				if ( ! empty ( $mailchimp_api ) ) {

					
					include_once( WP_PLUGIN_DIR.'/parlour-core/includes/MCAPI.class.php');
					

					$api_key = $mailchimp_api;

					$mcapi = new MCAPI( $api_key );

					$merge_vars = Array (
						'EMAIL' => $_POST['bdt_mc_email']
					);

					$list_id = $_POST['bdt_mailchimp_list'];

					if ( $mcapi->listSubscribe( $list_id, $_POST['bdt_mc_email'], $merge_vars ) ) {
						// It worked!
						$msg = '<div class="alert alert-success alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' . esc_html__( 'Success!&nbsp; Check your inbox or spam folder for a message containing a confirmation link.', 'parlour-core' ) . '</div>';
					}
					else {
						// An error occurred, return error message
						$msg = '<div class="alert alert-danger alert-dismissible" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>' . esc_html__( 'Error:', 'parlour-core' ) . '</strong>&nbsp; ' . $mcapi->errorMessage . '</div>';
					}
				}
			}
			echo $msg; // Don't esc_html this, b/c we've already escaped it
			exit;
		}
	}
	add_action( 'init', 'bdt_do_action' );
}
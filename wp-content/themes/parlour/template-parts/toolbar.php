<?php 
$classes            = ['uk-container', 'uk-flex uk-flex-middle'];
$mb_toolbar         = (get_post_meta( get_the_ID(), 'parlour_toolbar', true ) != null) ? get_post_meta( get_the_ID(), 'parlour_toolbar', true ) : false;
$tm_toolbar         = (get_theme_mod( 'parlour_toolbar', 0)) ? 1 : 0;
$toolbar_left       = get_theme_mod( 'parlour_toolbar_left', 'tagline' );
$toolbar_right      = get_theme_mod( 'parlour_toolbar_right', 'social' );
$toolbar_cart       = get_theme_mod( 'parlour_woocommerce_cart' );
$classes[]          = (get_theme_mod( 'parlour_toolbar_fullwidth' )) ? 'uk-container-expand' : '';
$toolbar_left_hide  = (get_theme_mod( 'parlour_toolbar_left_hide_mobile' )) ? ' uk-visible@s' : '';
$toolbar_right_hide = (get_theme_mod( 'parlour_toolbar_right_hide_mobile' )) ? ' uk-visible@s' : '';
$toolbar_full_hide  = ( $toolbar_left_hide and $toolbar_right_hide ) ? ' uk-visible@s' : '';

?>

<?php if ($tm_toolbar and $mb_toolbar != true) : ?>
	<div class="tm-toolbar<?php echo esc_attr($toolbar_full_hide); ?>">
		<div<?php echo parlour_helper::attrs(['class' => $classes]) ?>>

			<?php if (!empty($toolbar_left)) : ?>
			<div class="tm-toolbar-l<?php echo esc_attr($toolbar_left_hide); ?>"><?php get_template_part( 'template-parts/toolbars/'.$toolbar_left ); ?></div>
			<?php endif; ?>

			<?php if (!empty($toolbar_right) or $toolbar_cart == 'toolbar') : ?>
			<div class="tm-toolbar-r uk-margin-auto-left uk-flex<?php echo esc_attr($toolbar_right_hide); ?>">
				<?php if ($toolbar_cart == 'toolbar') : ?>
					<div class="uk-display-inline-block">
						<?php get_template_part( 'template-parts/toolbars/'.$toolbar_right ); ?>
					</div>
					<div class="uk-display-inline-block uk-margin-small-left">
						<?php get_template_part('template-parts/woocommerce-cart'); ?>
					</div>
				<?php else: ?>
					<?php get_template_part( 'template-parts/toolbars/'.$toolbar_right ); ?>
				<?php endif; ?>
			</div>
			<?php endif; ?>

		</div>
	</div>
<?php endif; ?>
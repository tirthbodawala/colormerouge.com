<?php
    $title       = 'yes';  // TODO
    $meta        = 'yes';  // TODO
    $excerpt     = 'no';  // TODO
    $align       = 'center';  // TODO
    $social_link = 'yes'; // TODO

?>

<div class="bdt-assistant-content-wrapper uk-box-shadow-small bdt-assistant-align-<?php echo esc_attr($align); ?>">

    <?php if (has_post_thumbnail()) : ?>
        <div class="assistant-thumbnail uk-position-relative uk-overflow-hidden">
            <div class="assistant-thumbnail-design">
                <?php get_template_part( 'template-parts/assistants/media' ); ?>
                <div class="bdt-assistant-overlay uk-position-cover uk-overlay uk-overlay-gradient uk-position-z-index"></div>
            </div>  
        </div>
    <?php endif; ?>

    <?php if(( $title==='yes') or ( $meta==='yes') or ( $excerpt==='yes')) { ?>
        <div class="bdt-assistant-desc uk-padding uk-position-relative uk-position-z-index">

            <?php if( $title==='yes') { ?>
                <?php get_template_part( 'template-parts/assistants/title' ); ?>
            <?php }; 

            if( $meta==='yes') {

                echo get_the_term_list(get_the_ID(),'experiences', '<ul class="bdt-assistant-meta uk-flex-'.$align.' uk-margin-small-top uk-margin-remove"><li>', '</li><li>', '</li></ul>' );
            }; 

            if( $social_link === 'yes') { 
                get_template_part( 'template-parts/assistants/social-link' );
            }; ?>


            <?php if( $excerpt==='yes') { ?>
                <div class="uk-container uk-text-<?php echo esc_attr($align); ?> uk-container-small">
                        <?php get_template_part( 'template-parts/assistants/content' ); ?>
                </div>
            <?php }; ?>
        </div>
    <?php }; ?>
</div>
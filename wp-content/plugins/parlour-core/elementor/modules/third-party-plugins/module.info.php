<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

return [
	'title' => __( 'Third Party Plugins', 'parlour-core' ),
	'required' => true,
	'default_activation' => true,
];

<?php
// Bdthemes Post Block Modern

if (!function_exists('bdthemes_post_block_modern')) {
	function bdthemes_post_block_modern($atts){
		extract(shortcode_atts(array(
			'post_type'          => 'post',
			'posts'              => '4',
			'categories'         => 'all',
			'service_categories' => 'all',
			'expert_categories'  => 'all',
			'color'              => 'dark',
			'orderby'            => 'date',
			'order'              => 'DESC',
			'title'              => 'yes',
			'meta'               => 'yes',
			'excerpt'            => 'yes',
			'excerpt_limit'      => 15,
			'read_more'          => 'yes',
			'css_animation'      => '',
			'class'              => ''
		), $atts));

		

		global $post;
		$id      = uniqid('bdtpbm_');
		$classes = ['bdt-post-block-modern', 'uk-grid', 'uk-grid-match', $class];
		$output  = [];

		if ($post_type == 'services') {
			$taxonomy = 'service-categories';
			$categories = $service_categories;
		} elseif ($post_type == 'experts') {
			$taxonomy = 'expert-categories';
			$categories = $expert_categories;
		} else {
			$taxonomy = 'category';
		}

		$args = array(
			'post_type'      => $post_type,
			'posts_per_page' => $posts,
			'orderby'        => $orderby,
			'order'          => $order,
			'post_status'    => 'publish'
		);
		
		if($categories != 'all' && $categories != ''){
			$str = $categories;
			$arr = explode(',', $str); // string to array

			$args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field'    => 'slug',
				'terms'    => $arr
			);
		}

		$wp_query = new WP_Query($args);

		if( $wp_query->have_posts() ) : 

			$output[] = '<div id="'.$id.'" class="'.bdt_acssc($classes).'" uk-grid>';

				$count = 0;
			
				while ( $wp_query->have_posts() ) : $wp_query->the_post();

					$count++;

			  		$post_thumbnail= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

				  	if( $count == 1) {
				  		$output[] = '<div class="uk-width-3-5@m">'; // left part
				  			$output[] = '<div class="bdt-post-block-item left-part uk-position-relative uk-background-cover" style="background-image: url('.esc_url($post_thumbnail[0]).')">';
						  		
						  		$output[] = '<div class="bdt-pbm-desc uk-margin-medium-bottom uk-position-bottom-center uk-position-z-index uk-text-center uk-width-2-3">';

					            	if ( $meta == 'yes' and $post_type == 'post' ) {
										$meta_list = '<li><span>'.esc_attr(get_the_date('d F Y')).'</span></li><li>'.get_the_category_list(', ').'</li>';
										
										$output[] = '<ul class="bdt-pbm-meta uk-subnav uk-margin-remove-top uk-margin-remove-bottom uk-text-bold uk-flex-center uk-text-uppercase uk-link-reset">'.$meta_list.'</ul>';
									}

									if ( $title == 'yes' ) {
										$output[] = '<h4 class="uk-margin-remove-bottom"><a href="'.esc_url(get_permalink()).'" class="uk-link-reset" title="' . esc_attr(get_the_title()) . '">'.esc_html(get_the_title()) .'</a></h4>';
									}

									if ( $excerpt == 'yes' ) {
										$output[] = '<div class="uk-margin-small-top">'.wp_kses_post(parlour_core_custom_excerpt($excerpt_limit)).'</div>';
									}

						  		$output[] = '</div>';
						  		$output[] = '<div class="uk-position-cover uk-overlay-gradient"></div>';

							$output[] = '</div>';
						$output[] = '</div>';

				  		$output[] = '<div class="uk-width-2-5@m">'; // right part
					} else {
			  			$output[] = '<div class="bdt-post-block-item right-part uk-margin-medium-bottom">';
					  		
					  		$output[] = '<div class="bdt-pbm-desc uk-'.$color.'">';

								if ( $meta == 'yes' and $post_type == 'post' ) {
									$meta_list = '<li><span>'.esc_attr(get_the_date('d F Y')).'</span></li><li>'.get_the_category_list(', ').'</li>';
									
									$output[] = '<ul class="bdt-pbm-meta uk-subnav uk-margin-remove-top uk-margin-remove-bottom uk-text-bold uk-text-muted uk-text-uppercase uk-link-reset">'.$meta_list.'</ul>';
								}

								if ( $title == 'yes' ) {
									$output[] = '<h4 class="uk-margin-remove-bottom"><a href="'.esc_url(get_permalink()).'" class="uk-link-reset" title="' . esc_attr(get_the_title()) . '">'.esc_html(get_the_title()) .'</a></h4>';
								}

								if ( $excerpt == 'yes' ) {
									$output[] = '<div class="uk-margin-small-top">'.wp_kses_post(parlour_core_custom_excerpt($excerpt_limit)).'</div>';
								}

								if ( $read_more == 'yes') {
									$output[] = '<a href="'.esc_url(get_permalink()).'" class="uk-button uk-padding-remove-horizontal uk-margin-small-top uk-margin-small-right uk-text-bold">'.esc_html('View Details', 'quick').'<span uk-icon="icon: chevron-right"></span></a>';
								}

					  		$output[] = '</div>';

						$output[] = '</div>';
					}
			  
				endwhile;

				$output[] = '</div>';
		
			$output[] ='</div>';
		
		 	wp_reset_postdata();

 		endif;

		return implode("\n", $output);
	}
	add_shortcode('bdt_post_block_modern', 'bdthemes_post_block_modern');
}
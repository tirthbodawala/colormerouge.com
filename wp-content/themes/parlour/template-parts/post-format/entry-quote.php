<article id="post-<?php the_ID() ?>" <?php post_class('uk-article post-format-quote uk-text-'.get_theme_mod('parlour_blog_align', 'center')) ?> data-permalink="<?php the_permalink() ?>" typeof="Article">

    <?php get_template_part( 'template-parts/post-format/schema-meta' ); ?>
    
    <?php 
    $quote_text = get_post_meta( get_the_ID(), 'parlour_blog_quote', true );
    $quote_src = get_post_meta( get_the_ID(), 'parlour_blog_quotesrc', true );

    if (!empty($quote_text)) : ?>
    <div class="post-quote<?php echo (is_single()) ? ' uk-margin-large-bottom' : ' uk-margin-bottom'; ?>">
        <a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__('Permalink to %s', 'parlour'), the_title_attribute('echo=0') ); ?>" class="quote-text"><?php echo esc_html($quote_text); ?>
        <span class="quote-source"><?php echo esc_html($quote_src); ?></span></a>
    </div>

    <?php else : ?>

        <?php echo 'Please insert a Quote'; ?>

    <?php endif ?>
    
    <?php if(is_single()) : ?>
        <div class="uk-margin-medium-bottom uk-container uk-container-small">
            <?php get_template_part( 'template-parts/post-format/title' ); ?>

            <?php if(get_theme_mod('parlour_blog_meta', 1)) :?>
            <?php get_template_part( 'template-parts/post-format/meta' ); ?>
            <?php endif; ?>
        </div>
        
        <div class="uk-container uk-container-small">
            <?php get_template_part( 'template-parts/post-format/content' ); ?>

            <?php get_template_part( 'template-parts/post-format/read-more' ); ?>
        </div>
    <?php endif ?>

</article>
<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

return [
	'title' => __( 'Slider', 'parlour-core' ),
	'required' => true,
	'default_activation' => true,
];

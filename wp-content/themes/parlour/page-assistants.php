<?php 
/* Template Name: Assistants */

get_header(); 

// Layout
$position = (get_post_meta( get_the_ID(), 'parlour_page_layout', true )) ? get_post_meta( get_the_ID(), 'parlour_page_layout', true ) : get_theme_mod( 'parlour_page_layout', 'sidebar-right' );

$limit = (get_post_meta( get_the_ID(), 'parlour_assistant_limit', true ) != null ) ? get_post_meta( get_the_ID(), 'parlour_assistant_limit', true ) : '12';

$column     = rwmb_meta( 'parlour_assistant_column' );
$column_gap = rwmb_meta( 'parlour_assistant_column_gap');
$column_gap = ($column_gap) ? 'uk-grid-'.$column_gap : '';

?>

<div<?php echo parlour_helper::section(); ?>>
	<div<?php echo parlour_helper::container(); ?>>
		<div<?php echo parlour_helper::grid(); ?>>
			
			<div class="uk-width-expand bdt-assistant-grid">
				<main class="tm-content bdt-assistant-container <?php echo esc_attr($column_gap); ?> uk-child-width-1-<?php echo esc_attr($column); ?>@m" uk-grid>
					<?php 

						global $wp_query;
						// Pagination fix to work when set as Front Page
						// $paged = get_query_var('paged') ? get_query_var('paged') : 1;
						if ( get_query_var('paged') ) { $paged = get_query_var('paged'); } elseif ( get_query_var('page') ) { $paged = get_query_var('page'); } else { $paged = 1; }

						$assistant_options = array(); // fixes a PHP warning when no blog posts at all.
						$experiences = get_terms('experiences');
						if(isset($experiences)) {
							foreach ($experiences as $category) {
						           if(is_object($category))
									$assistant_options[$category->slug] = $category->name;
							}
						}	

						// Get Categories
						$categories = rwmb_meta( 'parlour_experiences', 'type=checkbox_list' );
						$categories = ($categories != null) ? $categories : $assistant_options;

						$args = array(
							'post_type'      => 'assistants',
							'posts_per_page' => intval($limit),
							'post_status'    => 'publish',
							'orderby'        => 'date',
							'order'          => 'DESC',
							'paged'          => $paged
						);
						
						$args['tax_query'][] = array(
							'taxonomy' => 'experiences',
							'field'    => 'slug',
							'terms'    => $categories
						);

						$wp_query = new WP_Query($args);

						if (have_posts()) : while (have_posts()) : the_post(); ?>
							
							<div class="bdt-assistant-item-wrapper">
								<div class="bdt-assistant-item">
									<?php get_template_part( 'template-parts/assistants/experiences/entry', get_post_format() ); ?>
								</div>
							</div>

						<?php endwhile; endif; ?>

				</main> <!-- end main -->
				<?php get_template_part( 'template-parts/pagination' ); ?>
			</div> <!-- end content -->

			<?php if($position == 'sidebar-left' || $position == 'sidebar-right') : ?>
				<aside<?php echo parlour_helper::sidebar($position); ?>>
				    <?php get_sidebar(); ?>
				</aside> <!-- end aside -->
			<?php endif; ?>
			
		</div> <!-- end grid -->
	</div> <!-- end container -->
</div> <!-- end tm main -->
	
<?php get_footer(); ?>

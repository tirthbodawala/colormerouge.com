<?php
namespace ElementorExtend\Modules\Carousel\Widgets;

use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Widget_Base;
use ElementorExtend\Modules\QueryControl\Controls\Group_Control_Posts;
use ElementorExtend\Modules\QueryControl\Module;
use Elementor\Controls_Manager;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class Post Carousel
 */
class Post_Carousel extends Widget_Base {

	/**
	 * @var \WP_Query
	 */
	private $_query = null;

	protected $_has_template_content = false;

	public function get_name() {
		return 'post-carousel';
	}

	public function get_title() {
		return __( 'Post Carousel', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-slider-push';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	public function get_script_depends() {
		return [ 'imagesloaded' ];
	}

	public function on_import( $element ) {
		if ( ! get_post_type_object( $element['settings']['posts_post_type'] ) ) {
			$element['settings']['posts_post_type'] = 'post';
		}

		return $element;
	}

	public function on_export( $element ) {
		$element = Group_Control_Posts::on_export_remove_setting_from_element( $element, 'posts' );
		return $element;
	}

	public function get_query() {
		return $this->_query;
	}

	protected function _register_controls() {
		$this->register_query_section_controls();
		//$this->register_meta_data_controls();
	}

	private function register_query_section_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_responsive_control(
			'columns',
			[
				'label'          => __( 'Columns', 'parlour-core' ),
				'type'           => Controls_Manager::SELECT,
				'default'        => '3',
				'tablet_default' => '2',
				'mobile_default' => '1',
				'options'        => [
					'1' => '1',
					'2' => '2',
					'3' => '3',
					'4' => '4',
					'5' => '5',
					'6' => '6',
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'posts_per_page',
			[
				'label' => __( 'Limit', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 6,
			]
		);

		$this->add_control(
			'thumbnail_show',
			[
				'label'        => __( 'Thumbnail Show', 'parlour-core' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => __( 'Show', 'parlour-core' ),
				'label_off'    => __( 'Hide', 'parlour-core' ),
				'return_value' => 'yes',
				'default'      => 'yes',
				'separator'    => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name' => 'thumbnail_size',
				'label' => __( 'Image Size', 'parlour-core' ),
				'exclude' => [ 'custom' ],
				'default' => 'medium',
				'prefix_class' => 'elementor-post-carousel--thumbnail-size-',
				'condition' => [
					'thumbnail_show' => 'yes',
				],
			]
		);

		$this->add_control(
			'show_title',
			[
				'label' => __( 'Show Title', 'parlour-core' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'label_off' => __( 'Off', 'parlour-core' ),
				'label_on' => __( 'On', 'parlour-core' ),
			]
		);

		$this->add_control(
			'title_tag',
			[
				'label' => __( 'Title HTML Tag', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'h1' => __( 'H1', 'parlour-core' ),
					'h2' => __( 'H2', 'parlour-core' ),
					'h3' => __( 'H3', 'parlour-core' ),
					'h4' => __( 'H4', 'parlour-core' ),
					'h5' => __( 'H5', 'parlour-core' ),
					'h6' => __( 'H6', 'parlour-core' ),
					'div' => __( 'div', 'parlour-core' ),
					'span' => __( 'span', 'parlour-core' ),
					'p' => __( 'p', 'parlour-core' ),
				],
				'default' => 'h4',
				'condition' => [
					'show_title' => 'yes',
				],
			]
		);


		$this->add_control(
			'meta_data',
			[
				'label' => __( 'Meta Data', 'parlour-core' ),
				'label_block' => true,
				'type' => Controls_Manager::SELECT2,
				'default' => [ 'date', 'comments' ],
				'multiple' => true,
				'options' => [
					'author' => __( 'Author', 'parlour-core' ),
					'date' => __( 'Date', 'parlour-core' ),
					'time' => __( 'Time', 'parlour-core' ),
					'comments' => __( 'Comments', 'parlour-core' ),
				],
				'separator' => 'before',
			]
		);


		$this->add_control(
			'show_excerpt',
			[
				'label'        => __( 'Excerpt', 'parlour-core' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => __( 'Show', 'parlour-core' ),
				'label_off'    => __( 'Hide', 'parlour-core' ),
				'return_value' => 'yes',
				'default'      => 'yes',
				'separator'    => 'before',
			]
		);

		$this->add_control(
			'excerpt_length',
			[
				'label'     => __( 'Excerpt Length', 'parlour-core' ),
				'type'      => Controls_Manager::NUMBER,
				'default'   => 15,
				'condition' => [
					'show_excerpt' => 'yes',
				],
			]
		);

		$this->add_control(
			'show_read_more',
			[
				'label'        => __( 'Read More', 'parlour-core' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => __( 'Show', 'parlour-core' ),
				'label_off'    => __( 'Hide', 'parlour-core' ),
				'return_value' => 'yes',
				'default'      => 'yes',
				'separator'    => 'before',
			]
		);

		$this->add_control(
			'read_more_text',
			[
				'label'       => __( 'Read More Text', 'parlour-core' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'Read More', 'parlour-core' ),
				'placeholder' => __( 'Read More', 'parlour-core' ),
				'condition'   => [
					'show_read_more' => 'yes',
				],
			]
		);

		$this->add_control(
			'navigation',
			[
				'label'   => __( 'Navigation', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'both',
				'options' => [
					'both'   => __( 'Arrows and Dots', 'parlour-core' ),
					'arrows' => __( 'Arrows', 'parlour-core' ),
					'dots'   => __( 'Dots', 'parlour-core' ),
					'none'   => __( 'None', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);


		$this->end_controls_section();

		$this->start_controls_section(
			'section_query',
			[
				'label' => __( 'Query', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_group_control(
			Group_Control_Posts::get_type(),
			[
				'name' => 'posts',
				'label' => __( 'Posts', 'parlour-core' ),
			]
		);

		$this->add_control(
			'advanced',
			[
				'label' => __( 'Advanced', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label' => __( 'Order By', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'post_date',
				'options' => [
					'post_date' => __( 'Date', 'parlour-core' ),
					'post_title' => __( 'Title', 'parlour-core' ),
					'menu_order' => __( 'Menu Order', 'parlour-core' ),
					'rand' => __( 'Random', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label' => __( 'Order', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'desc',
				'options' => [
					'asc' => __( 'ASC', 'parlour-core' ),
					'desc' => __( 'DESC', 'parlour-core' ),
				],
			]
		);

		Module::add_exclude_controls( $this );

		$this->end_controls_section();



		$this->start_controls_section(
			'section_additional_options',
			[
				'label' => __( 'Additional Options', 'parlour-core' ),
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label' => __( 'Autoplay', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'parlour-core' ),
					'no' => __( 'No', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => __( 'Autoplay Speed', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'infinite',
			[
				'label' => __( 'Infinite Loop', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'yes',
				'options' => [
					'yes' => __( 'Yes', 'parlour-core' ),
					'no' => __( 'No', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'speed',
			[
				'label' => __( 'Animation Speed', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 500,
				'frontend_available' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_navigation',
			[
				'label' => __( 'Navigation', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'navigation' => [ 'arrows', 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_arrows',
			[
				'label' => __( 'Arrows', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_style',
			[
				'label' => __( 'Arrows Style', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'dark',
				'options' => [
					'dark' => __( 'Dark', 'parlour-core' ),
					'light' => __( 'Light', 'parlour-core' ),
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_position',
			[
				'label' => __( 'Arrows Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -100,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-carousel .swiper-button-next' => 'right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .bdt-post-carousel .swiper-button-prev' => 'left: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_size',
			[
				'label' => __( 'Arrows Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 25,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-carousel .swiper-button-prev, {{WRAPPER}} .bdt-post-carousel .swiper-button-next' => 'height: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}}',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_dots',
			[
				'label' => __( 'Dots', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);


		$this->add_control(
			'dots_position',
			[
				'label' => __( 'Dots Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -80,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-carousel .swiper-pagination-bullets' => 'bottom: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_size',
			[
				'label' => __( 'Dots Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 5,
						'max' => 10,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-carousel .swiper-pagination-bullet' => 'height: {{SIZE}}{{UNIT}};width: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_design_layout',
			[
				'label' => __( 'Items', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'item_gap',
			[
				'label' => __( 'Item Gap', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 35,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'frontend_available' => true,
			]
		);


		$this->add_control(
			'item_background',
			[
				'label' => __( 'Background', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default'   => __( 'Default', 'parlour-core' ),
					'primary'   => __( 'Primary', 'parlour-core' ),
					'secondary' => __( 'Secondary', 'parlour-core' ),
					'muted'     => __( 'Muted', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'item_color',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'dark',
				'options' => [
					'dark'   => __( 'Dark', 'parlour-core' ),
					'light'   => __( 'Light', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'item_padding',
			[
				'label'   => __( 'Padding', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default' => __( 'Default', 'parlour-core' ),
					'small'   => __( 'Small', 'parlour-core' ),
					'medium'  => __( 'Medium', 'parlour-core' ),
					'large'   => __( 'Large', 'parlour-core' ),
					'none'    => __( 'None', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'item_shadow',
			[
				'label'   => __( 'Shadow', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'small',
				'options' => [
					'none'   => __( 'None', 'parlour-core' ),
					'small'  => __( 'Small', 'parlour-core' ),
					'medium' => __( 'Medium', 'parlour-core' ),
					'large'  => __( 'Large', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'item_hover_shadow',
			[
				'label'   => __( 'Hover Shadow', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'medium',
				'options' => [
					'none'   => __( 'None', 'parlour-core' ),
					'small'  => __( 'Small', 'parlour-core' ),
					'medium' => __( 'Medium', 'parlour-core' ),
					'large'  => __( 'Large', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'item_radius',
			[
				'label'        => __( 'Radius', 'parlour-core' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => __( 'Yes', 'parlour-core' ),
				'label_off'    => __( 'No', 'parlour-core' ),
				'return_value' => 'yes',
				'default'      => 'yes',
			]
		);

		$this->end_controls_section();

	}

	protected function register_meta_data_controls() {
		
	}

	protected function get_taxonomies() {
		$taxonomies = get_taxonomies( [ 'show_in_nav_menus' => true ], 'objects' );

		$options = [ '' => '' ];

		foreach ( $taxonomies as $taxonomy ) {
			$options[ $taxonomy->name ] = $taxonomy->label;
		}

		return $options;
	}

	protected function get_posts_tags() {
		$taxonomy = $this->get_settings( 'taxonomy' );

		foreach ( $this->_query->posts as $post ) {
			if ( ! $taxonomy ) {
				$post->tags = [];

				continue;
			}

			$tags = wp_get_post_terms( $post->ID, $taxonomy );

			$tags_slugs = [];

			foreach ( $tags as $tag ) {
				$tags_slugs[ $tag->term_id ] = $tag;
			}

			$post->tags = $tags_slugs;
		}
	}

	public function query_posts() {
		$query_args = Module::get_query_args( 'posts', $this->get_settings() );

		$query_args['posts_per_page'] = $this->get_settings( 'posts_per_page' );

		$this->_query = new \WP_Query( $query_args );
	}

	public function render() {
		$this->query_posts();

		$wp_query = $this->get_query();

		if ( ! $wp_query->found_posts ) {
			return;
		}

		add_filter( 'excerpt_more', [ $this, 'filter_excerpt_more' ], 20 );
		add_filter( 'excerpt_length', [ $this, 'filter_excerpt_length' ], 20 );

		$this->get_posts_tags();

		$this->render_loop_header();

		while ( $wp_query->have_posts() ) {
			$wp_query->the_post();

			$this->render_post();
		}

		$this->render_loop_footer();

		remove_filter( 'excerpt_length', [ $this, 'filter_excerpt_length' ], 20 );
		remove_filter( 'excerpt_more', [ $this, 'filter_excerpt_more' ], 20 );

		wp_reset_postdata();
	}

	public function filter_excerpt_length() {
		return $this->get_settings( 'excerpt_length' );
	}

	public function filter_excerpt_more( $more ) {
		return '';
	}

	protected function render_thumbnail() {
		$settings = $this->get_settings();

		if ('yes' !== $settings['thumbnail_show']) {
			return;
		}

		$settings['thumbnail_size'] = [
			'id' => get_post_thumbnail_id(),
		];

		$thumbnail_html = Group_Control_Image_Size::get_attachment_image_html( $settings, 'thumbnail_size' );
		?>
		<div class="tm-blog-thumbnail">
			<?php echo $thumbnail_html ?>
			<img class="tm-blog-entry-overlay" src="<?php echo get_template_directory_uri(); ?>/images/blog-entry-overlay.svg" alt="">
		</div>
		<?php
	}

	protected function render_meta_data() {
		/** @var array $settings. e.g. [ 'author', 'date', ... ] */
		$settings = $this->get_settings( 'meta_data' );
		if ( empty( $settings ) ) {
			return;
		}
		?>
		<ul class="uk-subnav uk-margin-small-top" uk-margin>
			<?php
			if ( in_array( 'author', $settings ) ) {
				$this->render_author();
			}

			if ( in_array( 'date', $settings ) ) {
				$this->render_date();
			}

			if ( in_array( 'time', $settings ) ) {
				$this->render_time();
			}

			if ( in_array( 'comments', $settings ) ) {
				$this->render_comments();
			}
			?>
		</ul>
		<?php
	}

	protected function render_author() {
		?>
		<li class="pc-author">
			<span><?php the_author(); ?></span>
		</li>
		<?php
	}

	protected function render_date() {
		?>
		<li class="pc-date">
			<span><?php echo apply_filters( 'the_date', get_the_date(), get_option( 'date_format' ), '', '' ); ?></span>
		</li>
		<?php
	}

	protected function render_time() {
		?>
		<li class="pc-time">
			<span><?php the_time(); ?></span>
		</li>
		<?php
	}

	protected function render_comments() {
		?>
		<li class="pc-avatar">
			<span><?php comments_number(); ?></span>
		</li>
		<?php
	}

	protected function render_title() {
		if ( ! $this->get_settings( 'show_title' ) ) {
			return;
		}

		$tag = $this->get_settings( 'title_tag' );
		$classes = ['uk-margin-small-bottom', 'uk-margin-small-top']
		?>

		<<?php echo $tag ?> class="<?php echo implode(" ", $classes); ?>">
		<a class="uk-link-reset" href="<?php echo get_permalink() ?>">
			<?php the_title() ?>
		</a>
		</<?php echo $tag ?>>
		<?php
	}


	protected function render_excerpt() {
		if ( ! $this->get_settings( 'show_excerpt' ) ) {
			return;
		}
		?>
		<div class="elementor-post__excerpt">
			<?php the_excerpt(); ?>
		</div>
		<?php
	}

	protected function render_read_more() {
		if ( ! $this->get_settings( 'show_read_more' ) ) {
			return;
		}
		?>
			<a class="uk-button uk-padding-remove-horizontal uk-margin-top uk-margin-small-right uk-button-text" href="<?php echo get_permalink(); ?>">
				<?php echo $this->get_settings( 'read_more_text' ); ?> <span uk-icon="icon: arrow-right"></span>
			</a>
		<?php
	}

	protected function render_post_header() {
		global $post;

		$tags_classes = array_map( function( $tag ) {
			return 'elementor-filter-' . $tag->term_id;
		}, $post->tags );

		$classes = [
			'elementor-post-carousel-item',
			'elementor-post',
			'swiper-slide',
			implode( ' ', $tags_classes ),
		];
		$classes[] = ($this->get_settings('item_background')) ? 'uk-background-'.$this->get_settings('item_background') : '';
		$classes[] = ($this->get_settings('item_color')) ? 'uk-'.$this->get_settings('item_color') : '';
		$classes[] = ($this->get_settings('item_shadow') != 'none') ? 'uk-box-shadow-'.$this->get_settings('item_shadow') : '';
		$classes[] = ($this->get_settings('item_shadow') != 'none') ? 'uk-box-shadow-hover-'.$this->get_settings('item_hover_shadow') : '';
		$classes[] = ($this->get_settings('item_radius') == 'yes') ? 'uk-border-rounded uk-overflow-hidden' : '';

		?>
		<article <?php post_class( $classes ); ?>>
		<?php
	}

	protected function render_post_footer() {
		?>
		</article>
		<?php
	}

	protected function render_overlay_header() {
		$classes = ['bdt-pc-desc'];
		if ($this->get_settings('item_padding') != 'none') {
			$classes[] = ($this->get_settings('item_padding') != 'default') ? 'uk-padding-'.$this->get_settings('item_padding') : 'uk-padding';
		} else {
			$classes[] = 'uk-margin-top';
		}
		?>
		<div class="<?php echo implode(" ", $classes);?>">
		<?php
	}

	protected function render_overlay_footer() {
		?>
		</div>
		<?php
	}

	protected function render_loop_header() {
		?>
		<div class="bdt-post-carousel">
			<div class="swiper-container">
				<div class="swiper-wrapper">
		<?php
	}

	protected function render_loop_footer() {
		$settings = $this->get_settings();
		?>
				</div>
			</div>
			
			<?php if ( 'none' !== $settings['navigation'] ) : ?>
				<?php if ( 'arrows' !== $settings['navigation'] ) : ?>
					<div class="swiper-pagination"></div>
				<?php endif; ?>
				
				<?php if ( 'dots' !== $settings['navigation'] ) : 
					$nav_style = ($settings['arrows_style'] == 'light') ? 'swiper-button-white' : 'swiper-button-black'; 
				?>
					<div class="swiper-button-next <?php echo esc_attr($nav_style); ?>"></div>
					<div class="swiper-button-prev <?php echo esc_attr($nav_style); ?>"></div>
				<?php endif; ?>
			<?php endif; ?>
			
			
		</div>

		<script>
			jQuery(document).ready(function($) {
			    "use strict";				    
			    var swiper = new Swiper(".elementor-element-<?php echo $this->get_id(); ?> .swiper-container", {
			        navigation: {
						nextEl: ".elementor-element-<?php echo $this->get_id(); ?> .swiper-button-nextt",
						prevEl: ".elementor-element-<?php echo $this->get_id(); ?> .swiper-button-prev",
					},
					pagination: {
						el        : ".elementor-element-<?php echo $this->get_id(); ?> .swiper-pagination",
						type      : 'bullets',
						clickable : true,
					},
			        "autoplay"      : <?php echo 'yes' === $settings['autoplay'] ? '{ "delay": ' . $settings['autoplay_speed'] . ' }' : 'false'; ?>,
			        "paginationClickable":true,
			        "loop": <?php echo ($settings['infinite'] == 'yes') ? 'true' : 'false'; ?>,
			        "speed": <?php echo $settings['speed']; ?>,
			        "slidesPerView": <?php echo $settings['columns']; ?>,
			        "spaceBetween":  <?php echo $settings['item_gap']['size']; ?>,
			        "breakpoints" : {
			            "1024" : {
			            	"slidesPerView": <?php echo $settings['columns']; ?>,
			            	"spaceBetween": <?php echo $settings['item_gap']['size']; ?>,
			            },
			            "768" : {
			            	"slidesPerView": <?php echo $settings['columns_tablet']; ?>,
			            	"spaceBetween": <?php echo $settings['item_gap']['size']; ?>,
			            },
			            "640" : {
			            	"slidesPerView": <?php echo $settings['columns_mobile']; ?>,
			            	"spaceBetween": <?php echo $settings['item_gap']['size']; ?>,
			            }
			        }
			    });
			});
		</script>
		<?php
	}

	protected function render_post() {
		$this->render_post_header();
		$this->render_thumbnail();
		$this->render_overlay_header();
		$this->render_title();
		$this->render_meta_data();
		$this->render_excerpt();
		$this->render_read_more();
		$this->render_overlay_footer();
		$this->render_post_footer();
	}
}

<?php
namespace ElementorExtend\Modules\Slider\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;
use ElementPack\Modules\QueryControl\Controls\Group_Control_Posts;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Class Slider
 */
class Slider extends Widget_Base {

	/**
	 * @var \WP_Query
	 */
	private $_query = null;

	protected $_has_template_content = false;

	public function get_name() {
		return 'slider';
	}

	public function get_title() {
		return __( 'Slider', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-slideshow';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	public function get_script_depends() {
		return [ 'imagesloaded' ];
	}

	public function on_import( $element ) {
		if ( ! get_post_type_object( $element['settings']['posts_post_type'] ) ) {
			$element['settings']['posts_post_type'] = 'services';
		}

		return $element;
	}

	public function on_export( $element ) {
		$element = Group_Control_Posts::on_export_remove_setting_from_element( $element, 'posts' );
		return $element;
	}

	public function get_query() {
		return $this->_query;
	}

	protected function _register_controls() {
		$this->register_query_section_controls();
		//$this->register_meta_data_controls();
	}

	private function register_query_section_controls() {

		$this->start_controls_section(
			'section_content_sliders',
			[
				'label' => __( 'Sliders', 'parlour-core' ),
			]
		);

		$this->add_control(
			'tabs',
			[
				'label' => __( 'Slider Items', 'parlour-core' ),
				'type' => Controls_Manager::REPEATER,
				'default' => [
					[
						'tab_title'   => __( 'Slide #1', 'parlour-core' ),
						'tab_content' => __( 'I am item content. Click edit button to change this text.', 'parlour-core' ),
					],
					[
						'tab_title'   => __( 'Slide #2', 'parlour-core' ),
						'tab_content' => __( 'I am item content. Click edit button to change this text.', 'parlour-core' ),
					],
					[
						'tab_title'   => __( 'Slide #3', 'parlour-core' ),
						'tab_content' => __( 'I am item content. Click edit button to change this text.', 'parlour-core' ),
					],
					[
						'tab_title'   => __( 'Slide #4', 'parlour-core' ),
						'tab_content' => __( 'I am item content. Click edit button to change this text.', 'parlour-core' ),
					],
				],
				'fields' => [
					[
						'name'        => 'tab_title',
						'label'       => __( 'Title', 'parlour-core' ),
						'type'        => Controls_Manager::TEXT,
						'default'     => __( 'Slide Title' , 'parlour-core' ),
						'label_block' => true,
					],
					[
						'name'  => 'tab_image',
						'label' => __( 'Image', 'parlour-core' ),
						'type'  => Controls_Manager::MEDIA,
					],
					[
						'name'       => 'tab_content',
						'label'      => __( 'Content', 'parlour-core' ),
						'type'       => Controls_Manager::WYSIWYG,
						'default'    => __( 'Slide Content', 'parlour-core' ),
						'show_label' => false,
					],
					[
						'name'        => 'tab_link',
						'label'       => __( 'Link', 'parlour-core' ),
						'type'        => Controls_Manager::URL,
						'placeholder' => 'http://your-link.com',
						'default'     => [
							'url' => '#',
						],
					],
				],
				'title_field' => '{{{ tab_title }}}',
			]
		);

		$this->end_controls_section();


		$this->start_controls_section(
			'section_content_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
			]
		);

		$this->add_responsive_control(
			'height',
			[
				'label' => __( 'Height', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 600,
				],
				'range' => [
					'px' => [
						'min' => 50,
						'max' => 1024,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'origin',
			[
				'label' => __( 'Origin', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'top-left',
				'options' => [
					'top-left'      => __('top-left', 'parlour-core') ,
					'top-center'    => __('top-center', 'parlour-core') ,
					'top-right'     => __('top-right', 'parlour-core') ,
					'center'        => __('center', 'parlour-core') ,
					'center-left'   => __('center-left', 'parlour-core') ,
					'center-right'  => __('center-right', 'parlour-core') ,
					'bottom-left'   => __('bottom-left', 'parlour-core') ,
					'bottom-center' => __('bottom-center', 'parlour-core') ,
					'bottom-right'  => __('bottom-right', 'parlour-core') ,
				],
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'parlour-core' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left'    => [
						'title' => __( 'Left', 'parlour-core' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'parlour-core' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'parlour-core' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'parlour-core' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'prefix_class' => 'elementor%s-align-',
				'description' => 'Use align to match position',
				'default' => '',
			]
		);

		$this->add_control(
			'show_title',
			[
				'label'     => __( 'Show Title', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'default'   => 'yes',
				'label_off' => __( 'Off', 'parlour-core' ),
				'label_on'  => __( 'On', 'parlour-core' ),
			]
		);


		$this->add_control(
			'show_button',
			[
				'label'        => __( 'Show Button', 'parlour-core' ),
				'type'         => Controls_Manager::SWITCHER,
				'label_on'     => __( 'Show', 'parlour-core' ),
				'label_off'    => __( 'Hide', 'parlour-core' ),
				'default'      => 'yes',
			]
		);

		$this->add_control(
			'navigation',
			[
				'label'   => __( 'Navigation', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'both',
				'options' => [
					'both'   => __( 'Arrows and Dots', 'parlour-core' ),
					'arrows' => __( 'Arrows', 'parlour-core' ),
					'dots'   => __( 'Dots', 'parlour-core' ),
					'none'   => __( 'None', 'parlour-core' ),
				],
				'frontend_available' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_slider_settings',
			[
				'label'     => __( 'Slider Settings', 'parlour-core' ),
			]
		);

		$this->add_control(
			'transition',
			[
				'label'   => __( 'Transition', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'slide',
				'options' => [
					'slide'     => __( 'Slide', 'parlour-core' ),
					'fade'      => __( 'Fade', 'parlour-core' ),
					'cube'      => __( 'Cube', 'parlour-core' ),
					'coverflow' => __( 'Coverflow', 'parlour-core' ),
					'flip'      => __( 'Flip', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'effect',
			[
				'label'   => __( 'Text Effect', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'slide',
				'options' => [
				'left'    => __( 'Slide Right to Left', 'parlour-core' ),
				'bottom'  => __( 'Slider Bottom to Top', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'     => __( 'Autoplay', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'On', 'parlour-core' ),
				'label_off' => __( 'Off', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label' => __( 'Autoplay Speed', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 5000,
				'condition' => [
					'autoplay' => 'yes',
				],
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'     => __( 'Infinite Loop', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'On', 'parlour-core' ),
				'label_off' => __( 'Off', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'speed',
			[
				'label' => __( 'Animation Speed', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 500,
				'frontend_available' => true,
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_button',
			[
				'label' => __( 'Button', 'parlour-core' ),
				'condition' => [
					'show_button' => 'yes',
				],
			]
		);

		$this->add_control(
			'button_text',
			[
				'label' => __( 'Button Text', 'parlour-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Read More', 'parlour-core' ),
				'placeholder' => __( 'Read More', 'parlour-core' ),
			]
		);

		$this->add_control(
			'icon',
			[
				'label' => __( 'Icon', 'parlour-core' ),
				'type' => Controls_Manager::ICON,
				'label_block' => true,
				'default' => '',
			]
		);

		$this->add_control(
			'icon_align',
			[
				'label' => __( 'Icon Position', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'right',
				'options' => [
					'left' => __( 'Before', 'parlour-core' ),
					'right' => __( 'After', 'parlour-core' ),
				],
				'condition' => [
					'icon!' => '',
				],
			]
		);

		$this->add_control(
			'icon_indent',
			[
				'label' => __( 'Icon Spacing', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 8,
				],
				'range' => [
					'px' => [
						'max' => 50,
					],
				],
				'condition' => [
					'icon!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-link-icon-right' => 'margin-left: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .bdt-slider .bdt-slide-link-icon-left' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_slider',
			[
				'label' => __( 'Slider', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'slider_background_color',
			[
				'label' => __( 'Background Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#61ce70',
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'content_padding',
			[
				'label' => __( 'Padding', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-desc' => 'margin: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_title',
			[
				'label' => __( 'Title', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_title' => [ 'yes' ],
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label' => __( 'Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_background',
			[
				'label' => __( 'Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-title' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'title_padding',
			[
				'label' => __( 'Padding', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => __( 'Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-title',
			]
		);

		$this->add_responsive_control(
			'title_space',
			[
				'label' => __( 'Space', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_text',
			[
				'label' => __( 'Text', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'text_color',
			[
				'label' => __( 'Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-text' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'text_background',
			[
				'label' => __( 'Background', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-text' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'text_padding',
			[
				'label' => __( 'Padding', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-text' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'text_typography',
				'label' => __( 'Text Typography', 'parlour-core' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-text',
			]
		);

		$this->add_responsive_control(
			'text_space',
			[
				'label' => __( 'Text Space', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-text' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_button',
			[
				'label' => __( 'Button', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_button' => 'yes',
				],
			]
		);

		$this->start_controls_tabs( 'tabs_button_style' );

		$this->start_controls_tab(
			'tab_button_normal',
			[
				'label' => __( 'Normal', 'parlour-core' ),
			]
		);

		$this->add_control(
			'button_text_color',
			[
				'label'     => __( 'Text Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-link' => 'color: {{VALUE}} !important;',
				],
			]
		);

		$this->add_control(
			'background_color',
			[
				'label'     => __( 'Background Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-link' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_button_hover',
			[
				'label' => __( 'Hover', 'parlour-core' ),
			]
		);

		$this->add_control(
			'hover_color',
			[
				'label' => __( 'Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-link:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_background_hover_color',
			[
				'label' => __( 'Background Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-link:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_hover_border_color',
			[
				'label' => __( 'Border Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'border_border!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-link:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_hover_animation',
			[
				'label' => __( 'Animation', 'parlour-core' ),
				'type' => Controls_Manager::HOVER_ANIMATION,
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'border',
				'label' => __( 'Border', 'parlour-core' ),
				'placeholder' => '1px',
				'default' => '1px',
				'selector' => '{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-link',
				'separator' => 'before',
			]
		);

		$this->add_control(
			'border_radius',
			[
				'label' => __( 'Border Radius', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-link' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'button_box_shadow',
				'selector' => '{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-link',
			]
		);

		$this->add_control(
			'button_padding',
			[
				'label' => __( 'Padding', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-link' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'typography',
				'label'    => __( 'Typography', 'parlour-core' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-slider .bdt-slide-item .bdt-slide-link',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_navigation',
			[
				'label' => __( 'Navigation', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'navigation' => [ 'arrows', 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_arrows',
			[
				'label' => __( 'Arrows', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_style',
			[
				'label' => __( 'Arrows Style', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'dark',
				'options' => [
					'dark' => __( 'Dark', 'parlour-core' ),
					'light' => __( 'Light', 'parlour-core' ),
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_position',
			[
				'label' => __( 'Arrows Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -100,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .swiper-button-next' => 'right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .bdt-slider .swiper-button-prev' => 'left: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'arrows_size',
			[
				'label' => __( 'Arrows Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 25,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .swiper-button-prev, {{WRAPPER}} .bdt-slider .swiper-button-next' => 'height: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}}',
				],
				'condition' => [
					'navigation' => [ 'arrows', 'both' ],
				],
			]
		);

		$this->add_control(
			'heading_style_dots',
			[
				'label' => __( 'Dots', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_position',
			[
				'label' => __( 'Dots Position', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => -80,
						'max' => 80,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .swiper-pagination-bullets' => 'bottom: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->add_control(
			'dots_size',
			[
				'label' => __( 'Dots Size', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 5,
						'max' => 10,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-slider .swiper-pagination-bullet' => 'height: {{SIZE}}{{UNIT}};width: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'navigation' => [ 'dots', 'both' ],
				],
			]
		);

		$this->end_controls_section();

	}
	
	protected function render_loop_header() {
		?>
		<div class="bdt-slider">
			<div class="swiper-container">
		<?php
	}

	protected function render_loop_footer() {

		$settings = $this->get_settings();
		?>
			</div>

			<?php if ( 'none' !== $this->get_settings('navigation') ) : ?>
				<?php if ( 'arrows' !== $this->get_settings('navigation') ) : ?>
					<div class="swiper-pagination"></div>
				<?php endif; ?>
				
				<?php if ( 'dots' !== $this->get_settings('navigation') ) : 
					$nav_style = ($this->get_settings('arrows_style') == 'light') ? 'swiper-button-white' : 'swiper-button-black'; 
				?>
					<div class="swiper-button-next <?php echo esc_attr($nav_style); ?>"></div>
					<div class="swiper-button-prev <?php echo esc_attr($nav_style); ?>"></div>
				<?php endif; ?>
			<?php endif; ?>

		</div>

		<script>
			jQuery(document).ready(function($) {
			    "use strict";				    
			    var swiper = new Swiper(".elementor-element-<?php echo $this->get_id(); ?> .swiper-container", {
			        navigation: {
						nextEl: ".elementor-element-<?php echo $this->get_id(); ?> .swiper-button-nextt",
						prevEl: ".elementor-element-<?php echo $this->get_id(); ?> .swiper-button-prev",
					},
					pagination: {
						el        : ".elementor-element-<?php echo $this->get_id(); ?> .swiper-pagination",
						type      : 'bullets',
						clickable : true,
					},
			        "autoplay"      : <?php echo 'yes' === $settings['autoplay'] ? '{ "delay": ' . $settings['autoplay_speed'] . ' }' : 'false'; ?>,
			        "paginationClickable":true,
			        "loop": <?php echo ($this->get_settings('infinite') == 'yes') ? 'true' : 'false'; ?>,
			        "speed": <?php echo $this->get_settings('speed'); ?>,
			        "slidesPerView": 1,
			        "spaceBetween":  0,
			        "effect": "<?php echo $this->get_settings('transition'); ?>",
			    });
			});
		</script>

		<?php
	}
	public function render() {
		$settings  = $this->get_settings();
		$url       = $target = $link_title = '';
		$classes   = ['bdt-slide-item', 'swiper-slide'];
		$classes[] = 'bdt-slide-effect-'.$settings['effect'];

		$animation = ($settings['button_hover_animation']) ? ' elementor-animation-'.$settings['button_hover_animation'] : '';


		$this->render_loop_header();

		?>
		<div class="swiper-wrapper">
			<?php $counter = 1; ?>
			<?php foreach ( $settings['tabs'] as $item ) : ?>

				<?php 
					$image_src = wp_get_attachment_image_src( $item['tab_image']['id'], 'full' );
					$image     =  ($image_src) ? $image_src[0] : '';

				?>
				
						<div class="<?php echo implode(" ", $classes); ?>" style="background-image: url('<?php echo  $image; ?>');">
				        	
				        	<div class="bdt-slide-desc uk-position-medium uk-position-<?php echo $settings['origin']; ?> uk-position-z-index">

								<?php if (( '' !== $item['tab_title'] ) && ( 'yes' == $settings['show_title'] )) : ?>
									<h2 class="bdt-slide-title uk-clearfix"><?php echo $item['tab_title']; ?></h2>
								<?php endif; ?>

								<?php if ( '' !== $item['tab_content'] ) : ?>
									<div class="bdt-slide-text"><?php echo $this->parse_text_editor( $item['tab_content'] ); ?></div>
								<?php endif; ?>

								<?php if (( ! empty( $item['tab_link']['url'] )) && ( 'yes' == $settings['show_button'] )): ?>
									<div><a href="<?php echo $item['tab_link']['url']; ?>" target="<?php echo $item['tab_link']['is_external']; ?>" class="bdt-slide-link uk-link-reset<?php echo $animation; ?>"><?php echo $settings['button_text']; ?>
										
										<?php if ($settings['icon']) : ?>
											<span class="bdt-slide-link-icon-<?php echo $settings['icon_align']; ?>">
												<i class="<?php echo $settings['icon']; ?>"></i>
											</span>
										<?php endif; ?>

									</a></div>
								<?php endif; ?>

					  		</div>

						</div>
				<?php
				$counter++;
			endforeach;
			?>
		</div>
		<?php

		$this->render_loop_footer();
	}
}

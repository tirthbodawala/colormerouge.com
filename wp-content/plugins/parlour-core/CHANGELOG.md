# Changelog

	1.2.3
	# Swiper related widget have navigation issue
	
	1.2.0
	# Fixed the demo upload problem and added updated demo files
	+ Requirement status added in demo panel
	# Fixed some elementor element
	^ Language file updated

	1.2.0
	+ New Demo Importer Added
	# Optimize many code block
	# Fixed many elementor bug and style
	^ Language file updated

	1.1.1
	+ Booked widget added
	# Fix some options in contact form 7 widget
	# Fix Testimonials carousel widget option
	# Fix Testimonials Slider widget option

	1.1.0
	+ Flip box widget added
	+ Panel Slider widget added
	+ Contact Form 7 widget added
	+ Image Compare widget added
	+ Trailer Box widget added
	+ Instagram widget added
	# Fixes some minor issues
	# Refine some coding structure
	$ Language file updated


	1.0.0
	+ Initial Release



	* -> Security Fix
	# -> Bug Fix
	$ -> Language fix or change
	+ -> Addition
	^ -> Change
	- -> Removed
	! -> Note

<?php
namespace ElementorExtend\Modules\Posts\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Scheme_Typography;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Post_Block_Modern extends Widget_Base {

	protected $_has_template_content = false;

	public function get_name() {
		return 'post-block-modern';
	}

	public function get_title() {
		return __( 'Post Block Modern', 'parlour-core' );
	}

	public function get_icon() {
		return 'eicon-posts-grid';
	}

	public function get_categories() {
		return [ 'theme-elements' ];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'post_type',
			[
				'label'   => __( 'Post Type', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'post',
				'options' => [
					'post'     => __( 'Post', 'parlour-core' ),
					'services' => __( 'Services', 'parlour-core' ),
				],
			]
		);


		$this->add_control(
			'source',
			[
				'label' => _x( 'Source', 'Posts Query Control', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Show All', 'parlour-core' ),
					'by_name' => __( 'Manual Selection', 'parlour-core' ),
				],
				'label_block' => true,
			]
		);

		$post_categories = get_terms( 'category' );

		$post_options = [];
		foreach ( $post_categories as $category ) {
			$post_options[ $category->slug ] = $category->name;
		}

		$this->add_control(
			'post_categories',
			[
				'label'       => __( 'Categories', 'parlour-core' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => $post_options,
				'default'     => [],
				'label_block' => true,
				'multiple'    => true,
				'condition'   => [
					'post_type' => 'post',
					'source'    => 'by_name',
				],
			]
		);

		$service_categories = get_terms( 'service-categories' );

		$service_options = [];
		foreach ( $service_categories as $category ) {
			$service_options[ $category->slug ] = $category->name;
		}

		$this->add_control(
			'service_categories',
			[
				'label'       => __( 'Categories', 'parlour-core' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => $service_options,
				'default'     => [],
				'label_block' => true,
				'multiple'    => true,
				'condition'   => [
					'post_type' => 'services',
					'source'    => 'by_name',
				],
			]
		);

		$this->add_control(
			'posts_limit',
			[
				'label' => __( 'Posts Limit', 'parlour-core' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 4,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => __( 'Order by', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'date',
				'options' => [
					'date'     => __( 'Date', 'parlour-core' ),
					'title'    => __( 'Title', 'parlour-core' ),
					'category' => __( 'Category', 'parlour-core' ),
					'rand'     => __( 'Random', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label'   => __( 'Order', 'parlour-core' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'DESC',
				'options' => [
					'DESC' => __( 'Descending', 'parlour-core' ),
					'ASC'  => __( 'Ascending', 'parlour-core' ),
				],
			]
		);

		$this->add_control(
			'title',
			[
				'label'     => __( 'Title', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'show_meta',
			[
				'label'     => __( 'Meta Data', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'show_excerpt',
			[
				'label'     => __( 'Excerpt', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);

		$this->add_control(
			'excerpt_length',
			[
				'label'   => __( 'Excerpt Length', 'parlour-core' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 15,
				'condition' => [
					'show_excerpt'   => 'yes',
				],
			]
		);

		$this->add_control(
			'show_read_more',
			[
				'label'     => __( 'Read More', 'parlour-core' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => __( 'yes', 'parlour-core' ),
				'label_off' => __( 'no', 'parlour-core' ),
				'default'   => 'yes',
			]
		);		
		
		$this->end_controls_section();

		$this->start_controls_section(
			'section_content_read_more',
			[
				'label' => __( 'Read More', 'parlour-core' ),
				'condition' => [
					'show_read_more' => 'yes',
				],
			]
		);

		$this->add_control(
			'read_more_text',
			[
				'label' => __( 'Read More Text', 'parlour-core' ),
				'type' => Controls_Manager::TEXT,
				'default' => __( 'Read More', 'parlour-core' ),
				'placeholder' => __( 'Read More', 'parlour-core' ),
			]
		);

		$this->add_control(
			'icon',
			[
				'label' => __( 'Icon', 'parlour-core' ),
				'type' => Controls_Manager::ICON,
				'label_block' => true,
				'default' => '',
			]
		);

		$this->add_control(
			'icon_align',
			[
				'label' => __( 'Icon Position', 'parlour-core' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'right',
				'options' => [
					'left' => __( 'Before', 'parlour-core' ),
					'right' => __( 'After', 'parlour-core' ),
				],
				'condition' => [
					'icon!' => '',
				],
			]
		);

		$this->add_control(
			'icon_indent',
			[
				'label' => __( 'Icon Spacing', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 8,
				],
				'range' => [
					'px' => [
						'max' => 50,
					],
				],
				'condition' => [
					'icon!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more-icon-right' => 'margin-left: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more-icon-left' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_left_part',
			[
				'label' => __( 'Left Part', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'left_part_date_heading',
			[
				'label' => __( 'Date', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'left_part_date_color',
			[
				'label' => __( 'Date Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .left-part .bdt-post-block-modern-meta span' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'left_part_date_typography',
				'label'    => __( 'Typography', 'parlour-core' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-post-block-modern .left-part .bdt-post-block-modern-meta span',
			]
		);

		$this->add_control(
			'left_part_category_heading',
			[
				'label' => __( 'Category', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'left_part_category_color',
			[
				'label' => __( 'Category Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .left-part .bdt-post-block-modern-meta a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'left_part_category_bg_color',
			[
				'label' => __( 'Category Background Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .left-part .bdt-post-block-modern-meta a' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'left_part_category_typography',
				'label'    => __( 'Typography', 'parlour-core' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-post-block-modern .left-part .bdt-post-block-modern-meta a',
			]
		);

		$this->add_control(
			'left_part_title_category',
			[
				'label' => __( 'Title', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'left_part_title_color',
			[
				'label' => __( 'Title Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .left-part .bdt-post-block-modern-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'left_part_title_typography',
				'label'    => __( 'Typography', 'parlour-core' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-post-block-modern .left-part .bdt-post-block-modern-title',
			]
		);

		$this->add_control(
			'left_part_excerpt_category',
			[
				'label' => __( 'Excerpt', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'left_part_excerpt_color',
			[
				'label' => __( 'Excerpt Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .left-part .bdt-post-block-modern-excerpt' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'left_part_excerpt_typography',
				'label'    => __( 'Typography', 'parlour-core' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-post-block-modern .left-part .bdt-post-block-modern-excerpt',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_design_right_part',
			[
				'label' => __( 'Right Part', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'right_part_date_heading',
			[
				'label' => __( 'Date', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'right_part_date_color',
			[
				'label' => __( 'Date Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .right-part .bdt-post-block-modern-meta span' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'right_part_date_typography',
				'label'    => __( 'Typography', 'parlour-core' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-post-block-modern .right-part .bdt-post-block-modern-meta span',
			]
		);

		$this->add_control(
			'right_part_category_heading',
			[
				'label' => __( 'Category', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'right_part_category_color',
			[
				'label' => __( 'Category Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .right-part .bdt-post-block-modern-meta a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'right_part_category_bg_color',
			[
				'label' => __( 'Category Background Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .right-part .bdt-post-block-modern-meta a' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'category_border_radius',
			[
				'label' => __( 'Category Border Radius', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .right-part .bdt-post-block-modern-meta a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'right_part_category_typography',
				'label'    => __( 'Typography', 'parlour-core' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-post-block-modern .right-part .bdt-post-block-modern-meta a',
			]
		);

		$this->add_control(
			'right_part_title_category',
			[
				'label' => __( 'Title', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'right_part_title_color',
			[
				'label' => __( 'Title Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .right-part .bdt-post-block-modern-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'right_part_title_typography',
				'label'    => __( 'Typography', 'parlour-core' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-post-block-modern .right-part .bdt-post-block-modern-title',
			]
		);

		$this->add_control(
			'right_part_excerpt_category',
			[
				'label' => __( 'Excerpt', 'parlour-core' ),
				'type' => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'right_part_excerpt_color',
			[
				'label' => __( 'Excerpt Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .right-part .bdt-post-block-modern-excerpt' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'right_part_excerpt_typography',
				'label'    => __( 'Typography', 'parlour-core' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-post-block-modern .right-part .bdt-post-block-modern-excerpt',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_read_more',
			[
				'label' => __( 'Read More', 'parlour-core' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'show_read_more' => 'yes',
				],
			]
		);

		$this->start_controls_tabs( 'tabs_read_more_style' );

		$this->start_controls_tab(
			'tab_read_more_normal',
			[
				'label' => __( 'Normal', 'parlour-core' ),
			]
		);

		$this->add_control(
			'read_more_color',
			[
				'label'     => __( 'Text Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more' => 'color: {{VALUE}} !important;',
				],
			]
		);

		$this->add_control(
			'read_more_background_color',
			[
				'label'     => __( 'Background Color', 'parlour-core' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'read_more_border',
				'label' => __( 'Border', 'parlour-core' ),
				'placeholder' => '1px',
				'default' => '1px',
				'selector' => '{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more',
				'separator' => 'before',
			]
		);

		$this->add_control(
			'read_more_border_radius',
			[
				'label' => __( 'Border Radius', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_read_more_hover',
			[
				'label' => __( 'Hover', 'parlour-core' ),
			]
		);

		$this->add_control(
			'read_more_hover_color',
			[
				'label' => __( 'Text Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more:hover' => 'color: {{VALUE}} !important;',
				],
			]
		);

		$this->add_control(
			'read_more_hover_background_color',
			[
				'label' => __( 'Background Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'read_more_hover_border_color',
			[
				'label' => __( 'Border Color', 'parlour-core' ),
				'type' => Controls_Manager::COLOR,
				'condition' => [
					'border_border!' => '',
				],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'read_more_hover_animation',
			[
				'label' => __( 'Animation', 'parlour-core' ),
				'type' => Controls_Manager::HOVER_ANIMATION,
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'read_more_box_shadow',
				'selector' => '{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more',
			]
		);

		$this->add_control(
			'read_more_padding',
			[
				'label' => __( 'Padding', 'parlour-core' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'read_more_typography',
				'label'    => __( 'Typography', 'parlour-core' ),
				'scheme'   => Scheme_Typography::TYPOGRAPHY_4,
				'selector' => '{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-read-more',
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_additional_options',
			[
				'label' => __( 'Additional Options', 'parlour-core' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'space_between',
			[
				'label' => __( 'Space Between', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'separator' => 'after',
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-meta' => 'margin-bottom: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-title' => 'margin-bottom: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-excerpt' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'item_spacing',
			[
				'label' => __( 'Item Spacing', 'parlour-core' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'separator' => 'after',
				'selectors' => [
					'{{WRAPPER}} .bdt-post-block-modern .bdt-post-block-modern-desc' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_section();
	}

	public function filter_excerpt_length() {
		return $this->get_settings( 'excerpt_length' );
	}

	public function filter_excerpt_more( $more ) {
		return '';
	}

	public function render() {
		$settings = $this->get_settings();
		
		global $post;
		$id      = uniqid('bdtpbm_');
		$classes = ['bdt-post-block-modern', 'uk-grid', 'uk-grid-match'];

		$animation = ($settings['read_more_hover_animation']) ? ' elementor-animation-'.$settings['read_more_hover_animation'] : '';

		$args = array(
			'post_type'      => $settings['post_type'],
			'posts_per_page' => $settings['posts_limit'],
			'orderby'        => $settings['orderby'],
			'order'          => $settings['order'],
			'post_status'    => 'publish'
		);



		if ( 'by_name' === $settings['source'] ) :
			if ($settings['post_type'] === 'post') :
					$categories = $settings['post_categories'];
					$taxonomy = 'category';
			elseif ($settings['post_type'] === 'services') :
					$categories = $settings['service_categories'];
					$taxonomy = 'service-categories';
			endif;

			$args['tax_query'][] = array(
				'taxonomy' => $taxonomy,
				'field'    => 'slug',
				'terms'    => $categories,
			);
		endif;

		$wp_query = new \WP_Query($args);

		if( $wp_query->have_posts() ) :

			add_filter( 'excerpt_more', [ $this, 'filter_excerpt_more' ], 20 );
			add_filter( 'excerpt_length', [ $this, 'filter_excerpt_length' ], 20 );

		?> 

			<div id="<?php echo $id; ?>" class="<?php echo bdt_acssc($classes); ?>">

				<?php $count = 0;
			
				while ( $wp_query->have_posts() ) : $wp_query->the_post();

					$count++;

			  		$post_thumbnail= wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );

				  	if( $count == 1) : ?>

				  		<div class="uk-width-3-5@m">
				  			<div class="bdt-post-block-modern-item left-part uk-position-relative" style="background-image: url(<?php echo esc_url($post_thumbnail[0]); ?>)">
						  		
						  		<div class="bdt-post-block-modern-desc uk-position-bottom-center uk-position-z-index uk-width-2-3@m ">

					            	<?php if ('yes' == $settings['show_meta']) : ?>

										<?php $meta_list = '<li><span>'.esc_attr(get_the_date('d F Y')).'</span></li><li>'.get_the_category_list(', ').'</li>'; ?>

										<ul class="bdt-post-block-modern-meta uk-subnav uk-flex-center"><?php echo $meta_list; ?></ul>

									<?php endif ?>

									<?php if ('yes' == $settings['title']) : ?>
										<h4 class="bdt-post-block-modern-title"><a href="<?php echo esc_url(get_permalink()); ?>" class="uk-link-reset" title="<?php echo esc_attr(get_the_title()); ?>"><?php echo esc_html(get_the_title()) ; ?></a></h4>
									<?php endif ?>

									<?php if ('yes' == $settings['show_excerpt']) : ?>
										<div class="bdt-post-block-modern-excerpt"><?php echo wp_kses_post(the_excerpt()); ?></div>
									<?php endif ?>

						  		</div>
						  		<div class="uk-position-cover uk-overlay-gradient"></div>

							</div>
						</div>

				  		<div class="uk-width-2-5@m">
					<?php else : ?>
			  			<div class="bdt-post-block-modern-item right-part">
					  		<div class="bdt-post-block-modern-desc uk-<?php echo $settings['color']; ?>">

								<?php if ('yes' == $settings['show_meta']) : ?>
									<?php $meta_list = '<li><span>'.esc_attr(get_the_date('d F Y')).'</span></li><li>'.get_the_category_list(', ').'</li>'; ?>
									
									<ul class="bdt-post-block-modern-meta uk-subnav"><?php echo $meta_list; ?></ul>
								<?php endif ?>

								<?php if ('yes' == $settings['title']) : ?>
									<h4 class="bdt-post-block-modern-title"><a href="<?php echo esc_url(get_permalink()); ?>" class="uk-link-reset" title="<?php echo esc_attr(get_the_title()); ?>"><?php echo esc_html(get_the_title()) ; ?></a></h4>
								<?php endif ?>

								<?php if ('yes' == $settings['show_excerpt']) : ?>
									<div class="bdt-post-block-modern-excerpt"><?php echo wp_kses_post(the_excerpt()); ?></div>
								<?php endif ?>

								<?php if ('yes' == $settings['show_read_more']) : ?>
									<a href="<?php echo esc_url(get_permalink()); ?>" class="bdt-post-block-modern-read-more uk-link-reset<?php echo $animation; ?>"><?php echo esc_html($settings['read_more_text']); ?>
										
										<?php if ($settings['icon']) : ?>
											<span class="bdt-post-block-modern-read-more-icon-<?php echo $settings['icon_align']; ?>">
												<i class="<?php echo $settings['icon']; ?>"></i>
											</span>
										<?php endif; ?>

									</a>
								<?php endif ?>

					  		</div>

						</div>
					<?php endif; ?>
			  
				<?php endwhile; ?>

				</div>
		
			</div>
		
		 	<?php 
				remove_filter( 'excerpt_length', [ $this, 'filter_excerpt_length' ], 20 );
				remove_filter( 'excerpt_more', [ $this, 'filter_excerpt_more' ], 20 );

				wp_reset_postdata(); 
			?>

 		<?php endif;
	}
}
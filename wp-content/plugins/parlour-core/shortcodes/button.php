<?php
// BdThemes Note Shortcode

if (!function_exists('bdthemes_button')) {
    function bdthemes_button($atts = null, $content = null) {
        extract(shortcode_atts(array(
            'text'          => 'BUTTON',
            'url'           => '#',
            'target'        => 'self',
            'icon'          => '',
            'style'         => 'default',
            'size'          => 'default',
            'radius'        => 'no',
            'disabled'      => 'no',
            
            'class'         => ''
        ), $atts));

        

        if ($size == 'full_width') {
            $size = 'uk-width-1-1';
        } else {
            $size     = ($size =='default') ? '' : 'uk-button-'.$size ;
        }

        $classes   = ['uk-button', 'uk-button-'.$style, $size, $class];
        $classes[] = ($radius == 'yes') ? 'uk-border-rounded' : '';

        if ($icon !== '') {
            $icon = '<span class="uk-margin-small-left" uk-icon="icon: '.$icon.'"></span>';
        }
        
        if ($disabled == 'yes') {
            $output[] = '<button class="'.bdt_acssc($classes).'" disabled>'.$text.$icon.'</button>';
        } else {
            $output[] = '<a href="' . $url . '" class="'.bdt_acssc($classes).'" target="_' . $target . '">'.$text.$icon.'</a>';
        }


        return implode("", $output);
    }
    // end of Accordion shortcode

    add_shortcode('bdt_button', 'bdthemes_button');
}
